﻿using Common;
using Spring.Data.Common;
using Spring.Data.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DAO
{
    /// <summary>
    /// 通用数据库访问类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="PK"></typeparam>
    public class BaseDao : AdoDaoSupport, IBaseDao
    {

        /// <summary>
        /// 构建 SqlCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlCommand</returns>
        private  SqlCommand BuildQueryCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in parameters)
            {
                if (parameter != null)
                {
                    // 检查未分配值的输出参数,将其分配以DBNull.Value.
                    if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                        (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    command.Parameters.Add(parameter);
                }
            }

            return command;
        }

        /// <summary>
        /// 创建 SqlCommand 对象实例(用来返回一个整数值)	
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlCommand 对象实例</returns>
        private SqlCommand BuildIntCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.Parameters.Add(new SqlParameter("ReturnValue",
                SqlDbType.Int, 4, ParameterDirection.ReturnValue,
                false, 0, 0, string.Empty, DataRowVersion.Default, null));
            return command;
        }
          

        public string RunProceOnlyPara(string storedProcName, IDataParameter[] parameters, string ReturnPara)
        {
            using (SqlConnection connection = new SqlConnection(AdoTemplate.DbProvider.ConnectionString))
            {
                connection.Open();
                SqlCommand command = BuildIntCommand(connection, storedProcName, parameters);
                command.ExecuteNonQuery();
                return command.Parameters["@" + ReturnPara].Value.ToString();

            }
        }


        public object ExecuteScalar(string sql)
        {
            object o = AdoTemplate.ExecuteScalar(CommandType.Text, sql);
            if (o == System.DBNull.Value) return null;
            return o;
        }

        public object SaveByIdentity(DtoData dto)
        {
            object o = null;
            //获取插入SQL
            string sql = ReflectionUtil.GetInsertSql(dto);
            //获取自增ID
            sql += ";select @@IDENTITY;";
            if (sql == null)
            {
                throw new ValidateException("保存出错！请联系管理员");
            }
            else
            {
                //获取参数
                IDbParameters insertParams = CreateDbParameters();
                insertParams = ReflectionUtil.GetParamLsit(insertParams, dto);
                o = AdoTemplate.ExecuteScalar(CommandType.Text, sql, insertParams);
            }
            return o;
        }

        public int Save(DtoData dto)
        {
            int c = 0;
            //获取插入SQL
            string sql = ReflectionUtil.GetInsertSql(dto);
            if (sql == null)
            {
                throw new ValidateException("保存出错！请联系管理员");
            }
            else
            {
                //获取参数
                IDbParameters insertParams = CreateDbParameters();
                insertParams = ReflectionUtil.GetParamLsit(insertParams, dto);
                c = AdoTemplate.ExecuteNonQuery(CommandType.Text, sql, insertParams);
            }
            return c;
        }

        public int Update(DtoData dto)
        {
            int c = 0;
            //获取更新SQL
            string sql = null;
            try
            {
                sql = ReflectionUtil.GetUpdateSql(dto);
            }
            catch (ValidateException ex)
            {
                throw new ValidateException(ex.Message);
            }

            if (sql == null)
            {
                throw new ValidateException("更新出错！请联系管理员");
            }
            else
            {
                //获取参数
                IDbParameters updateParams = CreateDbParameters();
                updateParams = ReflectionUtil.GetUpdateParamLsit(updateParams, dto);
                c = AdoTemplate.ExecuteNonQuery(CommandType.Text, sql, updateParams);
            }

            return c;
        }


        public int ExecuteBySql(string sql, List<DbParameterItem> param)
        {
            IDbParameters updateParams = CreateDbParameters();
            if (param != null && param.Count > 0)
            {
                foreach (DbParameterItem dp in param)
                {
                    updateParams.AddWithValue(dp.Name, dp.Value);
                }
            }
            int c = AdoTemplate.ExecuteNonQuery(CommandType.Text, sql, updateParams);
            return c;
        }

        public int ExecuteBySql(string sql)
        {
            int c = AdoTemplate.ExecuteNonQuery(CommandType.Text, sql);
            return c;
        }

        public int Delte(string table, object id)
        {
            if (id == null)
            {
                throw new ValidateException("删除失败，ID为空");
            }
            else if (table == null)
            {
                throw new ValidateException("删除失败，删除对象为空");
            }
            string sql = "delete from " + table + " where id=@id";
            IDbParameters updateParams = CreateDbParameters();
            updateParams.AddWithValue("id", id);
            int c = AdoTemplate.ExecuteNonQuery(CommandType.Text, sql, updateParams);
            return c;
        }

        public int GetCount(string sql, List<DbParameterItem> param, bool isJointSql)
        {
            //isJointSql为true时 根据param列表拼接条件SQL
            IDbParameters Params = CreateDbParameters();
            if (isJointSql)
            {
                sql = AddContion(sql, param, Params);
            }
            else
            {
                ParametersAddValue(param, Params);
            }

            object o = AdoTemplate.ExecuteScalar(CommandType.Text, sql, Params);

            return o == null ? 0 : Convert.ToInt32(o);
        }

        public int GetCount(string sql)
        {

            object o = AdoTemplate.ExecuteScalar(CommandType.Text, sql);
            return o == null ? 0 : Convert.ToInt32(o);
        }


        public DataRow GetOne(string sql, List<DbParameterItem> param, bool isJointSql)
        {
            //isJointSql为true时 根据param列表拼接条件SQL
            IDbParameters Params = CreateDbParameters();
            if (isJointSql)
            {
                sql = AddContion(sql, param, Params);
            }
            else
            {
                ParametersAddValue(param, Params);
            }

            DataTable dt = AdoTemplate.DataTableCreateWithParams(CommandType.Text, sql, Params);
            if (dt != null & dt.Rows.Count > 0)
            {
                return dt.Rows[0];
            }
            return null;
        }

        public DataRow GetOne(string sql)
        {

            DataTable dt = AdoTemplate.DataTableCreate(CommandType.Text, sql);
            if (dt != null & dt.Rows.Count > 0)
            {
                return dt.Rows[0];
            }
            return null;
        }

        public DataTable GetList(string sql, List<DbParameterItem> param, bool isJointSql)
        {
            //isJointSql为true时 根据param列表拼接条件SQL
            IDbParameters Params = CreateDbParameters();
            if (isJointSql)
            {
                sql = AddContion(sql, param, Params);
            }
            else
            {
                ParametersAddValue(param, Params);
            }

            DataTable dt = AdoTemplate.DataTableCreateWithParams(CommandType.Text, sql, Params);
            return dt;
        }

        public DataTable GetList(string sql)
        {
            DataTable dt = AdoTemplate.DataTableCreate(CommandType.Text, sql);
            return dt;
        }

        public int UpdateBatchByDataSet(DataSet dataSet, string tableName, string updateSql, List<DbParameterItem> param)
        {
            IDbParameters updateParams = CreateDbParameters();
            foreach (DbParameterItem di in param)
            {
                updateParams.Add(di.Name, di.dtype,0,di.Name);
            }
            return AdoTemplate.DataSetUpdate(dataSet, tableName,
                                        CommandType.Text, null, null,
                                        CommandType.Text, updateSql, updateParams,
                                        CommandType.Text, null, null);
        }

        public int InsertBatchByDataSet(DataSet dataSet, string tableName, string insertSql, List<DbParameterItem> param)
        {
            IDbParameters insertParams = CreateDbParameters();
            foreach (DbParameterItem di in param)
            {
                insertParams.Add(di.Name, di.dtype, 0, di.Name);
            }
            return AdoTemplate.DataSetUpdate(dataSet, tableName,
                                        CommandType.Text, insertSql, insertParams,
                                        CommandType.Text, null, null,
                                        CommandType.Text, null, null);
        }

        public DataTable GetPageListNotJoinSql(string sql, List<DbParameterItem> param)
        {
            IDbParameters Params = CreateDbParameters();
            ParametersAddValue(param, Params);
            sql = "select * from (" + sql + ") t where rownumber>=@strnum  and rownumber<=@endnum";

            DataTable dt = AdoTemplate.DataTableCreateWithParams(CommandType.Text, sql, Params);
            return dt == null ? new DataTable() : dt;
        }

        public DataTable GetPageList(string sql, List<DbParameterItem> param)
        {
            IDbParameters Params = CreateDbParameters();
            sql = AddContion(sql, param, Params);
            sql = "select * from (" + sql + ") t where rownumber>=@strnum  and rownumber<=@endnum";

            DataTable dt = AdoTemplate.DataTableCreateWithParams(CommandType.Text, sql, Params);
            return dt==null?new DataTable():dt;
        }

        /// <summary>
        /// 给IDbParameters添加参数
        /// </summary>
        /// <param name="param"></param>
        /// <param name="Params"></param>
        private void ParametersAddValue(List<DbParameterItem> param, IDbParameters Params)
        {
            if (param != null && param.Count > 0)
            {
                foreach (DbParameterItem di in param)
                {
                   Params.AddWithValue(di.Name, di.Value);
                }
            }
        }

        /// <summary>
        /// 组装SQL，给SQL添加查询条件
        /// </summary>
        /// <param name="sql"> 原SQL</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        private string AddContion(string sql, List<DbParameterItem> param, IDbParameters Params)
        {
            if (param != null && param.Count > 0)
            {
                if (!sql.Contains("where"))
                {
                    sql += " where 1=1 ";
                }
                Dictionary<string, int> da = new Dictionary<string, int>();
                foreach (DbParameterItem di in param)
                {
                    if ("strnum".Equals(di.Name) || "endnum".Equals(di.Name))
                    {
                        Params.AddWithValue(di.Name, di.Value);
                        continue;
                    }
                    if (di.op.Equals(ConstUtil.STATIC_STR))
                    {
                        sql += di.Name;
                    }
                    else
                    {
                        string fieldParam = getFormatField(di.Name, da);
                        if (di.op.Equals(ConstUtil.LIKE))
                        {
                            sql += " and " + di.Name + " like '%'+ @" + fieldParam + " + '%'";
                        }
                        else if (di.op.Equals(ConstUtil.LIKE_ST))
                        {

                            sql += " and " + di.Name + " like '%'+ @" + fieldParam;
                        }
                        else if (di.op.Equals(ConstUtil.LIKE_ED))
                        {

                            sql += " and " + di.Name + " like  @" + fieldParam + " + '%'";
                        }
                        else if (di.op.Equals(ConstUtil.DATESRT_EQ_DAY))
                        {

                            sql += " and datediff(d," + di.Name + ",CONVERT(datetime,@" + fieldParam + ",120))=0 ";
                        }
                        else if (di.op.Equals(ConstUtil.DATESRT_EGT_DAY))
                        {

                            sql += " and datediff(d," + di.Name + ",CONVERT(datetime,@" + fieldParam + ",120))>=0 ";
                        }
                        else if (di.op.Equals(ConstUtil.DATESRT_LGT_DAY))
                        {

                            sql += " and datediff(d," + di.Name + ",CONVERT(datetime,@" + fieldParam + ",120))<=0 ";
                        }
                        else
                        {
                            sql += " and " + di.Name + di.op + "@" + fieldParam;
                        }
                        Params.AddWithValue(fieldParam, di.Value);
                    }
                }
            }
            return sql;
        }

        /// <summary>
        /// 生成参数化查询变量名
        /// </summary>
        /// <param name="field">字段名</param>
        /// <param name="da">出现次数</param>
        /// <returns></returns>
        private string getFormatField(string field,Dictionary<string, int> da)
        {
            string result = field;
            //如果含有.(一般情况下，联表查询会出现)，则截取掉.和之前的字符作为变量名
            if (field.Contains("."))
            {
                int idx = field.IndexOf(".");
                string pre = field.Substring(0, idx);
                result = pre + field.Substring(idx + 1);
            }
            //如有重复，则在变量名后面加数字
            if (da.ContainsKey(result))
            {
                int times = da[result];
                times += 1;
                result = result + times.ToString();
                da[result] = times;
            }
            else
            {
                da.Add(result, 1);
            }
            return result;
        }

        
    }
}
