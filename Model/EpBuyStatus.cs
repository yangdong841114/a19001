﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// EP购买状态记录实体
    /// </summary>
    [Serializable]
    public class EpBuyStatus : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //挂卖记录id
        public int? sid { get; set; }

        //挂卖单号
        public string snumber { get; set; }

        //购买记录ID
        public int? bid { get; set; }

        //购买记录单号
        public string number{get;set;}

        //写入时间
        public DateTime? addTime { get; set; }

        //状态ID（跟着EpSaleRecord表变动)
        public int? flag { get; set; }

        //状态说明
        public string opStatus { get; set; }

        //操作人ID
        public int? uid { get; set; }

        //操作人编码
        public string userId { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------   
    }
}
