﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// Info表实体
    /// </summary>
    [Serializable]
    public class Info : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        public virtual int? uid { get; set; }
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        //标题
        public virtual string title { get; set; }
        //内容
        public virtual string infocontent { get; set; }
        //文章类型
        public virtual string type { get; set; }
        public virtual string fltype { get; set; }
        //添加时间
        public virtual DateTime? addTime { get; set; }
        //是否置顶，1：否，2：是
        public virtual int? isTop { get; set; }
        
        //点击数
        public virtual int? llcount { get; set; }
        public virtual int? dzcount { get; set; }
        public virtual int? plcount { get; set; }
        public virtual int? dpcount { get; set; }
        public virtual string imgUrl { get; set; }
        public virtual string imgBigUrl { get; set; }
        public virtual string linkUrl { get; set; }
        public virtual string infoJj { get; set; }
       

        public virtual DateTime? startTime { get; set; }

        public virtual DateTime? endTime { get; set; }
       
    }
}
