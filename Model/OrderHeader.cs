﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 订单头实体
    /// </summary>
    [Serializable]
    public class OrderHeader : Page, DtoData
    {
        //订单号
        public virtual string id { get; set; }
        //订单时间
        public virtual DateTime? orderDate { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        public virtual string userName { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //省
        public virtual string provinceName { get; set; }
        //市
        public virtual string cityName { get; set; }
        //区
        public virtual string areaName { get; set; }
        //详细地址
        public virtual string address { get; set; }
        public virtual string contry { get; set; } 
        //手机号
        public virtual string phone { get; set; }
        //收货人
        public virtual string receiptName { get; set; }
        //商品总数
        public virtual int? productNum { get; set; }
        //商品金额
        public virtual double? productMoney { get; set; }
        //下单时AIC挂牌价
        public virtual double? aicPrice { get; set; }
        //需支付AIC金额
        public virtual double? aicMoney { get; set; }
        public virtual double? sxf { get; set; }
        //订单状态
        public virtual int? status { get; set; }
        //订单类型：1：注册订单，2：复消订单
        public virtual int? typeId { get; set; }
        //支付方式
        public virtual string payType { get; set; }
        //物流公司编号
        public virtual string wlNo { get; set; }
        //物流公司名称
        public virtual string logName { get; set; }
        //物流单号
        public virtual string logNo { get; set; }

        public virtual int? saleUid { get; set; }
        public virtual string salephone { get; set; }

        //商家名称
        public virtual string mname { get; set; }

        public List<OrderItem> items { get; set; }

        public List<OrderStatus> osList { get; set; }



        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

    }
}
