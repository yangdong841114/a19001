﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 提现记录表实体
    /// </summary>
    [Serializable]
    public class TakeCash : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //提现金额
        public virtual double? epoints { get; set; }
        //手续费
        public virtual double? fee { get; set; }
        //提现时间
        public virtual DateTime? addtime { get; set; }
        //是否审核，1：未审核，2：已审核
        public virtual int? isPay { get; set; }
        //开户行
        public virtual string bankName { get; set; }
        //开户名
        public virtual string bankUser { get; set; }
        //银行卡号
        public virtual string bankCard { get; set; }
        //银行地址
        public virtual string bankAddress { get; set; }
        //汇率
        public virtual double? hv { get; set; }
        //审核人ID
        public virtual int? auditUid { get; set; }
        //审核人编码
        public virtual string auditUser { get; set; }
        //审核时间
        public virtual DateTime? auditTime { get; set; }
        //账户类型
        public virtual int? accountId { get; set; }

        //用户名
        public virtual string userName { get; set; }
        public virtual string bpxID { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
