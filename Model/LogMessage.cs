﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 程序异常日志表
    /// </summary>
    [Serializable]
    public class LogMessage : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //写入时间
        public virtual DateTime? addTime { get; set; }
        //类路路径
        public virtual string classUrl { get; set; }
        //方法名称
        public virtual string method { get; set; }
        //是否后台=> 0：前台，1：后台
        public virtual int? isBackGround { get; set; }
        //异常信息
        public virtual string message { get; set; }

        //异常栈信息
        public virtual string stackMsg { get; set; }


        public DateTime? startTime { get; set; }
        public DateTime? endTime { get; set; }
       
    }
}
