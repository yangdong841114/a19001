﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 下载专区实体
    /// </summary>
    [Serializable]
    public class DownLoadFile :Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //标题
        public virtual string title { get; set; }
        //上传时间
        public virtual DateTime? addTime { get; set; }
        //文件路径
        public virtual string fileUrl { get; set; }
        //上传用户ID
        public virtual int? addUid { get; set; }
        //上传用户编号
        public virtual string addUser { get; set; }

        public DateTime? startTime { get; set; }

        public DateTime? endTime { get; set; }
        
    }
}
