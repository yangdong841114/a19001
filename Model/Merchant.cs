﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 加盟商家实体
    /// </summary>
    [Serializable]
    public class Merchant : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //会员id
        public virtual int? uid { get; set; }
        //粉丝编号
        public virtual string userId { get; set; }
        //昵称
        public virtual string userName { get; set; }
        //商家名称
        public virtual string name { get; set; }
        //联系人
        public virtual string linkMan { get; set; }
        //联系电话
        public virtual string linkPhone { get; set; }
        //国家
        public virtual string contry { get; set; }
        //省
        public virtual string province { get; set; }
        //市
        public virtual string city { get; set; }
        //区
        public virtual string area { get; set; }
        //详细地址
        public virtual string address { get; set; }
        //经营范围
        public virtual string businessScope { get; set; }

        public virtual string refuseReason { get; set; } 
        //支付金额
        public virtual double? payAmount { get; set; }
        public virtual string imgUrl { get; set; }
        public virtual string imgYyzzUrl { get; set; }
        public virtual string imgSfzzmUrl { get; set; }
        public virtual string imgSfzfmUrl { get; set; }
        public virtual string imgAdurl1 { get; set; }
        public virtual string imgAdurl2 { get; set; }
        public virtual string imgAdurl3 { get; set; }
        public virtual string imgAdurl4 { get; set; }

        //状态,1：未审，2：已审
        public virtual int? flag { get; set; }
        //操作人ID
        public virtual int? opUid { get; set; }
        //操作人编号
        public virtual string opUserId { get; set; }
        //申请时间
        public virtual DateTime? addTime { get; set; }

        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
       
    }
}
