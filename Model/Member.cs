﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 用户表实体
    /// </summary>
    [Serializable]
    public class Member : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }
        //一级密码
        public virtual string password { get; set; }
        //二级密码
        public virtual string passOpen { get; set; }
        //三级密码
        public virtual string threepass { get; set; }
        //昵称
        public virtual string nickName { get; set; }
        //手机号码
        public virtual string phone { get; set; }
        //详细地址
        public virtual string address { get; set; }
        //电子邮箱
        public virtual string email { get; set; }
        //身份证号码
        public virtual string code { get; set; }
        //QQ
        public virtual string qq { get; set; }
        //省
        public virtual string province { get; set; }
        //市
        public virtual string city { get; set; }
        //区
        public virtual string area { get; set; }
        public virtual string zipCode { get; set; }
        //是否锁定=> 0：否，1：是
        public virtual int? isLock { get; set; }
        //是否报单中心=> 0：否，1：审核中，2：是
        public virtual int? isAgent { get; set; }
        //报单中心是否锁定=> 0：否，1：是
        public virtual int? agentIslock { get; set; }
        //报单中心名称
        public virtual string agentName { get; set; }
        //报单中心申请时间
        public virtual DateTime? applyAgentTime { get; set; }
        //报单中心开通时间
        public virtual DateTime? openAgentTime { get; set; }
        //报单中心注册金额
        public virtual double? regAgentmoney { get; set; }
        //开通报单中心操作人
        public virtual string agentOpUser { get; set; }
        //所属报单中心ID
        public virtual int? shopid { get; set; }
        //所属报单中心名称
        public virtual string shopName { get; set; }
        //会员级别
        public virtual int? uLevel { get; set; }
        //荣誉级别
        public virtual int? rLevel { get; set; }
        //系谱图区位=> 0：左区，1：右区
        public virtual int? treePlace { get; set; }
        //系谱图父节点ID
        public virtual int? fatherID { get; set; }
        //系谱图父节点名称
        public virtual string fatherName { get; set; }
        //系谱图中当前会员所在深度
        public virtual int? pLevel { get; set; }
        //系谱图中当前会员到根的路径
        public virtual string pPath { get; set; }
        //左区剩余业绩
        public virtual double? spare0 { get; set; }
        //右区剩余业绩
        public virtual double? spare1 { get; set; }
        public virtual double? spare2 { get; set; }
        //左区总业绩
        public virtual double? encash0 { get; set; }
        //右区总业绩
        public virtual double? encash1 { get; set; }
        public virtual double? encash2 { get; set; }
        //左区新业绩
        public virtual double? new0 { get; set; }
        //右区新业绩
        public virtual double? new1 { get; set; }
        public virtual double? new2 { get; set; }
        //PV
        public virtual double? pv { get; set; }
        //下线PV
        public virtual double? xxpv { get; set; }
        //推荐人ID
        public virtual int? reId { get; set; }
        //推荐人名称
        public virtual string reName { get; set; }
        //推荐图中当前会员到根的路径
        public virtual string rePath { get; set; }
        //推荐图中当前会员的深度
        public virtual int? reLevel { get; set; }
        //直推人数
        public virtual int? reCount { get; set; }
        //左半区直推人数
        public virtual int? reLCount { get; set; }
        //右半区直推人数
        public virtual int? reRCount { get; set; }
        //直推图中当前会员下线人数
        public virtual int? reTreeCount { get; set; }
        //注册金额
        public virtual double? regMoney { get; set; }
        //注册单数
        public virtual int? regNum { get; set; }
        //是否开通=> 0：否，1：是
        public virtual int? isPay { get; set; }
        //注册时间
        public virtual DateTime? addTime { get; set; }
        //开通时间
        public virtual DateTime? passTime { get; set; }
        //开通人ID
        public virtual int? byopenId { get; set; }
        //开通人编号
        public virtual string byopen { get; set; }
        //安全问题
        public virtual string answer { get; set; }
        //问题答案
        public virtual string question { get; set; }
        //是否空单=> 0：否，1：是
        public virtual int? empty { get; set; }

        //开户行
        public string bankName { get; set; }
        //银行卡号
        public string bankCard { get; set; }
        //开户支行（地址） 
        public string bankAddress { get; set; }
        public string todAddress { get; set; }
        public string toccAddress { get; set; }
        public string bikicoin { get; set; }
        public string bikiID { get; set; }
        public string bpxID { get; set; }
        //开户名
        public string bankUser { get; set; }
        //登录失败次数（登录成功后会置0）
        public virtual int? loginFailTimes { get; set; }
        //登录锁定到指定的时间
        public virtual DateTime? loginLockTime { get; set; }

        //是否管理员:0否，1是
        public virtual int? isAdmin { get; set; }
        //是否复投点位：0否，1是
        public virtual int? isFt { get; set; }
        //复投点位所属主帐号ID
        public virtual int? mainAccount { get; set; }
        public string dpj;

        //冗余字段------------------------------------------------------

        public string out_trade_no;

        //权限键值对
        public Dictionary<string, string> permission;
        public MemberAccount account { get; set; }
        //IP地址
        public string ipAddress { get; set; }
        //是否登录锁定
        public int? isLoginLock { get; set; }
        //登录锁定剩余分钟
        public int? remainingMinutes { get; set; }

        //直推图显示名称
        public string dtreeName { get; set; }

        //系谱图注册链接
        public string registerLink { get; set; }

        //会员详细地址
        public string infoLink { get; set; }

        public List<Member> reList { get; set; }

        public List<OrderItem> orderItems { get; set; }

        //原密保答案
        public string oldAnswer { get; set; }

        //是否第一次设置密保 Y:是 N:否
        public string fristAnswer { get; set; }

        //注册时来源 app:手机，
        public string sourceMachine { get; set; }

        //冗余字段------------------------------------------------------

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        public virtual DateTime? passStartTime { get; set; }

        public virtual DateTime? passEndTime { get; set; }
        //算力超时月数
        public virtual int? SlcsMonth { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
