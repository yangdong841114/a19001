﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 奖金汇总表实体
    /// </summary>
    [Serializable]
    public class CurencySummary : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //结算金额
        public virtual double? jsMoney { get; set; }
        //发放金额
        public virtual double? ffMoney { get; set; }
        //结算日期
        public virtual DateTime? dateTime { get; set; }
        //奖金类型
        public virtual int? cat { get; set; }   
       
    }
}
