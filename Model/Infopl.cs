﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// Info表实体
    /// </summary>
    [Serializable]
    public class Infopl : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        public virtual int? uid { get; set; }
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        //内容
        public virtual string infoplcontent { get; set; }
        //文章类型
        public virtual string type { get; set; }
     
        //添加时间
        public virtual DateTime? addTime { get; set; }

        public virtual int? infoId { get; set; }
        
        
        public virtual DateTime? startTime { get; set; }

        public virtual DateTime? endTime { get; set; }
       
    }
}
