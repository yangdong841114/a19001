﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 用户账户表实体
    /// </summary>
    [Serializable]
    public class MemberAccount : Page,DtoData
    {
        //用户ID
        public virtual int? id { get; set; }
        //点值
        public virtual double? agentDz { get; set; }
        //奖金币
        public virtual double? agentJj { get; set; }
        //购物币
        public virtual double? agentGw { get; set; }
        //复投币
        public virtual double? agentFt { get; set; }
        //累计奖金
        public virtual double? agentTotal { get; set; }
        //兑换码
        public virtual double? agentDhm { get; set; }
        //TOD币
        public virtual double? agentTod { get; set; }
        //分红值
        public virtual double? agentFhz { get; set; }
        //TOCC币
        public virtual double? agentTocc { get; set; }
        //算力
        public virtual double? agentSl { get; set; }
        //BPC
        public virtual double? agentBpc { get; set; }

        public virtual double? agentMoby { get; set; }

        //冗余
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        public virtual string gtaccountId { get; set; }
        public virtual string gtValue { get; set; }
        
       
    }
}
