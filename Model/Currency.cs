﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 奖金明细表实体
    /// </summary>
    [Serializable]
    public class Currency : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //奖金类型
        public virtual int? cat { get; set; }
        //描述
        public virtual string mulx { get; set; }
        //奖金金额
        public virtual double? epoints { get; set; }
        //费用1
        public virtual double? fee1 { get; set; }
        //费用2
        public virtual double? fee2 { get; set; }
        //费用3
        public virtual double? fee3 { get; set; }
        //费用4
        public virtual double? fee4 { get; set; }
        //费用5
        public virtual double? fee5 { get; set; }
        //费用6
        public virtual double? fee6 { get; set; }
        //费用7
        public virtual double? fee7 { get; set; }
        //结算时间
        public virtual DateTime? jstime { get; set; }
        //发放状态=> 0：未发，1：已发
        public virtual int? ff { get; set; }
        //发放时间
        public virtual DateTime? fftime { get; set; }
        //来源用户ID
        public virtual int? fuid { get; set; }
        //来源记录ID
        public virtual int? infoId { get; set; }
        //来源表
        public virtual string tableName { get; set; }
        //层数
        public virtual int? ceng { get; set; }

        public virtual double? yf { get; set; }
        public virtual double? sf { get; set; }
        public virtual string catName { get; set; }

        public virtual DateTime addDate { get; set; }
       
    }
}
