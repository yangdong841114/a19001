﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 充值记录表实体
    /// </summary>
    [Serializable]
    public class Charge :Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //会员名称
        public virtual string userName { get; set; }
        //金额
        public virtual double? epoints { get; set; }
        //提交时间
        public virtual DateTime? addTime { get; set; }
        //是否支付=> 1：否，2：是
        public virtual int? ispay { get; set; }
        //审核通过时间
        public virtual DateTime? passTime { get; set; }
        //类型
        public virtual int? typeId { get; set; }
        //转入会员帐户类型
        public virtual int? accounttypeId { get; set; }  
        //汇出银行
        public virtual string fromBank { get; set; }
        //汇入银行
        public virtual string toBank { get; set; }
        //银行卡号
        public virtual string bankCard { get; set; }
        //开户人
        public virtual string bankUser { get; set; }
        //汇款凭证
        public virtual string imgUrl { get; set; }
        //汇款时间
        public virtual string bankTime { get; set; }
        //审核人ID
        public virtual int? auditUid { get; set; }
        //审核人编码
        public virtual string auditUser { get; set; }
        //汇款备注
        public virtual string remark { get; set; }

        public virtual int? sysBankId { get; set; }

        //认购月数
        public virtual int? rgys { get; set; }
        //连续月数
        public virtual int? lxmonth { get; set; }
        //认购开始时间
        public virtual DateTime? ksTime { get; set; }
        //认购结束时间
        public virtual DateTime? jsTime { get; set; }
        //消耗兑换码
        public virtual double? xhdhm { get; set; }
        //返PV
        public virtual double? fpv { get; set; }
        //充值类型
        public virtual string czType { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        public virtual DateTime? startjsTime { get; set; }
        public virtual DateTime? endjsTime { get; set; }

        public virtual double? epointsPay { get; set; }
        public virtual double? epointsNotpay { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
    }
}
