﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
     [Serializable]
    public class ProductType : Page, DtoData
    {
        public virtual int? id { get; set; }
        public virtual int? grade { get; set; }
        public virtual string name { get; set; }
        public virtual int? parentId { get; set; }
        public virtual string parentNmae { get; set; }
        public virtual string remark { get; set; }
        public virtual string no { get; set; }
    }
}
