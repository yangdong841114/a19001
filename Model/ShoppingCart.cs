﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 购物车表实体
    /// </summary>
    [Serializable]
    public class ShoppingCart : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //商品ID
        public virtual int? productId { get; set; }
        //数量
        public virtual int? num { get; set; }
        //写入时间
        public virtual DateTime? addTime { get; set; }
        //修改时间
        public virtual DateTime? updateTime { get; set; }
        //是否选中，1：否，2：是
        public virtual int? isCheck { get; set; }



        //冗余字段

        public virtual double? price { get; set; }
        public virtual double? totalPrice { get; set; }

        public virtual string productName { get; set; }
        public virtual string productCode { get; set; }

        public virtual string imgUrl { get; set; }
       
    }
}
