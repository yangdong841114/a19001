﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// EP挂卖记录实体
    /// </summary>
    [Serializable]
    public class EpSaleRecord : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //挂卖单号
        public string number { get; set; }

        //待售数量
        public double? waitNum { get; set; }

        //挂卖数量
        public double? saleNum { get; set; }

        //已售出数量
        public double? scNum { get; set; }

        //挂卖人的手机号
        public string phone { get; set; }

        //挂卖人的QQ
        public string qq { get; set; }

        //挂卖人ID
        public int? uid { get; set; }

        //挂卖人编码
        public string userId { get; set; }

        //挂卖人姓名
        public string userName { get; set; }

        //挂卖时间
        public DateTime? addTime { get; set; }

        //状态: 0:挂卖中，1：部分售出，2：全部售出，3：已完成，4：取消挂卖
        public int? flag { get; set; }

        //手续费金额
        public double? fee { get; set; }

        //手续费比例
        public double? feeBili { get; set; }

        


        //查询 条件冗余字段start--------------------------------------------------------

        public List<int> flagList { get; set; }

        public double? egtWaitNum { get; set; }

        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
