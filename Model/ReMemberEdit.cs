﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 修改推荐关系实体
    /// </summary>
    [Serializable]
    public class ReMemberEdit : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }
        //旧推荐人
        public virtual string oldReName { get; set; }
        //新推荐人
        public virtual string newReName { get; set; }
        //修改时间
        public virtual DateTime? addTime { get; set; }
        //创建人ID
        public virtual int? createId { get; set; }
        //创建人编号
        public virtual string createUser { get; set; }



        //查询 条件start
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询条件end
       
    }
}
