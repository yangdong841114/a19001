﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 账户托管
    /// </summary>
    [Serializable]
    public class DelegaMember : Page, DtoData
    {
        //主键
        public int? id { get; set; }
        //托管到哪个账户ID
        public int? mainId { get; set; }
        //托管到那个账户编码
        public string mainUserId { get; set; }
        //被托管的账户ID
        public int? delegaId { get; set; }
        //被托管的账户编码
        public string delegaUserId { get; set; }
        //托管时间
        public DateTime? addTime { get; set; }

        //点值
        public virtual double? agentDz { get; set; }
        //奖金币
        public virtual double? agentJj { get; set; }
        //购物币
        public virtual double? agentGw { get; set; }
        //复投币
        public virtual double? agentFt { get; set; }

        //交易密码
        public string password { get; set; }

        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
    }
}
