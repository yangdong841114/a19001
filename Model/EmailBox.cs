﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 邮件中心实体
    /// </summary>
    [Serializable]
    public class EmailBox : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //标题
        public virtual string title { get; set; }
        //邮件内容
        public virtual string content { get; set; }
        //邮件内容
        public virtual int? type { get; set; }
        //类型描述
        public virtual string typeName { get; set; }
        public virtual string fromUserName { get; set; }
        //收信人ID
        public virtual int? toUid { get; set; }
        //收信人编码
        public virtual string toUser { get; set; }
        //发信人ID
        public virtual int? fromUid { get; set; }
        //发信人编码
        public virtual string fromUser { get; set; }
        //发信时间
        public virtual DateTime? addTime { get; set; }
        //是否阅读，1：未阅读，2：已阅读
        public virtual int? isRead { get; set; }
        //阅读时间
        public virtual DateTime? readTime { get; set; }
        //来源ID
        public virtual int? sourceId { get; set; }

        public virtual DateTime? startTime { get; set; }

        public virtual DateTime? endTime { get; set; }

        //源头ID
        public virtual int? fristId { get; set; }

        //是否发给所有人  2：是
        public virtual int? isAll { get; set; }

        public virtual int? productId { get; set; }


    }
}
