﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 文章表实体
    /// </summary>
    [Serializable]
    public class News : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //标题
        public virtual string title { get; set; }
        //内容
        public virtual string content { get; set; }
        //文章类型
        public virtual int? typeId { get; set; }
        //url
        public virtual string url { get; set; }
        //文件路径
        public virtual string filePath { get; set; }
        //点击数
        public virtual int? hits { get; set; }
        //作者
        public virtual string author { get; set; }
        //编辑者
        public virtual string editor { get; set; }
        //添加时间
        public virtual DateTime? addTime { get; set; }
        //修改时间
        public virtual DateTime? editTime { get; set; }
        //置顶时间
        public virtual DateTime? topTime { get; set; }
        //是否置顶，1：否，2：是
        public virtual int? isTop { get; set; }

        public virtual DateTime? startTime { get; set; }

        public virtual DateTime? endTime { get; set; }
       
    }
}
