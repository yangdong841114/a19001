﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 列表响应对象
    /// </summary>
    public class ResponseDtoMap<K, V> : ResponseData
    {
        public ResponseDtoMap()
        {
            base.status = "success";
        }

        public ResponseDtoMap(string status)
        {
            base.status = status;
        }

        //Dictionary<K,V>键值对列表
        public Dictionary<K,V> map{get;set;}
    }
}
