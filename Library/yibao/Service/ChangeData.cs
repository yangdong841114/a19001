﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;


    public class ChangeData
    {
        public ChangeData()
        {

        }

        /// <summary>
        /// 将Dictionary对象转化成HMAC签名的字符串数据
        /// </summary>
        /// <param name="info">request请求的数据信息</param>
        /// <param name="param">参加签名的数组</param>
        /// <param name="valueSplit">数据间的分隔符</param>
        /// <returns>签名字符串</returns>
        public static string toCreateHmacData(Dictionary<string,string> info, string[] paramRequests,string valueSplit)
        {
            //返回结果
            string result = "";

            foreach (string param in paramRequests)
            {
                result += result.Equals("")?(info[param]):(info[param].Equals("")?(info[param]) : valueSplit+info[param]);
            }
            return result;
        }

        /// <summary>
        /// 将Dictionary对象转化成HMAC签名的字符串数据
        /// </summary>
        /// <param name="info">request请求的数据信息</param>
        /// <param name="param">参加签名的数组</param>
        /// <returns>签名字符串</returns>
        public static string toCreateHmacData(Dictionary<string,string> info, string[] paramRequests)
        {
            //返回结果
            string result = "";

            foreach (string param in paramRequests)
            {
                result = result + info[param];
            }

            return result;
        }




    }
