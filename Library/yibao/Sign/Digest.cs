﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


public class Digest
{
    /// <summary>
    /// 区分MD5与HmacMD5
    /// </summary>
    public Digest() { }

    /// <summary>
    /// 按字段名的字典序进行排序生成签名
    /// </summary>
    /// <!--编码格式UTF-8-->
    /// <param name="data">存放签名数据的data</param>
    /// <param name="key">MD5签名的密钥</param>
    /// <returns></returns>
    public static string CreateHmac(Dictionary<String, String> data,string key)
    {
        //输出
        String resp = "";
        //字符串
        String unsignString = "";
        //签名信息
        List<String> nameList = new List<String>(data.Keys);
        //首先按字段名的字典序排列
        nameList.Sort();
        foreach (String name in nameList)
        {
            String value = data[name];
            if (value != null)
            {
                unsignString += name + "=" + value + "&";
            }
        }
        try
        {
            resp = CreateHmac(unsignString,key);
        }
        catch (Exception e)
        {
            e.ToString();
        }
        return resp;
    }

    /// <summary>
    /// 字符串HmacMD5签名
    /// <!--编码格式是UTF-8-->
    /// </summary>
    /// <param name="aValue">要签名的字符串信息</param>
    /// <param name="key">签名的密钥</param>
    /// <returns></returns>
    public static string CreateHmac(string aValue,string key)
    {

        byte[] k_ipad = new byte[64];
        byte[] k_opad = new byte[64];
        byte[] keyb;
        byte[] Value;
        keyb = Encoding.UTF8.GetBytes(key);
        Value = Encoding.UTF8.GetBytes(aValue);

        for (int i = keyb.Length; i < 64; i++)
            k_ipad[i] = 54;

        for (int i = keyb.Length; i < 64; i++)
            k_opad[i] = 92;

        for (int i = 0; i < keyb.Length; i++)
        {
            k_ipad[i] = (byte)(keyb[i] ^ 0x36);
            k_opad[i] = (byte)(keyb[i] ^ 0x5c);
        }

        HmacMD5 md = new HmacMD5();

        md.update(k_ipad, (uint)k_ipad.Length);
        md.update(Value, (uint)Value.Length);
        byte[] dg = md.finalize();
        md.init();
        md.update(k_opad, (uint)k_opad.Length);
        md.update(dg, 16);
        dg = md.finalize();

        return toHex(dg);
    }


    //*****************************************************************************





     //*****************************************************************************


    /// <summary>
    /// 验证签名是否相同
    /// </summary>
    /// <param name="value1">参数</param>
    /// <param name="value2">参数</param>
    /// <param name="param">变量名称</param>
    /// <returns></returns>
    public static string validateParam(string value1,string value2,string param)
    {
        string result = "";
        if (value1.Equals("")||value2.Equals(""))
        {
            result = param+"无值，验签失败";
            return result;
        }
        StringComparer comparer = StringComparer.OrdinalIgnoreCase;
        if (0==comparer.Compare(value1,value2))
        {
            result = param + "验签成功！";
        }
        else
        {
            result= param + "验签失败！";
        }
        return result;

    }





    public static string toHex(byte[] input)
    {
        if (input == null)
            return null;

        StringBuilder output = new StringBuilder(input.Length * 2);

        for (int i = 0; i < input.Length; i++)
        {
            int current = input[i] & 0xff;
            if (current < 16)
                output.Append("0");
            output.Append(current.ToString("x"));
        }

        return output.ToString();
    }


}