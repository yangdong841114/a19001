﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library
{
    public class sysInit
    {

        public static string[] QAClass()   //安全问题
        {
            string[] qc = { "", "我喜欢的动物？", "我父亲姓名？", "我母亲的生日？", "我毕业于哪所学校？", "我的生日是什么？"};
            return qc;
        }

        public static string[] Levels()  //会员级别
        {
            string[] lName = { "-", "会员", "助理", "讲师","高级讲师","金牌讲师","商界领袖" };
            return lName;
        }
        public static string Levels(int i)
        {
            string[] NC = Levels();
            return NC[i];
        }
        public static string Levels(object i)
        {
            return Levels(int.Parse(i.ToString()));
        }


        public static string[] NewsClass()   //信息类型   顺序请不要修改，如没有请留空
        {
            string[] NC = { "未知类型", "精品资讯", "联系我们", "如何开始直播？", "下载专区", "开播协议"};
            return NC;
        }
        public static string NewsClass(int i)  
        {
            string[] NC = NewsClass();
            return NC[i];
        }


        public static string[] BonusClass()   //奖金类型
        {
            string[] BC = { "", "分享提成", "分红", "主播收益", "打赏提成", "充值分成", "打赏分成", "广告分成" };
            return BC;
        }

        public static string[] JournalClass()   //流水账类型   顺序请不要修改，如没有请留空
        {

            string[] NC = { "未知类型", "资源豆", "播豆","积分" };
            return NC;
        }
        public static string JournalClass(int v)   //流水账类型   顺序请不要修改，如没有请留空
        {

            string[] NC = JournalClass();
            return NC[v];
        }

        /// <summary>
        /// 项目类型
        /// </summary>
        public static string[] zdClass1()
        {
            string[] TC = { "未知类型", "金融理财", "消费商品" };
            return TC;
        }
        public static string zdClass1(int i)
        {
            string[] TC = zdClass1();
            return TC[i];
        }

        /// <summary>
        /// 制度类型
        /// </summary>
        public static string[] zdClass2()
        {
            string[] TC = { "未知类型", "太阳线", "一条线", "双轨", "三轨", "分盘" };
            return TC;
        }
        public static string zdClass2(int i)
        {
            string[] TC = zdClass2();
            return TC[i];
        }

        /// <summary>
        /// 广告类型
        /// </summary>
        public static string[] adClass()   
        {
            string[] TC = { "未知类型", "首页滚动广告1", "首页滚动广告2", "首页滚动广告3", "首页滚动广告4", "首页滚动广告5", "注册广告", "成功注册广告", "登录页广告", "找人脉广告", "一呼百应广告", "制度设计广告", "忘记密码广告", "直播间广告", "学生登陆广告", "主播登录广告" };

            return TC;
        }
        public static string adClass(int i)
        {
            string[] TC = adClass();
            return TC[i];
        }
        public static string adClass(object i)
        {
            string[] TC = adClass();
            return TC[int.Parse(i.ToString())];
        }


        public static string[] adSize()
        {
            string[] TC = { "-", "1000×350", "1000×350", "1000×350", "1000×350", "1000×350", "400×440", "1000×350", "1920×520", "972×120", "1000×300", "962×120", "1920×520", "直播间广告", "510*360","240*185" };

            return TC;
        }
        public static string adSize(string i)
        {
            string[] TC = adSize();
            return TC[int.Parse(i)];
        }

        /// <summary>
        /// 支付类型
        /// </summary>
        public static string[] payClass()   
        {
            string[] TC = { "-", "资源豆", "播豆"};

            return TC;
        }


        public static bool Pwd2Switch()  //二级密码是否每进入一次页面都要输入
        {
            return false;
         }

        public static bool Pwd3Switch()  //三级密码是否每进入一次页面都要输入
        {
            return false;
        }


    }
}
