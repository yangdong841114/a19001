﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.wyy.Response
{
    public class ResponseVideo 
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public Video ret { get; set; }
    }
}
