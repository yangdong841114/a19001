﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.wyy.Response
{
    public class setAlwaysRecordResponse 
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string msg { get; set; }
    }
}
