﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.wyy.Response
{
    public class GetVideo
    {
        /// <summary>
        /// 视频文件ID
        /// </summary>
        public long vid { get; set; }
        /// <summary>
        /// 视频文件在点播桶中的存储路径
        /// </summary>
        public string orig_video_key { get; set; }
        /// <summary>
        /// 录制后文件名，格式为filename_YYYYMMDD-HHmmssYYYYMMDD-HHmmss, 文件名录制起始时间（年月日时分秒) -录制结束时间（年月日时分秒)
        /// </summary>
        public string video_name { get; set; }
        /// <summary>
        /// 用户ID，是用户在网易云视频与通信业务的标识，用于与其他用户的业务进行区分。通常，用户不需关注和使用。
        /// </summary>
        public string uid { get; set; }
        /// <summary>
        /// 消息ID，同一条消息nId全局唯一，网络超时或接收方返回非200状态码时根据业务规则进行重发，接收方接到多条通知情况下可用于进行消息去重ID
        /// </summary>
        public string nId { get; set; }
        /// <summary>
        /// 录制文件起始时间戳(毫秒)
        /// </summary>
        public string beginTime { get; set; }
        /// <summary>
        /// 录制文件结束时间戳(毫秒)
        /// </summary>
        public string endTime { get; set; }
        /// <summary>
        /// 频道ID
        /// </summary>
        public string cid { get; set; }
    }
}
