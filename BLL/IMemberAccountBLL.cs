﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 会员账户业务逻辑接口
    /// </summary>
    public interface IMemberAccountBLL : IBaseBLL<MemberAccount>
    {
        /// <summary>
        ///  增加会员账户余额
        ///  增加非空属性的金额（在原金额上增加）
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        int UpdateAdd(MemberAccount account);

        /// <summary>
        /// 减少会员账户余额
        /// 减少非空属性的金额（在原金额上减少）
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        int UpdateSub(MemberAccount account);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<MemberAccount> GetListPage(MemberAccount model);
        DataTable GetListPageDataTable(MemberAccount model);
        /// <summary>
        /// 汇总查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        MemberAccount GetTotalSummary(MemberAccount model);

        /// <summary>
        /// 根据会员ID获取账户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        MemberAccount GetModel(int id);

        /// <summary>
        /// 导出流水excel
        /// </summary>
        /// <returns></returns>
        DataTable GetLiuShuiExcel();
    }
}
