﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 奖金查询接口
    /// </summary>
    public interface ICurrencyBLL
    {

        /// <summary>
        /// 分页查询（按日期汇总)
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<CurrencySum> GetAllSumCurrency(CurrencySum model);
        DataTable GetAllSumCurrencyDataTable(CurrencySum model);

        /// <summary>
        /// 分页查询（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<CurrencySum> GetByUserSumCurrency(CurrencySum model);

        /// <summary>
        /// 查询奖金明细记录（按某个会员)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PageResult<Currency> GetDetailListPage(Currency model);

        /// <summary>
        /// 分页查询日拨比
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PageResult<Bobi> GetBoBiListPage(Bobi model);

        /// <summary>
        /// 查询拨比汇总
        /// </summary>
        /// <returns></returns>
        Bobi GetTotalBobi();

        /// <summary>
        /// 获取底部汇总数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<CurrencySum> GetByUserSumCurrencyFooter(CurrencySum model);


        /// <summary>
        /// 导出拨比率数据
        /// </summary>
        /// <returns></returns>
        DataTable GetBobiExcelList();

        /// <summary>
        /// 导出奖金日查询数据
        /// </summary>
        /// <returns></returns>
        DataTable GetBounsExcel();


        /// <summary>
        /// 导出奖金excel（按用户汇总某天的数据)
        /// 
        DataTable GetBounsDetailExcel(CurrencySum model);

        /// <summary>
        /// 导出会员奖金明细excel（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        DataTable GetUserBonusDetailExcel(CurrencySum model);
    }
}
