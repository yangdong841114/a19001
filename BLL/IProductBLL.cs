﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 商品业务逻辑接口
    /// </summary>
    public interface IProductBLL : IBaseBLL<Product>
    {
        /// <summary>
        /// 检查商品编码是否存在
        /// </summary>
        /// <param name="productCode">商品编码</param>
        /// <param name="id">id>0时表示更新，更新时，要剔除掉原商品编码</param>
        /// <returns></returns>
        bool isExistsProductCode(string productCode, int id);

        /// <summary>
        /// 分页查询商品
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<Product> GetListPage(Product model, string fields);
        PageResult<Product> GetListPageAll(Product model, string fields);
        List<Product> GetList(string sql);
        /// <summary>
        /// 分页查询商家商品
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        PageResult<Product> GetMerchantListPage(Product model, string fields);

        /// <summary>
        /// 保存商品
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        Product SaveProduct(Product model, Member current);

        /// <summary>
        /// 更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        Product UpdateProduct(Product model, Member current);

        /// <summary>
        /// 商品上架
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveShelve(int id);

        /// <summary>
        /// 商品下架
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveCancelShelve(int id);

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);

        /// <summary>
        /// 审核商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveAuditProduct(int id, Member current);

        /// <summary>
        /// 根据ID查询商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Product GetModel(int id);
    }
}
