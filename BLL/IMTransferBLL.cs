﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 会员转账业务逻辑接口
    /// </summary>
    public interface IMTransferBLL : IBaseBLL<MTransfer>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<MTransfer> GetListPage(MTransfer model);
        DataTable GetListPageDataTable(MTransfer model);

        /// <summary>
        /// 保存转账申请
        /// </summary>
        /// <param name="model">保存数据</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        int SaveMTransfer(MTransfer model, Member current);

        ///// <summary>
        ///// 取消转账申请
        ///// </summary>
        ///// <param name="id">转账申请ID</param>
        ///// <param name="current">当前会员</param>
        ///// <returns></returns>
        //int UpdateCancel(int id, Member current);


        ///// <summary>
        ///// 审核转账申请
        ///// </summary>
        ///// <param name="id">转账申请ID</param>
        ///// <param name="current">当前登录用户</param>
        ///// <returns></returns>
        //int UpdateAudit(int id, Member current);
        int WK(string WKLX, Member current);
        /// <summary>
        /// 导出转账excel
        /// </summary>
        /// <returns></returns>
        DataTable GetTransferExcel();
    }
}
