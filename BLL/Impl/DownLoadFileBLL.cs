﻿using Common;
using DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL.Impl
{
    public class DownLoadFileBLL : IDownLoadFileBLL
    {
        private System.Type type = typeof(DownLoadFile);
        public IBaseDao dao { get; set; }

        public PageResult<DownLoadFile> GetListPage(DownLoadFile model)
        {
            PageResult<DownLoadFile> page = new PageResult<DownLoadFile>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from DownLoadFile m where 1=1 ";
            string countSql = "select count(1) from DownLoadFile where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.title))
                {
                    param.Add(new DbParameterItem("title", ConstUtil.LIKE, model.title));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<DownLoadFile> list = new List<DownLoadFile>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((DownLoadFile)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Save(DownLoadFile model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("标题不能为空"); }
            if (ValidateUtils.CheckNull(model.fileUrl)) { throw new ValidateException("上传文件不能为空"); }
            model.addTime = DateTime.Now;
            model.addUid = current.id;
            model.addUser = current.userId;
            return dao.Save(model);
        }

        public int Delete(int id)
        {
            return dao.ExecuteBySql("delete from DownLoadFile where id=" + id);
        }
    }
}
