﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class InfoBLL : IInfoBLL
    {
        private System.Type type = typeof(Info);
        public IBaseDao dao { get; set; }


        public Info GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Info where id=" + id);
            if (row == null) return null;
            Info model = (Info)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public List<Info> GetList(string where)
        {
            string sql = "select * from Info " + where;
            DataTable dt = dao.GetList(sql);
            List<Info> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Info>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Info)ReflectionUtil.GetModel(typeof(Info), row));
                }
            }
            return list;
        }
        public List<Info> GetListTop10(string where)
        {
            string sql = "select top 20 * from Info " + where;
            DataTable dt = dao.GetList(sql);
            List<Info> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Info>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Info)ReflectionUtil.GetModel(typeof(Info), row));
                }
            }
            return list;
        }

        

        public PageResult<Info> GetListPage(Info model, string fields)
        {
            PageResult<Info> page = new PageResult<Info>();
            string sql = "select " + fields + ",row_number() over(order by addTime desc, m.id desc) rownumber from Info m where 1=1 ";
            string countSql = "select count(1) from Info where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (model.type != null)
                {
                    param.Add(new DbParameterItem("type", ConstUtil.EQ, model.type));
                }
                if (model.fltype != null)
                {
                    param.Add(new DbParameterItem("fltype", ConstUtil.EQ, model.fltype));
                }
                if (model.title!= null)
                {
                    param.Add(new DbParameterItem("title", ConstUtil.LIKE, model.title));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Info> list = new List<Info>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Info)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Info model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            else if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            if (model.infocontent.Length > 10000) { throw new ValidateException("内容太长或含有乱码"); }
            int c = dao.Update(model);
            return c;
        }

        public int Save(Info model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            else if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            model.uid = login.id;
            model.userName = login.userName;
            model.userId = login.userId;
            if (model.infocontent.Length > 10000) { throw new ValidateException("内容太长或含有乱码"); }
            int c = dao.Save(model);
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Info", id);
        }

        public int DeletePl(List<int> list)
        {
            if (list == null || list.Count == 0)
            {
                throw new ValidateException("要删除的内容为空");
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.Append(list[i]).Append(",");
            }

            string sql = "delete from Info where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
            return dao.ExecuteBySql(sql);
        }


        public int SaveOrUpdate(Info model, Member login)
        {
            if (model == null) { throw new ValidateException("内容为空"); }
            else if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("修改内容为空"); }
            if (model.infocontent.Length > 10000) { throw new ValidateException("内容太长或含有乱码"); }
            int c = 0;
            c = Update(model, login);
            
            return c;
        }

        public int SaveTop(int id)
        {
            string sql = "update Info set isTop=2 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

        public int SaveCancelTop(int id)
        {
            string sql = "update Info set isTop=1 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

        public int Addllcount(int id)
        {
            string sql = "update Info set llcount=llcount+1 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

    }
}
