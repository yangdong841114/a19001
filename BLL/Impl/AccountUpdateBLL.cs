﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class AccountUpdateBLL : BaseBLL<AccountUpdate>, IAccountUpdateBLL
    {

        private System.Type type = typeof(AccountUpdate);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new AccountUpdate GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            AccountUpdate mb = (AccountUpdate)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new AccountUpdate GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            AccountUpdate mb = (AccountUpdate)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<AccountUpdate> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<AccountUpdate> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<AccountUpdate>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((AccountUpdate)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<AccountUpdate> GetList(string sql)
        {
            List<AccountUpdate> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<AccountUpdate>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((AccountUpdate)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }


        public int UpdateMemberAcount(AccountUpdate model, Member current)
        {
            string isFs=model.isFs;
            if (isFs == "" || isFs == null) isFs = "n";
            model.isFs=null;
            if (model == null) { throw new ValidateException("操作内容为空"); }
            if (ValidateUtils.CheckNull(model.userId)) { throw new ValidateException("会员账户为空"); }
            if (ValidateUtils.CheckIntZero(model.accountType)) { throw new ValidateException("账户类型为空"); }
            if (ValidateUtils.CheckDoubleZero(model.epoints)) { throw new ValidateException("充值金额不能为0"); }

            Member m = memberBLL.GetModelByUserId(model.userId);
            if (m == null) { throw new ValidateException("会员账户不存在"); }

            //保存记录
            DateTime now = DateTime.Now;
            model.uid = m.id;
            model.userName = m.userName;
            model.addTime = now;
            model.addUid = current.id;
            model.addUser = current.userId;
            object o = dao.SaveByIdentity(model);
            int id = Convert.ToInt32(o);

            bool isAdd = model.epoints > 0 ? true : false; //是否增加，true:增加,false:减少

            //修改会员账户
            MemberAccount old = accountBLL.GetOne("select * from MemberAccount where id=" + m.id); //原账户
            MemberAccount account = new MemberAccount(); //修改的金额
            account.id = m.id;

            //流水账对象
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = model.accountType;
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "后台手动充值";
            if (model.accountType == ConstUtil.JOURNAL_TOD)//后台：账户充值，给TOD币账户充值业务摘要修改为：分红
            liu.abst = "分红";
            if (model.accountType == ConstUtil.JOURNAL_MOBY)//后台：账户充值，mobby码业务摘要修改为：權利性通證
            liu.abst = "權利性通證";
            liu.income = 0;
            liu.outlay = 0;
            liu.addtime = now;
            liu.sourceId = id;
            liu.tableName = "AccountUpdate";
            liu.addUid = current.id;
            liu.addUser = current.userId;

            //取绝对值金额,计算会员账户、计算流水账last
            double absEpoints = System.Math.Abs(model.epoints.Value);

            if (model.accountType == ConstUtil.JOURNAL_DZ)         //点值
            {
                account.agentDz = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentDz.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentDz.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_FTB)    //复投币
            {
                account.agentFt = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = absEpoints;
                    liu.last = old.agentFt.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentFt.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_GWB)    //购物币
            {
                account.agentGw = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentGw.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentGw.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_JJB)    //奖金币
            {
                account.agentJj = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentJj.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentJj.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_DHM)    //兑换码
            {
                account.agentDhm = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentDhm.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentDhm.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_TOD)    //TOD
            {
                account.agentTod = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentTod.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentTod.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_FHZ)    //分红值
            {
                account.agentFhz = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentFhz.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentFhz.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_TOCC)    //TOCC
            {
                account.agentTocc = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentTocc.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentTocc.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_SL)    //算力
            {
                account.agentSl = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentSl.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentSl.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_BPC)    //BPC
            {
                account.agentBpc = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentBpc.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentBpc.Value - absEpoints;
                }
            }
            else if (model.accountType == ConstUtil.JOURNAL_MOBY)    //MOBY
            {
                account.agentMoby = absEpoints;
                //0:增加，1：减少
                if (isAdd)
                {
                    liu.income = model.epoints;
                    liu.last = old.agentMoby.Value + absEpoints;
                }
                else
                {
                    liu.outlay = model.epoints;
                    liu.last = old.agentMoby.Value - absEpoints;
                }
            }
            else
            {
                throw new ValidateException("账户类型错误");
            }
            if (liu.last < 0 && isFs != "y") throw new ValidateException("扣减后会员账户余额将变成负数-确认要执行操作吗");
            // true：增加用户账户，false：减少用户账户
            if (isAdd) { accountBLL.UpdateAdd(account); }
            else { accountBLL.UpdateSub(account); }

            //记录流水账
            liushuiBLL.Save(liu);

            //记录操作日志
            OperateLog log = new OperateLog();
            log.recordId = m.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "手动充值：" + m.userId + "：" + model.epoints ;
            log.tableName = "AccountUpdate";
            log.recordName = "后台用户账户";
            this.SaveOperateLog(log);

            return id;
        }

        public PageResult<AccountUpdate> GetListPage(AccountUpdate model)
        {
            PageResult<AccountUpdate> page = new PageResult<AccountUpdate>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from AccountUpdate m where 1=1 ";
            string countSql = "select count(1) from AccountUpdate where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.accountType))
                {
                    param.Add(new DbParameterItem("accountType", ConstUtil.EQ, model.accountType));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<AccountUpdate> list = new List<AccountUpdate>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((AccountUpdate)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetListPageDataTable(AccountUpdate model)
        {
            string sql = @"select m.userId,m.userName,m.epoints,DaccountType.name as accountTypeName,m.addTime,m.addUser,m.remark from AccountUpdate m left join DataDictionary DaccountType
on m.accountType=DaccountType.id where 1=1 ";
          
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.accountType))
                {
                    param.Add(new DbParameterItem("m.accountType", ConstUtil.EQ, model.accountType));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }


            DataTable dt = dao.GetList(sql, param, true);

            return dt;
        }


        public DataTable GetAccountUpdateExcel()
        {
            string sql = "select userId,userName,epoints,d.name as accountType,m.addTime,addUser,m.remark from AccountUpdate m left join DataDictionary d on d.id=m.accountType  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

    }
}
