﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class LiuShuiZhangBLL : BaseBLL<LiuShuiZhang>, ILiuShuiZhangBLL
    {

        private System.Type type = typeof(LiuShuiZhang);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }


        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new LiuShuiZhang GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            LiuShuiZhang mb = (LiuShuiZhang)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new LiuShuiZhang GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            LiuShuiZhang mb = (LiuShuiZhang)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<LiuShuiZhang> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<LiuShuiZhang> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<LiuShuiZhang>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LiuShuiZhang)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<LiuShuiZhang> GetList(string sql)
        {
            List<LiuShuiZhang> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<LiuShuiZhang>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LiuShuiZhang)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<LiuShuiZhang> GetListPage(LiuShuiZhang model)
        {
            PageResult<LiuShuiZhang> page = new PageResult<LiuShuiZhang>();
            string sql = "select m.*,(case when income=0 then '支出' else '收入' end )optype,(case when income=0 then outlay else income end )epotins, "+
                         " row_number() over(order by m.id desc) rownumber  from LiuShuiZhang m inner join member b on m.uid=b.id where b.ispay=1 ";
            string countSql = "select count(1) from LiuShuiZhang m inner join member b on m.uid=b.id where b.ispay=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.accountId))
                {
                    param.Add(new DbParameterItem("m.accountId", ConstUtil.EQ, model.accountId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<LiuShuiZhang> list = new List<LiuShuiZhang>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LiuShuiZhang)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public double GetsrMoney(LiuShuiZhang model)
        {
            string sql = "select SUM(income) from LiuShuiZhang where 1=1";
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and uid=" + model.uid;
                }
                if (!ValidateUtils.CheckIntZero(model.accountId))
                {
                    sql += " and accountId=" + model.accountId;
                }
                if (model.startTime != null)
                {
                    sql += " and addTime>='" + model.startTime + "'";
                }
                if (model.endTime != null)
                {
                    sql += " and addTime<='" + model.endTime.Value.ToShortDateString() + " 23:59:59'";
                }
            }
            object o = dao.ExecuteScalar(sql);
            return o == null ? 0 : Convert.ToDouble(o);
        }

        public double GetzcMoney(LiuShuiZhang model)
        {
            string sql = "select SUM(outlay) from LiuShuiZhang where 1=1";
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and uid=" + model.uid;
                }
                if (!ValidateUtils.CheckIntZero(model.accountId))
                {
                    sql += " and accountId=" + model.accountId;
                }
                if (model.startTime != null)
                {
                    sql += " and addTime>='" + model.startTime + "'";
                }
                if (model.endTime != null)
                {
                    sql += " and addTime<='" + model.endTime.Value.ToShortDateString() + " 23:59:59'";
                }
            }
            object o = dao.ExecuteScalar(sql);
            return o == null ? 0 : Convert.ToDouble(o);
        }

        public int UpdateBalance(LiuShuiZhang model, Member current)
        {
            List<DbParameterItem> param = new List<DbParameterItem>();
            model.userId = dao.ExecuteScalar("select userId from LiuShuiZhang where id=" + model.id).ToString();
            string sql = "select last from LiuShuiZhang where 1=1 ";
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.id))
                {
                    sql += " and id=" + model.id;
                }
            }
            object o = dao.ExecuteScalar(sql);
            decimal Beforemoney = decimal.Parse(o.ToString());
            decimal Aftermoney = decimal.Parse(model.last.ToString());
            int c=dao.Update(model);

            //记录操作日志
            OperateLog op = new OperateLog();
            op.recordId = model.id;
            op.uid = current.id;
            op.userId = current.userId;
            op.ipAddress = current.ipAddress;
            op.mulx = "手动修改流水记录余额，记录id为："+model.id+",该记录属于"+model.userId+"用户。修改前：" + Beforemoney + "，修改后：" + Aftermoney;
            op.tableName = "LiuShuiZhang";
            op.recordName = "后台修改流水";
            this.SaveOperateLog(op);

            return c;
        }

        public DataTable GetUserLiuShuiExcel(LiuShuiZhang model)
        {
            string sql = " select m.userId,b.userName,d.name as accountId,m.abst,m.income,m.outlay,m.last,m.addtime,(case when income=0 then '支出' else '收入' end )optype,m.addUser from LiuShuiZhang m inner join member b on m.uid=b.id left join DataDictionary d on d.id=m.accountId  where b.ispay=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    sql += " and uid=" + model.uid;
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
         
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

    }
}
