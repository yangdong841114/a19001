﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class FamilyTreeBLL : IFamilyTreeBLL
    {
        private int _x = 1; //当前会员
        private int _y = 2; //轨数
        private int _z = 3; //层数
        public IBaseDao dao { get; set; }
        public IParamSetBLL psBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public void SetParmValue(int x, int y, int z)
        {
            this._x = x;
            this._y = y;
            this._z = z;
        }


        #region 输出Table
        /// <summary>
        ///  输出Table
        /// </summary>
        /// <param name="a">分支，输入2为二叉树</param>
        /// <param name="b">级数，输入2显示2级</param>
        /// <returns></returns>
        private string createTable()
        {
            
            int a = this._y; //轨数
            int b = this._z;
            string Table = "";

            int m = 0; //位置编号
            int TdNum = (int)Math.Pow(a, b - 1); //最低层人数
            for (int i = 0; i < b; i++)
            { //遍历所有层  
                int k = TdNum / (int)Math.Pow(a, i); //合并td数
                string colspan = " colspan='" + k + "'";
                if (k <= 1) colspan = "";

                Table += "<tr>";

                //会员信息层
                for (int j = 0; j < TdNum / k; j++)
                {
                    double width = 100 / (TdNum / k);
                    Table += "<td width='" + width + "%' align='center' " + colspan + ">{" + m + "}</td>"; ++m;
                }
                Table += "</tr>";

                if (k > 1) // line层
                {
                    Table += "<tr>";
                    for (int j = 0; j < TdNum / k; j++)
                    {
                        Table += "<td" + colspan + ">";
                        Table += "<img src='/Content/map/l_down.gif' height='30'>";
                        Table += "<img src='/Content/map/line.gif' width='" + (int)(a - 1) * (50 / a) + "%' height='30'>";
                        Table += "<img src='/Content/map/up.gif' height='30'>";
                        Table += "<img src='/Content/map/line.gif' width='" + (int)(a - 1) * (50 / a) + "%' height='30'>";
                        Table += "<img src='/Content/map/r_down.gif' alt='' height='30'>";
                        Table += "</td>";
                    }
                    Table += "</tr>";
                }
            }

            Table = "<table style='width: 100%;'>" + Table + "</table>";  //整合
            return Table;
        }

        #endregion

        public static DataTable GetNewDataTable(DataTable dt, string condition)
        {
            DataTable newdt = new DataTable();
            newdt = dt.Clone();
            DataRow[] dr = dt.Select(condition);
            for (int i = 0; i < dr.Length; i++)
            {
                newdt.ImportRow((DataRow)dr[i]);
            }
            return newdt;//返回的查询结果
        }

        private Dictionary<string, Member> GetDi(List<Member> list, int viewId)
        {
            //List<Member> Nodes = new List<Member>();
            Dictionary<string, Member> di = new Dictionary<string, Member>();
            int[] arr = new int[7];
            int treeplace = 0;  //左区开始

            DataTable dt_Currency = dao.GetList("select * from Currency where cat=46 and DateDiff(dd,jstime,getdate())=0");
            for (int i = 0; i < list.Count;i++ )
            {
                if (GetNewDataTable(dt_Currency, "uid='" + list[i].id.Value + "'").Rows.Count > 0) list[i].dpj = "yes";
                else list[i].dpj = "no";
            }

                foreach (Member m in list)  //先绑定父节点
                {
                    if (m.id.ToString() == viewId.ToString())
                    {
                        di.Add("1", m);
                        arr[0] = m.id.Value;
                        break;
                    }
                }

            for (int i = 1; i < 7; i++)   //遍历并重新绑定 Model.member
            {
                bool mIsExists = false;
                foreach (Member m in list)  //取出
                {
                    if (m.fatherID.ToString() == viewId.ToString() && m.treePlace.ToString() == treeplace.ToString())
                    {
                        //Nodes.Add(m);
                        di.Add((i + 1) + "", m);
                        arr[i] = m.id.Value;
                        mIsExists = true;
                        break;
                    }
                }
                if (!mIsExists)
                {

                    Member m = new Member();
                    arr[i] = -1;
                    m.isPay = 0;
                    m.id = -1;
                    m.treePlace = treeplace;
                    m.fatherID = viewId;
                    m.uLevel = 0;
                    m.spare0 = 0;
                    m.spare1 = 0;
                    m.encash0 = 0;
                    m.encash1 = 0;
                    m.new0 = 0;
                    m.new1 = 0;
                    if (viewId > 0)
                    {
                        Member father = memberBLL.GetModelById(viewId);
                        m.isPay = father.isPay;
                        //m.reCount = memberBLL.GetCount("select count(1) from Member where reId=" + viewId + " and ispay=1 and empty=0");
                    }
                    //if (m.reCount >= rightCount)
                    //{
                    //    m.reCount = 1;
                    //}
                    //else
                    //{
                    //    m.reCount = 0;
                    //}
                    di.Add((i + 1) + "", m);
                    //Nodes.Add(m);
                }
                if (treeplace < 2 - 1)  //左右
                {
                    ++treeplace;
                }
                else
                {
                    treeplace = 0;     //上下
                    viewId = arr[i / 2];
                }
            }
            return di;
        }


        public Dictionary<string, Member> GetMemberList(int rootId)
        {
            Member root = memberBLL.GetModelByIdNoPassWord(rootId);
            if (root == null) { return null; }
            int rootLevel = root.uLevel.Value;

            List<Member> u = new List<Member>();
            string sql = "select id,userId,userName,uLevel,fatherID,treePlace,pLevel,spare0,spare1,spare2,new0,new1,new2,encash0,encash1,encash2,isPay,reLCount,reRCount " +
                        " from Member m2 where " +
                         " isFt=0 and isAdmin = 0 and ((pPath like '%,'+@rootId+',%' and pLevel-@pLevel <=2) or id = @id)";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("rootId", null, rootId.ToString()))
                            .Add(new DbParameterItem("pLevel", null, root.pLevel.Value))
                            .Add(new DbParameterItem("id", null, rootId))
                            .Result();
            u = memberBLL.GetMemberList(sql, param, false);
            Dictionary<string, Member> di = GetDi(u, rootId);
            return di;
        }


        #region 遍历树  层序遍历
        /// <summary>
        ///  遍历树
        /// </summary>
        /// <param name="viewId">当前会员作为跟节点</param>
        /// <param name="a">分支，输入2为二叉树</param>
        /// <param name="b">级数，输入2显示2级</param>
        /// <returns></returns>
        public string CreateTreeHtml(int rootId, int ceng, string registerLink,string infoLink)
        {
            this._z = ceng;
            this._x = rootId;
            int viewId = this._x;
            Dictionary<string, ParameterSet> di = psBLL.GetDictionaryByCodes("branchCount");

            if (di["branchCount"] != null && di["branchCount"].paramValue != null)
            {
                this._y = Convert.ToInt32(di["branchCount"].paramValue);
            }
            int a = this._y;
            int b = this._z;

            Member root = memberBLL.GetModelById(rootId);
            int rootLevel = root.uLevel.Value;

            List<Member> u = new List<Member>();
            //  int plevel = bn.GetNodePlevel(viewId);
            string sql = "select id,userId,userName,uLevel,fatherID,treePlace,pLevel,spare0,spare1,spare2,new0,new1,new2,encash0,encash1,encash2,isPay,reLCount,reRCount from Member where " +
                         " isFt=0 and isAdmin = 0 and ((pPath like '%,'+@rootId+',%' and pLevel-@pLevel <=@ceng) or id = @id)";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("rootId", null, rootId.ToString()))
                            .Add(new DbParameterItem("ceng", null, ceng))
                            .Add(new DbParameterItem("pLevel", null, root.pLevel.Value))
                            .Add(new DbParameterItem("id", null, rootId))
                            .Result();
            u = memberBLL.GetMemberList(sql, param, false);

            List<Member> Nodes = new List<Member>();
            int RootTotal = (int)(Math.Pow(a, b) - 1) / (a - 1);
            int[] arr = new int[RootTotal];
            int treeplace = 0;  //左区开始

            foreach (Member m in u)  //先绑定父节点
            {
                if (m.id.ToString() == viewId.ToString())
                {
                    Nodes.Add(m);
                    arr[0] = m.id.Value;
                    break;
                }
            }
            for (int i = 1; i < RootTotal; i++)   //遍历并重新绑定 Model.member
            {
                bool mIsExists = false;
                foreach (Member m in u)  //取出
                {
                    if (m.fatherID.ToString() == viewId.ToString() && m.treePlace.ToString() == treeplace.ToString())
                    {
                        m.infoLink = infoLink;
                        Nodes.Add(m);
                        arr[i] = m.id.Value;
                        mIsExists = true;
                        break;
                    }
                }
                if (!mIsExists)
                {
                    Member m = new Member();
                    arr[i] = -1;
                    if (viewId > 0) { m = memberBLL.GetModelById(Convert.ToInt32(viewId)); }//得到父节点的实体
                    m.isPay = m.isPay; //父节点是否开通
                    m.id = -1;
                    m.registerLink = registerLink;
                    m.treePlace = treeplace;
                    m.fatherID = viewId;
                    Nodes.Add(m);
                }
                if (treeplace < a - 1)  //左右
                {
                    ++treeplace;
                }
                else
                {
                    treeplace = 0;     //上下
                    viewId = arr[i / a];
                }
            }

            return NodesList(Nodes);
        }

        #endregion

        #region 所有节点实体
        /// <summary>
        ///  所有节点实体
        /// </summary>
        private string NodesList(List<Member> li)
        {
            List<string> Nodes = new List<string>();

            Member father = null;

            foreach (Member m in li)  //取出
            {
                StringBuilder str = new StringBuilder();

                if (m.id > 0)
                {
                    str.Append("<div class=\"memberpedigree\"><div class=\"memberbox\">");
                    string scss = ""; //会员style
                    if (m.isPay.Value == 1)
                    {
                        //普卡样式
                        if (m.uLevel.Value == 3) { scss = "panel-primary"; }
                        //银卡样式
                        else if (m.uLevel.Value == 4) { scss = "panel-warning"; }
                        //金卡样式
                        else if (m.uLevel.Value == 5) { scss = "panel-danger"; }
                    }
                    str.Append("<div style=\"min-height: 125px;\" class=\"panels " + scss + "\"  >");
                    str.Append("<div class=\"panels-heading\"><a href='javascript:void(0);' class='forward_to' style='color:black;' userId='" + m.userId + "'> " + m.userId + "(" + m.userName + ") </a></div>");
                    str.Append("<div class=\"panels-body\">");
                    str.Append("<table >");
                    if (this._y == 2)
                    {
                        str.Append("<tr><td width='70'>" + m.encash0 + "</td><th width='30'>总pv</th><td>" + m.encash1 + "</td></tr>");
                        str.Append("<tr><td>" + m.new0 + "</td><th>新pv</th><td>" + m.new1 + "</td></tr>");
                        str.Append("<tr><td>" + m.spare0 + "</td><th>余pv</th><td>" + m.spare1 + "</td></tr>");
                        str.Append("<tr><td>" + m.reLCount + "</td><th>直推</th><td>" + m.reRCount + "</td></tr>");
                    }
                    else if (this._y == 3)
                    {
                        str.Append("<tr><td width='46'>" + m.encash0 + "</td><td>" + m.encash1 + "</td><td>" + m.encash2 + "</td><th width='30'>总pv</th></tr>");
                        str.Append("<tr><td>" + m.new0 + "</td><td>" + m.new1 + "</td><td>" + m.new2 + "</td><th width='30'>新pv</th></tr>");
                        str.Append("<tr><td>" + m.spare0 + "</td><td>" + m.spare1 + "</td><td>" + m.spare2 + "</td><th width='30'>余pv</th></tr>");
                        str.Append("<tr><td>" + m.reLCount + "</td><td>" + m.reRCount + "</td><td>" + m.reRCount + "</td><th width='30'>直推</th></tr>");
                    }
                    str.Append("</table></div></div></div></div>");
                }
                else
                {
                    if (m.isPay == 1)
                    {
                        if (m.fatherID > 0 && m.treePlace == 0)
                        {
                            str.Append("<div class=\"memberpedigree\">");
                            str.Append("<div class='memberbox'>");
                            str.Append("<div class='panels'>");
                            str.Append("<div class='panels-heading'> <a href='#" + m.registerLink + "/" + m.fatherID + m.treePlace.ToString() + "'>添加新会员</a> </div>");
                            str.Append("<div class='panels-body'>");
                            str.Append("</div></div></div></div>");
                            //str.Append("<div><a href='Register.aspx?fatherid=" + m.fatherID + "&treeplace=0'>空位申请</a></div>");
                        }
                        else
                        {
                            str.Append("<div class=\"memberpedigree\">");
                            str.Append("<div class='memberbox'>");
                            str.Append("<div class='panels'>");
                            str.Append("<div class='panels-heading'> <a href='#" + m.registerLink + "/" + m.fatherID + m.treePlace.ToString() + "'>添加新会员</a> </div>");
                            str.Append("<div class='panels-body'>");
                            str.Append("</div></div></div></div>");
                            //str.Append("<div><a href='Register.aspx?fatherid=" + m.fatherID + "&treeplace=1'>空位申请</a></div>");
                        }
                    }
                    else
                    {
                        str.Append("<div class=\"memberpedigree\">");
                        str.Append("<div class='memberbox'>");
                        str.Append("<div class='panels'>");
                        str.Append("<div class='panels-heading'>空位</div>");
                        str.Append("<div class='panels-body'>");
                        str.Append("</div></div></div></div>");
                    }
                    //str.Append("<p>空位</p>");
                }

                Nodes.Add(str.ToString());
                father = m;
            }
            return ChildNode(Nodes);

        }
        #endregion


        #region 取出单个节点实体 ChildNode
        /// <summary>
        ///  取出单个节点实体
        /// </summary>
        private string ChildNode(List<string> li)
        {
            string Table = createTable();
            string[] str = li.ToArray();
            for (int k = 0; k < str.Length; k++)
            {
                Table = Table.Replace("{" + k + "}", str[k]);
            }
            return Table;

        }
        #endregion

    }
}
