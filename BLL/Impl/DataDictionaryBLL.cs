﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class DataDictionaryBLL : BaseBLL<DataDictionary>, IDataDictionaryBLL
    {
        private System.Type type = typeof(DataDictionary);
        public IParamSetBLL paramBLL { get; set; }
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new DataDictionary GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            DataDictionary mb = (DataDictionary)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new DataDictionary GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            DataDictionary mb = (DataDictionary)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<DataDictionary> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<DataDictionary> list = new List<DataDictionary>();
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((DataDictionary)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<DataDictionary> GetList(string sql)
        {
            List<DataDictionary> list = new List<DataDictionary>();
            ;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((DataDictionary)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<DataDictionary> GetListPage(DataDictionary da)
        {
            PageResult<DataDictionary> page = new PageResult<DataDictionary>();
            string sql = "select * from (select d.*,row_number() over(order by d.parentId asc ,d.id desc) rownumber from DataDictionary d where d.parentId<>0 ";
            string countSql = "select count(1) from DataDictionary where parentId<>0 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (da != null)
            {
                if (!ValidateUtils.CheckIntZero(da.parentId))
                {
                    sql += " and parentId = @parentId";
                    countSql += " and parentId = @parentId";
                    param.Add(new DbParameterItem("parentId", null, da.parentId));
                }
                if (!ValidateUtils.CheckNull(da.name))
                {
                    sql += " and name like '%'+@name+'%'";
                    countSql += " and name like '%'+@name+'%'";
                    param.Add(new DbParameterItem("name", null, da.name));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql,param,false);
            int start = (da.page.Value - 1) * da.rows.Value + 1; //开始条数
            int end = da.page.Value * da.rows.Value;
            
            //查询列表
            sql+= " ) t where t.rownumber>=@start and t.rownumber<=@end";
            param.Add(new DbParameterItem("start", null, start));
            param.Add(new DbParameterItem("end", null, end));
            
            DataTable dt = dao.GetList(sql, param, false);
            List<DataDictionary> list = new List<DataDictionary>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((DataDictionary)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public Dictionary<string, List<DataDictionary>> GetAllToDictionary(string language)
        {
            Dictionary<string, List<DataDictionary>> rr = null;
            string where = " where 1=1";
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("transferToccDHMkg", "transferTodToOtherTodkg", "transferToccToTodkg");
            if (param["transferToccDHMkg"].paramValue != "开")
            {
                where += " and id not in (90)";
            }
            if (param["transferTodToOtherTodkg"].paramValue != "开")
            {
                where += " and id not in (94)";
            }
            if (param["transferToccToTodkg"].paramValue != "开")
            {
                where += " and id not in (95)";
            }
            DataTable dt = dao.GetList("select * from DataDictionary" + where);
            if (dt != null && dt.Rows.Count > 0)
            {
                rr = new Dictionary<string, List<DataDictionary>>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    DataDictionary pr = (DataDictionary)ReflectionUtil.GetModel(type, row);
                    if (pr.parentId > 0)
                    {
                        if (language == "rb")
                        {
                            pr.name = pr.namerb;
                        }
                        List<DataDictionary> list = null;
                        if (rr.ContainsKey(pr.type))
                        {
                            list = rr[pr.type];
                            list.Add(pr);
                            rr[pr.type] = list;
                        }
                        else
                        {
                            list = new List<DataDictionary>();
                            list.Add(pr);
                            rr.Add(pr.type, list);
                        }
                    }
                }
            }
            return rr;
        }

        public Dictionary<string, List<DataDictionary>> GetAllToDictionaryManage()
        {
            Dictionary<string, List<DataDictionary>> rr = null;
            string where = " where 1=1";
            DataTable dt = dao.GetList("select * from DataDictionary" + where);
            if (dt != null && dt.Rows.Count > 0)
            {
                rr = new Dictionary<string, List<DataDictionary>>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    DataDictionary pr = (DataDictionary)ReflectionUtil.GetModel(type, row);
                    if (pr.parentId > 0)
                    {
                        List<DataDictionary> list = null;
                        if (rr.ContainsKey(pr.type))
                        {
                            list = rr[pr.type];
                            list.Add(pr);
                            rr[pr.type] = list;
                        }
                        else
                        {
                            list = new List<DataDictionary>();
                            list.Add(pr);
                            rr.Add(pr.type, list);
                        }
                    }
                }
            }
            return rr;
        }
        

        public Dictionary<int, string> GetDictionaryName(params int[] ids)
        {
            if (ids == null || ids.Length == 0) { throw new ValidateException("参数为空"); }
            List<int> list = new List<int>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ids.Length; i++)
            {
                sb.Append(ids[i]).Append(",");
            }
            string val = sb.ToString().Substring(0, sb.Length - 1);
            string sql = "select id,name from DataDictionary where id in (" + val + ")";

            Dictionary<int, string> result = new Dictionary<int, string>();
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    result.Add(Convert.ToInt32(row["id"]), row["name"].ToString());
                }
            }
            return result;
        }
    }
}
