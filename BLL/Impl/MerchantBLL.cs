﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MerchantBLL : IMerchantBLL
    {

        private System.Type type = typeof(Merchant);


        public IBaseDao dao { get; set; }

        public IMemberBLL mBLL { get; set; }
        public IMemberAccountBLL accountBLL { get; set; }
        public ILiuShuiZhangBLL liuBLL { get; set; }
        public IEmailBoxBLL emailBLL { get; set; }

        public PageResult<Merchant> GetListPage(Merchant model)
        {
            PageResult<Merchant> page = new PageResult<Merchant>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from Merchant m where 1=1 ";
            string countSql = "select count(1) from Merchant m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //查询条件
            if (model != null)
            {
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.name != null)
                {
                    param.Add(new DbParameterItem("m.name", ConstUtil.LIKE, model.name));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Merchant> list = new List<Merchant>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Merchant)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public List<Merchant> GetList(string where)
        {
            string sql = "select * from Merchant " + where;
            DataTable dt = dao.GetList(sql);
            List<Merchant> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Merchant>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Merchant)ReflectionUtil.GetModel(typeof(Merchant), row));
                }
            }
            return list;
        }


        public Merchant SaveRecord(Merchant model, Member current)
        {
            if (model == null) { throw new ValidateException("提交内容为空"); }
            if (ValidateUtils.CheckNull(model.name)) { throw new ValidateException("请输入商家名称"); }
            if (ValidateUtils.CheckNull(model.linkMan)) { throw new ValidateException("请输入联系人"); }
            if (ValidateUtils.CheckNull(model.linkPhone)) { throw new ValidateException("请输入联系电话"); }
            //if (ValidateUtils.CheckNull(model.province)) { throw new ValidateException("请输入省"); }
            //if (ValidateUtils.CheckNull(model.city)) { throw new ValidateException("请输入市"); }
            //if (ValidateUtils.CheckNull(model.area)) { throw new ValidateException("请输入区"); }
            if (ValidateUtils.CheckNull(model.address)) { throw new ValidateException("请输入详细地址"); }
            if (ValidateUtils.CheckNull(model.businessScope)) { throw new ValidateException("请输入经营范围"); }

            model.uid = current.id;
            model.userId = current.userId;
            model.userName = current.userName;
            model.flag = 1;
            model.addTime = DateTime.Now;

            model.id = Convert.ToInt32(dao.SaveByIdentity(model));
            return model;
        }

        public Merchant UpdateRecord(Merchant model)
        {
            //原有记录为驳回状态则重新提交
            string sql = "select * from Merchant where id=" + model.id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("审核的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            if (vo.flag == 3)
                model.flag = 1;

            dao.Update(model);
            return model;
        }

        public int SaveAudit(int id, Member current)
        {
            string sql = "select * from Merchant where id="+id;
            DataRow row = dao.GetOne(sql);
            if(row == null){ throw new ValidateException("审核的记录不存在");}
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            vo.flag = 2;
            vo.opUid = current.id;
            vo.opUserId = current.userId;
            //发平台邮件
            EmailBox mailSend = new EmailBox();
            mailSend.title = "商家审核通过通知";
            mailSend.content = "商家申请已通过";
            mailSend.toUser = vo.userId;
            emailBLL.SaveSend(mailSend,current);
            dao.ExecuteBySql("insert into RoleMember  values(" + vo.uid + ",8)");
            return dao.Update(vo);
        }

        public int SaveRefuse(int id, string refuseReason, Member current)
        {
            string sql = "select * from Merchant where id=" + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("审核的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            vo.flag = 3;
            vo.refuseReason = refuseReason;
            vo.opUid = current.id;
            vo.opUserId = current.userId;
          
            return dao.Update(vo);
        }

        public int Delete(int id)
        {
            string sql = "select * from Merchant where id=" + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("删除的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            if (vo.flag == 2) { throw new ValidateException("已审核的记录不能删除"); }
            return dao.Delte("Merchant", id);
        }

        public Merchant GetModel(int uid)
        {
            string sql = "select * from Merchant where uid=" + uid;
            DataRow row = dao.GetOne(sql);
            if (row == null) { return null; }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            return vo;
        }


        public Merchant GetModelByPk(int id)
        {
            string sql = "select * from Merchant where id=" + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { return null; }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            return vo;
        }

        public void SaveScanPay(Merchant m, Member current, double ethTormb, double ethTopoc, double upaySxf)
        {
            if (m == null || m.uid == null) { throw new ValidateException("收款商家不存在"); }
            if (m.payAmount == null || m.payAmount <= 0) { throw new ValidateException("付款金额格式错误"); }
            Merchant old = GetModel(m.uid.Value);
            if (old == null) { throw new ValidateException("收款商家不存在"); }

            Member fk = mBLL.GetModelById(current.id.Value);
            MemberAccount fk_account = accountBLL.GetModel(current.id.Value);
            double aic = Math.Round(m.payAmount.Value * ethTopoc / ethTormb, 2);
            if (fk_account.agentGw < aic) { throw new ValidateException("POC账户余额不足"); }

            double aic_sxf = Math.Round(aic*upaySxf/100, 2);
            Member sk = mBLL.GetModelById(old.uid.Value);
            MemberAccount sk_account = accountBLL.GetModel(old.uid.Value);
          

            MemberAccount ma = new MemberAccount();
            double lsmoney = (aic - aic_sxf);
            ma.id = sk.id;
            ma.agentGw = lsmoney;
            accountBLL.UpdateAdd(ma);

            LiuShuiZhang skLiu = new LiuShuiZhang();
            skLiu.uid = sk.id;
            skLiu.userId = sk.userId;
            skLiu.accountId = ConstUtil.JOURNAL_GWB;
            skLiu.abst = "收款：" + fk.userId + "扫码支付,扣" + aic_sxf + "手续费";
            skLiu.income = lsmoney;
            skLiu.outlay = 0;
            skLiu.last = sk_account.agentGw + lsmoney;
            skLiu.addtime = DateTime.Now;
            skLiu.addUid = current.id;
            skLiu.addUser = current.userId;

            ma = new MemberAccount();
            lsmoney = aic;
            ma.id = fk.id;
            ma.agentGw = lsmoney;
            accountBLL.UpdateSub(ma);

            LiuShuiZhang fkLiu = new LiuShuiZhang();
            fkLiu.uid = fk.id;
            fkLiu.userId = fk.userId;
            fkLiu.accountId = ConstUtil.JOURNAL_GWB;
            fkLiu.abst = "扫码支付：" + old.name + "（" + old.userId + "）";
            fkLiu.income = 0;
            fkLiu.outlay = aic;
            fkLiu.last = fk_account.agentGw - aic;
            fkLiu.addtime = DateTime.Now;
            fkLiu.addUid = current.id;
            fkLiu.addUser = current.userId;

            liuBLL.Save(skLiu);
            liuBLL.Save(fkLiu);
        }
    }
}