﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class InfoplBLL : IInfoplBLL
    {
        private System.Type type = typeof(Infopl);
        public IBaseDao dao { get; set; }


        public Infopl GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Infopl where id=" + id);
            if (row == null) return null;
            Infopl model = (Infopl)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public DataTable GetList(string where)
        {
            return dao.GetList("select * from Infopl where " + where);
        }
        public List<Infopl> GetListModel(string where)
        {
            string sql = "select * from Infopl " + where;
            DataTable dt = dao.GetList(sql);
            List<Infopl> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Infopl>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Infopl)ReflectionUtil.GetModel(typeof(Infopl), row));
                }
            }
            return list;
        }

        public PageResult<Infopl> GetListPage(Infopl model, string fields)
        {
            PageResult<Infopl> page = new PageResult<Infopl>();
            string sql = "select " + fields + ",row_number() over(order by topTime desc, m.id desc) rownumber from Infopl m where 1=1 ";
            string countSql = "select count(1) from Infopl where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (model.type != null)
                {
                    param.Add(new DbParameterItem("type", ConstUtil.EQ, model.type));
                }
              
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Infopl> list = new List<Infopl>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Infopl)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Infopl model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            int c = dao.Update(model);
            return c;
        }

        public int Save(Infopl model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            int c = dao.Save(model);
            if (model.type == "dz") dao.ExecuteBySql("update Info set dzcount=dzcount+1,plcount=plcount+1 where id=" + model.infoId);
            if (model.type == "dp") dao.ExecuteBySql("update Info set dpcount=dpcount+1,plcount=plcount+1 where id=" + model.infoId);
            if (model.type == "pl") dao.ExecuteBySql("update Info set plcount=plcount+1 where id=" + model.infoId);
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Infopl", id);
        }


       

    }
}
