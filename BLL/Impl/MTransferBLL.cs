﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MTransferBLL : BaseBLL<MTransfer>, IMTransferBLL
    {

        private System.Type type = typeof(MTransfer);
        public IBaseDao dao { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IDataDictionaryBLL ddBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new MTransfer GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            MTransfer mb = (MTransfer)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new MTransfer GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            MTransfer mb = (MTransfer)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<MTransfer> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<MTransfer> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<MTransfer>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MTransfer)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<MTransfer> GetList(string sql)
        {
            List<MTransfer> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MTransfer>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MTransfer)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<MTransfer> GetListPage(MTransfer model)
        {
            PageResult<MTransfer> page = new PageResult<MTransfer>();
            string sql = "select t.*,row_number() over(order by t.id desc) rownumber from MTransfer t where 1=1 ";
            string countSql = "select count(1) from MTransfer t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.fromUserId))
                {
                    param.Add(new DbParameterItem("fromUserid", ConstUtil.LIKE, model.fromUserId));
                }
                if (!ValidateUtils.CheckNull(model.toUserId))
                {
                    param.Add(new DbParameterItem("toUserid", ConstUtil.LIKE, model.toUserId));
                }
                if (!ValidateUtils.CheckIntZero(model.fromUid))
                {
                    param.Add(new DbParameterItem("fromUid", ConstUtil.EQ, model.fromUid));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<MTransfer> list = new List<MTransfer>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MTransfer)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetListPageDataTable(MTransfer model)
        {
          
            string sql = @"select DtypeId.name as typeIdName,t.fromUserId,t.fromUserName,t.toUserId,t.toUserName,t.epoints,t.addTime from MTransfer t left join DataDictionary DtypeId
on t.typeId=DtypeId.id where 1=1 ";
          
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.fromUserId))
                {
                    param.Add(new DbParameterItem("t.fromUserid", ConstUtil.LIKE, model.fromUserId));
                }
                if (!ValidateUtils.CheckNull(model.toUserId))
                {
                    param.Add(new DbParameterItem("t.toUserid", ConstUtil.LIKE, model.toUserId));
                }
                if (!ValidateUtils.CheckIntZero(model.fromUid))
                {
                    param.Add(new DbParameterItem("t.fromUid", ConstUtil.EQ, model.fromUid));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("t.typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
           

            DataTable dt = dao.GetList(sql, param, true);

            return dt;
        }

        public int SaveMTransfer(MTransfer model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (ValidateUtils.CheckIntZero(model.typeId)) { throw new ValidateException("请选择转账类型"); }
            if (ValidateUtils.CheckDoubleZero(model.epoints)) { throw new ValidateException("请录入转账金额"); }
            //if (model.typeId.Value == ConstUtil.TRANSFER_SF_TOCC_TO_DHM) { throw new ValidateException("此类型已禁用"); }
            DateTime now = DateTime.Now;

            //转出账户为当前登录账户
            model.fromUid = current.id;
            model.fromUserId = current.userId;
            model.fromUserName = current.userName;
            model.fee = 0;

            //默认已审核
            model.flag = 2;
            model.addTime = now;
            model.auditTime = model.addTime;
            model.auditUid = current.id;
            model.auditUser = current.userId;

            //转出会员金额账户
            MemberAccount act = accountBLL.GetModel(model.fromUid.Value);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.outlay = model.epoints;
            fromLs.addtime = now;
            fromLs.uid = model.fromUid;
            fromLs.userId = model.fromUserId;
            fromLs.tableName = "MTransfer";
            fromLs.addUid = current.id;
            fromLs.addUser = current.userId;

            LiuShuiZhang toLs = new LiuShuiZhang();   //转入流水
            toLs.income = model.epoints;
            toLs.addtime = now;
            toLs.tableName = "MTransfer";
            toLs.addUid = current.id;
            toLs.addUser = current.userId;
            
            string opName = "";

            //转账参数，最小金额、倍数
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("transfeTodToDhm", "transfeToccToDhm", "transfeTodToTocc", "transferBeiTodToMoby", "transferToccToTod", "transfeToccToTodSxfbili", "transferTodToBPX");
            double transfeTodToDhm = Convert.ToDouble(param["transfeTodToDhm"].paramValue);
            double transfeToccToDhm = Convert.ToDouble(param["transfeToccToDhm"].paramValue);
            double transfeTodToTocc = Convert.ToDouble(param["transfeTodToTocc"].paramValue);
            double transferBeiTodToMoby = Convert.ToDouble(param["transferBeiTodToMoby"].paramValue);
            double transferToccToTod = Convert.ToDouble(param["transferToccToTod"].paramValue);
            double transfeToccToTodSxfbili = Convert.ToDouble(param["transfeToccToTodSxfbili"].paramValue);
            double transferTodToBPX = Convert.ToDouble(param["transferTodToBPX"].paramValue);

            //if (model.epoints.Value < transferMin) { throw new ValidateException("转账金额必须>=" + transferMin); }
            //if (model.epoints.Value % transferBei != 0) { throw new ValidateException("转账金额必须是" + transferMin + "的倍数"); }

            //	TOD币 转自身 兑换码, 1 TOD币= 1兑换码
            if (model.typeId.Value == ConstUtil.TRANSFER_SF_TOD_TO_DHM)
            {
                if (model.epoints > act.agentTod.Value) { throw new ValidateException("TOD币-余额不足"); }
                //转入账户为当前账户
                model.toUid = current.id;
                model.toUserId = current.userId;
                model.toUserName = current.userName;
                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_SF_TOD_TO_DHM))[ConstUtil.TRANSFER_SF_TOD_TO_DHM];
                fromLs.accountId = ConstUtil.JOURNAL_TOD;   //转出-TOD币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentTod.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_DHM;     //转入-兑换码
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = act.agentDhm.Value + model.epoints.Value * transfeTodToDhm;
                toLs.income = model.epoints.Value * transfeTodToDhm;

                //减少自身-TOD币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentTod = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加自身-兑换码
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentDhm = model.epoints.Value * transfeTodToDhm;
                accountBLL.UpdateAdd(add);

            }
            //	TOD币转自身TOCC币
            else if (model.typeId.Value == ConstUtil.TRANSFER_SF_TOD_TO_TOCC)
            {
                if (model.epoints > act.agentTod.Value) { throw new ValidateException("TOD币-余额不足"); }
                //转入账户为当前账户
                model.toUid = current.id;
                model.toUserId = current.userId;
                model.toUserName = current.userName;
                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_SF_TOD_TO_TOCC))[ConstUtil.TRANSFER_SF_TOD_TO_TOCC];
                fromLs.accountId = ConstUtil.JOURNAL_TOD;   //转出-TOD币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentTod.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_TOCC;     //转入-TOCC币
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = act.agentTocc.Value + model.epoints.Value * transfeTodToTocc;
                toLs.income = model.epoints.Value * transfeTodToTocc;

                //减少自身-TOD币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentTod = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加自身-TOCC币
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentTocc = model.epoints.Value * transfeTodToTocc;
                accountBLL.UpdateAdd(add);

            }
            //TOCC币转自身兑换码
            else if (model.typeId.Value == ConstUtil.TRANSFER_SF_TOCC_TO_DHM)
            {
                if (model.epoints > act.agentTocc.Value) { throw new ValidateException("TOCC币-余额不足"); }
                //转入账户为当前账户
                model.toUid = current.id;
                model.toUserId = current.userId;
                model.toUserName = current.userName;
                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_SF_TOCC_TO_DHM))[ConstUtil.TRANSFER_SF_TOCC_TO_DHM];
                fromLs.accountId = ConstUtil.JOURNAL_TOCC;   //转出-TOCC币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentTocc.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_DHM;     //转入-兑换码
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = act.agentDhm.Value + model.epoints.Value * transfeToccToDhm;
                toLs.income = model.epoints.Value * transfeToccToDhm;

                //减少自身-TOCC币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentTocc = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加自身-兑换码
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentDhm = model.epoints.Value * transfeToccToDhm;
                accountBLL.UpdateAdd(add);

            }
            //	TOD币转自身MOBY码
            if (model.typeId.Value == ConstUtil.TRANSFER_SF_TOD_TO_MOBY)
            {
                if (model.epoints > act.agentTod.Value) { throw new ValidateException("TOD币-余额不足"); }
                if (model.epoints.Value % transferBeiTodToMoby != 0) { throw new ValidateException("转账金额必须是" + transferBeiTodToMoby + "的倍数"); }
                //转入账户为当前账户
                model.toUid = current.id;
                model.toUserId = current.userId;
                model.toUserName = current.userName;
                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_SF_TOD_TO_MOBY))[ConstUtil.TRANSFER_SF_TOD_TO_MOBY];
                fromLs.accountId = ConstUtil.JOURNAL_TOD;   //转出-TOD币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentTod.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_MOBY;     //转入-MOBY码
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = act.agentMoby.Value + model.epoints.Value * transferTodToBPX;
                toLs.income = model.epoints * transferTodToBPX;

                //减少自身-TOD币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentTod = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加自身-MOBY码
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentMoby = model.epoints.Value * transferTodToBPX;
                accountBLL.UpdateAdd(add);

            }
            //兑换码转他人兑换码
            else if (model.typeId.Value == ConstUtil.TRANSFER_OT_DHM_TO_DHM)
            {
                //转入账户为model.toUserId
                if (model.epoints > act.agentDhm.Value) { throw new ValidateException("兑换码-余额不足"); }
                if (ValidateUtils.CheckNull(model.toUserId)) { throw new ValidateException("转入账户不能为空"); }
                Member mb = memberBLL.GetModelByUserId(model.toUserId);
                if (mb == null) { throw new ValidateException("转入账户不存在"); }
                if (mb.id.Value == model.fromUid.Value) { throw new ValidateException("不能转给自身"); }
                model.toUid = mb.id;
                model.toUserId = mb.userId;
                model.toUserName = mb.userName;
                //转入会员账户
                MemberAccount inact = accountBLL.GetModel(mb.id.Value);

                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_OT_DHM_TO_DHM))[ConstUtil.TRANSFER_OT_DHM_TO_DHM];
                fromLs.accountId = ConstUtil.JOURNAL_DHM;   //转出-兑换码
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentDhm.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_DHM;     //转入-兑换码
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = inact.agentDhm.Value + model.epoints.Value;

                //减少自身-兑换码
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentDhm = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加转入会员-兑换码
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentDhm = model.epoints;
                accountBLL.UpdateAdd(add);
            }
            //TOD币转他人TOD币
            else if (model.typeId.Value == ConstUtil.TRANSFER_OT_TOD_TO_TOD)
            {
                //转入账户为model.toUserId
                if (model.epoints > act.agentTod.Value) { throw new ValidateException("TOD币-余额不足"); }
                if (ValidateUtils.CheckNull(model.toUserId)) { throw new ValidateException("转入账户不能为空"); }
                Member mb = memberBLL.GetModelByUserId(model.toUserId);
                if (mb == null) { throw new ValidateException("转入账户不存在"); }
                if (mb.id.Value == model.fromUid.Value) { throw new ValidateException("不能转给自身"); }
                model.toUid = mb.id;
                model.toUserId = mb.userId;
                model.toUserName = mb.userName;
                //转入会员账户
                MemberAccount inact = accountBLL.GetModel(mb.id.Value);

                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_OT_TOD_TO_TOD))[ConstUtil.TRANSFER_OT_TOD_TO_TOD];
                fromLs.accountId = ConstUtil.JOURNAL_TOD;   //转出-TOD币
                fromLs.abst = opName + ",转给" + model.toUserId + "扣除";
                fromLs.last = act.agentTod.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_TOD;     //转入-TOD币
                toLs.abst = opName + ",由" + model.fromUserId + "转入增加";
                toLs.last = inact.agentTod.Value + model.epoints.Value;

                //减少自身-TOD币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentTod = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加转入会员-TOD币
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentTod = model.epoints;
                accountBLL.UpdateAdd(add);
            }
            //	TOCC 币 转自身 TOD币
            else if (model.typeId.Value == ConstUtil.TRANSFER_SF_TOCC_TO_TOD)
            {
                if (model.epoints > act.agentTocc.Value) { throw new ValidateException("TOCC币-余额不足"); }
                //转入账户为当前账户
                model.toUid = current.id;
                model.toUserId = current.userId;
                model.toUserName = current.userName;
                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_SF_TOCC_TO_TOD))[ConstUtil.TRANSFER_SF_TOCC_TO_TOD];
                fromLs.accountId = ConstUtil.JOURNAL_TOCC;   //转出-TOCC币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.outlay = model.epoints.Value * (100 + transfeToccToTodSxfbili) / 100;
                fromLs.last = act.agentTocc.Value - (model.epoints.Value*(100+ transfeToccToTodSxfbili)/100);

                toLs.accountId = ConstUtil.JOURNAL_TOD;     //转入-TOD币
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.income = model.epoints.Value * transferToccToTod;
                toLs.last = act.agentTod.Value + model.epoints.Value* transferToccToTod;

                //减少自身-TOCC币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentTocc = model.epoints.Value * (100 + transfeToccToTodSxfbili) / 100;
                accountBLL.UpdateSub(sub);

                //增加自身-TOD币
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentTod = model.epoints.Value* transferToccToTod;
                accountBLL.UpdateAdd(add);
            }

            //保存转账记录
            object o = this.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);

            //流水补充
            fromLs.sourceId = newId;
            toLs.sourceId = newId;
            toLs.uid = model.toUid;
            toLs.userId = model.toUserId;
            //保存流水
            liushuiBLL.Save(fromLs);
            liushuiBLL.Save(toLs);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "转账：" + opName + ",转出会员：" + model.fromUserId + "，转入会员：" + model.toUserId;
            log.tableName = "MTransfer";
            log.recordName = "会员转账";
            this.SaveOperateLog(log);

            //是否需要短信通知
            MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_TRANSFER);
            if (bf.flag == 1)
            {
                //发送给会员短信
                string messge = bf.msg + "" + model.epoints.ToString();
                noticeBLL.SendMessage(current.phone, messge);
            }
            return newId;
        }

        public int WK(string WKLX, Member current)
        {

            if (dao.GetList("select  * from Charge where czType='sl' and uid=" + current.id + " and jsTime>getdate()").Rows.Count > 0)
            {

                Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("WKDzTomy", "WKmyToTocc", "DtljdzBZ1", "DtljdzBZ2", "DtljdzBZ3", "DtljdzBZ4", "WKxxBZ1", "WKsxBZ1", "WKxxBZ2", "WKsxBZ2", "WKxxBZ3", "WKsxBZ3", "WKxxBZ4", "WKsxBZ4", "WKmtcsDlpt", "WKmtcsFbwz", "WKmtcsPl", "WKmtcsSp", "WKmtcsGg");
                double WKDzTomy = Convert.ToDouble(param["WKDzTomy"].paramValue);
                double WKmyToTocc = Convert.ToDouble(param["WKmyToTocc"].paramValue);
                double DtljdzBZ1 = Convert.ToDouble(param["DtljdzBZ1"].paramValue);
                double DtljdzBZ2 = Convert.ToDouble(param["DtljdzBZ2"].paramValue);
                double DtljdzBZ3 = Convert.ToDouble(param["DtljdzBZ3"].paramValue);
                double DtljdzBZ4 = Convert.ToDouble(param["DtljdzBZ4"].paramValue);
                double WKxxBZ1 = Convert.ToDouble(param["WKxxBZ1"].paramValue);
                double WKsxBZ1 = Convert.ToDouble(param["WKsxBZ1"].paramValue);
                double WKxxBZ2 = Convert.ToDouble(param["WKxxBZ2"].paramValue);
                double WKsxBZ2 = Convert.ToDouble(param["WKsxBZ2"].paramValue);
                double WKxxBZ3 = Convert.ToDouble(param["WKxxBZ3"].paramValue);
                double WKsxBZ3 = Convert.ToDouble(param["WKsxBZ3"].paramValue);
                double WKxxBZ4 = Convert.ToDouble(param["WKxxBZ4"].paramValue);
                double WKsxBZ4 = Convert.ToDouble(param["WKsxBZ4"].paramValue);

                int WKmtcsDlpt = Convert.ToInt16(param["WKmtcsDlpt"].paramValue);
                int WKmtcsFbwz = Convert.ToInt16(param["WKmtcsFbwz"].paramValue);
                int WKmtcsPl = Convert.ToInt16(param["WKmtcsPl"].paramValue);
                int WKmtcsSp = Convert.ToInt16(param["WKmtcsSp"].paramValue);
                int WKmtcsGg = Convert.ToInt16(param["WKmtcsGg"].paramValue);
                double sumDz = Convert.ToDouble(dao.GetList("select isnull(SUM(income),0) as sumDz from LiuShuiZhang where uid=" + current.id + " and accountId=86 and  DATEDIFF(day,addTime,GETDATE())=0 and tableName like '%WK%'").Rows[0]["sumDz"].ToString());
                int countDz = Convert.ToInt16(dao.GetList("select COUNT(1) as countDz from LiuShuiZhang where uid=" + current.id + " and accountId=86 and  DATEDIFF(day,addTime,GETDATE())=0 and tableName like '%" + WKLX + "%'").Rows[0]["countDz"].ToString());
                double WKxx = 0;//下限
                double WKsx = 0;//上限
                if (((WKLX == "WKDlpt" && countDz < WKmtcsDlpt) || (WKLX == "WKFbwz" && countDz < WKmtcsFbwz) || (WKLX == "WKPl" && countDz < WKmtcsPl) || (WKLX == "WKSp" && countDz < WKmtcsSp) || (WKLX == "WKGg" && countDz < WKmtcsGg)) && current.uLevel == 4)
                {
                    if (sumDz >= DtljdzBZ1)
                    {
                        WKxx = WKxxBZ1;
                        WKsx = WKsxBZ1;
                    }
                    if (sumDz >= DtljdzBZ2)
                    {
                        WKxx = WKxxBZ2;
                        WKsx = WKsxBZ2;
                    }
                    if (sumDz >= DtljdzBZ3)
                    {
                        WKxx = WKxxBZ3;
                        WKsx = WKsxBZ3;
                    }
                    if (sumDz >= DtljdzBZ4)
                    {
                        WKxx = WKxxBZ4;
                        WKsx = WKsxBZ4;
                    }
                    double getDz = WKxx + (WKsx - WKxx) * (DateTime.Now.Second / Convert.ToDouble(60));//上下限范围随机生成
                    LiuShuiZhang toLs = new LiuShuiZhang();   //转入流水
                    toLs.uid = current.id;
                    toLs.userId = current.userId;
                    toLs.income = getDz;
                    toLs.outlay = 0;
                    toLs.addtime = DateTime.Now;
                    toLs.tableName = WKLX;
                    toLs.addUid = current.id;
                    toLs.addUser = current.userId;
                    toLs.accountId = ConstUtil.JOURNAL_DZ;     //转入-点值
                    toLs.abst = "挖矿增加";
                    toLs.last = 0;//将自动转成TOCC
                    liushuiBLL.Save(toLs);
                    MemberAccount inact = accountBLL.GetModel(current.id.Value);
                    //MemberAccount add = new MemberAccount();
                    //add.id = current.id;
                    //add.agentTocc = getDz * WKDzTomy * WKmyToTocc;
                    //accountBLL.UpdateAdd(add);

                    //LiuShuiZhang toToccLs = new LiuShuiZhang();   //转入流水
                    //toToccLs.uid = current.id;
                    //toToccLs.userId = current.userId;
                    //toToccLs.income = getDz * WKDzTomy * WKmyToTocc;
                    //toToccLs.outlay = 0;
                    //toToccLs.addtime = DateTime.Now;
                    //toToccLs.tableName = WKLX;
                    //toToccLs.addUid = current.id;
                    //toToccLs.addUser = current.userId;
                    //toToccLs.accountId = ConstUtil.JOURNAL_TOCC;     //转入-TOCC
                    //toToccLs.abst = "挖矿增加";
                    //toToccLs.last = inact.agentTocc.Value + getDz * WKDzTomy * WKmyToTocc;//将自动转成TOCC
                    //liushuiBLL.Save(toToccLs);
                }
                return 1;
            }
            else
                return 0;
            
        }


        public DataTable GetTransferExcel()
        {
            string sql = "select d.name as typeId,t.fromUserId,t.fromUserName,t.toUserId,t.toUserName,t.epoints,t.addTime from MTransfer t left join DataDictionary d on d.id=t.typeId where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

    }
}
