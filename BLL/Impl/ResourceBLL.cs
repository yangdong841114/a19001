﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ResourceBLL : BaseBLL<Resource>, IResourceBLL
    {
        private System.Type type = typeof(Resource);
        public IBaseDao dao { get; set; }


        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Resource GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            Resource model = (Resource)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public new Resource GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Resource model = (Resource)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public new List<Resource> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Resource> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Resource>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Resource)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Resource> GetList(string sql)
        {
            List<Resource> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Resource>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Resource)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public List<Resource> GetRoleResource(int roleId)
        {
            if (ValidateUtils.CheckIntZero(roleId)) { throw new ValidateException("角色不能为空"); }
            string sql = "select resourceId id from RoleResource where roleId = " + roleId;
            //string sql = "select  id from RoleResource where roleId = " + roleId;
            List<Resource> list = this.GetList(sql);
            return list;
        }

        public List<Resource> GetListByUser(int uid)
        {
            string sql = "select * from resource r where exists(select resourceId from " +
                        " (select distinct rr.resourceId from RoleMember rm inner join Member m on rm.memberId = m.id inner join RoleResource rr on " +
                        " rm.roleId = rr.roleId where m.id = @uid) t where t.resourceId = r.id) order by r.sortno asc";
            //添加查询条件
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("uid", null, uid)).Result();

            List<Resource> list = null;
            DataTable dt = dao.GetList(sql, param, false);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Resource>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Resource)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public List<Resource> GetListByUserAndParent(int uid, string parentId)
        {
            //string sql = "select * from resource r where exists(select resourceId from " +
            //            " (select distinct rr.resourceId from RoleMember rm inner join Member m on rm.memberId = m.id inner join RoleResource rr on " +
            //            " rm.roleId = rr.roleId where m.id = @uid) t where t.resourceId = r.id and t.resourceId<>@parentId and t.resourceId like @parentId + '%') order by r.sortno asc";
            string sql = "select * from resource r where exists(select resourceId from " +
                        " (select distinct rr.resourceId from RoleMember rm inner join Member m on rm.memberId = m.id inner join RoleResource rr on " +
                        " rm.roleId = rr.roleId where m.id = @uid) t where t.resourceId = r.id and t.resourceId<>@parentId) order by r.sortno asc";
            //添加查询条件
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("uid", null, uid))
                .Add(new DbParameterItem("parentId", null, parentId))
                .Result();

            List<Resource> list = null;
            DataTable dt = dao.GetList(sql, param, false);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Resource>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Resource e_Resource = (Resource)ReflectionUtil.GetModel(type, row);
                    e_Resource.resourceName = e_Resource.resourceNameFt;
                    list.Add(e_Resource);
                }
            }
            return list;
        }

        public List<Resource> GetConstantParentListByUserAndParent(int uid, string parentId)
        {
            string sql = "select * from resource r where exists(select resourceId from " +
                        " (select distinct rr.resourceId from RoleMember rm inner join Member m on rm.memberId = m.id inner join RoleResource rr on " +
                        " rm.roleId = rr.roleId where m.id = @uid) t where t.resourceId = r.id  and t.resourceId like @parentId + '%') order by r.sortno asc";
            //添加查询条件
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("uid", null, uid))
                .Add(new DbParameterItem("parentId", null, parentId))
                .Result();

            List<Resource> list = null;
            DataTable dt = dao.GetList(sql, param, false);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Resource>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Resource e_Resource = (Resource)ReflectionUtil.GetModel(type, row);
                    e_Resource.resourceName = e_Resource.resourceNameFt;
                    list.Add(e_Resource);
                }
            }
            return list;
        }

        public List<Resource> GetListByPartentId(int uid, string parentId)
        {
            string sql = "select * from resource r where exists(select resourceId from " +
                        " (select distinct rr.resourceId from RoleMember rm inner join Member m on rm.memberId = m.id inner join RoleResource rr on " +
                        " rm.roleId = rr.roleId where m.id = @uid) t where t.resourceId = r.id  and r.parentResourceId = @parentId) order by r.sortno asc";
            //添加查询条件
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("uid", null, uid))
                .Add(new DbParameterItem("parentId", null, parentId))
                .Result();

            List<Resource> list = null;
            DataTable dt = dao.GetList(sql, param, false);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Resource>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Resource e_Resource = (Resource)ReflectionUtil.GetModel(type, row);
                    e_Resource.resourceName = e_Resource.resourceNameFt;
                    list.Add(e_Resource);
                }
            }
            return list;
        }

        public Resource SaveResource(Resource model)
        {
            //生成ID,ID规则：上级ID+两位数字，查询时可不走递归，like 上级ID+后半部分模糊匹配
            if (model.parentResourceId == "无")
            {
                model.parentResourceId = "0";
                //生成根节点ID
                string sql = "select MAX(id) from Resource where parentResourceId = '0' or parentResourceId = '无'";
                object o = dao.ExecuteScalar(sql);
                int maxId = o == null ? 99 : Convert.ToInt32(o);
                model.id = (maxId + 1) + "";
            }
            else
            {
                string parentId = model.parentResourceId;
                string sql = "select MAX(id) from Resource where parentResourceId = '" + model.parentResourceId + "'"; ;
                object o = dao.ExecuteScalar(sql);
                if (o == null) { model.id = parentId + "01"; }
                else
                {
                    model.id = (Convert.ToInt32(o) + 1) + "";
                }
            }
            dao.Save(model);
            return model;
        }

        public Resource UpdateResource(Resource model)
        {
            dao.Update(model);
            return model;
        }

        public string DelteResource(string id)
        {
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.LIKE_ED, id)).Result();
            //检查节点是否存在
            if (ValidateUtils.CheckNull(id)) { return "删除的节点不能为空"; }
            string sql = "select count(1) from Resource where id like @id+'%'";
            int exists = dao.GetCount(sql, param,false);
            if (exists > 0)
            {
                //检查节点及下级节点是否有引用
                sql = "select COUNT(1) from RoleResource r where r.resourceId in (select id from Resource where id like @id + '%')";
                int count = dao.GetCount(sql, param, false);
                if (count > 0)
                {
                    return "该节点或下级节点已被角色使用，不能删除";
                }
                else
                {
                    sql = "delete from Resource where id like @id+'%'";
                    int c = dao.ExecuteBySql(sql, param);
                    return "success";
                }
            }
            else
            {
                return "删除失败，该节点不存在";
            }
        }
    }
}
