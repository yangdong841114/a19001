﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;

namespace BLL.Impl
{
    public class DelegaMemberBLL : IDelegaMemberBLL
    {

        private System.Type type = typeof(DelegaMember);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public PageResult<DelegaMember> GetListPage(DelegaMember model)
        {
            PageResult<DelegaMember> page = new PageResult<DelegaMember>();
            string sql = "select m.*,b.agentDz,b.agentJj,b.agentGw,b.agentFt,row_number() over(order by m.id desc) rownumber from DelegaMember m "+
                         " inner join MemberAccount b on m.delegaId = b.id where 1=1 ";
            string countSql = "select count(1) from DelegaMember m inner join MemberAccount b on m.delegaId = b.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            if (model == null || ValidateUtils.CheckIntZero(model.mainId)) { throw new ValidateException("主账户不能为空"); }
            
            //查询条件
            if (!ValidateUtils.CheckIntZero(model.mainId))
            {
                param.Add(new DbParameterItem("m.mainId", ConstUtil.EQ, model.mainId));
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<DelegaMember> list = new List<DelegaMember>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((DelegaMember)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int SaveDelegaMember(DelegaMember model)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (ValidateUtils.CheckIntZero(model.mainId)) { throw new ValidateException("主账户不能为空"); }
            if (ValidateUtils.CheckNull(model.delegaUserId)) { throw new ValidateException("托管账户不能为空"); }
            if (ValidateUtils.CheckNull(model.password)) { throw new ValidateException("交易密码不能为空"); }
            //被托管的账户
            Member delega = memberBLL.GetModelByUserId(model.delegaUserId);
            if (delega == null) { throw new ValidateException("托管账户不存在"); }
            if (delega.id.Value == model.mainId.Value) { throw new ValidateException("不能托管自己"); }
            string sql = "select count(1) from DelegaMember where delegaId = " + delega.id;
            int count = dao.GetCount(sql);
            if (count > 0) { throw new ValidateException("该账户已被托管"); }

            model.password = DESEncrypt.EncryptDES(model.password, ConstUtil.SALT);
            if (model.password != delega.threepass) { throw new ValidateException("交易密码错误"); }
            model.password = null;
            model.delegaId = delega.id;
            model.addTime = DateTime.Now;
            dao.Save(model);

            //发送给会员短信
            string messge = "您的账户："+delega.userId+"，已托管至主账号："+model.mainUserId+"，如非本人操作，请及时与管理员联系，以免造成不必要的损失！";
            noticeBLL.SendMessage(delega.phone,messge);
            return 1;
        }

        public int Delete(int id)
        {
            string sql = "delete from DelegaMember where id= " + id;
            return dao.ExecuteBySql(sql);
        }


        public string SaveTransfer(int uid)
        {
            //调用存储过程
            string pro = "childToMain";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@uid",SqlDbType.Int,10),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = uid;
            p[1].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result != "success")
            {
                throw new ValidateException(result);
            }
            return result;
        }

    }
}
