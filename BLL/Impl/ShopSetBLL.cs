﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ShopSetBLL : BaseBLL<ShopSet>, IShopSetBLL
    {

        private System.Type type = typeof(ShopSet);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new ShopSet GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            ShopSet mb = (ShopSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ShopSet GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ShopSet mb = (ShopSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<ShopSet> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<ShopSet> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<ShopSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ShopSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<ShopSet> GetList(string sql)
        {
            List<ShopSet> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ShopSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ShopSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public ShopSet SaveShopSet(ShopSet model, Member current)
        {
            string err = null;
            //校验start----------------------------------
            //非空校验
            if (model == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(model.userId)) { err = "会员编码不能为空"; }
            if (err != null) { throw new ValidateException(err); }

            //修改的会员
            Member m = memberBLL.GetModelByUserId(model.userId);
            if (m == null) { throw new ValidateException("该会员不存在"); }
            if (m.isAgent == 2) { throw new ValidateException("该会员已是报单中心"); }
            
            //校验end----------------------------------

            //开通会员为报单中心
            string sql = "update member set isAgent=2,agentIslock=0,agentName=userId,applyAgentTime=getDate(),"+
                         "openAgentTime=getDate(),regAgentmoney=0,agentOpUser='" + current.userId + "' where id=" + m.id; ;
            dao.ExecuteBySql(sql);

            //保存记录
            int oldReId = m.reId.Value;
            model.addTime = DateTime.Now;
            model.createId = current.id;
            model.createUser = current.userId;
            model.uid = m.id;
            model.userName = m.userName;
            object o = this.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);
            model.id = newId;

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "设置为报单：" + model.userId;
            log.tableName = "ShopSet";
            log.recordName = "设置报单中心";
            this.SaveOperateLog(log);

            if (newId <= 0)
            {
                throw new ValidateException("设置失败，请联系管理员");
            }

            return model;
        }


        public int OpenAgent(int memberId, Member current)
        {
            string err = null;
            //校验start----------------------------------
            //非空校验
            if (ValidateUtils.CheckIntZero(memberId)) { err = "要开通会员不能为空"; }
            if (err != null) { throw new ValidateException(err); }

            //会员
            Member m = memberBLL.GetModelById(memberId);
            if (m == null) { throw new ValidateException("该会员不存在"); }
            if (m.isAgent != 1) { throw new ValidateException("该会员不是待开通状态"); }
            if (m.isAgent == 2) { throw new ValidateException("该会员已是报单中心"); }
            //校验end----------------------------------

            //开通会员为报单中心
            string sql = "update member set isAgent=2,agentIslock=0,agentName=userId," +
                         "openAgentTime=getDate(),agentOpUser='" + current.userId + "' where id=" + m.id;
            dao.ExecuteBySql(sql);

            //记录流水账
            MemberAccount oldAct = accountBLL.GetOne("select * from MemberAccount where id=" + m.id);
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = ConstUtil.JOURNAL_DZB;
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "开通报单中心返还点值";
            liu.income = m.regAgentmoney;
            liu.outlay = 0;
            liu.last = oldAct.agentDz + m.regAgentmoney;
            liu.addtime = DateTime.Now;
            liu.sourceId = m.id;
            liu.tableName = "Member";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);

            //返还点值
            MemberAccount account = new MemberAccount();
            account.id = m.id;
            account.agentDz = m.regAgentmoney;
            accountBLL.UpdateAdd(account);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = m.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "开通报单中心：" + m.userId;
            log.tableName = "ShopSet";
            log.recordName = "开通报单中心";
            this.SaveOperateLog(log);

            return 1;
        }

        public int DeleteAgent(int memberId)
        {
            string err = null;
            if (ValidateUtils.CheckIntZero(memberId)) { err = "要删除的会员不能为空"; }
            if (err != null) { throw new ValidateException(err); }

            //会员
            Member m = memberBLL.GetModelById(memberId);
            if (m == null) { throw new ValidateException("该会员不存在"); }
            if (m.isAgent != 1) { throw new ValidateException("该会员不是待开通状态,不能删除"); }

            //删除待开通报单中心
            string sql = "update member set isAgent=0,agentIslock=0,agentName='',applyAgentTime=null,regAgentmoney=0 " +
                         " where id=" + m.id; ;
            return dao.ExecuteBySql(sql);
        }


        public int UpdateLock(List<int> list, int agentIslock)
        {
            if (list == null || list.Count == 0)
            {
                throw new ValidateException("要更新的内容为空");
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.Append(list[i]).Append(",");
            }
            string sql = "update member set agentIslock = @agentIslock where id in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("agentIslock", null, agentIslock))
                           .Result();
            return dao.ExecuteBySql(sql, param);

        }

        public PageResult<ShopSet> GetListPage(ShopSet model)
        {
            PageResult<ShopSet> page = new PageResult<ShopSet>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from ShopSet m where 1=1 ";
            string countSql = "select count(1) from ShopSet where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<ShopSet> list = new List<ShopSet>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ShopSet)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int InsertApplyShop(int uid)
        {
            Member mm = memberBLL.GetModelById(uid);
            if (mm == null) { throw new ValidateException("会员不存在"); }
            else if (mm.isAgent == 1) { throw new ValidateException("已经在待审核中，请勿重复申请"); }
            else if (mm.isAgent == 2) { throw new ValidateException("您已经是报单中心"); }

            //报单中心申请所需金额
            List<string> codes = new List<string>();
            codes.Add("shopPrice");
            Dictionary<string, ParameterSet> di = paramBLL.GetToDictionary(codes);
            double regAgentmoney = Convert.ToDouble(di["shopPrice"].paramValue);
            string sql = "update Member set isAgent=1,agentIslock=0,agentName=userId,applyAgentTime=getDate(),regAgentmoney="+regAgentmoney+" where id=" + uid;
            int c = dao.ExecuteBySql(sql);
            if (c > 0)
            {
                //发送短信
                MobileNotice nt = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_ADD_SHOP);
                if (nt.flag == 1)
                {
                    //发送给会员短信
                    noticeBLL.SendMessage(nt.phone, nt.msg);
                }
            }
            return c;
        }

    }
}
