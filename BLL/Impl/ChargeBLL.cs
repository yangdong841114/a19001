﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ChargeBLL : BaseBLL<Charge>, IChargeBLL
    {

        private System.Type type = typeof(Charge);
        public IBaseDao dao { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public ISystemBankBLL bankBLL { get; set; }
        public IParamSetBLL paramBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Charge GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            Charge mb = (Charge)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new Charge GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Charge mb = (Charge)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<Charge> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Charge> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<Charge>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Charge)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Charge> GetList(string sql)
        {
            List<Charge> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Charge>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Charge)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Charge> GetListPage(Charge model)
        {
            PageResult<Charge> page = new PageResult<Charge>();
            string sql = "select t.*,m.userName,row_number() over(order by t.id desc) rownumber from Charge t left join Member m on t.uid = m.id where 1=1 ";
            string countSql = "select count(1) from Charge t left join Member m on t.uid = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.ispay))
                {
                    param.Add(new DbParameterItem("t.ispay", ConstUtil.EQ, model.ispay));
                }
                if (!ValidateUtils.CheckNull(model.czType))
                {
                    param.Add(new DbParameterItem("t.czType", ConstUtil.LIKE, model.czType));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (model.startjsTime != null)
                {
                    param.Add(new DbParameterItem("t.jsTime", ConstUtil.DATESRT_LGT_DAY, model.startjsTime));
                }
                if (model.endjsTime != null)
                {
                    param.Add(new DbParameterItem("t.jsTime", ConstUtil.DATESRT_EGT_DAY, model.endjsTime));
                }
                
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Charge> list = new List<Charge>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Charge)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double all_epointsPay=0;
            double all_epointsNotpay=0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Charge model_row=(Charge)ReflectionUtil.GetModel(type, row);
                    if(model_row.ispay == 1)
                    all_epointsNotpay += model_row.epoints.Value;
                    if(model_row.ispay == 2)
                    all_epointsPay += model_row.epoints.Value;
                }
            }
            Charge foot_model = new Charge();
            list = new List<Charge>();
            foot_model.epointsNotpay = all_epointsNotpay;
            foot_model.epointsPay = all_epointsPay;
            list.Add(foot_model);
            page.footer = list;
            return page;
        }

        public DataTable GetExcelListPage(Charge model)
        {

            string sql = "select case when t.typeId=1 then '注册分' else '申购分' end typeId,m.userId,m.userName,t.fromBank,t.epoints,t.bankTime,t.toBank,t.bankCard,t.bankUser," +
                        "t.addTime,case when t.ispay=1 then '待审核' else '已通过' end status from Charge t left join Member m on t.uid = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("t.typeId", ConstUtil.EQ, model.typeId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.ispay))
                {
                    param.Add(new DbParameterItem("t.ispay", ConstUtil.EQ, model.ispay));
                }
                if (!ValidateUtils.CheckNull(model.czType))
                {
                    param.Add(new DbParameterItem("t.czType", ConstUtil.LIKE, model.czType));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public DataTable GetChargeExcel()
        {
            string sql = "select m.userId,m.userName,t.fromBank,t.epoints,t.bankTime,t.toBank,t.bankCard,t.bankUser," +
                        "t.addTime,case when t.ispay=1 then '待审核' else '已通过' end status from Charge t left join Member m on t.uid = m.id where t.czType='cz' ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public int SaveCharge(Charge model, Member current)
        {
            //是否首次购买算力
            string isScgmsl = "no";
            if (model == null) { throw new ValidateException("保存内容为空"); }

            if (model.czType=="cz")
            { 
            if (ValidateUtils.CheckIntZero(model.sysBankId)) { throw new ValidateException("汇入银行不能为空"); }
            if (ValidateUtils.CheckNull(model.fromBank)) { throw new ValidateException("汇出银行不能为空"); }
            if (model.bankTime == null) { throw new ValidateException("汇款时间不能为空"); }
            if (ValidateUtils.CheckDoubleZero(model.epoints)) { throw new ValidateException("请录入汇款金额"); }
            }
            MemberAccount act = accountBLL.GetModel(current.id.Value);//帐户
            if (model.czType == "sl")
            {
                if (model.rgys == null || model.rgys <= 0) { throw new ValidateException("认购月数格式不正确"); }
               
                //确定开始结束时间
                Charge yxrq = GetOne("select top 1 * from Charge where czType='sl' and uid=" + current.id + " order by jsTime desc");
                if (yxrq == null) isScgmsl = "yes";
                DateTime ksTime = DateTime.Now;
                model.lxmonth = model.rgys.Value;
                if(yxrq != null)
                {
                if (yxrq.jsTime >= DateTime.Now)
                {
                ksTime = yxrq.jsTime.Value;
                model.lxmonth += yxrq.lxmonth;//累加连续月
                }
                }
                DateTime jsTime = ksTime.AddMonths(model.rgys.Value);
                model.ksTime = ksTime;
                model.jsTime = jsTime;

                //连续月数有多少


                Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("upUlevel1Yc", "upUlevel1Fpv", "upUlevel1Yslf", "upUlevel1Yfpv");
                double upUlevel1Yc = Convert.ToDouble(param["upUlevel1Yc"].paramValue);
                double upUlevel1Fpv = Convert.ToDouble(param["upUlevel1Fpv"].paramValue);
                double upUlevel1Yslf = Convert.ToDouble(param["upUlevel1Yslf"].paramValue);
                double upUlevel1Yfpv = Convert.ToDouble(param["upUlevel1Yfpv"].paramValue);

                double xhdhm = 0;
                double fpv=0;
                //次认购算力还要加上200美元的认购矿机的费用
                if (yxrq == null) {xhdhm += upUlevel1Yc;fpv+=upUlevel1Fpv;}
                xhdhm += upUlevel1Yslf * model.rgys.Value;
                fpv +=upUlevel1Yfpv * model.rgys.Value;
                model.fpv = fpv;
                model.xhdhm = xhdhm;
                model.epoints = xhdhm;
                if (act.agentDhm < xhdhm) { throw new ValidateException("兑换码余额不足"); }
            }
            DateTime now = DateTime.Now;
            model.uid = current.id;
            model.userId = current.userId;
            model.addTime = now;
            model.ispay = 1;
            model.typeId = ConstUtil.CHARGE_BANK; //银行汇款
            if (model.czType=="cz")
            { 
            SystemBank bank = bankBLL.GetModel(model.sysBankId.Value);
            if (bank == null) { throw new ValidateException("汇入银行不能为空"); }
            model.toBank = bank.bankName;
            model.bankCard = bank.bankCard;
            model.bankUser = bank.bankUser;
            }
            model.sysBankId = null;
            object o = dao.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);

            //扣兑换码写流水
            if (model.czType == "sl")
            {
                //减少自身-兑换码
                MemberAccount sub = new MemberAccount();
                sub.id = model.uid;
                sub.agentDhm = model.xhdhm;
                accountBLL.UpdateSub(sub);
                //流水帐
                LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
                fromLs.outlay = model.xhdhm;
                fromLs.addtime = now;
                fromLs.uid = model.uid;
                fromLs.userId = model.userId;
                fromLs.tableName = "Charge";
                fromLs.addUid = current.id;
                fromLs.addUser = current.userId;
                fromLs.accountId = ConstUtil.JOURNAL_DHM; //兑换码  
                fromLs.abst = "认购算力扣除";
                fromLs.last = act.agentDhm.Value - model.xhdhm.Value;
                liushuiBLL.Save(fromLs);
            }
            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            if (model.czType == "cz")
            { 
            log.mulx = "充值记录(id=" + newId + "，会员=" + model.userId + ") 申请充值";
            log.tableName = "Charge";
            log.recordName = "会员充值";
            }
            if (model.czType == "sl")
            {
                log.mulx = "算力认购记录(id=" + newId + "，会员=" + model.userId + ") ";
                log.tableName = "Charge";
                log.recordName = "算力认购";
            }
            this.SaveOperateLog(log);

             if (model.czType == "sl")
            {
            //更新自身PV值和上线PV值
                dao.ExecuteBySql("update Member  set uLevel=4, pv=pv+" + model.fpv + " where id=" + model.uid);//更新自身PV值同时成为正式会员

                

            dao.ExecuteBySql("update Member set xxpv=xxpv+" + model.fpv + " where id in (0" + current.pPath + "0)");//上线PV值
            dao.ExecuteBySql("exec calYeji " + model.uid + "," + model.fpv);//系谱图PV值同时会运行循环对碰奖
            //直推奖 首次购买算力才算
            if (isScgmsl == "yes")
            {
                dao.ExecuteBySql("exec ztjBonus " + model.uid + "," + model.fpv);
                dao.ExecuteBySql("exec updateDirectPush " + model.uid);//执行更新左右半区人数
            }
            }
            //是否需要短信通知
            MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_CHONG_ZHI);
            if (bf.flag == 1)
            {
                //发送给管理员短信
                noticeBLL.SendMessage(bf.phone, bf.msg);
            }
            return newId;
        }

        public int SavePayCharge(Charge model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (model.bankTime == null) { throw new ValidateException("支付时间不能为空"); }
            if (ValidateUtils.CheckDoubleZero(model.epoints)) { throw new ValidateException("请录入充值金额"); }
            DateTime now = DateTime.Now;
            model.ispay = 2;


            object o = dao.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);

            //充值会员
            Member m = memberBLL.GetModelAndAccountNoPass(model.uid.Value);

            //增加会员点值
            MemberAccount ma = new MemberAccount();
            ma.id = m.id;
            if (model.accounttypeId == ConstUtil.JOURNAL_DHM)
            ma.agentDhm = model.epoints;
            if(model.accounttypeId == ConstUtil.JOURNAL_TOD)
            ma.agentTod = model.epoints;
            accountBLL.UpdateAdd(ma);

            //记录流水帐
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = model.accounttypeId;  //点值
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "会员充值增加";
            liu.outlay = 0;
            liu.income = model.epoints;
            if(model.accounttypeId == ConstUtil.JOURNAL_DHM)
            liu.last = m.account.agentDhm.Value + model.epoints.Value;
            if (model.accounttypeId == ConstUtil.JOURNAL_TOD)
            liu.last = m.account.agentTod.Value + model.epoints.Value;
            liu.addtime = DateTime.Now;
            liu.sourceId = model.id;
            liu.tableName = "Charge";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "充值记录(id=" + newId + "，会员=" + model.userId + "，订单号=" + model.bankCard + ") 支付宝充值";
            log.tableName = "Charge";
            log.recordName = "会员充值";
            this.SaveOperateLog(log);

            //是否需要短信通知
            MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_CHONG_ZHI);
            if (bf.flag == 1)
            {
                //发送给管理员短信
                noticeBLL.SendMessage(bf.phone, bf.msg);
            }
            return newId;
        }

        public int UpdateCancel(int id, Member current)
        {
            Charge ca = this.GetOne("select * from Charge where id=" + id);
            if (ca == null) { throw new ValidateException("未找到充值记录"); }
            if (ValidateUtils.CheckIntZero(ca.uid)) { throw new ValidateException("充值记录中会员不存在"); }
            if (ca.ispay.Value == 2) { throw new ValidateException("充值记录已审核，不能取消"); }

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = ca.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "充值记录(id=" + ca.id + "，会员=" + ca.userId + ") 取消充值";
            log.tableName = "Charge";
            log.recordName = "会员充值";
            this.SaveOperateLog(log);

            //删除充值记录
            string sql = "delete from Charge where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int UpdateAudit(int id, Member current)
        {
            Charge ca = this.GetOne("select * from Charge where id=" + id);
            if (ca == null) { throw new ValidateException("未找到充值记录"); }
            if (ValidateUtils.CheckIntZero(ca.uid)) { throw new ValidateException("充值记录中会员不存在"); }
            if (ca.ispay.Value != 1) { throw new ValidateException("充值记录不是待审核，不能操作"); }
            Charge update = new Charge();
            update.id = id;
            update.ispay = 2;
            update.passTime = DateTime.Now;
            update.auditUid = current.id;
            update.auditUser = current.userId;
            int c = this.Update(update);

            //充值会员
            Member m = memberBLL.GetModelAndAccountNoPass(ca.uid.Value);

            //增加会员点值
            MemberAccount ma = new MemberAccount();
            ma.id = m.id;
            if (ca.accounttypeId == ConstUtil.JOURNAL_DHM)
                ma.agentDhm = ca.epoints;
            if (ca.accounttypeId == ConstUtil.JOURNAL_TOD)
                ma.agentTod = ca.epoints;
            accountBLL.UpdateAdd(ma);

            //记录流水帐
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = ca.accounttypeId;  //点值
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "会员充值增加";
            liu.outlay = 0;
            liu.income = ca.epoints;
            if (ca.accounttypeId == ConstUtil.JOURNAL_DHM)
                liu.last = m.account.agentDhm.Value + ca.epoints.Value;
            if (ca.accounttypeId == ConstUtil.JOURNAL_TOD)
                liu.last = m.account.agentTod.Value + ca.epoints.Value;
            liu.addtime = DateTime.Now;
            liu.sourceId = ca.id;
            liu.tableName = "Charge";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = ca.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "充值记录(id=" + ca.id + ",会员=" + ca.userId + ")审核通过";
            log.tableName = "Charge";
            log.recordName = "会员充值";
            this.SaveOperateLog(log);

            return c;
        }

    }
}
