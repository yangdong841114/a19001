﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class TakeCashBLL : BaseBLL<TakeCash>, ITakeCashBLL
    {

        private System.Type type = typeof(TakeCash);
        public IBaseDao dao { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new TakeCash GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            TakeCash mb = (TakeCash)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new TakeCash GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            TakeCash mb = (TakeCash)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<TakeCash> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<TakeCash> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<TakeCash>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((TakeCash)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<TakeCash> GetList(string sql)
        {
            List<TakeCash> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<TakeCash>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((TakeCash)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }


        /// <summary>
        /// 获取已审核提现总金额
        /// </summary>
        /// <returns></returns>
        public double GetAuditMoney()
        {
            object o = dao.ExecuteScalar("select SUM(epoints) from TakeCash where isPay =2");
            return o == null ? 0 : Convert.ToDouble(o);
        }

        /// <summary>
        /// 获取待审核提现总金额
        /// </summary>
        /// <returns></returns>
        public double GetNoAduditMoney()
        {
            object o = dao.ExecuteScalar("select SUM(epoints) from TakeCash where isPay =1");
            return o == null ? 0 : Convert.ToDouble(o);
        }

        public PageResult<TakeCash> GetListPage(TakeCash model)
        {
            PageResult<TakeCash> page = new PageResult<TakeCash>();
            string sql = "select t.*,m.userName,m.bpxID,row_number() over(order by t.id desc) rownumber from TakeCash t left join Member m on t.uid = m.id where 1=1 ";
            string countSql = "select count(1) from TakeCash t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isPay))
                {
                    param.Add(new DbParameterItem("t.isPay", ConstUtil.EQ, model.isPay));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<TakeCash> list = new List<TakeCash>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((TakeCash)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetExcelListPage(TakeCash model)
        {

            string sql = "select t.userId,m.userName,t.addtime,t.epoints,t.fee,(t.epoints-t.fee) smoney,t.bankName,t.bankCard,t.bankUser,t.bankAddress,t.auditUser,"+
                         "case when t.isPay=1 then '待审核' when t.isPay=3 then '已取消' else '已通过' end status from TakeCash t inner join Member m on t.uid = m.id  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isPay))
                {
                    param.Add(new DbParameterItem("t.isPay", ConstUtil.EQ, model.isPay));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public int SaveTakeCash(TakeCash model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            DateTime now = DateTime.Now;
            model.uid = current.id;
            model.userId = current.userId;
            model.addtime = now;
            model.hv = 1;
            model.isPay = 1;
            //model.accountId = ConstUtil.JOURNAL_JJB;
            if (ValidateUtils.CheckNull(model.userId)) { throw new ValidateException("提现会员不能为空"); }

            //当前用户
            Member m = memberBLL.GetModelAndAccountNoPass(model.uid.Value);
            if (m == null) { throw new ValidateException("提现会员不能为空"); }
            if (model.epoints.Value <= 0) { throw new ValidateException("提现金额不能为空"); }
            if (model.accountId == ConstUtil.JOURNAL_TOD)
            if (model.epoints.Value > m.account.agentTod.Value) { throw new ValidateException("TOD币-余额不足"); }
            if (model.accountId == ConstUtil.JOURNAL_TOCC)
                if (model.epoints.Value > m.account.agentTocc.Value) { throw new ValidateException("TOCC币-余额不足"); }

            //提现参数：倍数、最小金额、手续费比例
            Dictionary<string, ParameterSet> di = paramBLL.GetDictionaryByCodes(ConstUtil.TX_BEI, ConstUtil.TX_FEE, ConstUtil.TX_MIN, "txday");
            double txbei = Convert.ToInt32(di[ConstUtil.TX_BEI].paramValue);    //提现金额需是xx的倍数
            double txfee = Convert.ToInt32(di[ConstUtil.TX_FEE].paramValue);    //提现手续费比例
            double txmin = Convert.ToInt32(di[ConstUtil.TX_MIN].paramValue);    //提现最小金额
            string txday =","+di["txday"].paramValue;

            //每月8,18,28设定的号数才能申请兑现，一天可申请兑现多次
            if (txday.IndexOf("," + DateTime.Now.Day.ToString()) == -1) { throw new ValidateException("每月" + txday + "才可提现"); }
            if (model.epoints.Value < txmin) { throw new ValidateException("提现金额需>=" + txmin); }
            if (model.epoints.Value % txbei != 0) { throw new ValidateException("提现金额需是" + txbei + "的倍数"); }
            model.fee = txfee;
            //if (m.bankName == null || m.bankCard == null || m.bankUser == null || m.bankAddress == null){ throw new ValidateException("请先完善个人资料中的银行相关信息"); }

            //保存提现记录
            model.bankName = m.bankName;
            model.bankCard = m.bankCard;
            model.bankUser = m.bankUser;
            model.bankAddress = m.bankAddress;
            object o = dao.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);

            //减少会员奖金币余额
            MemberAccount ua = new MemberAccount();
            ua.id = model.uid;
            if (model.accountId == ConstUtil.JOURNAL_TOD)
                ua.agentTod = model.epoints;
            if (model.accountId == ConstUtil.JOURNAL_TOCC)
                ua.agentTocc = model.epoints;
            accountBLL.UpdateSub(ua);

            //记录流水帐
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = model.accountId;  //奖金币
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "会员提现扣除";
            liu.income = 0;
            liu.outlay = model.epoints;
            if (model.accountId == ConstUtil.JOURNAL_TOD)
                liu.last = m.account.agentTod.Value - model.epoints.Value;
            if (model.accountId == ConstUtil.JOURNAL_TOCC)
                liu.last = m.account.agentTocc.Value - model.epoints.Value;
            liu.addtime = now;
            liu.sourceId = newId;
            liu.tableName = "TakeCash";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "提现记录(id=" + newId + "，会员=" + model.userId + ") 申请提现";
            log.tableName = "TakeCash";
            log.recordName = "会员提现";
            this.SaveOperateLog(log);

            //是否需要短信通知
            MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_TX_BEFORE);
            if (bf.flag == 1)
            {
                //发送给会员短信
                string messge = bf.msg + "" + model.epoints.ToString();
                noticeBLL.SendMessage(m.phone, messge);
            }
            MobileNotice at = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_TX_AFTER);
            if (at.flag == 1)
            {
                //发送给管理员短信
                noticeBLL.SendMessage(at.phone, at.msg);
            }

            return newId;
        }

        public int UpdateCancel(int id, Member current)
        {
            TakeCash ca = this.GetOne("select * from TakeCash where id=" + id);
            if (ca == null) { throw new ValidateException("未找到提现记录"); }
            if (ValidateUtils.CheckIntZero(ca.uid)) { throw new ValidateException("提现记录中会员不存在"); }
            if (ca.isPay.Value == 2) { throw new ValidateException("提现记录已审核，不能取消"); }

            Member m = memberBLL.GetModelAndAccountNoPass(ca.uid.Value);

            //退回会员奖金币账户
            MemberAccount update = new MemberAccount();
            update.id = ca.uid;
            if (ca.accountId == ConstUtil.JOURNAL_TOD)
                update.agentTod = ca.epoints;
            if (ca.accountId == ConstUtil.JOURNAL_TOCC)
                update.agentTocc = ca.epoints;
            accountBLL.UpdateAdd(update);

            //记录流水帐
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = ca.accountId;  //奖金币
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "取消会员提现退回";
            liu.income = ca.epoints;
            liu.outlay = 0;
            if (ca.accountId == ConstUtil.JOURNAL_TOD)
                liu.last = m.account.agentTod.Value + ca.epoints.Value;
            if (ca.accountId == ConstUtil.JOURNAL_TOCC)
                liu.last = m.account.agentTocc.Value + ca.epoints.Value;
            liu.addtime = DateTime.Now;
            liu.sourceId = ca.id;
            liu.tableName = "TakeCash";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = ca.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "提现记录(id=" + ca.id + "，会员=" + ca.userId + ") 取消提现";
            log.tableName = "TakeCash";
            log.recordName = "会员提现";
            this.SaveOperateLog(log);

            //删除提现记录
            string sql = "update TakeCash set isPay=3  where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int UpdateAudit(int id, Member current)
        {
            TakeCash ca = this.GetOne("select * from TakeCash where id=" + id);
            if (ca == null) { throw new ValidateException("未找到提现记录"); }
            if (ValidateUtils.CheckIntZero(ca.uid)) { throw new ValidateException("提现记录中会员不存在"); }
            if (ca.isPay.Value != 1) { throw new ValidateException("提现记录不是待审核，不能操作"); }
            TakeCash update = new TakeCash();
            update.id = id;
            update.isPay = 2;
            update.auditTime = DateTime.Now;
            update.auditUid = current.id;
            update.auditUser = current.userId;
            int c = this.Update(update);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = ca.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "提现记录(id=" + ca.id + ",会员=" + ca.userId + ")审核通过";
            log.tableName = "TakeCash";
            log.recordName = "会员提现";
            this.SaveOperateLog(log);

            return c;
        }

        public DataTable GetTakeCashExcel()
        {
            string sql = "select t.userId,m.userName,t.addtime,t.epoints,t.fee,(t.epoints-t.fee) smoney,t.bankName,t.bankCard,t.bankUser,t.bankAddress,t.auditUser," +
                         "case when t.isPay=1 then '待审核' when t.isPay=3 then '已取消' else '已通过' end status from TakeCash t inner join Member m on t.uid = m.id  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

    }
}
