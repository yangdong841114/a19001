﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;

namespace BLL.Impl
{
    public class OrderBLL : IOrderBLL
    {

        private System.Type hType = typeof(OrderHeader);
        private System.Type iType = typeof(OrderItem);
        private System.Type sType = typeof(OrderStatus);
        public IMemberAccountBLL accountBLL { get; set; }
        public IBaseDao dao { get; set; }

        public IProductBLL productBLL { get; set; }

        public PageResult<OrderHeader> GetListPage(OrderHeader model)
        {
            PageResult<OrderHeader> page = new PageResult<OrderHeader>();
            //--------------------------------------------- 查询订单头 start ----------------------------------------------------------
            string sql = "select o.*,row_number() over(order by o.id desc) rownumber from OrderHeader o inner join Member m on o.uid = m.id where m.isPay=1 ";
            string countSql = "select count(1) from OrderHeader o inner join Member m on o.uid = m.id where m.isPay=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.id))
                {
                    param.Add(new DbParameterItem("o.id", ConstUtil.LIKE, model.id));
                }
                if (!ValidateUtils.CheckIntZero(model.saleUid))
                {
                    param.Add(new DbParameterItem("o.saleUid", ConstUtil.EQ, model.saleUid));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("o.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("o.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (!ValidateUtils.CheckIntZero(model.status))
                {
                    param.Add(new DbParameterItem("o.status", ConstUtil.EQ, model.status));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("o.orderDate", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("o.orderDate", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<OrderHeader> list = new List<OrderHeader>();
            Dictionary<string, OrderHeader> headerMap = new Dictionary<string, OrderHeader>();
            StringBuilder headerIds = new StringBuilder(); //订单头ID
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    OrderHeader E_OrderHeader = (OrderHeader)ReflectionUtil.GetModel(hType, row);
                    if (E_OrderHeader.saleUid.Value != 1)
                        E_OrderHeader.salephone = dao.GetList("select * from Merchant where uid=" + E_OrderHeader.saleUid.Value.ToString()).Rows[0]["linkPhone"].ToString();
                    OrderHeader hd = E_OrderHeader;
                    headerMap.Add(hd.id, hd);
                    headerIds.Append("'").Append(hd.id).Append("',");
                    list.Add(hd);
                }
            }

            //--------------------------------------------- 查询订单头 end ----------------------------------------------------------

            if (headerIds.Length > 0)
            {
                string hids = headerIds.ToString().Substring(0, headerIds.Length - 1);
                //--------------------------------------------- 查询订单体 start ----------------------------------------------------------
                sql = "select i.*,p.imgUrl productImg from OrderItem i inner join Product p on i.productId = p.id where i.hid in (" + hids + ")";
                DataTable itemDt = dao.GetList(sql);
                Dictionary<string, List<OrderItem>> itemMap = new Dictionary<string, List<OrderItem>>();
                if (itemDt != null && itemDt.Rows.Count > 0)
                {
                    for (int i = 0; i < itemDt.Rows.Count; i++)
                    {
                        DataRow row = itemDt.Rows[i];
                        OrderItem item = (OrderItem)ReflectionUtil.GetModel(iType, row);
                        List<OrderItem> itemList = null;
                        if (itemMap.ContainsKey(item.hid))
                        {
                            itemList = itemMap[item.hid];
                            itemList.Add(item);
                            itemMap[item.hid] = itemList;
                        }
                        else
                        {
                            itemList = new List<OrderItem>();
                            itemList.Add(item);
                            itemMap.Add(item.hid, itemList);
                        }
                    }
                }
                //--------------------------------------------- 查询订单体 end ----------------------------------------------------------

                //--------------------------------------------- 查询订单状态记录 start ----------------------------------------------------------

                sql = "select * from OrderStatus i where i.hid in (" + hids + ")";
                DataTable statusDt = dao.GetList(sql);
                Dictionary<string, List<OrderStatus>> statusMap = new Dictionary<string, List<OrderStatus>>();
                if (statusDt != null && statusDt.Rows.Count > 0)
                {
                    for (int i = 0; i < statusDt.Rows.Count; i++)
                    {
                        DataRow row = statusDt.Rows[i];
                        OrderStatus st = (OrderStatus)ReflectionUtil.GetModel(sType, row);
                        List<OrderStatus> stList = null;
                        if (statusMap.ContainsKey(st.hid))
                        {
                            stList = statusMap[st.hid];
                            stList.Add(st);
                            statusMap[st.hid] = stList;
                        }
                        else
                        {
                            stList = new List<OrderStatus>();
                            stList.Add(st);
                            statusMap.Add(st.hid, stList);
                        }
                    }
                }

                //--------------------------------------------- 查询订单状态记录 end ----------------------------------------------------------
                //汇总记录
                for (var i = 0; i < list.Count; i++)
                {
                    OrderHeader h = list[i];
                    h.items = itemMap[h.id];
                    h.osList = statusMap[h.id];
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetExcelList(OrderHeader model)
        {
            string sql = "select h.userId,h.id,h.receiptName,h.phone,h.address,h.productNum,h.productMoney,h.orderDate,i.productNumber,i.productName,i.num " +
                         "from OrderHeader h inner join OrderItem i on h.id = i.hid inner join Member m on h.uid = m.id where m.isPay=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.id))
                {
                    param.Add(new DbParameterItem("h.id", ConstUtil.LIKE, model.id));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("h.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("h.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("h.orderDate", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("h.orderDate", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public List<OrderHeader> GetList(OrderHeader model)
        {
            List<OrderHeader> result = new List<OrderHeader>();
            //--------------------------------------------- 查询订单头 start ----------------------------------------------------------
            string sql = "select o.* from OrderHeader o inner join Member m on o.uid = m.id where m.isPay = 1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.id))
                {
                    param.Add(new DbParameterItem("o.id", ConstUtil.LIKE, model.id));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("o.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("o.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("o.orderDate", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("o.orderDate", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            DataTable dt = dao.GetList(sql, param, true);
            Dictionary<string, OrderHeader> headerMap = new Dictionary<string, OrderHeader>();
            StringBuilder headerIds = new StringBuilder(); //订单头ID
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    OrderHeader hd = (OrderHeader)ReflectionUtil.GetModel(hType, row);
                    headerMap.Add(hd.id, hd);
                    headerIds.Append("'").Append(hd.id).Append("',");
                    result.Add(hd);
                }
            }

            //--------------------------------------------- 查询订单头 end ----------------------------------------------------------

            if (headerIds.Length > 0)
            {
                string hids = headerIds.ToString().Substring(0, headerIds.Length - 1);
                //--------------------------------------------- 查询订单体 start ----------------------------------------------------------
                sql = "select * from OrderItem i where i.hid in (" + hids + ")";
                DataTable itemDt = dao.GetList(sql);
                Dictionary<string, List<OrderItem>> itemMap = new Dictionary<string, List<OrderItem>>();
                if (itemDt != null && itemDt.Rows.Count > 0)
                {
                    for (int i = 0; i < itemDt.Rows.Count; i++)
                    {
                        DataRow row = itemDt.Rows[i];
                        OrderItem item = (OrderItem)ReflectionUtil.GetModel(iType, row);
                        List<OrderItem> itemList = null;
                        if (itemMap.ContainsKey(item.hid))
                        {
                            itemList = itemMap[item.hid];
                            itemList.Add(item);
                            itemMap[item.hid] = itemList;
                        }
                        else
                        {
                            itemList = new List<OrderItem>();
                            itemList.Add(item);
                            itemMap.Add(item.hid, itemList);
                        }
                    }
                }
                //--------------------------------------------- 查询订单体 end ----------------------------------------------------------

                //--------------------------------------------- 查询订单状态记录 start ----------------------------------------------------------

                sql = "select * from OrderStatus i where i.hid in (" + hids + ")";
                DataTable statusDt = dao.GetList(sql);
                Dictionary<string, List<OrderStatus>> statusMap = new Dictionary<string, List<OrderStatus>>();
                if (statusDt != null && statusDt.Rows.Count > 0)
                {
                    for (int i = 0; i < statusDt.Rows.Count; i++)
                    {
                        DataRow row = statusDt.Rows[i];
                        OrderStatus st = (OrderStatus)ReflectionUtil.GetModel(sType, row);
                        List<OrderStatus> stList = null;
                        if (statusMap.ContainsKey(st.hid))
                        {
                            stList = statusMap[st.hid];
                            stList.Add(st);
                            statusMap[st.hid] = stList;
                        }
                        else
                        {
                            stList = new List<OrderStatus>();
                            stList.Add(st);
                            statusMap.Add(st.hid, stList);
                        }
                    }
                }

                //--------------------------------------------- 查询订单状态记录 end ----------------------------------------------------------
                //汇总记录
                for (var i = 0; i < result.Count; i++)
                {
                    OrderHeader h = result[i];
                    h.items = itemMap[h.id];
                    h.osList = statusMap[h.id];
                }
            }

            return result;
        }

        public string SaveFxOrder(OrderHeader oh)
        {
            if (oh == null) { throw new ValidateException("订单信息为空 "); }
            if (ValidateUtils.CheckNull(oh.phone)) { throw new ValidateException("联系电话不能为空 "); }
            if (ValidateUtils.CheckNull(oh.receiptName)) { throw new ValidateException("联系人不能为空 "); }
            //if (ValidateUtils.CheckNull(oh.provinceName)) { throw new ValidateException("省不能为空 "); }
            //if (ValidateUtils.CheckNull(oh.cityName)) { throw new ValidateException("市不能为空 "); }
            //if (ValidateUtils.CheckNull(oh.address)) { throw new ValidateException("详细地址不能为空 "); }
            if (ValidateUtils.CheckIntZero(oh.typeId)) { throw new ValidateException("订单类型不能为空 "); }
            //订单ID,联系电话,联系人,联系地址,会员ID
            string sn = OrderNumberUtils.GetRandomNumber("D") + "||" + oh.phone + "||" + oh.receiptName + "||" + oh.address + "||" + oh.uid + "||" + oh.provinceName + "||" + oh.cityName + "||" + oh.areaName + "||" + oh.contry;
            //检查商品是否有重复
            string checkSql = "select *,p.userId as payUserId from ShoppingCart s inner join Product p on s.productId = p.id where s.isCheck = 2  and s.uid = " + oh.uid;
            var checkList = dao.GetList(checkSql);
            var i = 0;
            foreach (DataRow item in checkList.Rows)
            {
                var payUserId = item["payUserId"].ToString();
                var productName = item["productName"].ToString();
                if (payUserId == oh.userId.ToString()) { i++; }

            }
            if (i > 0) { throw new ValidateException("请不要购买自己发布的商品"); }
            //调用存储过程
            string pro = "createFxOrder";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@sn",SqlDbType.VarChar,1000),
                  new SqlParameter("@accounttypeId",SqlDbType.Int),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = sn;
            p[1].Value = 37;//TOD
            p[2].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                foreach (DataRow item in checkList.Rows)
                {
                    var payUserId = item["payUserId"].ToString();
                    string memberSql = "select * from Member where userId = '" + payUserId + "'";
                    var memberModel = dao.GetList(memberSql);
                    JhInterface.SendQQmail(memberModel.Rows[0]["email"].ToString(), "系统邮件", "您有新的待发货订单，请及时上线处理");
                }
            }
            return result;
        }


        public string SaveRegisterOrder(OrderHeader oh)
        {
            if (oh == null) { throw new ValidateException("订单信息为空 "); }
            if (ValidateUtils.CheckNull(oh.phone)) { throw new ValidateException("联系电话不能为空 "); }
            if (ValidateUtils.CheckNull(oh.receiptName)) { throw new ValidateException("联系人不能为空 "); }
            if (ValidateUtils.CheckNull(oh.address)) { throw new ValidateException("联系地址不能为空 "); }
            if (ValidateUtils.CheckIntZero(oh.typeId)) { throw new ValidateException("订单类型不能为空 "); }
            if (oh.items == null || oh.items.Count == 0) { throw new ValidateException("商品信息不能为空 "); }
            oh.id = OrderNumberUtils.GetRandomNumber("D");
            oh.orderDate = DateTime.Now;

            foreach (OrderItem it in oh.items)
            {
                it.hid = oh.id;
                it.pv = 0;
            }
            //批量保存订单条目
            SaveOrderItems(oh.items);

            //保存状态记录
            OrderStatus s = new OrderStatus();
            s.hid = oh.id;
            s.operate = "注册提交订单";
            s.addTime = oh.orderDate;
            s.status = 1;
            s.uid = oh.uid;
            s.userId = oh.userId;
            dao.Save(s);

            //保存订单头
            oh.status = 1;
            oh.typeId = 1;
            oh.payType = "现金";
            oh.items = null;
            int c = dao.Save(oh);
            return oh.id;
        }

        private int SaveOrderItems(List<OrderItem> list)
        {
            if (list == null || list.Count == 0) { throw new ValidateException("要保存的内容为空"); }

            DataTable dt = new DataTable("OrderItem");
            dt.Columns.Add(new DataColumn("id", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("hid", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("productId", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("productName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("productNumber", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("price", typeof(double)));
            dt.Columns.Add(new DataColumn("num", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("total", typeof(double)));
            dt.Columns.Add(new DataColumn("pv", typeof(double)));
            foreach (OrderItem r in list)
            {
                DataRow row = dt.NewRow();
                row["hid"] = r.hid;
                row["productId"] = r.productId;
                row["productName"] = r.productName;
                row["productNumber"] = r.productNumber;
                row["price"] = r.price;
                row["num"] = r.num;
                row["total"] = r.total;
                row["pv"] = r.pv;
                dt.Rows.Add(row);
            }
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("hid", DbType.String, null))
                .Add(new DbParameterItem("productId", DbType.Int32, null))
                .Add(new DbParameterItem("productName", DbType.String, null))
                .Add(new DbParameterItem("productNumber", DbType.String, null))
                .Add(new DbParameterItem("price", DbType.Double, null))
                .Add(new DbParameterItem("num", DbType.Int32, null))
                .Add(new DbParameterItem("total", DbType.Double, null))
                .Add(new DbParameterItem("pv", DbType.Double, null))
                .Result();
            string sql = "insert into OrderItem(hid,productId,productName,productNumber,price,num,total,pv) values(@hid,@productId,@productName,@productNumber,@price,@num,@total,@pv)";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            return dao.InsertBatchByDataSet(set, "OrderItem", sql, param);
        }

        public int DeleteOrder(string id)
        {
            if (ValidateUtils.CheckNull(id)) { throw new ValidateException("订单编号不能为空"); }
            string sql = "select * from OrderHeader where id = @id";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id", null, id)).Result();
            DataRow row = dao.GetOne(sql, param, false);
            if (row == null) { throw new ValidateException("订单不存在"); }
            int status = Convert.ToInt32(row["status"]);
            if (status != 1) { throw new ValidateException("订单状态不是【待发货】，不能删除"); }
            //调用存储过程
            string pro = "deleteOrder";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@sn",SqlDbType.VarChar,50),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = id;
            p[1].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public int SaveLogistic(List<OrderHeader> list)
        {
            if (list == null || list.Count == 0) { throw new ValidateException("发货订单不能为空"); }
            //拼装ID
            StringBuilder sb = new StringBuilder();
            foreach (OrderHeader oh in list)
            {
                if (ValidateUtils.CheckNull(oh.logNo)) { throw new ValidateException("物流单号不能为空"); }
                if (ValidateUtils.CheckNull(oh.logName)) { throw new ValidateException("物流公司不能为空"); }
                sb.Append("'").Append(oh.id).Append("',");
            }
            string ids = sb.ToString().Substring(0, sb.Length - 1);
            string sql = "exec('select * from OrderHeader where id in ('+@ids+')')";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("ids", null, ids)).Result();
            DataTable dt = dao.GetList(sql, param, false);
            if (dt == null || dt.Rows.Count == 0) { throw new ValidateException("发货订单不存在"); }
            Dictionary<string, DataRow> di = new Dictionary<string, DataRow>();
            foreach (DataRow row in dt.Rows)
            {
                di.Add(row["id"].ToString(), row);
            }

            //检查订单是否存在、状态
            foreach (OrderHeader oh in list)
            {
                if (!di.ContainsKey(oh.id)) { throw new ValidateException("订单【" + oh.id + "】不存在"); }
                DataRow row = di[oh.id];
                int status = Convert.ToInt32(row["status"]);
                if (status == 3) { throw new ValidateException("订单【" + oh.id + "】状态是【已完成】，不能发货"); }
                DataRow[] rows = dt.Select("id='" + oh.id + "'");
                row = rows[0];
                row["logName"] = oh.logName;
                row["logNo"] = oh.logNo;
                row["wlNo"] = oh.wlNo;
                row["status"] = 2;
            }
            param = ParamUtil.Get()
                .Add(new DbParameterItem("logName", DbType.String, null))
                .Add(new DbParameterItem("logNo", DbType.String, null))
                .Add(new DbParameterItem("status", DbType.Int32, null))
                .Add(new DbParameterItem("id", DbType.String, null))
                 .Add(new DbParameterItem("wlNo", DbType.String, null))
                .Result();
            sql = "update OrderHeader set logName=@logName,logNo=@logNo,wlNo=@wlNo,status=@status where id=@id";

            //调用存储过程
            string pro = "distributionRoyalty";
            foreach (DataRow row in dt.Rows)
            {
                IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@oid",SqlDbType.VarChar,100),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
                p[0].Value = row["id"].ToString();
                p[1].Direction = ParameterDirection.Output;
                string result = dao.RunProceOnlyPara(pro, p, "return");
                if (result != "success")
                {
                    throw new ValidateException(result);
                }
            }


            DataSet set = new DataSet();
            set.Tables.Add(dt);
            dt.TableName = "OrderHeader";
            return dao.UpdateBatchByDataSet(set, "OrderHeader", sql, param);
        }
        public int SaveSureReceive(OrderHeader order)
        {
            //是否扫码
            bool isscan = false;
            if (order.payType == "scan")
                isscan = true;
            if (order == null) { throw new ValidateException("操作内容为空"); }
            else if (order.id == null) { throw new ValidateException("订单不存在"); }

            string sql = "select * from OrderHeader where id=@id";
            DataRow row = dao.GetOne(sql, ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, order.id)).Result(), false);
            if (row == null) { throw new ValidateException("订单不存在"); }
            OrderHeader oh = (OrderHeader)ReflectionUtil.GetModel(hType, row);
            //扫码支付无需已发货必须是未发货
            if (oh.status != 2 && false == isscan) { throw new ValidateException("订单不是【已发货】，不能收货"); }
            if (oh.status == 3 && true == isscan) { throw new ValidateException("扫码支付订单【已完成】，不能收货"); }
            if (Common.SqlChecker.CheckKeyWord(oh.id) != "") { throw new ValidateException("含SQL注入字符" + Common.SqlChecker.CheckKeyWord(oh.id)); }
            double orderSxfbili = Convert.ToDouble(dao.GetList("select * from ParameterSet where paramCode='orderSxfbili'").Rows[0]["paramValue"].ToString());
            double sxf = oh.productMoney.Value * orderSxfbili / 100;
            //更新状态
            int c = dao.ExecuteBySql("update OrderHeader set status=3,sxf=" + sxf + " where id='" + oh.id + "'");

            Member sk = (Member)ReflectionUtil.GetModel(typeof(Member), dao.GetOne("select * from Member where id=" + oh.saleUid.Value));
            MemberAccount sk_account = accountBLL.GetModel(oh.saleUid.Value);

            MemberAccount ma = new MemberAccount();
            double lsmoney = oh.productMoney.Value - sxf;
            ma.id = sk.id;
            ma.agentTod = lsmoney;
            accountBLL.UpdateAdd(ma);

            LiuShuiZhang skLiu = new LiuShuiZhang();
            skLiu.uid = sk.id;
            skLiu.userId = sk.userId;
            skLiu.accountId = ConstUtil.JOURNAL_TOD;
            skLiu.abst = "订单【" + oh.id + "】确认收货";
            skLiu.income = lsmoney;
            skLiu.outlay = 0;
            skLiu.last = sk_account.agentTod + lsmoney;
            skLiu.addtime = DateTime.Now;
            skLiu.addUid = 1;
            skLiu.addUser = "system";
            dao.Save(skLiu);

            return c;
        }
        public LogisticResult GetLogisticMsg(string id)
        {
            string sql = "select * from OrderHeader where id=@id";
            DataRow row = dao.GetOne(sql, ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, id)).Result(), false);
            if (row == null) { throw new ValidateException("订单不存在"); }
            OrderHeader oh = (OrderHeader)ReflectionUtil.GetModel(hType, row);
            if (oh.status < 2) { throw new ValidateException("订单未发货，无法查询"); }
            LogisticResult result = JhInterface.GetLogisticMsg(oh.wlNo, oh.logNo);
            return result;
        }
    }
}
