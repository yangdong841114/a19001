﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ParamSetBLL : BaseBLL<ParameterSet>, IParamSetBLL
    {
        private System.Type type = typeof(ParameterSet);
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new ParameterSet GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            ParameterSet mb = (ParameterSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ParameterSet GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ParameterSet mb = (ParameterSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<ParameterSet> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<ParameterSet> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ParameterSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<ParameterSet> GetList(string sql)
        {
            List<ParameterSet> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ParameterSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public Dictionary<string, ParameterSet> GetAllToDictionary()
        {
            Dictionary<string, ParameterSet> rr = null;
            DataTable dt = dao.GetList("select * from parameterSet");
            if (dt != null && dt.Rows.Count > 0)
            {
                rr = new Dictionary<string, ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    ParameterSet pr = (ParameterSet)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.paramCode, pr);
                }
            }
            return rr;
        }

        public int UpdateList(List<ParameterSet> list)
        {
            DataTable dt = dao.GetList("select * from parameterSet");
            if(list==null || list.Count==0){
                throw new ValidateException("要保存的内容为空");
            }
            foreach(ParameterSet ps in list){
                if(ValidateUtils.CheckNull(ps.paramValue))
                {
                    if (ps.paramCode == "userIdPrefix")
                    {
                        ps.paramValue = "";
                    }
                    else
                    {
                        throw new ValidateException(ps.paramCode + "：的值为空");
                    }
                }
                DataRow[] rows = dt.Select("paramCode='"+ps.paramCode+"'");
                if(rows==null || rows.Length==0){
                    throw new ValidateException(ps.paramCode+"没有找到数据");
                }
                rows[0]["paramValue"] = ps.paramValue;
            }
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("paramValue", DbType.String, null))
                .Add(new DbParameterItem("paramCode",DbType.String,null))
                .Result();
            string sql = "update ParameterSet set paramValue=@paramValue where paramCode=@paramCode";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            dt.TableName = "ParameterSet";
            return dao.UpdateBatchByDataSet(set, "ParameterSet", sql, param);
        }

        public Dictionary<string, ParameterSet> GetDictionaryByCodes(params string[] paramCode)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < paramCode.Length; i++)
            {
                list.Add(paramCode[i]);
            }
            return GetToDictionary(list);
        }

        public void AutoSlEmail()
        {
            Dictionary<string, ParameterSet> param = GetDictionaryByCodes("SlnoticeDay", "SlnoticeLockMonth", "SlLockMonth");
            int SlnoticeDay = Convert.ToInt16(param["SlnoticeDay"].paramValue);
            int SlnoticeLockMonth = Convert.ToInt16(param["SlnoticeLockMonth"].paramValue);
            int SlLockMonth = Convert.ToInt16(param["SlLockMonth"].paramValue);
            DataTable dt = dao.GetList("select * from VMemberSl where id>3 and id in (select id from Member where sendMailTime is null or DateDiff(dd,sendMailTime,getdate())>20) and email is not null and  DateDiff(dd,getdate(),jsTime)>0 and DateDiff(dd,getdate(),jsTime)<=" + SlnoticeDay);//20天内没发过邮件,算力结束时间在10天内
            if(dt.Rows.Count>0)
            {
                string body = "尊敬的会员:" + dt.Rows[0]["userId"].ToString() + ",您的算力将于" +dt.Rows[0]["jsTime"].ToString() + "到期，请及时续费";
                string toEmail = dt.Rows[0]["email"].ToString();
                //toEmail = "78872162@qq.com";//测试
                string title = "算力到期提醒";
                Common.JhInterface.SendQQmail(toEmail, title, body);
                dao.ExecuteBySql("update Member set sendMailTime=getdate() where id=" + dt.Rows[0]["id"].ToString());
            }
            dt = dao.GetList("select * from VMemberSl where id>3 and id in (select id from Member where sendMailTime is null or DateDiff(dd,sendMailTime,getdate())>20) and email is not null and  DateDiff(MONTH,jsTime,getdate())>=" + SlnoticeLockMonth);//20天内没发过邮件,算力结束时间超过11个月
            if (dt.Rows.Count > 0)
            {
                string body = "尊敬的会员:" + dt.Rows[0]["userId"].ToString() + ",您的算力已于" + dt.Rows[0]["jsTime"].ToString() + "到期，连续12个月，不续费购买算力，您的帐号将被冻结，所有权利义务亦同时终止";
                string toEmail = dt.Rows[0]["email"].ToString();
                //toEmail = "78872162@qq.com";//测试
                string title = "算力到期提醒";
                Common.JhInterface.SendQQmail(toEmail, title, body);
                dao.ExecuteBySql("update Member set sendMailTime=getdate() where id=" + dt.Rows[0]["id"].ToString());
            }
            //连续12个月没续费就要被冻结了
            dao.ExecuteBySql("update Member set isLock=1 where id>3 and isAdmin!=1 and id in (select id from VMemberSl where DateDiff(MONTH,jsTime,getdate())>=" + SlLockMonth + ")");
        }

        public Dictionary<string, ParameterSet> GetToDictionary(List<string> paramCodeList)
        {
            Dictionary<string, ParameterSet> rr = new Dictionary<string, ParameterSet>();
            string sql = "select * from ParameterSet ";
            if (paramCodeList != null && paramCodeList.Count>0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string s in paramCodeList)
                {
                    sb.Append("'").Append(s).Append("',");
                }
                sql += " where paramCode in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
            }
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    ParameterSet pr = (ParameterSet)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.paramCode, pr);
                }
            }
            return rr;
        }
    }
}
