﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MemberAccountBLL : BaseBLL<MemberAccount>, IMemberAccountBLL
    {

        private System.Type type = typeof(MemberAccount);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new MemberAccount GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            MemberAccount mb = (MemberAccount)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new MemberAccount GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            MemberAccount mb = (MemberAccount)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<MemberAccount> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<MemberAccount> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<MemberAccount>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<MemberAccount> GetList(string sql)
        {
            List<MemberAccount> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MemberAccount>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public int UpdateAdd(MemberAccount account)
        {
            if (account != null)
            {
                if (ValidateUtils.CheckIntZero(account.id))
                {
                    throw new ValidateException("修改账户的会员不能为空");
                }
                string sql = "update MemberAccount set ";
                bool exists = false;
                if (!ValidateUtils.CheckDoubleZero(account.agentDz))
                {
                    sql += " agentDz=agentDz+" + account.agentDz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFt))
                {
                    sql += " agentFt=agentFt+" + account.agentFt + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentGw))
                {
                    sql += " agentGw=agentGw+" + account.agentGw + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentJj))
                {
                    sql += " agentJj=agentJj+" + account.agentJj + ",";
                    exists = true;
                }
                //本系统
                if (!ValidateUtils.CheckDoubleZero(account.agentDhm))
                {
                    sql += " agentDhm=agentDhm+" + account.agentDhm + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTod))
                {
                    sql += " agentTod=agentTod+" + account.agentTod + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFhz))
                {
                    sql += " agentFhz=agentFhz+" + account.agentFhz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTocc))
                {
                    sql += " agentTocc=agentTocc+" + account.agentTocc + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentSl))
                {
                    sql += " agentSl=agentSl+" + account.agentSl + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBpc))
                {
                    sql += " agentBpc=agentBpc+" + account.agentBpc + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentMoby))
                {
                    sql += " agentMoby=agentMoby+" + account.agentMoby + ",";
                    exists = true;
                }
                if (exists)
                {
                    sql = sql.Substring(0, sql.Length - 1);
                    sql += " where id=" + account.id;
                    return dao.ExecuteBySql(sql);
                }
            }
            return 0;
        }

        public int UpdateSub(MemberAccount account)
        {
            if (account != null)
            {
                if (ValidateUtils.CheckIntZero(account.id))
                {
                    throw new ValidateException("修改账户的会员不能为空");
                }
                string sql = "update MemberAccount set ";
                bool exists = false;
                if (!ValidateUtils.CheckDoubleZero(account.agentDz))
                {
                    sql += " agentDz=agentDz-" + account.agentDz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFt))
                {
                    sql += " agentFt=agentFt-" + account.agentFt + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentGw))
                {
                    sql += " agentGw=agentGw-" + account.agentGw + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentJj))
                {
                    sql += " agentJj=agentJj-" + account.agentJj + ",";
                    exists = true;
                }
                //本系统
                if (!ValidateUtils.CheckDoubleZero(account.agentDhm))
                {
                    sql += " agentDhm=agentDhm-" + account.agentDhm + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTod))
                {
                    sql += " agentTod=agentTod-" + account.agentTod + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFhz))
                {
                    sql += " agentFhz=agentFhz-" + account.agentFhz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTocc))
                {
                    sql += " agentTocc=agentTocc-" + account.agentTocc + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentSl))
                {
                    sql += " agentSl=agentSl-" + account.agentSl + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBpc))
                {
                    sql += " agentBpc=agentBpc-" + account.agentBpc + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentMoby))
                {
                    sql += " agentMoby=agentMoby-" + account.agentMoby + ",";
                    exists = true;
                }

                if (exists)
                {
                    sql = sql.Substring(0, sql.Length - 1);
                    sql += " where id=" + account.id;
                    return dao.ExecuteBySql(sql);
                }
            }
            return 0;
        }

        public PageResult<MemberAccount> GetListPage(MemberAccount model)
        {
            PageResult<MemberAccount> page = new PageResult<MemberAccount>();
            string sql = "select m.userId,m.userName,a.*,row_number() over(order by a.id desc) rownumber from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            string countSql = "select count(1) from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.gtValue))
                {
                    if(model.gtaccountId=="36")
                    {
                        sql += " and agentDhm>=" + model.gtValue;
                        countSql += " and agentDhm>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "37")
                    {
                        sql += " and agentTod>=" + model.gtValue;
                        countSql += " and agentTod>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "38")
                    {
                        sql += " and agentFhz>=" + model.gtValue;
                        countSql += " and agentFhz>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "39")
                    {
                        sql += " and agentTocc>=" + model.gtValue;
                        countSql += " and agentTocc>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "86")
                    {
                        sql += " and agentDz>=" + model.gtValue;
                        countSql += " and agentDz>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "87")
                    {
                        sql += " and agentSl>=" + model.gtValue;
                        countSql += " and agentSl>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "88")
                    {
                        sql += " and agentBpc>=" + model.gtValue;
                        countSql += " and agentBpc>=" + model.gtValue;
                    }
                    if (model.gtaccountId == "93")
                    {
                        sql += " and agentMoby>=" + model.gtValue;
                        countSql += " and agentMoby>=" + model.gtValue;
                    }
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<MemberAccount> list = new List<MemberAccount>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public DataTable GetListPageDataTable(MemberAccount model)
        {
         
            PageResult<MemberAccount> page = new PageResult<MemberAccount>();
            string sql = "select m.userId,m.userName,a.agentDhm,a.agentTod,a.agentFhz,a.agentTocc,a.agentDz,a.agentSl,a.agentBpc,a.agentMoby from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
          
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.gtValue))
                {
                    if(model.gtaccountId=="36")
                    {
                        sql += " and agentDhm>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "37")
                    {
                        sql += " and agentTod>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "38")
                    {
                        sql += " and agentFhz>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "39")
                    {
                        sql += " and agentTocc>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "86")
                    {
                        sql += " and agentDz>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "87")
                    {
                        sql += " and agentSl>=" + model.gtValue;
                     
                    }
                    if (model.gtaccountId == "88")
                    {
                        sql += " and agentBpc>=" + model.gtValue;
                       
                    }
                    if (model.gtaccountId == "93")
                    {
                        sql += " and agentMoby>=" + model.gtValue;
                      
                    }
                }
            }



            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
        

        public MemberAccount GetTotalSummary(MemberAccount model)
        {
            string sql = "select '' userId,'汇总合计：' userName, sum(agentDz)agentDz,sum(agentJj)agentJj,sum(agentGw)agentGw,sum(agentFt)agentFt,sum(agentDhm)agentDhm,sum(agentTod)agentTod,sum(agentFhz)agentFhz,sum(agentTocc)agentTocc,sum(agentSl)agentSl,sum(agentBpc)agentBpc,sum(agentMoby)agentMoby  from MemberAccount " +
                        " a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.gtValue))
                {
                    if (model.gtaccountId == "36")
                    {
                        sql += " and agentDhm>=" + model.gtValue;
                        
                    }
                    if (model.gtaccountId == "37")
                    {
                        sql += " and agentTod>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "38")
                    {
                        sql += " and agentFhz>=" + model.gtValue;
                     
                    }
                    if (model.gtaccountId == "39")
                    {
                        sql += " and agentTocc>=" + model.gtValue;
                     
                    }
                    if (model.gtaccountId == "86")
                    {
                        sql += " and agentDz>=" + model.gtValue;
                     
                    }
                    if (model.gtaccountId == "87")
                    {
                        sql += " and agentSl>=" + model.gtValue;
                    
                    }
                    if (model.gtaccountId == "88")
                    {
                        sql += " and agentBpc>=" + model.gtValue;
                      
                    }
                    if (model.gtaccountId == "93")
                    {
                        sql += " and agentMoby>=" + model.gtValue;
                       
                    }
                }
            }
           MemberAccount result = this.GetOne(sql, param);
           return result;
        }

        public MemberAccount GetModel(int id)
        {
            string sql = "select * from MemberAccount ";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id",ConstUtil.EQ,id)).Result();
            return this.GetOne(sql, param);
        }


        public DataTable GetLiuShuiExcel()
        {
            string sql = "  select m.userId,m.userName,a.agentDz,a.agentDhm,a.agentTod,a.agentFhz,a.agentTocc,a.agentSl,a.agentBpc,a.agentMoby from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
    }
}
