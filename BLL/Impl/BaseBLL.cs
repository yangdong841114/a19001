﻿using Common;
using DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Impl
{
    public abstract class BaseBLL<T> : IBaseBLL<T>
    {
        public abstract IBaseDao GetDao();

        /// <summary>
        /// 保存操作日志
        /// </summary>
        /// <param name="model">操作日志对象</param>
        protected void SaveOperateLog(OperateLog log)
        {
            string err = null;

            //非空校验
            if (log == null) { err = "操作日志对象为空"; }
            if (ValidateUtils.CheckNull(log.userId)) { err = "操作日志会员编码不能为空"; }
            //else if (ValidateUtils.CheckNull(log.ipAddress)) { err = "操作日志会员地址不能为空"; }
            else if (ValidateUtils.CheckNull(log.tableName)) { err = "操作日志业务名不能为空"; }
            else if (ValidateUtils.CheckIntZero(log.uid)) { err = "操作日志会员ID不能为空"; }
            if (err != null)
            {
                throw new ValidateException(err);
            }
            //重新new 一个过滤掉子类其他属性
            log.addTime = DateTime.Now;

            //保存记录
            this.Save(log);
        }

        public object SaveByIdentity(DtoData dto)
        {
            return GetDao().SaveByIdentity(dto);
        }

        public int Save(DtoData dto)
        {
            return GetDao().Save(dto);
        }

        public int Update(DtoData dto)
        {
            return GetDao().Update(dto);
        }

        public int Delte(string table, object id)
        {
            return GetDao().Delte(table, id);
        }

        public int GetCount(string sql, List<Common.DbParameterItem> param)
        {
            return GetDao().GetCount(sql, param, true);
        }

        public int GetCount(string sql)
        {
            return GetDao().GetCount(sql);
        }

        public T GetOne(string sql, List<Common.DbParameterItem> param)
        {
            throw new NotImplementedException();
        }

        public T GetOne(string sql)
        {
            throw new NotImplementedException();
        }

        public List<T> GetList(string sql, List<Common.DbParameterItem> param)
        {
            throw new NotImplementedException();
        }

        public List<T> GetList(string sql)
        {
            throw new NotImplementedException();
        }
    }
}
