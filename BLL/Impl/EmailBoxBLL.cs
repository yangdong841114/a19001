﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;

namespace BLL.Impl
{
    public class EmailBoxBLL : IEmailBoxBLL
    {

        private System.Type type = typeof(EmailBox);

        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public PageResult<EmailBox> GetSendListPage(EmailBox model)
        {
            PageResult<EmailBox> page = new PageResult<EmailBox>();
            string sql = "select m.*,row_number() over(order by m.id desc) rownumber from EmailBox m where 1=1";
            string countSql = "select count(1) from EmailBox m where 1=1";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (model == null || ValidateUtils.CheckIntZero(model.fromUid)) { throw new ValidateException("查询参数不能为空 "); }
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.type) && model.type != 3)
                {
                    param.Add(new DbParameterItem("m.type", ConstUtil.EQ, model.type));
                }
                if (!ValidateUtils.CheckIntZero(model.fromUid))
                {
                    param.Add(new DbParameterItem("m.fromUid", ConstUtil.EQ, model.fromUid));
                }
                if (!ValidateUtils.CheckNull(model.title))
                {
                    param.Add(new DbParameterItem("m.title", ConstUtil.LIKE, model.title));
                }
                if (!ValidateUtils.CheckNull(model.toUser))
                {
                    param.Add(new DbParameterItem("m.toUser", ConstUtil.LIKE, model.toUser));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<EmailBox> list = new List<EmailBox>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((EmailBox)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public List<EmailBox> GetReceiveList(EmailBox model)
        {
            List<EmailBox> list = new List<EmailBox>();
            string sql = "select m.* from EmailBox m where (m.id =@id or m.fristId = @fristId) order by id desc";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (model == null || ValidateUtils.CheckIntZero(model.fristId)) { throw new ValidateException("查询参数不能为空 "); }

            param.Add(new DbParameterItem("id", ConstUtil.EQ, model.fristId));
            param.Add(new DbParameterItem("fristId", ConstUtil.EQ, model.fristId));


            DataTable dt = dao.GetList(sql, param, false);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    var listModel = (EmailBox)ReflectionUtil.GetModel(type, row);
                    DataTable dtType = dao.GetList("select name from DataDictionary where id = " + listModel.type + "");
                    listModel.typeName = dtType.Rows[0]["name"].ToString();
                    list.Add(listModel);
                }
            }
            return list;
        }
        public string GetFromUserName(int productId)
        {
            string sql = "select userId from Product where id = " + productId;
            DataTable dt = dao.GetList(sql);
            return dt.Rows[0]["userId"].ToString();
        }
        public PageResult<EmailBox> GetReceiveListPage(EmailBox model)
        {
            PageResult<EmailBox> page = new PageResult<EmailBox>();
            string sql = "select m.*,row_number() over(order by m.id desc) rownumber from EmailBox m where 1=1 ";
            string countSql = "select count(1) from EmailBox m where 1=1";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (model == null || ValidateUtils.CheckIntZero(model.toUid)) { throw new ValidateException("查询参数不能为空 "); }
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.type))
                {
                    param.Add(new DbParameterItem("m.type", ConstUtil.EQ, model.type));
                }
                if (!ValidateUtils.CheckIntZero(model.toUid))
                {
                    param.Add(new DbParameterItem("m.toUid", ConstUtil.EQ, model.toUid));
                }
                if (!ValidateUtils.CheckNull(model.title))
                {
                    param.Add(new DbParameterItem("m.title", ConstUtil.LIKE, model.title));
                }
                if (!ValidateUtils.CheckNull(model.fromUser))
                {
                    param.Add(new DbParameterItem("m.fromUser", ConstUtil.LIKE, model.fromUser));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<EmailBox> list = new List<EmailBox>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((EmailBox)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public int SaveSend(EmailBox model, Member current)
        {
            if (model == null) { throw new ValidateException("发件内容为空"); }
            if (ValidateUtils.CheckNull(model.toUser)) { throw new ValidateException("收件人为空"); }
            if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("主题为空"); }
            if (ValidateUtils.CheckNull(model.content)) { throw new ValidateException("内容为空"); }

            Member m = null;
            if (model.toUser == "平台" || model.toUser == "平臺" || model.toUser == "プラットフォーム")
            {
                m = memberBLL.GetModelById(1);
                model.toUser = m.userId;
                model.toUid = m.id;
            }
            else
            {
                m = memberBLL.GetModelByUserId(model.toUser);
            }

            //固定发送给管理员的userId,Member表id=1的记录
            Member admin = memberBLL.GetModelById(1);
            //如果是发给管理员，则判断toUser是否等于Member表id=1的记录的userId
            if (m == null || (m.isAdmin == 1 && m.id != admin.id)) { throw new ValidateException("收件人不存在"); }
            if (m.id == current.id) { throw new ValidateException("不能给自己发邮件"); }
            model.fromUid = current.id;
            model.fromUser = current.userId;
            model.toUid = m.id;
            model.isRead = 1;
            model.addTime = DateTime.Now;
            int c = dao.Save(model);
            //如果是发送给管理员，则是否需要发送短信
            if (m.id == admin.id)
            {
                MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_ADMIN_EMAIL);
                if (bf.flag == 1)
                {
                    //发送给会员短信
                    noticeBLL.SendMessage(bf.phone, bf.msg);
                }
            }
            return m.id.Value;
        }

        public string SaveAllSend(EmailBox model)
        {
            if (model == null) { throw new ValidateException("发件内容为空"); }
            if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("主题为空"); }
            if (ValidateUtils.CheckNull(model.content)) { throw new ValidateException("内容为空"); }
            //调用存储过程
            string pro = "sendAllEmail";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@title",SqlDbType.VarChar,100),
                  new SqlParameter("@type",SqlDbType.Int),
                  new SqlParameter("@content",SqlDbType.VarChar),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = model.title;
            p[1].Value = model.type;
            p[2].Value = model.content;
            p[3].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result != "success")
            {
                throw new ValidateException(result);
            }
            return result;
        }

        public int SaveReceive(EmailBox model, Member current)
        {
            if (model == null) { throw new ValidateException("发件内容为空"); }
            if (ValidateUtils.CheckNull(model.toUser)) { throw new ValidateException("收件人为空"); }
            if (ValidateUtils.CheckNull(model.title)) { throw new ValidateException("主题为空"); }
            if (ValidateUtils.CheckNull(model.content)) { throw new ValidateException("内容为空"); }
            Member m = memberBLL.GetModelByUserId(model.toUser);
            //固定发送给管理员的userId,Member表id=1的记录
            Member admin = memberBLL.GetModelById(1);

            //如果是发给管理员，则判断toUser是否等于Member表id=1的记录的userId
            if (m == null || (m.isAdmin == 1 && m.id != admin.id)) { throw new ValidateException("收件人不存在"); }
            if (m.id == current.id) { throw new ValidateException("不能给自己发邮件"); }
            model.fromUid = current.id;
            model.fromUser = current.userId;
            model.toUid = m.id;
            model.isRead = 1;
            model.addTime = DateTime.Now;

            int c = dao.Save(model);

            //如果是发送给管理员，则是否需要发送短信
            if (m.id == admin.id)
            {
                MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_ADMIN_EMAIL);
                if (bf.flag == 1)
                {
                    //发送给会员短信
                    noticeBLL.SendMessage(bf.phone, bf.msg);
                }
            }
            return m.id.Value;
        }

        public int DeleteSend(int id)
        {
            //发件箱是根据fromUid查询， 把对应记录的fromUid改为空即可
            string sql = "select * from EmailBox where id = " + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("邮件记录不存在"); }
            sql = "update EmailBox set fromUid = 0 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

        public int DeleteReceive(int id)
        {
            //收件箱是根据toUid查询， 把对应记录的toUid改为空即可
            string sql = "select * from EmailBox where id = " + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("邮件记录不存在"); }
            sql = "update EmailBox set toUid = 0 where id = " + id;
            return dao.ExecuteBySql(sql);
        }

        public int UpdateRead(int id)
        {
            return dao.ExecuteBySql("update EmailBox set isRead = 2,readTime = getDate() where id=" + id);
        }

    }
}
