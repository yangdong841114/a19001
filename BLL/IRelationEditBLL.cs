﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IRelationEditBLL : IBaseBLL<RelationEdit>
    {
        /// <summary>
        /// 保存修改推荐关系记录
        /// </summary>
        /// <param name="mb">实体对象</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        RelationEdit SaveRelationEdit(RelationEdit mb, Member current);
        RelationEdit SaveRelationEdit1(RelationEdit mb, Member current);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="fields">要查询的列</param>
        /// <returns></returns>
        PageResult<RelationEdit> GetListPage(RelationEdit model);
    }
}
