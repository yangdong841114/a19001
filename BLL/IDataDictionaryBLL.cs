﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IDataDictionaryBLL : IBaseBLL<DataDictionary>
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="da"></param>
        /// <returns></returns>
        PageResult<DataDictionary> GetListPage(DataDictionary da);

        /// <summary>
        /// 获取DataDictionary键值对列表
        /// key:type  value:List<DataDictionary>列表
        /// </summary>
        /// <returns></returns>
        Dictionary<string, List<DataDictionary>> GetAllToDictionary(string language);
        Dictionary<string, List<DataDictionary>> GetAllToDictionaryManage();
        /// <summary>
        /// 根据ID获取名称
        /// </summary>
        /// <param name="ids">可变参数，ID</param>
        /// <returns>key:id,value:name</returns>
        Dictionary<int, string> GetDictionaryName(params int[] ids);
    }
}
