﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 基础设置业务逻辑接口
    /// </summary>
    public interface IBaseSetBLL
    {
        //获取基础数据设置
        BaseSet GetModel();

        //修改基础数据设置
        int SaveModel(BaseSet model);

        //保存广告图片
        int SaveBanner(Banner bn);

        //获取所有广告图
        List<Banner> GetBannerList();
        List<Banner> GetBannerList(int str, int end);
        //获取id范围广告图

        //清空数据库
        int ClearDataBase();

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="path">备份文件存放的文件夹路径</param>
        /// <returns></returns>
        string BackUpDb(string path);

        /// <summary>
        /// 删除备份
        /// </summary>
        /// <param name="id">记录ID</param>
        /// <param name="path">存放的文件夹路径</param>
        /// <returns></returns>
        bool deleteBak(int id,string path);

        /// <summary>
        /// 分页查询数据库备份
        /// </summary>
        /// <returns></returns>
        PageResult<BackModel> GetBackModelListPage(BackModel model);
    }
}
