﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 账户托管业务逻辑接口
    /// </summary>
    public interface IDelegaMemberBLL
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<DelegaMember> GetListPage(DelegaMember model);

        /// <summary>
        /// 保存账户托管
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveDelegaMember(DelegaMember model);

        /// <summary>
        /// 删除账户托管
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);

        /// <summary>
        /// 子账户转入到主账户
        /// </summary>
        /// <param name="uid">主账户ID</param>
        /// <returns></returns>
        string SaveTransfer(int uid);

    }
}
