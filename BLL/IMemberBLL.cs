﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IMemberBLL : IBaseBLL<Member>
    {
        /// <summary>
        /// 开通会员
        /// </summary>
        /// <param name="uid">被开通点会员ID</param>
        /// <param name="flag">0：实单，1：空单</param>
        /// <param name="current">当前操作用户</param>
        /// <returns></returns>
        string OpenMember(int uid, int flag, Member current);

        /// <summary>
        /// 条件查询会员列表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="isJoinSql"></param>
        /// <returns></returns>
        List<Member> GetMemberList(string sql, List<Common.DbParameterItem> param, bool isJoinSql);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="mb">用户信息，帐号、密码</param>
        /// <param name="isMan">0：后台登录 1：前台登录，</param>
        /// <returns>响应对象</returns>
        ResponseDtoData loginUser(Member mb, int isMan);

        /// <summary>
        /// 根据ID查询用户
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        Member GetModelById(int memberId);

        /// <summary>
        /// 根据ID查询用户,并清除密码与密保问题
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        Member GetModelByIdNoPassWord(int memberId);

        /// <summary>
        /// 根据ID查询用户,并清除密码与密保问题、并查询会员账户信息
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        Member GetModelAndAccountNoPass(int memberId);

        /// <summary>
        /// 根据用户名查询用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Member GetModelByUserId(string userId);

        /// <summary>
        /// 根据用户名查询用户
        /// </summary>
        /// <param name="userId">用户编号</param>
        /// <param name="isAdmin">是否前台会员，0：前台会员，1：后台会员</param>
        /// <returns></returns>
        Member GetModelByUserId(string userId,int isAdmin);

        /// <summary>
        /// 注册会员
        /// </summary>
        /// <param name="mb">会员信息</param>
        /// <returns></returns>
        Member SaveMember(Member mb);

        /// <summary>
        /// 分页查询前台会员
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="fields">要查询的列</param>
        /// <returns></returns>
        PageResult<Member> GetMemberListPage(Member model,string fields);
        DataTable GetMemberListPageDataTable(Member model, string fields);
        /// <summary>
        /// 批量启用冻结用户
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        int UpdateLock(List<int> list,int isLock);


        /// <summary>
        /// 后台查询直接推图
        /// </summary>
        /// <param name="userId">查询条件</param>
        /// <returns></returns>
        List<Member> GetDirectTreeList(string userId);

        /// <summary>
        /// 前台查询直接推图，只能查询当前登录用户及下级节点
        /// </summary>
        /// <param name="userId">查询条件</param>
        /// <param name="model">当前登陆用户</param>
        /// <returns></returns>
        List<Member> GetDirectTreeList(string userId, Member current);

        /// <summary>
        /// 分页查询后台会员
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="current">当前登录用户</param>
        /// <returns></returns>
        PageResult<Member> GetAdminListPage(Member model,Member current);

        /// <summary>
        /// 保存管理员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveAdmin(Member model);

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteById(int id);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="memberId">会员ID</param>
        /// <param name="oldPass">原密码</param>
        /// <param name="newPass"新密码></param>
        /// <param name="flag">密码类型：1：登录密码，2：安全密码，3：交易密码</param>
        /// <returns>0:失败，其他：成功</returns>
        int UpdatePassword(int memberId,string oldPass, string newPass, int flag);

        /// <summary>
        /// 找回密码时修改密码
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        int UpdateForgetPass(Member m);

        /// <summary>
        /// 根据会员级别获取注册金额、注册单数
        /// </summary>
        /// <param name="ulevel"></param>
        /// <returns></returns>
        object[] GetRegMoneyAndNum(int ulevel);

        /// <summary>
        /// 找系谱图最左边那条线，做下面的会员
        /// </summary>
        /// <returns></returns>
        string GetLeftUserId(int uid);

        /// <summary>
        /// 修改会员
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        int UpdateMember(Member model, Member current);


        /// <summary>
        /// 获取进入注册页面时的默认加载数据
        /// </summary>
        /// <returns></returns>
        Dictionary<string, object> GetRegisterDefaultMsg(int uid);

        /// <summary>
        /// 检查用户编码是否存在
        /// </summary>
        /// <param name="userId">用户编码</param>
        /// <param name="flag">1:检查用户是否存在，2：检查接点人编码，3：检查推荐人编码，4，检查报单中心编码</param>
        /// <returns></returns>
        ResponseDtoData CheckUserId(string userId, int flag);

        /// <summary>
        /// 获取上线链路（系谱图、直推图）
        /// </summary>
        /// <param name="type">1：系谱图，2：直推图</param>
        /// <param name="uid"></param>
        /// <returns></returns>
        string GetParentLinkString(int type,int uid);


        /// <summary>
        /// APP手机版查询直推图
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        Member GetDirectTree(int uid, Member current, string language);


        Member GetDirectTree(int uid, Member current);

        /// <summary>
        /// 修改密保
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        int SaveChangeSecurityPass(Member m);


        /// <summary>
        /// 导出会员excel数据
        /// </summary>
        /// <param name="Type">会员类型</param>
        /// <returns></returns>
        DataTable GetMemberExcelList(int Type);
    }
}
