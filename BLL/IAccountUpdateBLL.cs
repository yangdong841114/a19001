﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 账户充值业务逻辑接口
    /// </summary>
    public interface IAccountUpdateBLL : IBaseBLL<AccountUpdate>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<AccountUpdate> GetListPage(AccountUpdate model);
        DataTable GetListPageDataTable(AccountUpdate model);

        /// <summary>
        /// 增加或减少会员账户
        /// </summary>
        /// <param name="model">实体</param>
        /// <param name="current">当前用户</param>
        /// <returns></returns>
        int UpdateMemberAcount(AccountUpdate model, Member current);

        /// <summary>
        /// 导出后台给会员充值记录excel
        /// </summary>
        /// <returns></returns>
        DataTable GetAccountUpdateExcel();
    }
}
