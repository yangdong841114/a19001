﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 短信通知业务逻辑接口
    /// </summary>
    public interface IMobileNoticeBLL
    {

        /// <summary>
        /// 获取短信通知map
        /// </summary>
        /// <returns></returns>
        Dictionary<string, MobileNotice> GetToDictionary();

        /// <summary>
        /// 根据code获取对象
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        MobileNotice GetModel(string code);

        /// <summary>
        /// 批量更新参数值
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        int UpdateList(List<MobileNotice> list);

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <param name="msg">信息</param>
        /// <returns></returns>
        string SendMessage(string phone, string msg);

        /// <summary>
        /// 群发短信
        /// </summary>
        /// <param name="phones"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        string SendMessage(List<string> phones, string msg);

    }
}
