﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// Info业务逻辑接口
    /// </summary>
    public interface IInfoBLL
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        PageResult<Info> GetListPage(Info model, string fields);
        List<Info> GetList(string where);
        List<Info> GetListTop10(string where);

        /// <summary>
        /// 根据ID获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Info GetModelById(int id);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Update(Info model, Member login);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Save(Info model, Member login);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);
        int DeletePl(List<int> list);

        /// <summary>
        /// 保存或更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveOrUpdate(Info model, Member login);

        /// <summary>
        /// 置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveTop(int id);

        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveCancelTop(int id);
        int Addllcount(int id);

    }
}
