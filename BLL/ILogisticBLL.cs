﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace BLL
{
    /// <summary>
    /// 订单管理业务逻辑接口
    /// </summary>
    public interface ILogisticBLL
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<Logistic> GetListPage(Logistic model);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<Logistic> GetList();


        /// <summary>
        /// 从接口同步数据
        /// </summary>
        /// <returns></returns>
        int Save();
    }
}
