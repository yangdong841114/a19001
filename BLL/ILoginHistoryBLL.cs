﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 登录日志业务逻辑接口
    /// </summary>
    public interface ILoginHistoryBLL
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<LoginHistory> GetListPage(LoginHistory model, Member current);

        /// <summary>
        /// 保存登录日志
        /// </summary>
        /// <param name="lg"></param>
        /// <returns></returns>
        int Save(LoginHistory lg);

    }
}
