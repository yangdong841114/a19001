
define(['text!UEpMyOrderDetail.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UEpMyOrderDetail, $) {

    var controller = function (sid) {

        var statusDto = { 0: "待买家付款", 1: "待卖家收款", 2: "已完成", 3: "已取消" };

        appView.html(UEpMyOrderDetail);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化下拉框
        $("#flag").empty();
        $("#flag").append("<option value='-1'>--全部--</option>");
        $("#flag").append("<option value='0'>待买家付款</option>");
        $("#flag").append("<option value='1'>待卖家收款</option>");
        $("#flag").append("<option value='2'>已完成</option>");
        $("#flag").append("<option value='3'>已取消</option>");

        //初始化表格
        var grid = utils.newGrid("dg", {
            title: '交易明细',
            showFooter: true,
            columns: [[
                { field: 'number', title: '买单单号', width: '18%' },
                { field: 'snumber', title: '挂卖单号', width: '18%' },
                { field: 'userId', title: '买家编号', width: '10%' },
                { field: 'buyNum', title: '购买数量', width: '10%' },
                { field: 'payMoney', title: '应付金额', width: '10%' },
                { field: 'addTime', title: '购买日期', width: '12%' },
                { field: 'status', title: '状态', width: '10%' },
                {
                    field: '_operate', title: '操作', width: '70', align: 'center', formatter: function (val, row, index) {
                        if (row.flag == 1) {
                        return '&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >确认收款</a>';
                        } else {
                            return '';
                        }
                    }
                }
            ]],
            url: "UEpMyOrderDetail/GetListPage?sid=" + sid
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["status"] = statusDto[data.rows[i]["flag"]];
                }
            }
            return data;
        }, function () {
            //行确认收款
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("您确定已收到款项吗？", function () {
                        utils.AjaxPost("UEpMyOrderDetail/PaySure", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();

                            }
                        });
                    });
                }
            });
          
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        });

        //返回按钮
        $("#backBtn").on("click", function () {
            history.go(-1);
        })


        controller.onRouteChange = function () {
        };
    };

    return controller;
});