
define(['text!UShoppingCart.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UShoppingCart, $) {

    var controller = function (name) {

        //计算总金额
        calTotalSumt = function () {
            var total = 0;
            $(".ppppCheckBox").each(function () {
                var dataId = $(this).attr("dataId"); //id 
                if (this.checked) {
                    total = total + parseFloat($("#totalPrice"+dataId).html());
                }
            });
            $("#submitMoney").html(total);
        }

        var g = /^\d+(\.{0,1}\d+){0,1}$/;

        //初始化省、市、区下拉框
        initProvince = function (dto) {
            $("#cityName").empty();
            $("#cityName").append("<option value='0'>--请选择--</option>");
            $("#areaName").empty();
            //$("#areaName").append("<option value='0' selected='true'>--请选择--</option>");
            $("#provinceName").empty();
            $("#provinceName").append("<option value='0'>--请选择--</option>");
            if (areaData && areaData.length > 0) {
                for (var i = 0; i < areaData.length; i++) {
                    var pro = areaData[i];
                    $("#provinceName").append("<option value='" + pro.value + "' dataId='" + pro.id + "' >" + pro.value + "</option>");
                    if (dto.province == pro.value) {
                        var childs = pro.childs;
                        if (childs && childs.length > 0) {
                            for (var j = 0; j < childs.length; j++) {
                                var child = childs[j];
                                $("#cityName").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                                if (dto.city == child.value) {
                                    var grandchilds = child.childs;
                                    if (grandchilds && grandchilds.length > 0) {
                                        for (var k = 0; k < grandchilds.length; k++) {
                                            var grandchild = grandchilds[k];
                                            $("#areaName").append("<option value='" + grandchild.value + "'>" + grandchild.value + "</option>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };

        utils.AjaxPostNotLoadding("UShoppingCart/GetList", { }, function (result) {
            appView.html(UShoppingCart);
            if (result.status == "success") {
                var map = result.map;

                //默认收货信息
                var memberInfo = map.memberInfo;
                $("#receiptName").val(memberInfo.userName);
                $("#phone").val(memberInfo.phone);
                $("#address").val(memberInfo.address);
                initProvince(memberInfo);
                $("#provinceName").val(memberInfo.province);
                $("#cityName").val(memberInfo.city);
                $("#areaName").val(memberInfo.area);

                //省改变事件
                $("#provinceName").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#cityName").empty();
                    $("#cityName").append("<option value='0'>--请选择--</option>");
                    $("#areaName").empty();
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#cityName").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                });

                //市改变事件
                $("#cityName").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#areaName").empty();
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#areaName").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                })

                var list = map.list; //购物车信息
                if (list && list.length > 0) {
                    var cont = '';
                    for (var i = 0; i < list.length; i++) {
                        var pro = list[i];
                        var checked = pro.isCheck == 1 ? '' : 'checked="checked"';
                        cont += '<div class="row" id="div' + pro.id + '"><div class="col-sm-12"><table ><tr><td>商品编码：</td><td>' + pro.productCode + '</td><td style="padding-left:50px;">商品名称：</td>' +
                               '<td>' + pro.productName + '</td></tr></table></div><div class="col-sm-1" style="padding-top:50px;text-align:center;">'+
                               '<input style="height:20px;width:20px; cursor:pointer;" dataId="' + pro.id + '" ' + checked + ' type="checkbox" class="ppppCheckBox" /></div>' +
                               '<div class="col-sm-2" style="padding-right:0px;padding-top:5px;">' +
                               '<img data-toggle="lightbox"  src="' + pro.imgUrl + '" data-image="' + pro.imgUrl + '" class="img-thumbnail" alt="" style="width:225px;height:130px"></div>' +
                               '<div class="col-sm-6" style="padding-top:10px;"><table class="table table-borderless"><tr><td align="right" style="padding-top:15px;">市场价：</td>' +
                               '<td id="fxPrice' + pro.id + '" style="color: red; padding-top: 15px;">' + pro.price + '</td><td colspan="4" align="right">' +
                               '<button type="button" class="btn btn-danger delBtn" dataId="' + pro.id + '"> 删除 </button></td></tr>' +
                               '<tr><td align="right" style="padding-top:15px;">购买数量：</td>' +
                               '<td align="right"><button type="button" class="btn btn-primary subBtn" dataId="' + pro.id + '"> - </button></td><td width="80px;">' +
                               '<input type="text" class="form-control numtext" id="buyNum' + pro.id + '" dataId="' + pro.id + '" dataValue="' + pro.num + '" style="width:80px;" value="' + pro.num + '" /></td>' +
                               '<td align="left"><button type="button" class="btn btn-primary addBtn" dataId="' + pro.id + '"> + </button></td>' +
                               '<td align="right" style="padding-top:15px;">总金额：</td>' +
                               '<td align="left" id="totalPrice' + pro.id + '" style="color: red; padding-top: 15px;">' + pro.totalPrice + '</td>' +
                               '</tr></table></div></div><hr/>';
                    }
                    $("#content").html(cont);

                    //图片控件
                    $(".img-thumbnail").each(function (index, ele) {
                        $(this).lightbox();
                    })

                    //计算总金额
                    calTotalSumt();

                    //全选按钮
                    $("#checkAll").on("click", function () {
                        var checked = $("#checkAll")[0].checked;
                        var hasBox = false;
                        $(".ppppCheckBox").each(function () {
                            this.checked = checked;
                            hasBox = true;
                        });
                        if (hasBox) {
                            var isChecked = checked ? 2 : 1;
                            utils.AjaxPost("UShoppingCart/SaveCheckAll", { isChecked: isChecked }, function (result) {
                                if (result.status == "success") {
                                    calTotalSumt();
                                }
                            });
                        }
                    });

                    //清空购物车
                    $("#clearBtn").on("click", function () {
                        var hasBox = false;
                        $(".ppppCheckBox").each(function () {
                            this.checked = checked;
                            hasBox = true;
                        });
                        if (hasBox) {
                            utils.confirm("您确定要清空购物车吗？", function () {
                                utils.AjaxPost("UShoppingCart/DeleteAll", {}, function (result) {
                                    if (result.status == "success") {
                                        $("#content").remove()
                                        calTotalSumt();
                                        utils.showSuccessMsg("清空成功！");
                                    }
                                });
                            });
                        }
                    })

                    //删除按钮
                    $(".delBtn").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId");
                            utils.confirm("确定把该商品从购物车删除吗？", function () {
                                utils.AjaxPost("UShoppingCart/Delete", { id: dataId }, function (result) {
                                    if (result.status == "success") {
                                        $("#div" + dataId).remove()
                                        calTotalSumt();
                                        utils.showSuccessMsg("删除成功！");
                                    }
                                });
                            });
                        })
                    });

                    //减少数量按钮
                    $(".subBtn").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var old = numDom.val();
                            if (!g.test(old)) return; //不是数字
                            if (old <= 1) return;
                            var num = old - 1;
                            var data = { id: dataId, num:num };
                            utils.AjaxPost("UShoppingCart/Update", data, function (result) {
                                if (result.status == "success") {
                                    var price = $("#fxPrice" + dataId).html();
                                    numDom.val(num);
                                    numDom.attr("dataValue", num);
                                    $("#totalPrice" + dataId).html(price * num);
                                    calTotalSumt();
                                }
                            });
                        })
                    });

                    //增加数量按钮
                    $(".addBtn").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var old = numDom.val();
                            if (!g.test(old)) return; //不是数字
                            if (old <= 0) return;
                            var num = parseInt(old) + 1;
                            var data = { id: dataId, num: num };
                            utils.AjaxPost("UShoppingCart/Update", data, function (result) {
                                if (result.status == "success") {
                                    var price = $("#fxPrice" + dataId).html();
                                    numDom.val(num)
                                    numDom.attr("dataValue", num);
                                    $("#totalPrice" + dataId).html(price * num);
                                    calTotalSumt();
                                }
                            });
                        })
                    });

                    //改变数量离开焦点
                    $(".numtext").each(function () {
                        var dom = $(this);
                        dom.on("blur", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var num = numDom.val();
                            //如果是错误格式或小于等于0，自动改回来
                            if (!g.test(num) || parseInt(num) <= 0) {
                                numDom.val(numDom.attr("dataValue"));
                            } else {
                                var data = { id: dataId, num: num };
                                utils.AjaxPost("UShoppingCart/Update", data, function (result) {
                                    if (result.status == "success") {
                                        var price = $("#fxPrice" + dataId).html();
                                        $("#buyNum" + dataId).val(num)
                                        $("#buyNum" + dataId).attr("dataValue", num);
                                        $("#totalPrice" + dataId).html(price * num);
                                        calTotalSumt();
                                    }
                                });
                            }
                        })
                    });

                    //CheckBox点击事件
                    $(".ppppCheckBox").each(function () {
                        var dom = $(this);
                        var dataId = dom.attr("dataId"); //id 
                        dom.on("click", function () {
                            var isChecked = dom[0].checked ? 2 : 1;
                            var data = { id: dataId, isChecked: isChecked };
                            utils.AjaxPost("UShoppingCart/SaveChecked", data, function (result) {
                                if (result.status == "success") {
                          
                                    calTotalSumt();
                                }
                            });
                        });
                    });

                    //获得焦点
                    $(".form-control").each(function (index, ele) {
                        var current = $(this);
                        var parent = current.parent();
                        current.on("focus", function (event) {
                            parent.removeClass("has-error");
                            utils.destoryPopover(current);
                        })
                    })

                    //提交订单
                    $("#saveBtn").on("click", function () {
                        var ids = [];
                        $(".ppppCheckBox").each(function () {
                            var box = $(this);
                            if (box[0].checked) {
                                ids.push(box.attr("dataId"));
                            }
                        });
                        if (ids && ids.length > 0) {
                            var isChecked = true;
                            if ($("#receiptName").val() == 0) {
                                var current = $("#receiptName");
                                var parent = current.parent();
                                parent.addClass("has-error");
                                utils.showPopover(current, "请填写收货人", "popover-danger");
                                isChecked = false;
                            }
                            if ($("#phone").val() == 0) {
                                var current = $("#phone");
                                var parent = current.parent();
                                parent.addClass("has-error");
                                utils.showPopover(current, "请填写联系电话", "popover-danger");
                                isChecked = false;
                            }
                            if ($("#provinceName").val() == 0) {
                                var current = $("#provinceName");
                                var parent = current.parent();
                                parent.addClass("has-error");
                                utils.showPopover(current, "请选择省", "popover-danger");
                                isChecked = false;
                            }
                            if ($("#cityName").val() == 0) {
                                var current = $("#cityName");
                                var parent = current.parent();
                                parent.addClass("has-error");
                                utils.showPopover(current, "请选择市", "popover-danger");
                                isChecked = false;
                            }
                            if ($("#address").val() == 0) {
                                var current = $("#address");
                                var parent = current.parent();
                                parent.addClass("has-error");
                                utils.showPopover(current, "请填收货地址", "popover-danger");
                                isChecked = false;
                            }
                            if (isChecked) {
                                utils.confirm("确定要提交订单吗？", function () {
                                    var data = {
                                        receiptName: $("#receiptName").val(), phone: $("#phone").val(), address: $("#address").val(),
                                        provinceName: $("#provinceName").val(), cityName: $("#cityName").val(), areaName: $("#areaName").val(),
                                    };
                                    utils.AjaxPost("UShoppingCart/SaveOrder", data, function (result) {
                                        if (result.status == "success") {
                                            utils.showSuccessMsg("提交成功！");
                                            location.href = "#UOrder";
                                        } else {
                                            utils.showErrMsg(result.msg);
                                        }
                                    });
                                });
                            }
                        } else {
                            utils.showErrMsg("您还未选择任何商品");
                        }
                    })
                }

            } else {
                utils.showErrMsg(result.msg);
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});