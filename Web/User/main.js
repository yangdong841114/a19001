
define(['text!main.html', 'jquery', 'j_easyui', 'zui'], function (main, $) {

    var controller = function (name) {
        ////设置标题
        //$("#center").panel("setTitle","会员注册");

        ShowTakeCash = function () {
            if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "") {
                utils.showErrMsg("请先完善银行信息！");
            } else {
                location.href = "#UTakeCash";
            }
        }

        //截取字符串
        sub = function (str, n) {
            var r = /[^\x00-\xff]/g;
            if (str.replace(r, "mm").length <= n) { return str; }
            var m = Math.floor(n / 2);
            for (var i = m; i < str.length; i++) {
                if (str.substr(0, i).replace(r, "mm").length >= n) {
                    return str.substr(0, i) + "...";
                }
            }
            return str;
        }


        utils.AjaxPostNotLoadding("UserWeb/GetMainData", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                var map = result.map;
                if (map) {
                    var user = map.user;
                    var account = map.account;
                    $("#agentDz").html(account.agentDz.toFixed(2));        //点值
                    $("#agentJj").html(account.agentJj.toFixed(2));        //奖金币
                    $("#agentGw").html(account.agentGw.toFixed(2));        //购物币
                    $("#agentFt").html(account.agentFt.toFixed(2));        //复投币
                    $("#agentTotal").html(account.agentTotal.toFixed(2));  //累计奖金
                    $("#userId").html(user.userId);
                    $("#userId2").html(user.userId);
                    $("#uLevel").html(cacheMap["ulevel"][user.uLevel]);
                    $("#rLevel").html(cacheMap["rLevel"][user.rLevel]);
                    $("#bankName").val(user.bankName);
                    $("#bankCard").val(user.bankCard);
                    $("#bankUser").val(user.bankUser);
                    $("#bankAddress").val(user.bankAddress);
                    var baseset = map.baseSet;
                    //广告图
                    var banners = map.banners;
                    if(banners && banners.length>0){
                        var banHtml = "";
                        for(var i=0;i<banners.length;i++){
                            banHtml += '<li style="background:url('+banners[i].imgUrl+') no-repeat center;"></li>';
                        }
                        $("#slides").html(banHtml);
                    }
                    //客服
                    var kf = "";
                    kf += '<li><a target="_blank" href="http:\//wpa.qq.com/msgrd?v=3&uin=' + baseset.qq1 + '&site=qq&menu=yes"><img border="0" src="/Content/images/qq.png"><span>客服<br>'+baseset.qName1+'</span></a></li>';
                    kf += '<li><a target="_blank" href="http:\//wpa.qq.com/msgrd?v=3&uin=' + baseset.qq2 + '&site=qq&menu=yes"><img border="0" src="/Content/images/qq.png"><span>客服<br>' + baseset.qName2 + '</span></a></li>';
                    kf += '<li><a target="_blank" href="http:\//wpa.qq.com/msgrd?v=3&uin=' + baseset.qq3 + '&site=qq&menu=yes"><img border="0" src="/Content/images/qq.png"><span>客服<br>' + baseset.qName3 + '</span></a></li>';
                    kf += '<li><a target="_blank" href="http:\//wpa.qq.com/msgrd?v=3&uin=' + baseset.qq4 + '&site=qq&menu=yes"><img border="0" src="/Content/images/qq.png"><span>客服<br>' + baseset.qName4 + '</span></a></li>';
                    $("#serviceQQ").html(kf);

                    //文章
                    var newsList = map.newsList;
                    var hh = "";
                    if (newsList && newsList.length > 0) {
                        for (var i = 0; i < newsList.length; i++) {
                            var n = newsList[i];
                            hh += '<li><a href="#UArticleDetail/'+n.id+'" title="'+ n.title +'">' + sub(n.title,43) + '</a><span>' + utils.changeDateFormat(n.addTime) + '</span></li>';
                        }
                        $("#newsItem").html(hh);
                    }

                    //推广链接
                    var siteUrl = map.siteUrl;
                    $("#siteUrl").attr("href", siteUrl);
                    $("#siteUrl").html(siteUrl);

                    //复制链接
                    var clip = new ZeroClipboard(document.getElementById("copyUrl"));

                    //二维码
                    var qrcode = map.qrcode;
                    $("#qrcode").attr("src", "/UpLoad/qrcode/" + qrcode);
                }

                //滚动广告图
                $(".mainbanner").flexslider({
                    slideshowSpeed: 3500,
                    directionNav: false,
                    pauseOnAction: false,
                    animation: "fade",
                    controlNav: false
                });


            } else {
                utils.showErrMsg(result.msg);
            }
        });

        
        
        

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});