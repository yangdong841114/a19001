﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class CourierResult<T>
    {
        public string resultcode { get; set; }
        public string reason { get; set; }
        public List<T> result { get; set; }
        public int error_code { get; set; }
    }
}