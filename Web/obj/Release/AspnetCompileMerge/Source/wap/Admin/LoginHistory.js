
define(['text!LoginHistory.html', 'jquery'], function (LoginHistory, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("操作日志")
        appView.html(LoginHistory);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var flagObj = { "0": "成功", "1": "失败（帐号不存在）", "2": "失败（密码验证失败）", "3": "失败（帐号未开通）", "4": "失败（帐号被冻结）", "5": "失败（帐号被登录锁定）" };
        var flagList = [{ id: "", name: "全部" },{ id: "0", name: "成功" }, { id: "1", name: "失败（帐号不存在）" }, { id: "2", name: "失败（密码验证失败）" },
                        { id: "3", name: "失败（帐号未开通）" }, { id: "4", name: "失败（帐号被冻结）" }, { id: "5", name: "失败（帐号被登录锁定）" }];

        //初始化查询区select
        utils.InitMobileSelect('#flagName', '请选择状态', flagList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#LoginHistorydatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/LoginHistory/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["loginTime"] = utils.changeDateFormat(rows[i]["loginTime"]);
                            rows[i]["flag"] = flagObj[rows[i]["flag"]];

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)" style="overflow: hidden"><time>' + dto.loginTime + '</time><span class="sum">' + dto.userId + '</span>';
                            if (dto.flag == "成功") {
                                html += '<span class="ship">' + dto.flag + '</span>';
                            } else {
                                html += '<span class="noship">' + dto.flag + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>登录帐号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>IP</dt><dd>' + dto.loginIp + '</dd></dl><dl><dt>状态</dt><dd>' + dto.flag + '</dd></dl>' +
                            '<dl><dt>是否前台</dt><dd>' + dto.isMan + '</dd></dl><dl><dt>登录时间</dt><dd>' + dto.loginTime + '</dd></dl>' +
                            '<dl><dt>描述</dt><dd>' + dto.mulx + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#LoginHistoryitemList").append(html);
                    }, function () {
                        $("#LoginHistoryitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#LoginHistoryitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userId"] = $("#userId").val();
            param["flag"] = $("#flag").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});