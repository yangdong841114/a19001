
define(['text!DirectTree.html', 'jquery'], function (DirectTree, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("直推图");
        
        //展开
        openAndClose = function (id, recount) {
            if (recount > 0 && !$("#cont" + id).attr("isload")) {
                utils.AjaxPost("/Admin/DirectTree/GetAppDirectTree", { uid: id }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        var dto = result.result;
                        var childs = dto.reList;
                        var html = "";
                        if (childs && childs.length > 0) {
                            html += "<dl>";
                            for (var i = 0; i < childs.length; i++) {
                                var child = childs[i];
                                html += '<dt><a id="item' + child.id + '" href="javascript:openAndClose(\'' + child.id + '\',' + child.reCount + ');">';
                                if (child.reCount > 0) {
                                    html += '<span class="adult"></span>';
                                } else {
                                    html += '<span></span>';
                                }
                                html += '<i class="fa fa-user-circle-o"></i>' + child.dtreeName + '</a></dt><dd id="cont' + child.id + '">';
                                html += '</dd>'
                            }
                            html += "</dl>";
                            $("#cont" + id).html(html);
                        }
                        $("#item" + id).parent().toggleClass("bg");
                        $("#item" + id).parent().next("dd").slideToggle();
                        $("#cont" + id).attr("isload", "true");
                    }
                });
            } else {
                $("#item" + id).parent().toggleClass("bg");
                $("#item" + id).parent().next("dd").slideToggle();
            }
        }

        //初始化
        initTree = function (dto) {
            $("#usertree").empty();
            var childs = dto.reList;
            var html = '<dl><dt><a id="item' + dto.id + '" href="javascript:openAndClose(\'' + dto.id + '\',' + dto.reCount + ');">'
            if (dto.reCount > 0) {
                html += '<span class="adult"></span>';
            }
            html += '<i class="fa fa-user-circle-o"></i>' + dto.dtreeName + '</a></dt><dd id="cont' + dto.id + '" isload="true">';
            if (childs && childs.length > 0) {
                html += "<dl>";
                for (var i = 0; i < childs.length; i++) {
                    var child = childs[i];
                    html += '<dt><a id="item' + child.id + '" href="javascript:openAndClose(\'' + child.id + '\',' + child.reCount + ');">';
                    if (child.reCount > 0) {
                        html += '<span class="adult"></span>';
                    }
                    html += '<i class="fa fa-user-circle-o"></i>' + child.dtreeName + '</a></dt><dd id="cont' + child.id + '">';
                    html += '</dd>'
                }
                html += "</dl>";
            }
            html += '</dd></dl>'
            $("#usertree").html(html);
        }

        //初始化加载数据
        utils.AjaxPostNotLoadding("/Admin/DirectTree/GetAppDirectTree", { uid: 1 }, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(DirectTree);
                var dto = result.result;
                initTree(dto);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    utils.AjaxPost("/Admin/DirectTree/GetAppDirectTreeByUserId", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            if (result.msg == "none") {
                                $("#usertree").empty();
                            } else {
                                var dto = result.result;
                                initTree(dto);
                                $("#other").empty();
                                if (result.other && result.other.length > 0) {
                                    $("#other").html("上线链路：<br/>" + result.other);
                                }
                            }
                        }
                    });
                })

                //我的直推图
                $("#selfBtn").on("click", function () {
                    utils.AjaxPost("/Admin/DirectTree/GetAppDirectTree", { uid: 1 }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            var dto = result.result;
                            initTree(dto);
                            $("#other").empty();
                            if (result.other && result.other.length > 0) {
                                $("#other").html("上线链路：<br/>" + result.other);
                            }
                        }
                    });
                })
            }

        });


        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});