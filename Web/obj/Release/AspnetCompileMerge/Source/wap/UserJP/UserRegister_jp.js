
define(['text!UserRegister.html', 'jquery'], function (UserRegister, $) {

    var controller = function (agm) {

        //設置標題
        $("#title").html("会員登録");
        appView.html(UserRegister);

        var pmap = null;

        //初始化默認數據
        setDefaultData = function (map) {

            if (map.loginPass && map.loginPass != null) {
                $("#password").val(map.loginPass);
                $("#passwordRe").val(map.loginPass);
                $("#dfpass1").html(map.loginPass);
            }
            if (map.passOpen && map.passOpen != null) {
                $("#passOpen").val(map.passOpen);
                $("#passOpenRe").val(map.passOpen);
                $("#dfpass2").html(map.passOpen);
            }
            if (map.threePass && map.threePass != null) {
                $("#threepass").val(map.threePass);
                $("#threepassRe").val(map.threePass);
                $("#dfpass3").html(map.threePass);
            }
            if (map.fatherName && map.fatherName != null) {
                $("#fatherName").val(map.fatherName);
            }
            if (map.defaultReName && map.defaultReName != null) {
                $("#reName").val(map.defaultReName);
            }
            if (map.zcxy && map.zcxy != null) {
                $("#regContent").html(map.zcxy);
            }

            if (map.userIdPrefix && map.userIdPrefix != null) {
                $("#qzUserId").html(map.userIdPrefix);
            }
        }

        var data = { uid: 0 };
        var treePlace = undefined;
        //系譜圖參數， userId+treeplace,最後壹位為系譜圖區域
        if (agm) {
            var fuid = agm.substring(0, agm.length - 1);
            treePlace = agm.substring(agm.length - 1, agm.length);
            treePlace = treePlace == 0 ? -1 : treePlace;
            data = { uid: fuid };
        }

        //第壹步到第二步
        step1To2 = function () {
            if ($("#userId").val() != 0) {
                var usercode = utils.CheckSpecialCharacters($("#userId").val());
                $("#userId").val(usercode);
            }
            if ($("#userId").val() == 0) {
                utils.showErrMsg("会員番号を入力してください");
                $("#userId").focus();
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("ログインパスワードを入力してください");
                $("#password").focus();
            } else if ($("#passOpen").val() == 0) {
                utils.showErrMsg("セキュリティパスワードを入力してください");
                $("#passOpen").focus();
            } else if ($("#threepass").val() == 0) {
                utils.showErrMsg("取引のパスワードを入力してください");
                $("#threepass").focus();
            } else if ($("#passwordRe").val() != $("#password").val()) {
                utils.showErrMsg("ログインパスワードとログインパスワードが違っていることを確認します");
                $("#passwordRe").focus();
            } else if ($("#passOpenRe").val() != $("#passOpen").val()) {
                utils.showErrMsg("セキュリティパスワードとセキュリティパスワードが違っていることを確認します");
                $("#passOpenRe").focus();
            } else if ($("#threepassRe").val() != $("#threepass").val()) {
                utils.showErrMsg("取引のパスワードと取引のパスワードが違っていることを確認します");
                $("#threepassRe").focus();
            } else {
                $("#setpTab1").css("display", "none");
                $("#setpTab2").css("display", "block");
            }
        }

        //第二步返回第壹步
        back2To1 = function () {
            $("#setpTab2").css("display", "none");
            $("#setpTab1").css("display", "block");
        }

        //第二步到第三步
        step2To3 = function () {
            if ($("#fatherName").val() == 0) {
                utils.showErrMsg("接点人番号を入力してください");
                $("#fatherName").focus();
            } else if ($("#reName").val() == 0) {
                utils.showErrMsg("推薦者番号を入力してください");
                $("#reName").focus();
            } else {
                $("#setpTab2").css("display", "none");
                $("#setpTab3").css("display", "block");
            }
        }

        //第三步返回第二步
        back3To2 = function () {
            $("#setpTab3").css("display", "none");
            $("#setpTab2").css("display", "block");
        }

        //打開註冊協議
        openRegisterWin = function (index) {
            $("#zcxydiv").css("display", "block");
            $("#setpTab2").css("display", "none");
        }

        //同意並註冊
        agreeRegister = function (ind) {
            $("#zcxydiv").css("display", "none");
            $("#setpTab2").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        var indexChecked = [0, 0, 0];

        ////打開時確認選中數據
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        ////選擇確認
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //選擇左右區
        checkedTreePlace = function (index) {
            $("#treePlace1").removeClass("active");
            $("#treePlace2").removeClass("active");
            if (index == -1) {
                $("#treePlace1").addClass("active");
            } else {
                $("#treePlace2").addClass("active");
            }
        }

        //查詢參數
        this.param = utils.getPageData();

        var productInit = false;

        var re = /^[0-9]+$/;

        //減少數量
        subNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val) && parseInt(val) > 0) {
                $("#buyNum" + id).val(parseInt(val) - 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) - 1);
                calTotalMoney();
            }
        }

        //增加數量
        addNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val)) {
                $("#buyNum" + id).val(parseInt(val) + 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) + 1);
                calTotalMoney();
            }
        }

        //錄入數量
        changeBuyNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (!re.test(val)) {
                $("#buyNum" + id).val($("#buyNum" + id).attr("dataVal"));
            } else {
                calTotalMoney();
            }
        }

        //計算總金額
        calTotalMoney = function () {
            var total = 0;
            $(".productBox").each(function () {
                if (this.checked) {
                    var id = $(this).attr("dataId");
                    var num = $("#buyNum" + id).val();
                    if (re.test(num)) {
                        num = parseInt(num);
                        var price = parseFloat($(this).attr("dataPrice"));
                        total = total + (num * price);
                    }
                }
            });
            $("#totalMoney").html(total);
        }

        //商品勾選的上壹步
        prevProduct = function () {
            $("#productSelectDiv").css("display", "none");
            $("#setpTab3").css("display", "block");
        }

        //下壹步，商品勾選
        netProduct = function () {
            var checked = true;
            //數據校驗
            $("#setpTab3 input").each(function (index, ele) {
                var jdom = $(this);
                if (jdom.attr("emptymsg") && jdom.val() == 0) {
                    utils.showErrMsg(jdom.attr("emptymsg"));
                    jdom.focus();
                    checked = false;
                }
                if (!checked) { return false; }
            });
            if (checked) {
                //手機號碼驗證
                var ptext = /^1(3|4|5|7|8)\d{9}$/;
                 if ($("#read")[0].checked == false) {
                     utils.showErrMsg("登録プロトコルにチェックしてください");
                } else {
                    $("#setpTab3").css("display", "none");
                    $("#productSelectDiv").css("display", "block");
                    scrollTo(0, 0);
                    if (!productInit) {
                        var dropload = $('#productData').dropload({
                            scrollArea: window,
                            domDown: { domNoData: '<p class="dropload-noData"></p>' },
                            loadDownFn: function (me) {
                                utils.LoadPageData("/User/UserRegister/GetProductListPage", param, me,
                                    function (rows, footers) {
                                        var html = "";
                                        for (var i = 0; i < rows.length; i++) {
                                            var dto = rows[i];
                                            html += '<dl><dt style="vertical-align:middle;"><table style="float:left;"><tr>' +
                                                  '<td><input type="checkbox" onclick="calTotalMoney()" dataName="' + dto.productName + '" dataId="' + dto.id + '" dataPrice="'
                                                  + dto.price + '" class="productBox" style="float:left;width:2rem;height:2rem;" />&nbsp;</td>' +
                                                  '<td><img src="' + dto.imgUrl + '" /></td></tr></table></dt>' +
                                                  '<dd><div class="cartcomminfo"><dl><dt></dt><dd><span>' + dto.productCode + '</span></dd></dl>' +
                                                  '<h2>' + dto.productName + '</h2><h3>¥' + dto.price + '</h3><dl><dt></dt>' +
                                                  '<dd><button class="btnminus" onclick=\'subNum("' + dto.id + '")\'><i class="fa fa-minus"></i></button>' +
                                                  '<input type="number" onblur=\'changeBuyNum("' + dto.id + '")\' dataVal="0" class="txtamount" id="buyNum' + dto.id + '" value="0" />' +
                                                  '<button class="btnplus" onclick=\'addNum("' + dto.id + '")\'><i class="fa fa-plus"></i></button></dd>' +
                                                  '</dl></div></dd></dl>';
                                        }
                                        $("#productDataList").append(html);
                                    }, function () {
                                        $("#productDataList").append('<p class="dropload-noData">データがありません</p>');
                                    });
                            }
                        });

                        productInit = true;
                    }
                }// end - else
            }
        }

        //提交註冊按鈕
        $("#saveBtn").bind("click", function () {
            var fs = {};
            $("#setpTab1 input").each(function () {
                fs[this.id] = this.value;
            });
            $("#setpTab2 input").each(function () {
                fs[this.id] = this.value;
            });

            if ($("#read")[0].checked) { fs["read"] = "read"; }
            var checked2 = true;


            if (checked2) {

                if ($("#treePlace1").hasClass("active")) { fs["treePlace"] = -1; }
                else { fs["treePlace"] = 1; }
                fs["userId"] = $("#qzUserId").html() + "" + fs["userId"];
                fs["sourceMachine"] = "app";
                utils.AjaxPost("/User/UserRegister/RegisterMember", fs, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        var mm = result.result;
                        regSuccess = mm;
                        location.href = "#UregSuccess";
                    }
                });

            }
        });

        //加載默認數據
        utils.AjaxPostNotLoadding("/User/UserRegister/GetDefaultData", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                pmap = result.map;
                setDefaultData(pmap);

                //從系譜圖註冊的默認區域
                if (treePlace) {
                    checkedTreePlace(treePlace);
                }

                ////初始化銀行帳號下拉框
                //var bankLit = cacheList["UserBank"];
                //utils.InitMobileSelect('#bankName', '開戶行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                //    $("#bankName").val(data[0].name);
                //});

                //省市區選擇
                var proSet = utils.InitMobileSelect('#province', '都道府県を選択する', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '都道府県を選択する', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '都道府県を選択する', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        } else if (prev[0].id == "reName" || prev[0].id == "shopName" || prev[0].id == "fatherName") {
                            $("#" + prev[0].id + "" + prev.attr("checkflag")).empty();
                        }
                        dom.prev().val("");
                    });
                });

                //綁定獲離開焦點事件
                $("input").each(function (index, ele) {
                    var dom = $(this);
                    if (dom.attr("checkflag")) {
                        var flag = dom.attr("checkflag");
                        var domId = dom[0].id;
                        dom.bind("blur", function (event) {
                            if (dom.val() && dom.val() != 0) {
                                var userId = dom.val();
                                utils.AjaxPostNotLoadding("/User/UserRegister/CheckUserId", { userId: userId, flag: flag }, function (result) {
                                    if (result.status == "fail") {
                                        $("#" + domId + "" + flag).html(result.msg);
                                    } else {
                                        $("#" + domId + "" + flag).html(result.msg);
                                    }
                                });
                            }
                        });
                    }
                });

            }
        });

        controller.onRouteChange = function () {
            //銷毀模態窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});