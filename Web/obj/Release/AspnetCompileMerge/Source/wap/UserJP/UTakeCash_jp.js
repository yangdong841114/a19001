
define(['text!UTakeCash.html', 'jquery'], function (UTakeCash, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    ////初始化日期選擇框
                    //utils.initCalendar(["startTime", "endTime"]);

                    ////清空查詢條件按鈕
                    //$("#clearQueryBtn").bind("click", function () {
                    //    utils.clearQueryParam();
                    //})

                    //查詢按鈕
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加載數據
                searchMethod();
            }
        }

        //查詢參數
        this.param = utils.getPageData();
        var dropload = null;

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#TakeCashItemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        $("#title").html("会員が現金に換える");
        var dto = null;

        var fee = 0;

        //設置默認數據
        setDefaultValue = function (dto) {
            $("#agentTod").html(dto.account.agentTod);
            $("#agentTocc").html(dto.account.agentTocc);
            $("#agentsj").html("0");
            $("#epoints").val("");
        }

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UTakeCash/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UTakeCash);

                var accounttypeList = [{ id: '37', value: "TOD貨幣" }, { id: '39', value: "TOCC貨幣" }];
                //初始化下拉框
                utils.InitMobileSelect('#accounttypeValue', 'アカウントのタイプ', accounttypeList, null, [0], null, function (indexArr, data) {
                    $("#accounttypeValue").val(data[0].value);
                    $("#accounttypeId").val(data[0].id);
                });


                dto = result.result;

                fee = dto.regMoney;
                $("#txsxf").val(dto.regMoney);

                //初始默認值
                setDefaultValue(dto);

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        dom.prev().val("");
                        $("#agentsj").html("0");
                    });
                });


                //綁定離開焦點事件
                $("#epoints").bind("blur", function () {
                    var val = $("#epoints").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (g.test(val)) {
                        var txfee = parseFloat(val) - fee;
                        $("#agentsj").html(txfee);
                    } else {
                        $("#agentsj").html("0");
                    }
                });


                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();

                });

                //保存校驗
                $("#saveBtn").on('click', function () {
                    var val = $("#epoints").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (val == 0) {
                        utils.showErrMsg("現金引き出しの金額を入力してください");
                    } else if (!g.test(val)) {
                        utils.showErrMsg("現金引き出しの金額フォーマットが正しくないです");
                    }
                    else {
                        $("#txje").html($("#epoints").val());
                        $("#sjje").html($("#agentsj").html());
                        utils.showOrHiddenPromp();

                    }
                })

                //確認按鈕
                $("#sureBtn").on('click', function () {
                    utils.AjaxPost("/User/UTakeCash/SaveTakeCash", { epoints: $("#epoints").val(), accountId: $("#accounttypeId").val() }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                            if (result.msg.indexOf("まず個人情報を充実させてください") != -1)
                                location.href = '#UMemberInfo';
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作が成功しました！");
                            dto = result.result;
                            setDefaultValue(dto);
                            searchMethod();
                        }
                    });
                });


                //分頁插件
                dropload = $('#TakeCashDatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData('/User/UTakeCash/GetListPage', param, me,
                            function (rows) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    if (rows[i].isPay == 1)
                                        rows[i]["status"] = "待審核";
                                    if (rows[i].isPay == 2)
                                        rows[i]["status"] = "已通過";
                                    if (rows[i].isPay == 3)
                                        rows[i]["status"] = "已取消";
                                    rows[i]["rmoney"] = rows[i].epoints - rows[i].fee;
                                    rows[i]["addtime"] = utils.changeDateFormat(rows[i]["addtime"]);
                                    rows[i].accountId = cacheMap["JournalClass"][rows[i].accountId];
                                    var dto = rows[i];

                                    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addtime + '</time><span class="sum">+' + dto.epoints + '</span>';
                                    if (dto.status == "已通過") {
                                        html += '狀態：<span><font class="status-success">' + dto.status + '</font></span>';
                                    } else {
                                        html += '狀態：<span><font class="status-red">' + dto.status + '</font></span>';
                                    }

                                    html += '<i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo"><div class="btnbox"></div>' +
                                    '<dl><dt>現金化期日</dt><dd>' + dto.addtime + '</dd></dl><dl><dt>現金化口座</dt><dd>' + dto.accountId + '</dd></dl>' +
                                    '<dl><dt>現金に換える</dt><dd>' + dto.epoints + '</dd></dl><dl><dt>手数料</dt><dd>' + dto.fee + '</dd></dl>' +
                                    '<dl><dt>実際の金額</dt><dd>' + dto.rmoney + '</dd></dl><dl><dt>狀態</dt><dd>' + dto.status + '</dd></dl>' +
                                    '</div></li>';
                                }
                                $("#TakeCashItemList").append(html);
                            }, function () {
                                $("#TakeCashItemList").append('<p class="dropload-noData">データがありません</p>');
                            });
                    }
                });

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});