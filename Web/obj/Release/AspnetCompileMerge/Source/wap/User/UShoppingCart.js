
define(['text!UShoppingCart.html', 'jquery', 'j_easyui'], function (UShoppingCart, $) {

    var controller = function (name) {

        $("#title").html("購物車");


        //計算總金額
        calTotalSumt = function () {
            var total = 0;
            var meiyuanConvertTod = $("#meiyuanConvertTod").val();
            $(".txtamount").each(function () {
                var dataId = $(this).attr("dataId"); //id 
                total = total + (parseFloat($("#fxPrice" + dataId).html()) * parseFloat(this.value));
                totalTod = total * meiyuanConvertTod;
            });
            $("#submitMoney").html(total);
            $("#submitMoneyTod").html(totalTod);
        }

        var g = /^\d+(\.{0,1}\d+){0,1}$/;

        var indexChecked = [0, 0, 0];

        //打開時確認選中數據
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //選擇確認
        selectProvince = function (data) {
            $("#provinceName").val(data[0].value);
            $("#cityName").val(data[1].value);
            if (data.length > 2) {
                $("#areaName").val(data[2].value);
            }
        }

        utils.AjaxPostNotLoadding("/User/UShoppingCart/GetList", {}, function (result) {
            appView.html(UShoppingCart);
            if (result.status == "success") {
                var map = result.map;

                //台湾，日本，中国，美国，韩国，香港，新加坡，越南，菲律宾
                var gjdqList = [{ id: '台湾', value: "台湾" }, { id: '日本', value: "日本" }, { id: '中国', value: "中国" }, { id: '美国', value: "美国" }, { id: '韩国', value: "韩国" }, { id: '香港', value: "香港" }, { id: '新加坡', value: "新加坡" }, { id: '越南', value: "越南" }, { id: '菲律宾', value: "菲律宾" }];
                //初始化下拉框
                utils.InitMobileSelect('#contry', '國家地區', gjdqList, null, [0], null, function (indexArr, data) {
                    $("#contry").val(data[0].value);
                    //$("#accounttypeId").val(data[0].id);
                });
                //省市區選擇
                //var proSet = utils.InitMobileSelect('#provinceName', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);
                //var citySet = utils.InitMobileSelect('#cityName', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);
                //var areaSet = utils.InitMobileSelect('#areaName', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);

                //默認收貨信息
                var memberInfo = map.memberInfo;
                var meiyuanConvertTod = map.meiyuanConvertTod;
                $("#receiptName").val(memberInfo.userName);
                $("#phone").val(memberInfo.phone);
                $("#address").val(memberInfo.address == 0 ? "" : memberInfo.address);
                $("#provinceName").val(memberInfo.province == 0 ? "" : memberInfo.province);
                $("#contry").val(memberInfo.bankAddress == 0 ? "" : memberInfo.bankAddress);
                $("#cityName").val(memberInfo.city == 0 ? "" : memberInfo.city);
                $("#areaName").val(memberInfo.area == 0 ? "" : memberInfo.area);
                $("#agentTod").html(memberInfo.account.agentTod);
                $("#meiyuanConvertTod").val(meiyuanConvertTod);

                var list = map.list; //購物車信息
                if (list && list.length > 0) {
                    var cont = '';
                    for (var i = 0; i < list.length; i++) {
                        var pro = list[i];
                        var checked = pro.isCheck == 1 ? '' : 'checked="checked"';
                        cont += '<dl id="div' + pro.id + '"><dt><img src="' + pro.imgUrl + '" /></dt>' +
                               '<dd><div class="cartcomminfo"><h2>' + pro.productName + '</h2>' +
                               '<h3 >$<span id="fxPrice' + pro.id + '">' + pro.price + '</span>美元</h3><dl><dt>購買數量</dt>' +
                               '<dd><button class="btnminus" dataId="' + pro.id + '"><i class="fa fa-minus"></i></button>' +
                               '<input type="text" class="txtamount" id="buyNum' + pro.id + '" dataId="' + pro.id + '" value="' + pro.num + '" dataValue="' + pro.num + '" />' +
                               '<button class="btnplus" dataId="' + pro.id + '"><i class="fa fa-plus"></i></button></dd>' +
                               '</dl><dl><dt>總金額</dt><dd><span id="totalPrice' + pro.id + '">$' + pro.totalPrice + '美元</span></dd>' +
                               '</dl></div><button class="delbtn" dataId="' + pro.id + '"><i class="fa fa-trash-o"></i><br />刪除</button></dd></dl>';
                        //cont += '<div class="row" id="div' + pro.id + '"><div class="col-sm-12"><table ><tr><td>商品編碼：</td><td>' + pro.productCode + '</td><td style="padding-left:50px;">商品名稱：</td>' +
                        //       '<td>' + pro.productName + '</td></tr></table></div><div class="col-sm-1" style="padding-top:50px;text-align:center;">'+
                        //       '<input style="height:20px;width:20px; cursor:pointer;" dataId="' + pro.id + '" ' + checked + ' type="checkbox" class="ppppCheckBox" /></div>' +
                        //       '<div class="col-sm-2" style="padding-right:0px;padding-top:5px;">' +
                        //       '<img data-toggle="lightbox"  src="' + pro.imgUrl + '" data-image="' + pro.imgUrl + '" class="img-thumbnail" alt="" style="width:225px;height:130px"></div>' +
                        //       '<div class="col-sm-6" style="padding-top:10px;"><table class="table table-borderless"><tr><td align="right" style="padding-top:15px;">市場價：</td>' +
                        //       '<td id="fxPrice' + pro.id + '" style="color: red; padding-top: 15px;">' + pro.price + '</td><td colspan="4" align="right">' +
                        //       '<button type="button" class="btn btn-danger delBtn" dataId="' + pro.id + '"> 刪除 </button></td></tr>' +
                        //       '<tr><td align="right" style="padding-top:15px;">購買數量：</td>' +
                        //       '<td align="right"><button type="button" class="btn btn-primary subBtn" dataId="' + pro.id + '"> - </button></td><td width="80px;">' +
                        //       '<input type="text" class="form-control numtext" id="buyNum' + pro.id + '" dataId="' + pro.id + '" dataValue="' + pro.num + '" style="width:80px;" value="' + pro.num + '" /></td>' +
                        //       '<td align="left"><button type="button" class="btn btn-primary addBtn" dataId="' + pro.id + '"> + </button></td>' +
                        //       '<td align="right" style="padding-top:15px;">總金額：</td>' +
                        //       '<td align="left" id="totalPrice' + pro.id + '" style="color: red; padding-top: 15px;">' + pro.totalPrice + '</td>' +
                        //       '</tr></table></div></div><hr/>';
                    }
                    $("#content").html(cont);

                    ////圖片控件
                    //$(".img-thumbnail").each(function (index, ele) {
                    //    $(this).lightbox();
                    //});

                    //計算總金額
                    calTotalSumt();

                    ////全選按鈕
                    //$("#checkAll").on("click", function () {
                    //    var checked = $("#checkAll")[0].checked;
                    //    var hasBox = false;
                    //    $(".ppppCheckBox").each(function () {
                    //        this.checked = checked;
                    //        hasBox = true;
                    //    });
                    //    if (hasBox) {
                    //        var isChecked = checked ? 2 : 1;
                    //        utils.AjaxPost("UShoppingCart/SaveCheckAll", { isChecked: isChecked }, function (result) {
                    //            if (result.status == "success") {
                    //                calTotalSumt();
                    //            }
                    //        });
                    //    }
                    //});

                    //輸入框取消按鈕
                    $(".erase").each(function () {
                        var dom = $(this);
                        dom.bind("click", function () {
                            var prev = dom.prev();
                            if (prev[0].id == "provinceName" || prev[0].id == "cityName" || prev[0].id == "area") {
                                $("#provinceName").val("");
                                $("#cityName").val("");
                                $("#areaName").val("");
                            }
                            dom.prev().val("");
                        });
                    });

                    //隱藏提示框
                    $(".hideprompt").click(function () {
                        utils.showOrHiddenPromp();

                    });

                    //清空購物車
                    $("#clearBtn").on("click", function () {
                        $("#propTitle").html("您確定要清空購物車嗎？");
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () {
                            sureClearCart();
                        });
                        utils.showOrHiddenPromp();
                        //var hasBox = false;
                        //$(".ppppCheckBox").each(function () {
                        //    this.checked = checked;
                        //    hasBox = true;
                        //});
                        //if (hasBox) {
                        //}
                    });

                    //清空購物車
                    sureClearCart = function () {
                        utils.AjaxPost("/User/UShoppingCart/DeleteAll", {}, function (result) {
                            if (result.status == "success") {
                                utils.showOrHiddenPromp();
                                $("#content").remove()
                                calTotalSumt();
                                utils.showSuccessMsg("清空成功！");
                            }
                        });
                    }

                    //刪除按鈕
                    $(".delbtn").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId");
                            //utils.confirm("確定把該商品從購物車刪除嗎？", function () {
                            utils.AjaxPost("/User/UShoppingCart/Delete", { id: dataId }, function (result) {
                                if (result.status == "success") {
                                    $("#div" + dataId).remove()
                                    calTotalSumt();
                                    utils.showSuccessMsg("刪除成功！");
                                }
                            });
                            //});
                        })
                    });

                    //減少數量按鈕
                    $(".btnminus").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var old = numDom.val();
                            if (!g.test(old)) return; //不是數字
                            if (old <= 1) return;
                            var num = old - 1;
                            var data = { id: dataId, num: num };
                            utils.AjaxPostNotLoadding("/User/UShoppingCart/Update", data, function (result) {
                                if (result.status == "success") {
                                    var price = $("#fxPrice" + dataId).html();
                                    numDom.val(num);
                                    numDom.attr("dataValue", num);
                                    $("#totalPrice" + dataId).html(price * num);
                                    calTotalSumt();
                                }
                            });
                        })
                    });

                    //增加數量按鈕
                    $(".btnplus").each(function () {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var old = numDom.val();
                            if (!g.test(old)) return; //不是數字
                            if (old <= 0) return;
                            var num = parseInt(old) + 1;
                            var data = { id: dataId, num: num };
                            utils.AjaxPostNotLoadding("/User/UShoppingCart/Update", data, function (result) {
                                if (result.status == "success") {
                                    var price = $("#fxPrice" + dataId).html();
                                    numDom.val(num)
                                    numDom.attr("dataValue", num);
                                    $("#totalPrice" + dataId).html(price * num);
                                    calTotalSumt();
                                }
                            });
                        })
                    });

                    //改變數量離開焦點
                    $(".txtamount").each(function () {
                        var dom = $(this);
                        dom.on("blur", function () {
                            var dataId = dom.attr("dataId"); //id
                            var numDom = $("#buyNum" + dataId);
                            var num = numDom.val();
                            //如果是錯誤格式或小於等於0，自動改回來
                            if (!g.test(num) || parseInt(num) <= 0) {
                                numDom.val(numDom.attr("dataValue"));
                            } else {
                                var data = { id: dataId, num: num };
                                utils.AjaxPostNotLoadding("/User/UShoppingCart/Update", data, function (result) {
                                    if (result.status == "success") {
                                        var price = $("#fxPrice" + dataId).html();
                                        $("#buyNum" + dataId).val(num)
                                        $("#buyNum" + dataId).attr("dataValue", num);
                                        $("#totalPrice" + dataId).html(price * num);
                                        calTotalSumt();
                                    }
                                });
                            }
                        })
                    });

                    ////CheckBox點擊事件
                    //$(".ppppCheckBox").each(function () {
                    //    var dom = $(this);
                    //    var dataId = dom.attr("dataId"); //id 
                    //    dom.on("click", function () {
                    //        var isChecked = dom[0].checked ? 2 : 1;
                    //        var data = { id: dataId, isChecked: isChecked };
                    //        utils.AjaxPost("UShoppingCart/SaveChecked", data, function (result) {
                    //            if (result.status == "success") {

                    //                calTotalSumt();
                    //            }
                    //        });
                    //    });
                    //});

                    //提交訂單
                    $("#saveBtn").on("click", function () {
                        var isChecked = true;
                        if ($("#receiptName").val() == 0) {
                            utils.showErrMsg("請填寫收貨人");
                        } else if ($("#phone").val() == 0) {
                            utils.showErrMsg("請填寫聯系電話");
                        //} else if ($("#provinceName").val() == 0) {
                        //    utils.showErrMsg("請選擇省");
                        //} else if ($("#cityName").val() == 0) {
                        //    utils.showErrMsg("請選擇市");
                        //} else if ($("#address").val() == 0) {
                        //    utils.showErrMsg("請填詳細地址");
                        } else {
                            $("#propTitle").html("您確定提交訂單嗎？");
                            $("#sureBtn").unbind();
                            $("#sureBtn").bind("click", function () {
                                sureSubmitOrder();
                            });
                            utils.showOrHiddenPromp();
                        }
                    });

                    sureSubmitOrder = function () {
                        var data = {
                            receiptName: $("#receiptName").val(), phone: $("#phone").val(), address: $("#address").val(),
                            provinceName: $("#provinceName").val(), cityName: $("#cityName").val(), areaName: $("#areaName").val(), contry: $("#contry").val(),
                        };
                        utils.AjaxPost("/User/UShoppingCart/SaveCheckAll", { isChecked: 2 }, function (result) {
                            if (result.status == "success") {
                                utils.AjaxPost("/User/UShoppingCart/SaveOrder", data, function (result) {
                                    if (result.status == "success") {
                                        utils.showOrHiddenPromp();
                                        utils.showSuccessMsg("提交成功！");
                                        location.href = "#UOrder";
                                    } else {
                                        utils.showErrMsg(result.msg);
                                    }
                                });
                            }
                        });
                    }
                }

            } else {
                utils.showErrMsg(result.msg);
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});