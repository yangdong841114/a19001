
define(['text!Password3_jp.html', 'jquery'], function (Password3, $) {

    var controller = function (prePage) {
        //设置标题
        $("#title").html("取引のパスワードを検証する");
        appView.html(Password3);
        

        $(".logingb").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });

        //检查密码
        checkPassWord = function () {
        	var val = $("#password").val();
            if (val == 0) {
                utils.showErrMsg("取引のパスワードを入力してください");
            } else {
                utils.AjaxPost("/User/UserWeb/CheckUPass3", { pass3: val }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        pass3 = true;
                        if (prePage.indexOf(".") != -1) {
                            var val = prePage.split(".");
                            location.href = "#" + val[0] + "/" + val[1];
                        } else {
                            location.href = "#" + prePage;
                        }
                    }
                });
            }
        }
        	
        $("#sureBtn").on("click", function () {
            checkPassWord();
        })

        controller.onRouteChange = function () {
            document.onkeydown = undefined;
        };
    };

    return controller;
});