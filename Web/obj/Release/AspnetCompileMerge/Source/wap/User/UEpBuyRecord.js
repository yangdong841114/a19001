
define(['text!UEpBuyRecord.html', 'jquery'], function (UEpBuyRecord, $) {

    var controller = function (name) {

        var statusDto = { 0: "待买家付款", 1: "待卖家收款", 2: "已完成", 3: "已取消" };
        var statusList = [{ id: -1, value: "全部" }, { id: 0, value: "待买家付款" }, { id: 1, value: "待卖家收款" }, { id: 2, value: "已完成" }]
        var buyTypeList = [{ id: 0, value: "全部" }, { id: 1, value: "普通购买" }, { id: 2, value: "批量购买" }]
        $("#title").html("购买记录");
        appView.html(UEpBuyRecord);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();
        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //初始化下拉框
        utils.InitMobileSelect('#flagName', '选择状态', statusList, null, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].value);
            $("#flag").val(data[0].id);
        });

        utils.InitMobileSelect('#typeIdName', '选择购买类型', buyTypeList, null, [0], null, function (indexArr, data) {
            $("#typeIdName").val(data[0].value);
            $("#typeId").val(data[0].id);
        });

        //查询参数
        this.param = utils.getPageData();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //取消买单
        cancleConfirm = function (id, number, snumber, buyNum) {
            $("#dialogTitle").html("确定取消买单吗？")
            $("#sureBtn").html("确定取消");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UEpBuyRecord/SaveCancel", { id: id }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("操作成功");
                        searchMethod();

                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //全选按钮
        $("#checkAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".itemcheckedbox").each(function () {
                this.checked = checked;
            });
        });

        //获取选中的记录
        getCheckedIds = function () {
            var rows = [];
            $(".itemcheckedbox").each(function () {
                if (this.checked) {

                    rows.push({ id: $(this).attr("dataId"), flag: $(this).attr("dataFlag"), number: $(this).attr("dataNumber") });
                }
            });
            return rows;
        }

        //批量付款按钮
        $("#batchPayBtn").bind("click", function () {
            var rows = getCheckedIds();
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要付款的记录！"); }
            else {
                var data = {};
                var checked = true;
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.flag != 0) {
                        checked = false;
                        utils.showErrMsg("购买记录【" + row.number + "】状态不是【待买家付款】！");
                        return;
                    }
                    data["list[" + i + "].id"] = row.id;
                }
                if (checked) {
                    $("#dialogTitle").html("确定批量付款吗？")
                    $("#sureBtn").html("确定付款");
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        utils.AjaxPost("/User/UEpBuyRecord/BatchPayMoney", data, function (result) {
                            utils.showOrHiddenPromp();
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("批量付款成功");
                                //重刷grid
                                searchMethod();

                            }
                        });
                    });
                    utils.showOrHiddenPromp();
                }
            }
        })

        //分页插件
        var dropload = $('#UEpBuyRecorddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData('/User/UEpBuyRecord/GetListPage', param, me,
                    function (rows) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = statusDto[rows[i]["flag"]];
                            rows[i]["buytype"] = rows[i]["typeId"] == 2 ? "批量购买" : "普通购买";
                            var dto = rows[i];

                            html += '<li><label><input class="operationche itemcheckedbox" type="checkbox" dataId="' + dto.id + '" dataFlag="' + dto.flag + '" dataNumber="' + dto.number + '" /></label>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)" style="padding-left:2.7rem;"><time>' + dto.number + '</time><span class="sum">' + dto.buyNum + '</span>';
                            if (dto.status == "已完成") {
                                html += '<span class="status ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="status noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div><div class="allinfo">';
                            if (dto.flag == 0) {
                                html += '<div class="btnbox"><ul class="tga1">' +
                                '<li><button class="sdelbtn" onclick=\'cancleConfirm(' + dto.id + ',"' + dto.number + '","' + dto.snumber + '",' + dto.buyNum + ')\'>取消买单</button></li>' +
                                '</ul></div>';
                            }
                            html += '<dl><dt>买单单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>购买日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '<dl><dt>买家编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.opUserId + '</dd></dl>' +
                            '<dl><dt>买入数量</dt><dd>' + dto.buyNum + '</dd></dl><dl><dt>应付金额</dt><dd>' + dto.payMoney + '</dd></dl>' +
                            '<dl><dt>购买类型</dt><dd>' + dto.buytype + '</dd></dl><dl><dt>批量单号</dt><dd>' + dto.batchNumber + '</dd></dl>' +
                            '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl><dl><dt>挂卖单号</dt><dd>' + dto.snumber + '</dd></dl>' +
                            '<dl><dt>卖家编号</dt><dd>' + dto.suserId + '</dd></dl><dl><dt>手机</dt><dd>' + dto.phone + '</dd></dl>' +
                            '<dl><dt>QQ</dt><dd>' + dto.qq + '</dd></dl><dl><dt>开户行</dt><dd>' + dto.bankName + '</dd></dl>' +
                            '<dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl><dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                            '<dl><dt>开户支行</dt><dd>' + dto.bankAddress + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#UEpBuyRecorditemList").append(html);
                    }, function () {
                        $("#UEpBuyRecorditemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UEpBuyRecorditemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["flag"] = $("#flag").val();
            param["snumber"] = $("#snumber").val();
            param["typeId"] = $("#typeId").val();
            param["userId"] = $("#userId").val();
            param["suserId"] = $("#suserId").val();
            param["opUserId"] = $("#opUserId").val();
            param["batchNumber"] = $("#batchNumber").val();
            param["number"] = $("#number").val();
            param["snumber"] = $("#snumber").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").bind("click", function () { searchMethod(); })


        controller.onRouteChange = function () {
        };
    };

    return controller;
});