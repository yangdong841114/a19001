
define(['text!financeMan.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("財務管理");


        ShowTakeCash = function () {
            if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "") {
                document.getElementById("waitThing_Txdz").style.display = "block";
                utils.showOrHiddenPromp();
            }
        }

        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });


                var map = result.map;
                var menuItems = map.menuItems
                var account = map.account;
                $("#agentTod").html(account.agentTod.toFixed(2));        //点值
                $("#agentMoby").html(account.agentMoby.toFixed(2));        //奖金币
                $("#agentFhz").html(account.agentFhz.toFixed(2));        //购物币
                $("#agentDhm").html(account.agentDhm.toFixed(2));        //复投币
                $("#agentTocc").html(account.agentTocc.toFixed(2));        //复投币
                $("#bankName").val(map.userInfo.bankName);
                $("#bankCard").val(map.userInfo.bankCard);
                $("#bankUser").val(map.userInfo.bankUser);
                $("#bankAddress").val(map.userInfo.bankAddress);
                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    var epHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("User/", "");
                        if (node.isShow == 0) {
                            if (node.id == "1020307" || node.id == "1020308" || node.id == "1020309" || node.id == '1020311') {
                                epHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                            } else {
                                if (node.id == "1020302") {
                                    //提现
                                    var txHtml = '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                                    //if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "")
                                    //    txHtml = '<li><div  onclick="ShowTakeCash()"><i  class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></div></li>';
                                    //else
                                        txHtml = '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                                    banHtml += txHtml;
                                }
                                else
                                    banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                            }
                        }
                    }

                    $("#itemCont").html(banHtml);
                    $("#epCont").html(epHtml);
                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});