
define(['text!UInfofb.html', 'jquery', 'text!NotJMS.html'], function (UInfofb, $, NotJMS) {

    var controller = function (name) {
        //設置標題
        $("#title").html("文章发布")
       
        appView.html(UInfofb);
        var dto = null;
        var editList = {};
        var dropload = null;
        var editor = null;
  
        var fltypeList = [{ id: 1, value: "最新资讯" }, { id: 2, value: "热门文章" }];
        utils.InitMobileSelect('#fltype', '分类', fltypeList, null, [0], null, function (indexArr, data) {
            $("#fltype").val(data[0].value);
            $("#fltypeId").val(data[0].id);
        });

        //查詢參數
        this.param = utils.getPageData();

        clearForm = function () {
            $("#id").val("");
            $("#imgUrl").val("");
            $("#Infotitle").val("");
            $("#infoJj").val("");
            utils.setEditorHtml(editor, "")
        }
        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#InfoitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //選項卡切換
        tabClick = function (index, bb) {
            if (index == 1) {
                document.getElementById("mainDiv").style.display = "block";
                document.getElementById("deployDiv").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active");
                if (bb) {
                    searchMethod();
                }
            } else {
                document.getElementById("mainDiv").style.display = "none";
                document.getElementById("deployDiv").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                dto = null;
                clearForm();
                $("#mainDiv").css("display", "none");
                $("#deployDiv").css("display", "block");
            }
        }
      
        //初始化編輯器
        editor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });



        //隱藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //刪除按鈕
        deleteRecord = function (id) {
            $("#sureBtn").unbind();
            //確認刪除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UserWeb/DeleteInfo", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("刪除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }



        //保存發布
        $("#saveInfoBtn").bind("click", function () {
            
            //非空校驗
            if ($("#Infotitle").val() == 0) {
                utils.showErrMsg("标题不能為空");
                utils.showErrMsg($("#title").val());
            } else if (editor.getText() == 0) {
                utils.showErrMsg("請輸入內容");
            } else {
                var formdata = new FormData();
                if (dto) {
                    formdata.append("id", $("#id").val());
                    formdata.append("imgUrl", $("#imgUrl").val());
                }
                formdata.append("fltype", $("#fltype").val());
                formdata.append("title", $("#Infotitle").val());
                formdata.append("infoJj", $("#infoJj").val());
                formdata.append("infocontent", utils.getEditorHtml(editor));
                utils.AjaxPostForFormData("/User/UserWeb/SaveOrUpdateInfo", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        //clearForm();
                        tabClick(1, true);
                        utils.showSuccessMsg("保存成功！");
                    }
                });
            }
        });


        //關閉發布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //編輯商品
        editRecord = function (id) {
            dto = editList[id];
            $("#id").val(id);
            $("#Infotitle").val(dto.title);
            $("#fltype").val(dto.fltype);
            $("#infoJj").val(dto.infoJj);
            utils.setEditorHtml(editor, dto.infocontent);
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        }

        //分頁加載控件
        dropload = $('#Infodatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UserWeb/GetListPageInfo", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style="">';
                            html += '&nbsp;<span class="sum">' + dto.fltype + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">';
                            //html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>刪除</button></li>';
                            html += '<li><button class="sdelbtn" onclick=\'editRecord(' + dto.id + ')\'>編輯</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>分类</dt><dd>' + dto.fltype + '</dd></dl>' +
                            '<dl><dt>标题</dt><dd>' + dto.title + '</dd></dl>' +
                            '<dl><dt>简介</dt><dd>' + dto.infoJj + '</dd></dl><dl><dt>發布日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '</div></li>';
                            editList[dto.id] = dto;
                        }
                        $("#InfoitemList").append(html);
                    }, function () {
                        $("#InfoitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });



       


        controller.onRouteChange = function () {

        };
    };

    return controller;
});