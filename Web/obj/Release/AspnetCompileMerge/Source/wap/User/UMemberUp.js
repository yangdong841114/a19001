
define(['text!UMemberUp.html', 'jquery'], function (UMemberUp, $) {

    var controller = function (name) {

        //设置标题
        $("#title").html("会员晋升");
        var dto = null;
        var levelIds = [3, 4, 5];

        //下拉框选择改变时执行的函数
        changeFunction = function (index) {
            for (var i = 0; i < levelIds.length; i++) {
                $("#lv" + levelIds[i]).removeClass("active");
            }

            $("#rmoney").empty();
            if (!$("#lv" + index).hasClass("disabled")) {
                $("#lv" + index).addClass("active");
                utils.AjaxPostNotLoadding("/User/UMemberUp/GetRegMoney", { ulevel: index }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#rmoney").html(result.msg);
                    }
                })
            }
        }

        //查询参数
        this.param = utils.getPageData();

        //设置表单默认数据
        setDefaultValue = function (dto) {
            $("#agentDz").html(dto.account.agentDz);
            $("#nowLevel").html(cacheMap["ulevel"][dto.uLevel]);
            $("#rmoney").html("0");

            //初始化晋升级别下拉框
            for (var i = 0; i < levelIds.length; i++) {
                $("#lv" + levelIds[i]).removeClass("disabled");
                $("#lv" + levelIds[i]).removeClass("active");
                if (levelIds[i] <= dto.uLevel) {
                    $("#lv" + levelIds[i]).addClass("disabled");
                }
            }
        };

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UMemberUp/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMemberUp);
                dto = result.result;

                //初始默认值
                setDefaultValue(dto);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                var dropload = $('#UMemberUpdatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/User/UMemberUp/GetListPage", param, me,
                            function (rows, footers) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                                    rows[i]["oldId"] = cacheMap["ulevel"][rows[i].oldId];
                                    rows[i]["newId"] = cacheMap["ulevel"][rows[i].newId];

                                    var dto = rows[i];
                                    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time>';
                                    html += '<span class="sum">' + dto.userId + '</span><span class="sum">' + dto.newId + '</span><i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo">' +
                                    '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                    '<dl><dt>晋升前级别</dt><dd>' + dto.oldId + '</dd></dl><dl><dt>晋升后级别</dt><dd>' + dto.newId + '</dd></dl>' +
                                    '<dl><dt>缴纳点值</dt><dd>' + dto.money + '</dd></dl><dl><dt>晋升日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                    '<dl><dt>操作人</dt><dd>' + dto.createUser + '</dd></dl>' +
                                    '</div></li>';
                                }
                                $("#UMemberUpitemList").append(html);
                            }, function () {
                                $("#UMemberUpitemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });

                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UMemberUpitemList").empty();
                    //param["startTime"] = $("#startTime").val();
                    //param["endTime"] = $("#endTime").val();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //保存
                $("#saveBtn").on('click', function () {
                    var newId = 0;
                    for (var i = 0; i < levelIds.length; i++) {
                        if ($("#lv" + levelIds[i]).hasClass("active")) {
                            newId = levelIds[i];
                        }
                    }
                    if (newId == 0) {
                        utils.showErrMsg("请选择要晋升的级别");
                    } else {
                        $("#newId").val(newId);
                        $("#jsjb").html(cacheMap["ulevel"][newId]);
                        $("#sxdzb").html($("#rmoney").html())
                        utils.showOrHiddenPromp();
                    }
                });

                //确定晋升
                $("#sureBtn").bind("click", function (dataId) {
                    utils.AjaxPost("/User/UMemberUp/Save", { newId: $("#newId").val() }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                            searchMethod();
                        }
                    });
                });

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});