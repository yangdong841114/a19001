
define(['text!UEpMyOrderDetail_jp.html', 'jquery'], function (UEpMyOrderDetail, $) {

    var controller = function (sid) {

        var statusDto = { 0: "待买家付款", 1: "待卖家收款", 2: "已完成", 3: "已取消" };
        var statusList = [{ id: -1, value: "全部" }, { id: 0, value: "待买家付款" }, { id: 1, value: "待卖家收款" }, { id: 2, value: "已完成" } ]
        //设置标题
        $("#title").html("交易明细");
        appView.html(UEpMyOrderDetail);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();
        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //初始化下拉框
        utils.InitMobileSelect('#flagName', '选择状态', statusList, null, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].value);
            $("#flag").val(data[0].id);
        });

        //查询参数
        this.param = utils.getPageData();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //确认收款
        sureConfirm = function (id, number, snumber, buyNum) {
            $("#gmdh").html(number);
            $("#mddh").html(snumber);
            $("#gmsl").html(buyNum);
            $("#yfje").html(buyNum);
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UEpMyOrderDetail/PaySure", { id: id }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("操作成功");
                        searchMethod();

                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //分页插件
        var dropload = $('#UEpMyOrderDetaildatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData('/User/UEpMyOrderDetail/GetListPage?sid='+ sid, param, me,
                    function (rows) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = statusDto[rows[i]["flag"]];
                            var dto = rows[i];

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.number + '</time><span class="sum">' + dto.buyNum + '</span>';
                            if (dto.status == "已完成") {
                                html += '<span class="status ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="status noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div><div class="allinfo">';
                            if (dto.flag == 1) {
                                html += '<div class="btnbox"><ul class="tga1">' +
                                '<li><button class="sdelbtn" onclick=\'sureConfirm(' + dto.id + ',"' + dto.number + '","' + dto.snumber + '",' + dto.buyNum + ')\'>确认收款</button></li>' +
                                '</ul></div>';
                            }
                            html += '<dl><dt>买单单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>挂卖单号</dt><dd>' + dto.snumber + '</dd></dl>' +
                            '<dl><dt>买家编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>购买数量</dt><dd>' + dto.buyNum + '</dd></dl>' +
                            '<dl><dt>应付金额</dt><dd>' + dto.payMoney + '</dd></dl><dl><dt>购买日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#UEpMyOrderDetailitemList").append(html);
                    }, function () {
                        $("#UEpMyOrderDetailitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UEpMyOrderDetailitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["flag"] = $("#flag").val();
            param["number"] = $("#number").val();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").bind("click", function () { searchMethod(); })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});