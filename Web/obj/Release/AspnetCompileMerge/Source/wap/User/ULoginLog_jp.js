
define(['text!ULoginLog_jp.html', 'jquery'], function (ULoginLog, $) {

    var controller = function (name) {

        $("#title").html("ログインログ");
        appView.html(ULoginLog);
        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })


        var flagObj = { "0": "成功", "1": "失敗(アカウントが存在しません)", "2": "失敗(パスワードの検証に失敗)", "3": "失敗(アカウント未開通)", "4": "失敗(アカウントが凍結されました)", "5": "失敗(アカウントがロックされました)" };

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ULoginLogdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/ULoginLog/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["loginTime"] = utils.changeDateFormat(rows[i]["loginTime"]);
                            rows[i]["flag"] = flagObj[rows[i]["flag"]];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.loginTime + '</time>';
                            if (dto.flag == '成功') {
                                html += '<span><font class="status-success">' + dto.flag + '</font></span>';
                            } else {
                                html += '<span><font class="status-red">' + dto.flag + '</font></span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>ログイン時間</dt><dd>' + dto.loginTime + '</dd></dl><dl><dt>アカウントを登録する</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>名前</dt><dd>' + dto.userName + '</dd></dl><dl><dt>IP</dt><dd>' + dto.loginIp + '</dd></dl>' +
                            '<dl><dt>説明</dt><dd>' + dto.mulx + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ULoginLogitemList").append(html);
                    }, function () {
                        $("#ULoginLogitemList").append('<p class="dropload-noData">データがありません</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ULoginLogitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});