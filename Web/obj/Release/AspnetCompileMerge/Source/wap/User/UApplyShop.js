
define(['text!UApplyShop_jp.html', 'jquery'], function (UApplyShop, $) {

    var controller = function (name) {
        
        $("#title").html("申请报单中心");
        var dto = undefined;

        //设置显示内容
        setShowContent = function (dto) {
            $("#agentForm").css("display", "none");
            $("#agent0").css("display", "none");
            $("#agent1").css("display", "none");
            $("#agent2").css("display", "none");
            if (dto.isAgent == 0) {
                $("#shopName").html(dto.userId);
                $("#mmy").html(dto.regAgentmoney);
                $("#agentForm").css("display", "block");
                $("#agent0").css("display", "block");
            } else if (dto.isAgent == 1) {
                $("#shopName").html(dto.agentName);
                $("#mmy").html(dto.regAgentmoney);
                $("#agentForm").css("display", "block");
                $("#agent1").css("display", "block");
            } else {
                $("#shopName2").html(dto.agentName);
                $("#agent2").css("display", "block");;
            }
        }

        //加载默认密码
        utils.AjaxPostNotLoadding("/User/UApplyShop/GetDefaultMsg", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UApplyShop);
                dto = result.result;
                
                setShowContent(dto);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //提交申请按钮
                $("#saveBtn").on('click', function () {
                    $("#bdzxbh").html($("#shopName").html());
                    $("#hkje").html($("#mmy").html());
                    utils.showOrHiddenPromp();
                });

                //确认提交按钮
                $("#sureBtn").on('click', function () {
                    if (dto && dto.isAgent == 0) {
                        utils.AjaxPost("/User/UApplyShop/ApplyShop", {}, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showOrHiddenPromp();
                                dto = result.result;
                                setShowContent(dto)
                                utils.showSuccessMsg("申请成功，请等待管理员审核！");
                            }
                        });
                    } else {
                        utils.showErrMsg("申请失败，无法确认当前登录用户");
                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});