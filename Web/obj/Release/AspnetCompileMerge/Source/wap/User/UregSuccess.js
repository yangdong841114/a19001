
define(['text!UregSuccess.html', 'jquery'], function (UregSuccess, $) {

    var controller = function (name) {

        $("#title").html("註冊成功");
        appView.html(UregSuccess);

        $("#userId").html("<span>會員編號：</span>" + regSuccess.userId);
        $("#regmoney").html("<span>註冊金額：</span>￥" + regSuccess.regMoney);
        $("#ulevel").html("<span>註冊級別：</span>" + cacheMap["ulevel"][regSuccess.uLevel]);

        controller.onRouteChange = function () {
        };
    };

    return controller;
});