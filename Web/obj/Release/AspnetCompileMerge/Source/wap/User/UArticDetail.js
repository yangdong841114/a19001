
define(['text!UArticDetail.html', 'jquery'], function (UArticDetail, $) {

    var controller = function (articId) {

        utils.AjaxPostNotLoadding("/User/UserWeb/GetSession", {}, function (result) {
            if (result.status != "fail") {
                if (result.msg == "rb") {
                    $("#title").html("公告の詳細");
                } else {
                    $("#title").html("公告詳情");
                }
            }
        });

        
        
        utils.AjaxPostNotLoadding("/User/UArticle/GetModel", { id: articId }, function (result) {
            if (result.status == "success") {
                appView.html(UArticDetail);
                var dto = result.result;
                $("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#contTitle").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});