
define(['text!myCenterMan.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("會員中心");
        $('#footerlink').css('display', 'block');
        $('#shopfooter').css('display', 'none');


        

        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);
              
                var map = result.map;
                if (map.first_userId == "noexit") location.href = "/Home/exitUserLogin";
                else
                {

               
                var menuItems = map.menuItems
                var account = map.account;
                $("#agentMoby").html(account.agentMoby.toFixed(2));        //点值
                $("#agentTod").html(account.agentTod.toFixed(2));        //奖金币
                $("#agentDhm").html(account.agentDhm.toFixed(2));        //购物币
                $("#agentTocc").html(account.agentTocc.toFixed(2));        //复投币

               

                var userInfo = map.userInfo;
                $("#ulevel").html(cacheMap["ulevel"][userInfo.uLevel]);
                $("#userName").html(userInfo.userName);
                $("#userId").html("會員賬號：" + userInfo.userId);

                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("User/", "");
                        if (node.isShow == 0) {
                            banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                        }
                    }
                    $("#itemCont").html(banHtml);
                }

                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    //utils.AjaxPostNotLoadding("/User/UserWeb/GetSession", {}, function (result) {
    //    if (result.status != "fail") {
    //        if (result.msg == "rb") {
    //            $("#duihuanma").html("兌換コード");
    //        } else {
    //            $("#duihuanma").html("兌換碼");
    //        }
    //    }
    //});

    return controller;
});