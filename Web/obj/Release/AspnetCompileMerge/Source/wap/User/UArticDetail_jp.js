
define(['text!UArticDetai_jpl.html', 'jquery'], function (UArticDetail, $) {

    var controller = function (articId) {

        $("#title").html("公告の詳細");
        
        utils.AjaxPostNotLoadding("/User/UArticle/GetModel", { id: articId }, function (result) {
            if (result.status == "success") {
                appView.html(UArticDetail);
                var dto = result.result;
                $("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#contTitle").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});