
define(['text!UFamilyTree.html', 'jquery', 'j_easyui', 'css!../Content/css/style.css'], function (UFamilyTree, $) {

    var controller = function (name) {

        bindTree = function (msg) {
            $("#treeDiv").html(msg);
            $(".forward_to").each(function () {
                var dom = $(this);
                dom.on("click", function () {
                    var userId = dom.attr("userId");
                    var data = { userId: userId, ceng: 3 };
                    utils.AjaxPost("UFamilyTree/GetFamilyTreeByCondition", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            $("#userId").val("");
                            $("#ceng").val("3");
                            bindTree(result.msg);
                        }
                    });
                })
            });
        }

        //初始化加载数据
        utils.AjaxPostNotLoadding("UFamilyTree/GetFamilyTree", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UFamilyTree);
                bindTree(result.msg);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    var param = $("#QueryForm").serializeObject();
                    utils.AjaxPost("UFamilyTree/GetFamilyTreeByCondition", param, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            bindTree(result.msg);
                        }
                    });
                })


                //我的系谱图按钮
                $("#selfBtn").on("click", function () {
                    utils.AjaxPost("UFamilyTree/GetFamilyTree", {}, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            $("#userId").val("");
                            $("#ceng").val("3");
                            bindTree(result.msg);
                        }
                    });
                })
            }

        });

        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});
