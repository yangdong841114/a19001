
define(['text!UDelegaMember.html', 'jquery', 'j_easyui', 'zui'], function (UDelegaMember, $) {

    var controller = function (name) {

        appView.html(UDelegaMember);


        ////查询grid
        //queryGrid = function () {
        //    var objs = $("#QueryForm").serializeObject();
        //    grid.datagrid("options").queryParams = objs;
        //    grid.datagrid("reload");
        //}

        $(".form-control").each(function () {
            var dom = $(this);
            dom.on("focus", function () {
                $(this).removeClass("inputError");
            })
        })

        //添加按钮
        $("#transferBtn").on("click", function () {
            utils.confirm("确定所有子账户金额转入主账户吗？", function () {
                utils.AjaxPost("UDelegaMember/Transfer", {}, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("转入成功！");
                        grid.datagrid("reload");
                    }
                })
            })
        })

        //添加按钮
        $("#addBtn").on("click", function () {
            var checked = true;
            if ($("#userId").val() == 0) {
                $("#userId").addClass("inputError");
                checked = false;
            }
            if ($("#password").val() == 0) {
                $("#password").addClass("inputError");
                checked = false;
            }
            if (checked) {
                var data = { password: $("#password").val(), delegaUserId: $("#userId").val() };
                utils.AjaxPost("UDelegaMember/Save", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("托管成功！");
                        $("#userId").val("");
                        $("#password").val("");
                        grid.datagrid("reload");
                    }
                })
            }
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            title: '托管账户列表',
            showFooter: true,
            columns: [[
             { field: 'delegaUserId', title: '托管会员编号', width: '15%' },
             { field: 'addTime', title: '托管时间', width: '15%' },
             { field: 'agentDz', title: '点值', width: '15%' },
             { field: 'agentJj', title: '奖金币', width: '15%' },
             { field: 'agentGw', title: '购物币', width: '15%' },
             { field: 'agentFt', title: '复投币', width: '15%' },
             {
                 field: '_operate', title: '操作', width: '10%', formatter: function (val, row, index) {
                     return '&nbsp;&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDelete" >取消托管</a>';
                 }
             }
            ]],
            url: "UDelegaMember/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {
            //行取消托管按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定要取消托管吗？", function () {
                        utils.AjaxPost("UDelegaMember/Delete", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                grid.datagrid("reload");
                            }
                        });
                    });
                    return false;
                }
            });
        })



        controller.onRouteChange = function () {
        };
    };

    return controller;
});