
define(['text!Password2.html', 'jquery', 'zui', 'css!../Content/css/style.css'], function (Password2, $) {

    var controller = function (prePage) {
        appView.html(Password2);

        ////失去焦点事件
         $(".form-control").each(function (index, ele) {
            var current = $(this);
            var parent = current.parent();
            var before = current.parent().parent().children().eq(0);
            current.on("focus", function (event) {
                parent.removeClass("has-error"); 
                utils.destoryPopover(current);
            });
        });

        //验证密码
        checkPassWord = function () {
            var val = $("#password").val();
            var current = $("#password");
            var parent = current.parent();
            parent.removeClass("has-error");
            utils.destoryPopover(current);
            if (val == 0) {
                parent.addClass("has-error");
                utils.showPopover(current, "请输入安全密码", "popover-danger");
            } else {
                utils.AjaxPost("UserWeb/CheckUPass2", { pass2: val }, function (result) {
                    if (result.status == "fail") {
                        parent.addClass("has-error");
                        utils.showPopover(current, result.msg, "popover-danger");
                    } else {
                        pass2 = true;
                        if (prePage.indexOf(".") != -1) {
                            var val = prePage.split(".");
                            location.href = "#" + val[0] + "/" + val[1];
                        } else {
                            location.href = "#" + prePage;
                        }
                    }
                });
            }
        }

        //回车键绑定
        document.onkeydown = function (event) {
            var e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 13) {
                checkPassWord();
            }
        };

        //提交按钮
        $("#sureBtn").on("click", function () {
            checkPassWord();
        });

        $("#password").focus();

        controller.onRouteChange = function () {
            document.onkeydown = undefined;
        };
    };

    return controller;
});