﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body>
    <header>
        <div class="">
            <i class="backicon"><a href="javascript:history.go(-1);">
                <img src="/Content/APP/User/images/btnback.png" /></a></i>
            <h2>設置新密碼</h2>
        </div>
        <div class="clear"></div>
    </header>
    <div class="entryinfo">
        <dl>
            <dt>登錄密碼</dt>
            <dd>
                <input type="password" required="required" class="entrytxt" placeholder="請輸入登錄密碼" id="pass1"/>
                <span class="erase"><i class="fa fa-times-circle-o"></i></span>
            </dd>
        </dl>
        <dl>
            <dt>安全密碼</dt>
            <dd>
                <input type="password" required="required" class="entrytxt" placeholder="請輸入安全密碼" id="pass2"/>
                <span class="erase"><i class="fa fa-times-circle-o"></i></span>
            </dd>
        </dl>
        <dl>
            <dt>交易密碼</dt>
            <dd>
                <input type="password" required="required" class="entrytxt" placeholder="請輸入交易密碼" id="pass3"/>
                <span class="erase"><i class="fa fa-times-circle-o"></i></span>
            </dd>
        </dl>
        <div id="msg" style="width:95%;text-align:center;font-size:1.4rem;padding:1rem 1rem 1rem 1rem;">
            &nbsp;<br />&nbsp;
        </div>
        <div class="btnbox">
            <button class="bigbtn" id="tjBtn">提交修改</button>
        </div>
        <div class="original" style="padding-bottom:2rem;"><a href="/Home/AppIndex">返回登錄</a> </div>
    </div>
    <script type="text/javascript">
        $(function () {

            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    dom.prev().val("");
                })
            });

            //顯示錯誤消息
            showErrorMsg = function (msg) {
                var msgbox = new $.zui.Messager('提示消息：' + msg, {
                    type: 'danger',
                    icon: 'warning-sign',
                    placement: 'center',
                    parent: 'body',
                    close: true
                });
                msgbox.show();
            }

            $("#tjBtn").on("click", function () {
                var checked = true;
                if ($("#pass1").val() == 0) {
                    showErrorMsg("請輸入登錄密碼");
                } else if ($("#pass2").val() == 0) {
                    showErrorMsg("請輸入安全密碼");
                } else if ($("#pass3").val() == 0) {
                    showErrorMsg("請輸入交易密碼");
                } else {
                    var data = { pass1: $("#pass1").val(), pass2: $("#pass2").val(), pass3: $("#pass3").val() };
                    $.ajax({
                        url: "/ForgetPass/ChangePss",
                        type: "POST",
                        data: data,
                        success: function (result) {
                            if (result.status == "fail") {
                                if (result.msg == "jump") {
                                    location.href = "/ForgetPass/Index";
                                } else {
                                    showErrorMsg(result.msg);
                                }
                            } else {
                                alert("重置密碼成功,請重新登錄");
                                location.href = "/Home/AppIndex";
                            }
                        }
                    });
                }
            })
        });
    </script>
</body>
</html>
