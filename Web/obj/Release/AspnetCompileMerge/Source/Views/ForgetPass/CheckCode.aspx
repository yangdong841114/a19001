﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/css/zui2.css" rel="stylesheet" type="text/css" />
    <link href="/Content/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/Content/js/jquery-3.2.1.min.js" id="main"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <script type="text/javascript" src="/Content/js/jquery.flexslider-min.js"></script>
</head>
<body>
    <div class="header">
        <div class="pull-left">
            <img class="lf" src="/Content/images/logo.png" />
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="mainnav">
    </div>
    <div class="">

        <div class="center-block tipsbox">
            <div class="tipscontbox" style="height: 320px; width: 500px;">
                <div class="tipsiconbox">
                    <i class="icon icon-user"></i>
                </div>
                <input type="hidden" id="flag" value="1" />
                <ul class="nav nav-tabs">
                    <li class="active"><a data-tab href="#tabContent1" style="font-weight: bold;">手机验证</a></li>
                    <li><a data-tab href="#tabContent2" style="font-weight: bold;">密保验证</a></li>
                </ul>
                <div style="padding-bottom: 10px;">&nbsp;</div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tabContent1" style="padding-top: 5px;">
                        <div>
                            <div class="col-sm-3">
                                <label for="exampleInputAddress1">验证手机号：</label>
                            </div>
                            <div class="col-sm-9" style="text-align: left">
                                <label for="exampleInputAddress1" class="text-yellow">
                                    <%:ViewData["phone"]%>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="padding-top: 5px;">
                                <label for="exampleInputPassword4">手机校验码：</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="yzm" disabled="disabled">
                            </div>
                            <div class="col-sm-2">
                                <button type="button" id="jyfBtn" class="btn btn-warning ">获取校验码</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tabContent2" style="padding-top: 5px;">
                        <div>
                            <div class="col-sm-3">
                                <label for="exampleInputAddress1">密保问题：</label>
                            </div>
                            <div class="col-sm-9" style="text-align: left">
                                <label for="exampleInputAddress1" class="text-yellow">
                                    <%:ViewData["question"]%>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="padding-top: 5px;">
                                <label for="exampleInputPassword4">密保答案：</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="answer">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12" style="padding-top: 5px; text-align: center">
                        <label for="exampleInputPassword4" id="msg"></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12" style="text-align: center">
                        <button type="button" id="tjBtn" class="btn btn-warning " >提交</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="ifooter">
        <div class="">
            <p><%:ViewData["copyright"]%></p>

            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {

            $('[data-tab]').on('shown.zui.tab', function (e) {
                if (e.target.innerText == "密保验证") {
                    $("#flag").val("2");
                } else {
                    $("#flag").val("1");
                }
            });

            $("#yzm").on("focus", function () {
                $("#yzm").removeClass("inputError");
                if ($("#yzm").popover) {
                    $("#yzm").popover("destroy");
                }
            })

            //提交
            $("#tjBtn").on("click", function () {
                var flag = $("#flag").val();
                if (flag == 1 && $("#yzm").val() == 0) {
                    showErrorMsg("请输入收到的验证码");
                } else if (flag == 2 && $("#answer").val() == 0) {
                    showErrorMsg("请输入密保答案");
                } else {
                    $.ajax({
                        url: "/ForgetPass/ValidateCode",
                        type: "POST",
                        data: "code=" + $("#yzm").val() + "&flag=" + flag + "&answer=" + $("#answer").val(),
                        success: function (result) {
                            if (result.status == "fail") {
                                var options = {
                                    container: 'body',  //在body层显示
                                    tipClass: 'popover-danger',
                                    trigger: 'manual',  //需手动显示
                                    template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content" style="color:red"></div></div>',
                                    content: result.msg        //显示文本
                                };
                                $("#yzm").addClass("inputError");
                                $("#yzm").popover(options).popover("show");   //构建
                            } else {
                                location.href = "/ForgetPass/ToChange";
                            }
                        }
                    });
                }
            });

            //回车键绑定
            document.onkeydown = function (event) {
                var e = event || window.event || arguments.callee.caller.arguments[0];
                if (e && e.keyCode == 13) {
                    $('#tjBtn').trigger("click");
                }
            };

            var flag = null;
            var s = 120;
            show = function () {
                if (s >= 1) {
                    $("#msg").html("校验码已发出，请注意查收短信，你可以在" + s + "秒后要求系统重新发送");
                } else {
                    clearInterval(flag);
                    s = 120;
                    $("#jyfBtn")[0].disabled = false;
                    $("#msg").html("");
                }
                s--;
            }

            //获取校验码
            $("#jyfBtn").on("click", function () {
                $.ajax({
                    url: "/ForgetPass/SendPhoneCode",
                    data: "etc=" + (new Date()).getTime(),
                    type: "POST",
                    success: function (result) {
                        if (result.status == "success") {
                            flag = setInterval(show, 1000);
                            $("#yzm")[0].disabled = false;
                            $("#tjBtn")[0].disabled = false;
                            $("#jyfBtn")[0].disabled = true;
                        }
                    }
                });
            })

        });
    </script>
</body>
</html>
