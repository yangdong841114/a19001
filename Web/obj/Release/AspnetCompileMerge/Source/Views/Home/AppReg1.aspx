﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <style type="text/css">
        .contentbox {
            position: relative;
        }
    </style>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <link type="text/css" href="/Content/APP/mobileSelect/mobileSelect.css" rel="stylesheet" />
    <script type="text/javascript" src="/Content/APP/mobileSelect/mobileSelect.js"></script>
    <link type="text/css" href="/Content/dropload/dropload.css" rel="stylesheet" />
    <script type="text/javascript" src="/Content/dropload/dropload.min.js"></script>
    <script type="text/javascript">

        //計算是否最後壹頁
        calLastPage = function (page, row, total) {
            if (total <= ((page - 1) * row)) { return true; }
            else { return false; }
        }

        //分頁加載數據
        LoadPageData = function (url, param, me, successFunction, noDataFunction) {
            $.ajax({
                url: url,// 請求網址
                type: 'POST',
                data: param,
                success: function (result) {
                    if (result == "NotPermission") { location.href = "#NoPermission"; }    //無權限訪問
                    else if (result == "NotAdminLogin") { location.href = "/AppManageLogin.html"; } //需要後臺登錄
                    else if (result == "NotUserLogin") { location.href = "/Home/AppIndex"; } //需要前臺登錄
                    else {
                        var rows = result.rows;
                        if (rows && rows != "null" && rows.length > 0) {
                            successFunction(rows, result.footer)
                            param.page += 1;
                            if (calLastPage(param.page, param.rows, result.total)) {
                                // 鎖定
                                me.lock();
                                // 無數據
                                me.noData(true);
                            }
                        } else {
                            if (noDataFunction) {
                                noDataFunction(rows, result.footer);
                            }
                            // 鎖定
                            me.lock();
                            // 無數據
                            me.noData(true);
                        }
                        me.resetload();
                    }
                },
                error: function (event, xhr, options, exc) {
                    self.showErrMsg("データの読み込みに失敗しました");
                    me.resetload();
                }
            });
        }


        //初始化select選擇框
        InitMobileSelect = function (selector, title, data, map, position, transition, callbackFn, showback) {
            $(selector)[0].readOnly = true;
            var catSelect = new MobileSelect({
                trigger: selector,
                title: title,
                wheels: [
                            { data: data }
                ],
                keyMap: map,
                position: position, //初始化定位 打開時默認選中的哪個 如果不填默認為0
                transitionEnd: function (indexArr, data) {
                    if (transition) {
                        transition(indexArr, data);
                    }
                },
                callback: function (indexArr, data) {
                    if (callbackFn) {
                        callbackFn(indexArr, data);
                    }
                },
                onShow: function (e) {
                    if (showback) {
                        showback(e);
                    }
                }
            });
        }

        //顯示錯誤信息
        showErrMsg = function (msg) {
            var msgbox = new $.zui.Messager('ヒントメッセージ：' + msg, {
                type: 'danger',
                icon: 'warning-sign',
                placement: 'center',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        //過濾特殊字符
        this.CheckSpecialCharacters = function (str) {
            var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
            var rs = "";
            for (var i = 0; i < str.length; i++) {
                rs =
                rs + str.substr(i, 1).replace(pattern, '');
            }
            return rs;
        }

        //查詢參數
        var param = { page: 1, rows: 10 };

        var productInit = false;

        var re = /^[0-9]+$/;

        //第壹步到第二步
        step1To2 = function () {
            if ($("#userId").val() != 0) {
                var usercode = CheckSpecialCharacters($("#userId").val());
                $("#userId").val(usercode);
            }
            if ($("#userId").val() == 0) {
                showErrMsg("会員番号を入力してください");
                $("#userId").focus();
            } else if ($("#password").val() == 0) {
                showErrMsg("ログインパスワードを入力してください");
                $("#password").focus();
            } else if ($("#passOpen").val() == 0) {
                showErrMsg("セキュリティパスワードを入力してください");
                $("#passOpen").focus();
            } else if ($("#threepass").val() == 0) {
                showErrMsg("取引のパスワードを入力してください");
                $("#threepass").focus();
            } else if ($("#passwordRe").val() != $("#password").val()) {
                showErrMsg("ログインパスワードとログインパスワードが違っていることを確認します");
                $("#passwordRe").focus();
            } else if ($("#passOpenRe").val() != $("#passOpen").val()) {
                showErrMsg("セキュリティパスワードとセキュリティパスワードが違っていることを確認します");
                $("#passOpenRe").focus();
            } else if ($("#threepassRe").val() != $("#threepass").val()) {
                showErrMsg("取引のパスワードと取引のパスワードが違っていることを確認します");
                $("#threepassRe").focus();
            } else {
                $("#setpTab1").css("display", "none");
                $("#setpTab2").css("display", "block");
            }
        }

        //第二步返回第壹步
        function back2To1() {
            $("#setpTab2").css("display", "none");
            $("#setpTab1").css("display", "block");
        }

        //第二步到第三步
        function step2To3() {
            if ($("#fatherName").val() == 0) {
                showErrMsg("接点人番号を入力してください");
                $("#fatherName").focus();
            } else if ($("#reName").val() == 0) {
                showErrMsg("推薦者番号を入力してください");
                $("#reName").focus();
            } else {
                $("#setpTab2").css("display", "none");
                $("#setpTab3").css("display", "block");
            }
        }

        //第三步返回第二步
        back3To2 = function () {
            $("#setpTab3").css("display", "none");
            $("#setpTab2").css("display", "block");
        }

        //打開註冊協議
        openRegisterWin = function (index) {
            $("#zcxydiv").css("display", "block");
            $("#setpTab2").css("display", "none");
        }

        //同意並註冊
        agreeRegister = function (ind) {
            $("#zcxydiv").css("display", "none");
            $("#setpTab2").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        var indexChecked = [0, 0, 0];

        ////打開時確認選中數據
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        ////選擇確認
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }
        //選擇左右區
        checkedTreePlace = function (index) {
            $("#treePlace1").removeClass("active");
            $("#treePlace2").removeClass("active");
            if (index == -1) {
                $("#treePlace1").addClass("active");
            } else {
                $("#treePlace2").addClass("active");
            }
        }

        //減少數量
        subNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val) && parseInt(val) > 0) {
                $("#buyNum" + id).val(parseInt(val) - 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) - 1);
                calTotalMoney();
            }
        }

        //增加數量
        addNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val)) {
                $("#buyNum" + id).val(parseInt(val) + 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) + 1);
                calTotalMoney();
            }
        }

        //錄入數量
        changeBuyNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (!re.test(val)) {
                $("#buyNum" + id).val($("#buyNum" + id).attr("dataVal"));
            } else {
                calTotalMoney();
            }
        }

        //計算總金額
        calTotalMoney = function () {
            var total = 0;
            $(".productBox").each(function () {
                if (this.checked) {
                    var id = $(this).attr("dataId");
                    var num = $("#buyNum" + id).val();
                    if (re.test(num)) {
                        num = parseInt(num);
                        var price = parseFloat($(this).attr("dataPrice"));
                        total = total + (num * price);
                    }
                }
            });
            $("#totalMoney").html(total);
        }

        //商品勾選的上壹步
        prevProduct = function () {
            $("#productSelectDiv").css("display", "none");
            $("#setpTab3").css("display", "block");
        }

        //下壹步，商品勾選
        netProduct = function () {
            var checked = true;
            //數據校驗
            $("#setpTab3 input").each(function (index, ele) {
                var jdom = $(this);
                if (jdom.attr("emptymsg") && jdom.val() == 0) {
                    showErrMsg(jdom.attr("emptymsg"));
                    jdom.focus();
                    checked = false;
                }
                if (!checked) { return false; }
            });
            if (checked) {
                //手機號碼驗證
                var ptext = /^1(3|4|5|6|7|8)\d{9}$/;
                if (!ptext.test($("#phone").val())) {
                    showErrMsg("携帯電話番号フォーマットが間違っています");
                    $("#phone").focus();
                } else if ($("#read")[0].checked == false) {
                    showErrMsg("登録プロトコルにチェックしてください");
                } else {
                    $("#setpTab3").css("display", "none");
                    $("#productSelectDiv").css("display", "block");
                    scrollTo(0, 0);
                    if (!productInit) {
                        var dropload = $('#productData').dropload({
                            scrollArea: window,
                            domDown: { domNoData: '<p class="dropload-noData"></p>' },
                            loadDownFn: function (me) {
                                LoadPageData("/Home/GetProductListPage", param, me,
                                    function (rows, footers) {
                                        var html = "";
                                        for (var i = 0; i < rows.length; i++) {
                                            var dto = rows[i];
                                            html += '<dl><dt style="vertical-align:middle;"><table style="float:left;"><tr>' +
                                                  '<td><input type="checkbox" onclick="calTotalMoney()" dataName="' + dto.productName + '" dataId="' + dto.id + '" dataPrice="'
                                                  + dto.price + '" class="productBox" style="float:left;width:2rem;height:2rem;" />&nbsp;</td>' +
                                                  '<td><img src="' + dto.imgUrl + '" /></td></tr></table></dt>' +
                                                  '<dd><div class="cartcomminfo"><dl><dt></dt><dd><span>' + dto.productCode + '</span></dd></dl>' +
                                                  '<h2>' + dto.productName + '</h2><h3>¥' + dto.price + '</h3><dl><dt></dt>' +
                                                  '<dd><button class="btnminus" onclick=\'subNum("' + dto.id + '")\'><i class="fa fa-minus"></i></button>' +
                                                  '<input type="number" onblur=\'changeBuyNum("' + dto.id + '")\' dataVal="0" class="txtamount" id="buyNum' + dto.id + '" value="0" />' +
                                                  '<button class="btnplus" onclick=\'addNum("' + dto.id + '")\'><i class="fa fa-plus"></i></button></dd>' +
                                                  '</dl></div></dd></dl>';
                                        }
                                        $("#productDataList").append(html);
                                    }, function () {
                                        $("#productDataList").append('<p class="dropload-noData">データがありません</p>');
                                    });
                            }
                        });


                    }
                }// end - else
            }
        }

        $(document).ready(function () {


            //提交註冊按鈕
            $("#saveBtn").bind("click", function () {
                var fs = {};
                $("#setpTab1 input").each(function () {
                    fs[this.id] = this.value;
                });
                $("#setpTab2 input").each(function () {
                    fs[this.id] = this.value;
                });

                if ($("#read")[0].checked) { fs["read"] = "read"; }
                var checked2 = true;

                if (checked2) {

                    if ($("#treePlace1").hasClass("active")) { fs["treePlace"] = -1; }
                    else { fs["treePlace"] = 1; }
                    fs["userId"] = $("#qzUserId").html() + "" + fs["userId"];
                    fs["sourceMachine"] = "app";
                    $.ajax({
                        url: "/Home/RegisterMember",           //請求地址
                        type: "POST",       //POST提交
                        data: fs,         //數據參數
                        beforeSend: function () {
                            document.getElementById("saveBtn").disabled = true;
                        },
                        success: function (result) {
                            document.getElementById("saveBtn").disabled = false;
                            if (result.status == "fail") {
                                showErrMsg(result.msg);
                            } else {
                                alert("登録成功");
                                location.href = "/Home/Index";
                            }
                        },
                        error: function (event, xhr, options, exc) {
                            document.getElementById("saveBtn").disabled = false;
                            showErrMsg("操作に失敗しました！");
                        }
                    });


                }
            });


            $.ajax({
                url: "/Home/GetRegBank",           //請求地址
                type: "POST",       //POST提交
                data: {},         //數據參數
                success: function (result) {
                    $("#regContent").html(result.map["zcxy"]);
                    var areaMap = null;
                    var areaData = null;
                    //區域
                    areaMap = result.map["area"];
                    if (areaMap) {
                        areaData = areaMap[0];
                        for (var i = 0; i < areaData.length; i++) {
                            var dto = areaData[i];
                            if (areaMap[dto.id]) {
                                dto["childs"] = areaMap[dto.id];
                                for (var j = 0; j < dto["childs"].length; j++) {
                                    var child = dto["childs"][j];
                                    if (areaMap[child.id]) {
                                        child["childs"] = areaMap[child.id];
                                    }
                                }
                            }
                        }
                    }

                    //省市區選擇
                    var proSet = InitMobileSelect('#province', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);
                    var citySet = InitMobileSelect('#city', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);
                    var areaSet = InitMobileSelect('#area', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);

                    //輸入框取消按鈕
                    $(".erase").each(function () {
                        var dom = $(this);
                        dom.bind("click", function () {
                            var prev = dom.prev();
                            if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                                $("#province").val("");
                                $("#city").val("");
                                $("#area").val("");
                            } else if (prev[0].id == "reName" || prev[0].id == "shopName" || prev[0].id == "fatherName") {
                                $("#" + prev[0].id + "" + prev.attr("checkflag")).empty();
                            }
                            dom.prev().val("");
                        });
                    });

                    //綁定獲離開焦點事件
                    $("input").each(function (index, ele) {
                        var dom = $(this);
                        if (dom.attr("checkflag")) {
                            var flag = dom.attr("checkflag");
                            var domId = dom[0].id;
                            dom.bind("blur", function (event) {
                                if (dom.val() && dom.val() != 0) {
                                    var userId = dom.val();
                                    $.ajax({
                                        url: "/Home/CheckUserId",           //請求地址
                                        type: "POST",       //POST提交
                                        data: { userId: userId, flag: flag },         //數據參數
                                        success: function (result) {
                                            if (result.status == "fail") {
                                                $("#" + domId + "" + flag).html(result.msg);
                                            } else {
                                                $("#" + domId + "" + flag).html(result.msg);
                                            }
                                        },
                                        error: function (event, xhr, options, exc) {
                                            showErrMsg("操作に失敗しました！");
                                        }
                                    });
                                }
                            });
                        }
                    });
                },
                error: function (event, xhr, options, exc) {
                    showErrMsg("操作に失敗しました！");
                }
            });



        });

    </script>
</head>
<body>
    <header id="headerHtml">
        <div class="membercentre">
            <%--<a href="#main"><i class="homeicon"><img src="/Content/APP/User/images/homew.png" /></i></a>--%>
            <h2 id="title">&nbsp;&nbsp;<%:ViewData["sitename"]%></h2>
        </div>
        <div class="clear"></div>
    </header>
    <main id="center" class="mainbox">
        <div class="entryinfo" id="setpTab1" >
    <input type="hidden" id="regMoney" />
    <input type="hidden" id="regNum" />
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;ログイン
    </div>
    <dl>
        <dt>会員番号</dt>
        <dd>
            <table style="padding:0rem 1rem 0rem 10rem;">
                <tr>
                    <td style="font-size:1.4rem;" id="qzUserId"><%:ViewData["userIdPrefix"]%></td>
                    <td>
                        <input type="text" class="entrytxt" required="required" placeholder="会員番号を入力してください" style="padding:0 0 0 5px;" id="userId" checkflag="1" name="userId" emptymsg="会員番号は空ではできません" />
                        <span class="erase"><i class="fa fa-times-circle-o"></i></span>
                    </td>
                </tr>
                
            </table>
        </dd>
    </dl>

    <dl>
        <dt>番号付けメッセージ</dt>
        <dd>
            <p><span id="userId1"></span></p>
        </dd>
    </dl>
    
    <%--<dl>
        <dt>默認登錄密碼</dt>
        <dd>
            <p><span id="dfpass1"></span></p>
        </dd>
    </dl>--%>
    <dl>
        <dt>ログインパスワード</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="ログインパスワードを入力してください" id="password" name="password" placeholder="パスワード" emptymsg="ログインパスワードが空ではありません" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>パスワードを確認する</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="パスワードを確認してください" id="passwordRe" name="passwordRe" placeholder="パスワード" emptymsg="ログインパスワードが空でないことを確認します" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <%--<dl>
        <dt>默認安全密碼</dt>
        <dd>
            <p><span id="dfpass2"></span></p>
        </dd>
    </dl>--%>
    <dl>
        <dt>セキュリティパスワード</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="セキュリティパスワードを入力してください" id="passOpen" name="passOpen" placeholder="パスワード" emptymsg="セキュリティパスワードが空です" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>セキュリティパスワードの確認</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="パスワードを確認してください" id="passOpenRe" name="passOpenRe" placeholder="パスワード" emptymsg="安全確認パスワードは空ではありません" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <%--<dl>
        <dt>默認交易密碼</dt>
        <dd>
            <p><span id="dfpass3"></span></p>
        </dd>
    </dl>--%>
    <dl>
        <dt>取引パスワード</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="取引のパスワードを入力してください" id="threepass" name="threepass" placeholder="パスワード" emptymsg="取引のパスワードは空です" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>取引パスワードの確認</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="パスワードを確認してください" id="threepassRe" name="threepassRe" placeholder="パスワード" emptymsg="取引のパスワードを確認すると空にできません" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga1">
            <li><button class="bigbtn" onclick="step1To2()">次のステップ：インターネット資料を記入する</button></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<div class="entryinfo" id="setpTab2" style="display:none;">
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-podcast" aria-hidden="true"></i>&nbsp;インターネット情報
    </div>
    <%--<dl>
        <dt>安置人編號</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="請輸入安置人編號" id="fatherName" name="fatherName" checkflag="2" emptymsg="安置人編號不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>安置人名稱</dt>
        <dd>
            <p><span id="fatherName2"></span></p>
        </dd>
    </dl>

    <dl>
        <dt>安置區域</dt>
        <dd>
            <div class="option">
                <ul class="tga3">
                    <li class="active" id="treePlace1"><a href="javascript:checkedTreePlace(-1);">左區</a></li>
                    <li id="treePlace2"><a href="javascript:checkedTreePlace(1);">右區</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </dd>
    </dl>--%>
    <dl>
        <dt>推薦者番号</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="reName" name="reName" value="<%:ViewData["reName"]%>"  checkflag="3" emptymsg="推薦人編號不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>推薦人の名前</dt>
        <dd>
            <p><span id="reName3"><%:ViewData["reUserName"]%></span></p>
        </dd>
    </dl>
     <dl>
        <dt>実名</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="実名を入力してください" id="userName" name="userName" emptymsg="真實姓名不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>ID番号/パスポート番号</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="ID番号/パスポート番号を入力してください" id="code" name="code" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
     <dl>
        <dt></dt>
        <dd style="text-align:center;padding-left:20%;padding-top:1.2rem;">
            <table>
                <tr>
                    <td><input type="checkbox" id="read" name="read" value="read" style="width:2rem;height:2rem;" />&nbsp;</td>
                    <td style="font-size:1.4rem;">私はすでに読んで同意しました</label><a href="javascript:openRegisterWin(1)" style="color:blue;">《註冊協議》</a></td>
                </tr>
            </table>
        </dd>
    </dl>
    
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="back2To1()" style="width:90%;">前のステップ</button></li>
             <li><button class="bigbtn" style="width:90%;" id="saveBtn">登録の提出</button></li>
           
        </ul>
        <div class="clear"></div>
    </div>
</div>
<div class="entryinfo" id="setpTab3" style="display: none; ">
     <li><button class="bigbtn" onclick="step2To3()" style="width:90%;">下壹步：填寫基礎資料</button></li>
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-address-book-o" aria-hidden="true"></i>&nbsp;基礎資料
    </div>
   
    
    <dl>
        <dt>手機號碼</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="請輸入手機號碼" id="phone" name="phone" emptymsg="手機號碼不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>省</dt>
        <dd>
            <input type="text" class="entrytxt area" id="province" name="province" placeholder="請選擇省" readonly emptymsg="請選擇省" />
        </dd>
    </dl>
    <dl>
        <dt>市</dt>
        <dd>
            <input type="text" class="entrytxt area" id="city" name="city" placeholder="請選擇市" readonly emptymsg="請選擇市" />
        </dd>
    </dl>
    <dl>
        <dt>エリア</dt>
        <dd>
            <input type="text" class="entrytxt area" id="area" name="area" />
        </dd>
    </dl>
    <dl>
        <dt>詳細住所</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="詳細住所を入力してください" id="address" name="address" emptymsg="詳細地址不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
   
    <div class="btnbox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="back3To2()" style="width:90%;">前のステップ</button></li>
            <li><button class="bigbtn" onclick="netProduct()" style="width:90%;">次のステップ：商品の選り取り</button></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>

<style type="text/css">
    .cont {
        font-size: 1.4rem;
        max-width: 100%;
        width: 100%;
        padding: 8px 8px 8px 8px;
        /*overflow-x: hidden;*/
        box-sizing: border-box;
        overflow-y: auto;
        padding-top: 10px;
        background-color: #fff;
    }

        .cont img {
            max-width: 100%;
        }

    .h1 {
        font-size: 2rem;
        text-align: center;
        font-weight: normal;
        margin-bottom: 1rem;
        color: #333;
        padding-left: 8px;
        padding-right: 8px;
    }
</style>

<div id="zcxydiv" style="display:none;">
    <div id="regContent" class="cont" >
        
    </div>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="agreeRegister(0)" style="width:90%;">閉じる</button></li>
            <li><button class="bigbtn" style="width:90%;" onclick="agreeRegister(1)">同意し、登録する</button></li>
        </ul>
    </div>
</div>

<div id="productSelectDiv" style="display:none;">
    <div class="bigtitle">
        <h2>各レベルの投資金額：</h2>
        <div class="clear"></div>
    </div>
    <div class="entryinfo">
        <div class="leveldivision">
            <ul class="tga3">
                <li><i class="fa fa-credit-card levelgeneral"></i><br />普通のカード：<span id="pk"><%:ViewData["pk"]%> </span></li>
                <li><i class="fa fa-credit-card levelsilver"></i><br />銀カード：<span id="yk"><%:ViewData["yk"]%> </span></li>
                <li><i class="fa fa-credit-card levelgold"></i><br />ゴールドカード：<span id="jk"><%:ViewData["jk"]%> </span></li>
            </ul>
            <div class="clear"></div>
        </div>
        <dl>
            <dt>注文総額</dt>
            <dd>
                <p><span id="totalMoney">0</span></p>
            </dd>
        </dl>
    </div>
    <div id="productData">
        <div class="cartlist cartlists" id="productDataList" >
        </div>
    </div>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="prevProduct()" style="width:90%;">前のステップ</button></li>
           
        </ul>
    </div>
</div>
    </main>

</body>
</html>

