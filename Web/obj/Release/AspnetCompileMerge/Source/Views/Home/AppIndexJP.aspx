﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<html lang="zh">--%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登錄</title>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <script type="text/javascript" src="/Content/APP/js/language.js"></script>
</head>
<body class="loginbg">
   <div class="loginenter">
    <div class="loginlogo">
        <img src="/Content/APP/User/images/logo1.png" />
        <p></p>
    </div>
 

  
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconuser.png" /></dt>
            <dd>
              <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" /></span>
                <input type="text" class="entertxt" id="userId" placeholder="請輸入用戶名" />
            </dd>
        </dl>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconpassword.png" /></dt>
            <dd> <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" /></span>
                <input type="password" class="entertxt" id="password" placeholder="請登錄密碼" />
            </dd>
        </dl>
        <div class="loginset">
            <label>
                <input type="checkbox" value="remember" id="remember" />記住我</label>
            <a href="/wap/UserJP/index.html">日文</a>
            <a href="/ForgetPass/Index">忘記密碼？</a>
            <div class="clear"></div>
        </div>
        <div class="lbtnbox">
            <button class="loginbtn" onclick="login()">登錄</button>
            <div  style="text-align:center; margin-top:10px;">
            <p><a style="color:#ffc41f;" href="/Home/Reg/bioplusx">註冊新用戶</a></p>
        </div>
        </div>
        
    </div>
     <div  style="display:none;">
    <script type="text/javascript">
      <%: @Html.Raw(ViewData["WebCode"])%>
        cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');
    </script>
          </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $(".erase").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });
    });

    //顯示錯誤消息
    showErrorMsg = function (msg) {
        var msgbox = new $.zui.Messager('提示消息：' + msg, {
            type: 'danger',
            icon: 'warning-sign',
            placement: 'center',
            parent: 'body',
            close: true
        });
        msgbox.show();
    }

    //登錄校驗
    login = function () {
        if ($("#userId").val() == 0) {
            showErrorMsg("用戶名不能為空");
        } else if ($("#password").val() == 0) {
            showErrorMsg("密碼不能為空");
        }
        else {
            var data = "userId=" + $("#userId").val() + "&password=" + $("#password").val();
            if (document.getElementById("remember").checked) {
                data += "&remenber=1";
            } else {
                data += "&remenber=0";
            }
            $.ajax({
                url: "/Common/MemberLoginByPhone",
                type: "POST",
                data: data,
                success: function (result) {
                    if (result.status == "fail") {
                        showErrorMsg(result.msg);
                    } else {
                        location.href = "/wap/UserJP/index.html";
                    }
                }
            });
            //$("#loginForm").submit();
        }
    }

</script>

</html>

