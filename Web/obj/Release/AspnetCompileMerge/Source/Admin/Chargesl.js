
define(['text!Chargesl.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Chargesl, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "算力");
        appView.html(Chargesl);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#Charge", toUid: 0 }, function () { });

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
            columns: [[
                { field: 'userName', title: '会员姓名', width: '100' },
                { field: 'rgys', title: '认购月数', width: '100' },
                { field: 'xhdhm', title: '消耗兑换码', width: '80' },
                { field: 'ksTime', title: '开始日期', width: '130' },
                { field: 'jsTime', title: '截止日期', width: '130' },
              
                { field: 'addTime', title: '认购日期', width: '130' },
                { field: 'fpv', title: '返PV', width: '130' }
               
            ]],
            url: "Charge/GetListPagesl"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["ksTime"] = utils.changeDateFormat(data.rows[i]["ksTime"]);
                    data.rows[i]["jsTime"] = utils.changeDateFormat(data.rows[i]["jsTime"]);
                }
                if (data && data.footer)
                {
                    var html_totalepoints = " 已审核总金额:XX.XX 待审核总金额:XX.XX";
                    for (var i = 0; i < data.footer.length; i++) {
                        var dto = data.footer[i];
                        html_totalepoints = '已审核总金额:' + dto.epointsPay + ' &nbsp;  待审核总金额:' + dto.epointsNotpay + '';
                    }
                    //$("#totalepoints").html(html_totalepoints);
                }
            }
            return data;
        }, function () {
            $(".img-thumbnail").each(function (index, ele) {
                $(this).lightbox();
            })

           
           
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "Charge/ExportChargeExcelsl";
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});