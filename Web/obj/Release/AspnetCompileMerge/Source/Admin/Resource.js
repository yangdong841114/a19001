
define(['text!Resource.html', 'jquery', 'j_easyui'], function (Resource, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "菜单管理");

        //ztree对象引用
        var resourceTree = null;

        //保存成功时的回调函数
        //parentNode:添加节点的上级节点
        //updateNode:更新的原节点
        //newObj:添加或更新操作后返回的对象
        saveSuccessCallBack = function (parentNode, updateNode, newObj) {
            //存在updateNode表示是更新操作，否则是添加节点操作
            if (updateNode != null) {
                updateNode.resourceName = newObj.resourceName;
                updateNode.curl = newObj.curl;
                updateNode.icon = newObj.icon;
                updateNode.isMenu = newObj.isMenu;
                updateNode.isShow = newObj.isShow;
                updateNode.imgUrl = newObj.imgUrl;
                updateNode.pass = newObj.pass;
                resourceTree.updateNode(updateNode);
            } else {
                resourceTree.addNodes(parentNode, -1, newObj);
            }
        }

        //根据obj加载表单数据
        loadForm = function (obj) {
            if (obj) {
                for (name in obj) {
                    if ($("#" + name)) {
                        $("#" + name).val(obj[name]);
                    }
                }
            }
        }

        //清空表单
        clearForm = function () {
            //清空隐藏域
            $("#id").val("");
            $("#parentResourceId").val("");
            //清空标题
            $("#formTitle").html("&nbsp;");
            //清空表单
            $('#editModalForm')[0].reset();;
        }

        utils.AjaxPostNotLoadding("Resource/GetList", null, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                var saveMsg = "";      //保存时提示消息
                var parentNode = null; //插入tree节点的上级节点
                var updateNode = null; //更新tree节点时的原节点

                //ztree设置
                var treesetting = {
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id", 	              // id编号字段名
                            pIdKey: "parentResourceId",   // 父id编号字段名
                            rootPId: null
                        },
                        key: {
                            name: "resourceName"    //显示名称字段名
                        }
                    },
                    view: {
                        showLine: false,            //不显示连接线
                        dblClickExpand: false       //禁用双击展开
                    },
                    callback: {
                        onClick: function (e, treeId, node) {
                            if (node.isParent) {
                                resourceTree.expandNode(node);
                            }
                            //组合表单数据
                            var obj = {
                                id: node.id, resourceName: node.resourceName, parentResourceId: (node.parentResourceId == 0 || node.parentResourceId == null) ? '无' : node.parentResourceId,
                                curl: node.curl, icon: node.icon, isMenu: node.isMenu, isShow: node.isShow, pass: node.pass==null?0:node.pass,imgUrl:node.imgUrl
                            }
                            obj["code"] = obj.id;
                            obj["parent"] = obj.parentResourceId;

                            parentNode = null;  
                            updateNode = node;
                            //清空表单数据
                            clearForm();
                            //根据选中的行记录加载数据
                            loadForm(obj);
                            $("#formTitle").html("编辑 " + node.resourceName + "（" + node.id + "）");
                            saveMsg = "您确定要保存对【" + node.resourceName + "（" + node.id + "）】的更改吗？";
                        }
                    }
                };

                //初始化布局，HTML
                appView.html(Resource);
                $('#resourceLayout').layout();

                //生成树
                resourceTree = $.fn.zTree.init($("#resourceTree"), treesetting, result.list);

                //查找根节点并自动展开
                var rootNodes = resourceTree.getNodesByFilter(function (node) {
                    if (node.parentResourceId == null || node.parentResourceId == 0) {
                        return true;
                    }
                });
                if (rootNodes && rootNodes.length > 0) {
                    for (var i = 0; i < rootNodes.length; i++) {
                        resourceTree.expandNode(rootNodes[i], true, false, true);
                    }
                }

                //初始化编辑区input text
                $(".easyui-textbox").each(function (i, ipt) {
                    $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });
                });

                //添加同级
                $("#addSameBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择添加同级节点的位置");
                    } else {
                        node = nodes[0];
                        var obj = {
                            id: "自动生成", parentResourceId: (node.parentResourceId == 0 || node.parentResourceId == null) ? '无' : node.parentResourceId
                        }
                        obj["code"] = obj.id;
                        obj["parent"] = obj.parentResourceId;

                        //添加节点位置的上级节点
                        updateNode = null;
                        if (obj.parentResourceId == "无") {
                            parentNode = null;
                        } else {
                            parentNode = node.getParentNode();
                        }

                        //清空表单数据
                        clearForm();
                        //根据选中的行记录加载数据
                        loadForm(obj);
                        $("#formTitle").html(node.resourceName + "（" + node.id + "）添加同级");
                        saveMsg = "您确定要在【" + node.resourceName + "（" + node.id + "）】添加同级节点吗？";
                    }
                })

                //添加下级
                $("#addSubBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择上级节点");
                    } else {
                        node = nodes[0];
                        var obj = {
                            id: "自动生成", parentResourceId: node.id
                        }
                        obj["code"] = obj.id;
                        obj["parent"] = obj.parentResourceId;

                        //添加节点位置的上级节点
                        updateNode = null;
                        parentNode = node;

                        //清空表单数据
                        clearForm();
                        //根据选中的行记录加载数据
                        loadForm(obj);
                        $("#formTitle").html(node.resourceName + "（" + node.id + "）添加下级");
                        saveMsg = "您确定要为【" + node.resourceName + "（" + node.id + "）】添加下级节点吗？";
                    }
                })

                //删除
                $("#deleteBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择需要删除的节点");
                    } else {
                        node = nodes[0];
                        utils.confirm("您确定要删除【" + node.resourceName + "（" + node.id + "）】及其下级所有子节点吗？", function () {
                            utils.AjaxPost("Resource/Delete", { id: node.id }, function (result) {
                                if (result.msg == "success") {
                                    resourceTree.removeChildNodes(node);//清空当前节点的子节点
                                    resourceTree.removeNode(node);      //清空当前节点
                                    utils.showSuccessMsg("删除成功");
                                    
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            })
                        });
                    }
                })

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var val = $("#parentResourceId").val();
                    if (!val || val == 0) { return; }
                    if ($("#editModalForm").form('validate')) {
                        utils.confirm(saveMsg, function () {
                            if (checkedForm()) {
                                var data = $("#editModalForm").serializeObject();
                                utils.AjaxPost("Resource/SaveOrUpdate", data, function (result) {
                                    if (result.status == "fail") {
                                        utils.showErrMsg(result.msg);
                                    } else {
                                        saveSuccessCallBack(parentNode, updateNode, result.result); //更新tree节点或添加节点
                                        utils.showSuccessMsg(result.msg);
                                        clearForm();        //清空表单
                                    }
                                })
                            }
                        })
                    }
                });

                //检查表单
                checkedForm = function () {
                    var ischecked = true;
                    if ($("#resourceName").val() == 0) {
                        $("#resourceName").addClass("inputError");
                        ischecked = false;
                    }
                    //菜单类型为资源的情况下，必须填写访问地址
                    if ($("#isMenu").val() == "N" && $("#curl").val()==0) {
                        $("#curl").addClass("inputError");
                        ischecked = false;
                    }
                    return ischecked;
                }

            }
        });

        controller.onRouteChange = function () {
            //销毁ztree
            if (resourceTree) {
                resourceTree.destroy();
            }
        };
    };

    return controller;
});