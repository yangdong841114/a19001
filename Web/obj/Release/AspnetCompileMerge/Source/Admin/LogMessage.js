
define(['text!LogMessage.html', 'jquery', 'j_easyui', 'datetimepicker'], function (LogMessage, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "异常消息");
        appView.html(LogMessage);

        //初始化日期
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //查看栈信息明细
        openDetailDialog = function (dataIndex) {
            var rows = grid.datagrid("getRows");
            if (rows && rows.length > 0) {
                var row = rows[dataIndex];
                $("#errMsg").val(row.message);
                $("#stackMsg").val(row.stackMsg);
                $('#errorContent').dialog({
                    width: 800,
                    height: 600,
                    closed: false,
                    cache: false,
                    modal: true
                });
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field: 'classUrl', title: '类路径', width: '10%' },
             { field: 'method', title: '方法名', width: '10%' },
             { field: 'addTime', title: '发生时间', width: '13%' },
             { field: 'message', title: '异常信息', width: '20%' },
             { field: 'stackMsg', title: '栈信息', width: '38%' },
             {
                 field: '_operate', title: '操作', width: '100', formatter: function (val, row, index) {
                     return '&nbsp;&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDelete" >删除</a>'+
                            '&nbsp;&nbsp;<a href="javascript:void(0);" dataIndex="' + index + '" class="gridFildQuery" >查看</a>';
                 }
             }
            ]],
            onDblClickRow: function (index, data) {
                openDetailDialog(index);
            },
            url: "LogMessage/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {

            //行查看按钮
            $(".gridFildQuery").each(function (i, dom) {
                dom.onclick = function () {
                    var dataIndex = $(dom).attr("dataIndex");
                    openDetailDialog(dataIndex);
                }
            });

            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定要删除该记录吗？", function () {
                        utils.AjaxPost("LogMessage/Delete", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("删除成功！");
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //清空按钮
        $("#removeAll").on("click", function () {
            utils.confirm("您确定要清空所有异常消息吗？", function () {
                utils.AjaxPost("LogMessage/RemoveAll", {}, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg("操作失败");
                    } else {
                        utils.showSuccessMsg("清除成功");
                        //重刷grid
                        queryGrid();
                    }
                });
            })
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#errorContent').dialog("destroy");
        };
    };

    return controller;
});