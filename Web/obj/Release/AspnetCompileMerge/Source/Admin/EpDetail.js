
define(['text!EpDetail.html', 'jquery', 'j_easyui', 'datetimepicker'], function (EpDetail, $) {

    var controller = function (sid) {
        //设置标题
        $("#center").panel("setTitle", "交易明细");
        appView.html(EpDetail);

        var statusDto = { 0: "待买家付款", 1: "待卖家收款", 2: "已完成", 3: "已取消" };

        //初始化下拉框
        $("#flag").empty();
        $("#flag").append("<option value='-1'>--全部--</option>");
        $("#flag").append("<option value='0'>待买家付款</option>");
        $("#flag").append("<option value='1'>待卖家收款</option>");
        $("#flag").append("<option value='2'>已完成</option>");

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            columns: [[
             { field: 'number', title: '买单单号', width: '170' },
             { field: 'userId', title: '买家编号', width: '100' },
             { field: 'buyNum', title: '购买数量', width: '70' },
             { field: 'payMoney', title: '应付金额', width: '70' },
             { field: 'addTime', title: '购买日期', width: '130' },
             { field: 'status', title: '状态', width: '80' },
             { field: 'suserId', title: '卖家编号', width: '100' },
             { field: 'phone', title: '手机', width: '100' },
             { field: 'qq', title: 'QQ', width: '100' },
             { field: 'bankName', title: '开户行', width: '100' },
             { field: 'bankCard', title: '银行卡号', width: '140' },
             { field: 'bankUser', title: '开户名', width: '100' },
             { field: 'bankAddress', title: '开户支行', width: '100' },
             {
                 field: '_operate', title: '操作', width: '160', align: 'center', formatter: function (val, row, index) {
                     if (row.flag == 0 || row.flag ==1) {
                         return '&nbsp;&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >取消买单</a>';
                     } else if (row.flag == 1) {
                         return '&nbsp;&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildEdit" >确认收款</a>';
                     } else {
                         return '';
                     }
                 }
             }
            ]],
            url: "EpDetail/GetListPage?sid="+sid
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    if (i == 0) {
                        $("#center").panel("setTitle", "挂卖记录【" + data.rows[i].snumber + "】交易明细");
                    }
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["status"] = statusDto[data.rows[i]["flag"]];
                }
            }
            return data;
        }, function () {
            //行确认收款
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("您确定收款吗？", function () {
                        utils.AjaxPost("EpDetail/PaySure", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                }
            });
            //行取消买单
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("您确认取消买单吗？", function () {
                        utils.AjaxPost("EpDetail/SaveCancel", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                }
            });
        }
        )

        //查询grid
        queryGrid = function () {
            var objs = $("#EpSaleQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
        };
    };

    return controller;
});