
define(['text!Registerprotocol.html', 'jquery'], function (Registerprotocol, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "注册协议");
        

        utils.AjaxPostNotLoadding("Registerprotocol/InitView", {}, function (result) {
            if (result.status == "success") {
                appView.html(Registerprotocol);
                var cont = "";
                if (result.result) {
                    cont = result.result.content;
                }
                //初始化编辑器
                $("#editor").height(document.body.offsetHeight - 230);
                var editor = new Quill("#editor", {
                    modules: {
                        toolbar: utils.getEditorToolbar()
                    },
                    theme: 'snow'
                });
                utils.setEditorHtml(editor, cont)

                $("#saveBtn").on("click", function () {
                    
                    if (editor.getText() == 0) {
                        utils.showErrMsg("请输入内容");
                    } else {
                        utils.AjaxPost("Registerprotocol/SaveByUeditor", { content: utils.getEditorHtml(editor) }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("保存成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                })
            }
        });

        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});