
define(['text!TakeCash.html', 'jquery', 'j_easyui', 'datetimepicker'], function (TakeCash, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "提现管理");
        appView.html(TakeCash);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#TakeCash", toUid: 0 }, function () { });

        utils.AjaxPostNotLoadding("TakeCash/GetTotalMoney", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                $("#auditMoney").html(result.msg);
                $("#noAuditMoney").html(result.other);
            }
        });

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "TakeCash/ExportTakeCashExcel";
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
            columns: [[
             { field: 'userName', title: '会员名称', width: '100' },
             { field: 'addtime', title: '申请日期', width: '130' },
             { field: 'epoints', title: '提现金额', width: '80' },
             { field: 'fee', title: '手续费', width: '80' },
             { field: 'smoney', title: '实际金额', width: '80' },
             { field: 'bankName', title: '开户行', width: '100' },
             { field: 'bankCard', title: '银行卡号', width: '130' },
             { field: 'bankUser', title: '开户名', width: '100' },
             { field: 'bankAddress', title: '开户地址', width: '150' },
             { field: 'status', title: '状态', width: '100' },
             { field: 'auditUser', title: '操作人', width: '80' },
             {
                 field: '_operate', title: '操作', width: '100', align: 'center', formatter: function (val, row, index) {
                     if (row.isPay == 1) {
                         return '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildEdit" >确认</a>&nbsp;&nbsp;' +
                                '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >取消</a>';
                     } else {
                         return "";
                     }
                 }
             }
            ]],
            url: "TakeCash/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addtime"] = utils.changeDateFormat(data.rows[i]["addtime"]);
                    data.rows[i]["smoney"] = data.rows[i].epoints - data.rows[i].fee;
                    if(data.rows[i].isPay == 1)
                        data.rows[i]["status"] ="待审核"; 
                    if(data.rows[i].isPay == 2)
                        data.rows[i]["status"] ="已通过"; 
                    if (data.rows[i].isPay == 3)
                        data.rows[i]["status"] = "已取消";
                }
            }
            return data;
        }, function () {
            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定审核通过吗？", function () {
                        utils.AjaxPost("TakeCash/AuditTakeCash", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
            //行删除按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定取消该记录吗？", function () {
                        utils.AjaxPost("TakeCash/CancelTakeCash", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});