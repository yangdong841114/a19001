
//一些公用JS
define(['jquery', 'j_easyui'], function ($) {

    var appUtils = function () {

        var self = this;

        this.popList = [];

        //获取富文本内容
        this.getEditorHtml = function (editor) {
            return editor.container.firstChild.innerHTML;
        }

        //设置富文本内容
        this.setEditorHtml = function (editor, cont) {
            editor.container.firstChild.innerHTML = cont;
        }

        //获取富文本标题头部
        this.getEditorToolbar = function () {
            return [['bold', 'italic', 'underline', 'strike'], [{ 'size': ['small', false, 'large', 'huge'] }], [{ 'color': [] }, { 'background': [] }], ['link', 'image'], [{ 'align': [] }]];
        }

        //清空查询条件
        this.clearQueryParam = function () {
            $(".searchbox input").each(function () {
                $(this).val("");
            });
            $(".searchdate input").each(function () {
                $(this).val("");
            });
        }

        //手机前台一级菜单原始图片
        this.appUserTopMenuImg = {};

        //后台一级菜单点击
        this.adminMenuClick = function (url,index) {
            $(".menuTop").each(function () {
                $(this).removeClass("active");
            });
            $("#menuTop" + index).addClass("active");
            location.href = url;
        }

        //前台一级菜单点击
        this.userMenuClick = function (url, index) {
            $(".menuTop").each(function () {
                var dataIndex = $(this).attr("dataIndex");
                $("#topMenuImg"+dataIndex).attr("src",self.appUserTopMenuImg[dataIndex]);
            });
            var imgName = self.appUserTopMenuImg[index];
            imgName = imgName.substring(0, imgName.indexOf("."));
            var newName = imgName + "s.png";
            $("#topMenuImg" + index).attr("src", newName);
            location.href = url;
        }

        //取消按钮
        this.CancelBtnBind = function (clearCallBack) {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    if (clearCallBack) {
                        clearCallBack(dom);
                    }
                    dom.prev().val("");
                })
            });
        }

        //显示或隐藏提示框
        this.showOrHiddenPromp = function () {
            $(".promptbg").toggleClass("promptbottom");
            $(".promptbgbox").toggleClass("promptbottom");
        }

        //初始化select选择框
        this.InitMobileSelect = function (selector, title, data, map, position, transition, callbackFn, showback) {
            $(selector)[0].readOnly = true;
            var catSelect = new mobileSelect({
                trigger: selector,
                title: title,
                wheels: [
                            { data: data }
                ],
                keyMap: map,
                position: position, //初始化定位 打开时默认选中的哪个 如果不填默认为0
                transitionEnd: function (indexArr, data) {
                    if (transition) {
                        transition(indexArr, data);
                    }
                },
                callback: function (indexArr, data) {
                    if (callbackFn) {
                        callbackFn(indexArr, data);
                    }
                },
                onShow: function (e) {
                    if (showback) {
                        showback(e);
                    }
                }
            });
        }

        //绑定搜索更多按钮的滑动效果的点击事件
        this.bindSearchmoreClick = function () {
            $('.searchmore').unbind();
            //绑定展开
            $('.searchmore').bind("click", function () {
                var $this = $('.searchmore i');
                $this.toggleClass('fa-rotate-180');
                $('.searchdate').slideToggle('slow');
            });
        }

        //初始化手机端的日期选择框
        this.initCalendar = function (ids) {
            if (ids || ids.length > 0) {
                for (var i = 0; i < ids.length; i++) {
                    calendar = new LCalendar();
                    $('#' + ids[i])[0].readOnly = true;
                    calendar.init({
                        'trigger': '#' + ids[i], //标签id
                        'type': 'date', //date 调出日期选择 datetime 调出日期时间选择 time 调出时间选择 ym 调出年月选择,
                        'minDate': (new Date().getFullYear() - 6) + '-' + 1 + '-' + 1, //最小日期
                        'maxDate': (new Date().getFullYear() + 3) + '-' + 12 + '-' + 31 //最大日期
                    });
                }
            }
        }

        //计算是否最后一页
        this.calLastPage = function (page, row, total) {
            if (total <= ((page - 1) * row)) { return true; }
            else { return false; }
        }

        //设置高度
        this.setHeight = function () {
            var $bodyheight = window.innerHeight;
            var $headerheigth = $(".headertop").outerHeight(true);
            var $footerheigth = $(".footerlink").outerHeight(true);
            $(".contentbox").height($bodyheight - $headerheigth - $footerheigth + "px");
        }

        //获取分页的参数对象
        this.getPageData = function () {
            return { page: 1, rows: 10 };
        }

        //点击展开详细信息
        this.showGridMessage = function (obj) {
            $(obj).children("i").toggleClass("fa-rotate-90");
            $(obj).next(".allinfo").slideToggle("slow");
            $(obj).next(".allinfo").toggleClass("allinfoheigth");
        }

        //分页加载数据
        this.LoadPageData = function (url, param, me, successFunction, noDataFunction) {
            $.ajax({
                url: url,// 请求网址
                type: 'POST',
                data: param,
                success: function (result) {
                    if (result == "NotPermission") { location.href = "#NoPermission"; }    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/AppManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/AppIndex"; } //需要前台登录
                    else {
                        var rows = result.rows;
                        if (rows && rows != "null" && rows.length > 0) {
                            successFunction(rows, result.footer)
                            param.page += 1;
                            if (self.calLastPage(param.page, param.rows, result.total)) {
                                // 锁定
                                me.lock();
                                // 无数据
                                me.noData(true);
                            }
                        } else {
                            if (noDataFunction) {
                                noDataFunction(rows, result.footer);
                            }
                            // 锁定
                            me.lock();
                            // 无数据
                            me.noData(true);
                        }
                        me.resetload();
                    }
                },
                error: function (event, xhr, options, exc) {
                    self.showErrMsg("加载数据失败");
                    me.resetload();
                }
            });
        }


        //确认对话框
        //msg：提示消息
        //okCallBack:用户点击确认后的回调函数
        //cancelCallBack:用户点击取消后的回调函数
        this.confirm = function (msg, okCallBack, cancelCallBack) {
            $.messager.confirm({
                ok: "确定",
                cancel: "取消",
                title: "提示",
                msg: msg,
                fn: function (ok) {
                    if (ok) { okCallBack(); }
                    else { cancelCallBack(); }
                }
            });
        }

        //如需要文件上传请用此方法
        //post ajax请求
        //url:请求地址
        //data:请求参数
        //successCallBack:请求成功的回调函数
        //errorCallBack:请求失败的回调函数
        this.AjaxPostForFormData = function (url, data, successCallBack, errorCallBack) {
            $.ajax({
                url: url,           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    //显示全屏loadding遮罩层
                    self.showFullScreenLoadding();
                },
                success: function (result) {
                    //隐藏全屏loadding遮罩层
                    self.hideFullScreenLoadding();
                    if (result == "NotPermission") { location.href = "#NoPermission"; }    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/AppManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/AppIndex"; } //需要前台登录
                    else {
                        //如有回调函数则调用
                        //没有回调函数默认显示操作成功消息框
                        if (successCallBack) {
                            successCallBack(result);
                        } else {
                            self.showSuccessMsg("操作成功！")
                        }
                    }
                },
                error: function (event, xhr, options, exc) {
                    self.hideFullScreenLoadding();//隐藏loadding遮罩层
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(event, xhr, options, exc);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        }

        //post ajax请求
        //url:请求地址
        //data:请求参数
        //successCallBack:请求成功的回调函数
        //errorCallBack:请求失败的回调函数
        this.AjaxPost = function (url, data, successCallBack, errorCallBack) {
            $.ajax({
                url: url,           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                beforeSend: function () {
                    //显示全屏loadding遮罩层
                    self.showFullScreenLoadding();
                },
                success: function (result) {
                    //隐藏全屏loadding遮罩层
                    self.hideFullScreenLoadding();
                    //如有回调函数则调用
                    //没有回调函数默认显示操作成功消息框
                    if (result == "NotPermission") { location.href = "#NoPermission"; }    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/AppManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/AppIndex"; } //需要前台登录
                    else {
                        if (successCallBack) {
                            successCallBack(result);
                        } else {
                            self.showSuccessMsg("操作成功！")
                        }
                    }
                },
                error: function (event, xhr, options, exc) {
                    self.hideFullScreenLoadding();//隐藏loadding遮罩层
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(event, xhr, options, exc);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        }

        this.AjaxPostNotLoadding = function (url, data, successCallBack, errorCallBack) {
            $.ajax({
                url: url,           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                success: function (result) {
                    //如有回调函数则调用
                    //没有回调函数默认显示操作成功消息框
                    if (result == "NotPermission") { location.href = "#NoPermission"; }    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/AppManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/AppIndex"; } //需要前台登录
                    else {
                        if (successCallBack) {
                            successCallBack(result);
                        } else {
                            self.showSuccessMsg("操作成功！")
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(XMLHttpRequest, textStatus, errorThrown);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        }

        //显示全屏loadding
        this.showFullScreenLoadding = function () {
            $.messager.progress({
                title: '请稍后',
                msg: '正在处理中...',
                interval: 100,
                text: ''
            });
        }

        //隐藏全屏loadding
        this.hideFullScreenLoadding = function () {
            $.messager.progress("close");
            //$('#mySmModal').modal('hide');
        }

        //显示红色失败边线框，obj为显示的dom
        this.addErrorClass = function (obj) {
            obj.removeClass("has-success");
            obj.addClass("has-error");
        }

        //显示绿色成功边线框，obj为显示的dom
        this.addSuccessClass = function (obj) {
            obj.removeClass("has-error");
            obj.addClass("has-success");
        }

        //销毁弹出层，obj为所属层dom
        this.destoryPopover = function (obj) {
            if (obj.popover) {
                obj.popover("destroy");
            }
        }

        //清空所有弹出层
        this.DestoryAllPopover = function () {
            if (self.popList && self.popList.length > 0) {
                for (var i = 0; i < self.popList.length; i++) {
                    self.destoryPopover(self.popList[i]);
                }
                self.popList = [];
            }
        }

        //显示弹出层,obj为所属层dom,msg:提示信息,cls:红色或绿色，默认绿色
        //有关详细信息请参考ZUI.js中的popover（弹出面板）章节
        this.showPopover = function (obj, msg, cls, container) {
            if (!cls) {
                cls = "popover-success";
            }
            //if (!container) {
            //    container = 'body';
            //}
            var color = cls == "popover-success" ? "green" : "red";
            var options = {
                container: 'body',  //在body层显示
                tipClass: cls,      //样式，popover-success：绿色，popover-danger：红色
                trigger: 'manual',  //需手动显示
                template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content" style="color:' + color + '"></div></div>',
                content: msg        //显示文本
            };
            if (container || container == false) {
                options["container"] = container;
            }
            var popDiv = obj.popover(options);   //构建
            popDiv.popover("show");    //显示
            self.popList.push(popDiv);
        }

        //显示错误消息
        this.showErrMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：' + msg, {
                type: 'danger',
                icon: 'warning-sign',
                placement: 'center',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        //过滤特殊字符
        this.CheckSpecialCharacters = function (str) {
            var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
            var rs = "";
            for (var i = 0; i < str.length; i++) {
                rs =
                rs + str.substr(i, 1).replace(pattern, '');
            }
            return rs;
        }

        //显示成功消息
        this.showSuccessMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：' + msg, {
                type: 'success',
                icon: 'check',
                placement: 'center',
                html: '<font style="">' + msg + '</font>',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        //asp.net mvc返回的datatime转换显示格式
        //val：需转换的值
        //format: time-> yyyy-mm-dd hh:mi:ss   date-> yyyy-mm-dd
        this.changeDateFormat = function (val, format) {
            if (val != null) {
                if (!format)
                    format = "time";
                var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
                //月份为0-11，所以+1，月份小于10时补个0
                var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                var dd = "";
                if (format == "time") {
                    var hour = date.getHours();
                    var minute = date.getMinutes();
                    var second = date.getSeconds();
                    var hs = hour > 9 ? hour : ("0" + hour);
                    var ms = minute > 9 ? minute : ("0" + minute);
                    var ss = second > 9 ? second : ("0" + second);
                    dd = date.getFullYear() + "-" + month + "-" + currentDate + " " + hs + ":" + ms + ":" + ss;
                } else if (format == "minute") {
                    var hour = date.getHours();
                    var minute = date.getMinutes();
                    var second = date.getSeconds();
                    var hs = hour > 9 ? hour : ("0" + hour);
                    var ms = minute > 9 ? minute : ("0" + minute);
                    dd = date.getFullYear() + "-" + month + "-" + currentDate + " " + hs + ":" + ms 
                }else if (format == "date") {
                    dd = date.getFullYear() + "-" + month + "-" + currentDate;
                }

                return dd;
            }

            return "";
        }

        //获取物流时间格式
        this.getWlDatetime = function (val) {
            if (val != null) {
                var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
                //月份为0-11，所以+1，月份小于10时补个0
                var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                var hour = date.getHours();
                var minute = date.getMinutes();
                var second = date.getSeconds();
                var hs = hour > 9 ? hour : ("0" + hour);
                var ms = minute > 9 ? minute : ("0" + minute);
                var ss = second > 9 ? second : ("0" + second);
                var dd = date.getFullYear() + "/" + month + "/" + currentDate;
                var time = hs + ":" + ms + ":" + ss;

                return [dd, time];
            }

            return "";
        }

        //序列化表单，包含radio,checkbox
        //$.fn.serializeObject = function () {
        //    var o = {};
        //    var a = this.serializeArray();
        //    $.each(a, function () {
        //        if (o[this.name] !== undefined) {
        //            if (!o[this.name].push) {
        //                o[this.name] = [o[this.name]];
        //            }
        //            o[this.name].push(this.value || '');
        //        } else {
        //            o[this.name] = this.value || '';
        //        }
        //    });
        //    var $radio = $('input[type=radio],input[type=checkbox]', this);
        //    $.each($radio, function () {
        //        if (!o.hasOwnProperty(this.name)) {
        //            o[this.name] = '';
        //        }
        //    });
        //    return o;
        //};

    };

    return new appUtils();
});