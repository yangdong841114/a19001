
define(['text!Password3.html', 'jquery'], function (Password3, $) {

    var controller = function (prePage) {
    	//设置标题
        $("#title").html("验证交易密码");
        appView.html(Password3);

        $(".logingb").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });

        //检查密码
        checkPassWord = function () {
            var val = $("#password").val();
            if (val == 0) {
                utils.showErrMsg("请录入交易密码");
            } else {
                utils.AjaxPost("/Admin/ManageWeb/CheckAPass3", { pass3: val }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        pass3 = true;
                        if (prePage.indexOf(".") != -1) {
                            var val = prePage.split(".");
                            location.href = "#" + val[0] + "/" + val[1];
                        } else {
                            location.href = "#" + prePage;
                        }
                    }
                });
            }
        }

        $("#sureBtn").on("click", function () {
            checkPassWord();
        })
        
        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});