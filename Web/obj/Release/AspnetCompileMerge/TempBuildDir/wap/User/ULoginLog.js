
define(['text!ULoginLog.html', 'jquery'], function (ULoginLog, $) {

    var controller = function (name) {

        $("#title").html("登錄日誌");
        appView.html(ULoginLog);
        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })


        var flagObj = { "0": "成功", "1": "失败（帐号不存在）", "2": "失败（密码验证失败）", "3": "失败（帐号未开通）", "4": "失败（帐号被冻结）", "5": "失败（帐号被登录锁定）" };

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ULoginLogdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/ULoginLog/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["loginTime"] = utils.changeDateFormat(rows[i]["loginTime"]);
                            rows[i]["flag"] = flagObj[rows[i]["flag"]];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.loginTime + '</time>';
                            if (dto.flag == '成功') {
                                html += '<span><font class="status-success">' + dto.flag + '</font></span>';
                            } else {
                                html += '<span><font class="status-red">' + dto.flag + '</font></span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>登錄時間</dt><dd>' + dto.loginTime + '</dd></dl><dl><dt>登錄帳號</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>姓名</dt><dd>' + dto.userName + '</dd></dl><dl><dt>IP</dt><dd>' + dto.loginIp + '</dd></dl>' +
                            '<dl><dt>描述</dt><dd>' + dto.mulx + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ULoginLogitemList").append(html);
                    }, function () {
                        $("#ULoginLogitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ULoginLogitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});