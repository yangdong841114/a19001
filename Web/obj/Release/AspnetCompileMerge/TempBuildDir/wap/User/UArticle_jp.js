
define(['text!UArticle_jp.html', 'jquery'], function (UArticle, $) {

    var controller = function (name) {

        $("#title").html("ニュース");
        appView.html(UArticle);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UAricledatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UArticle/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li><a href="#UArticDetail/' + dto.id + '">' + dto.title + '</a><time>' + dto.addTime + '</time></li>';
                        }
                        $("#UAricleitemList").append(html);
                    }, function () {
                        $("#UAricleitemList").append('<p class="dropload-noData">データがありません</p>');
                    });
            }
        });
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UAricleitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});