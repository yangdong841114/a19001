
define(['text!UCurrencyUserDetail_jp.html', 'jquery'], function (UCurrencyUserDetail, $) {

    var controller = function (agm) {
        if (agm) {
            $("#title").html("ボーナスの詳細");
            appView.html(UCurrencyUserDetail);

            //清空查詢條件按鈕
            $("#clearQueryBtn").bind("click", function () {
                utils.clearQueryParam();
            })

            //獎金類型選擇框
            var bonusClassList = $.extend(true, [], cacheList["BonusClass"]);
            bonusClassList.splice(0, 0, { id: 0, name: '全部' });
            utils.InitMobileSelect('#catName', 'ボーナスの種類', bonusClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                $("#catName").val(data[0].name);
                $("#cat").val(data[0].id);
            })

            //查詢參數
            this.param = utils.getPageData();

            var dropload = $('#UCurrencyUserDetailDataList').dropload({
                scrollArea: window,
                domDown: { domNoData: '<p class="dropload-noData"></p>' },
                loadDownFn: function (me) {
                    utils.LoadPageData("/User/UCurrency/GetDetailListPage?addDate=" + agm, param, me,
                        function (rows, footers) {
                            var html = "";
                            for (var i = 0; i < rows.length; i++) {
                                rows[i]["jstime"] = utils.changeDateFormat(rows[i]["jstime"]);
                                rows[i]["ff"] = rows[i]["ff"] == 1 ? "既発" : "未発";
                                rows[i]["yf"] = rows[i]["yf"].toFixed(2);
                                rows[i]["fee1"] = rows[i]["fee1"].toFixed(2);
                                rows[i]["fee2"] = rows[i]["fee2"].toFixed(2);
                                rows[i]["fee3"] = rows[i]["fee3"].toFixed(2);
                                rows[i]["sf"] = rows[i]["sf"].toFixed(2);
                                var dto = rows[i];
                                html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.catName + '</time><span class="sum">+' + dto.yf + '</span>' +
                                      '実の髪：<span class="sum">¥' + dto.sf + '</span><i class="fa fa-angle-right"></i></div>' +
                                      '<div class="allinfo">' +
                                      '<dl><dt>ボーナスの名前</dt><dd>' + dto.catName + '</dd></dl>' +
                                      '<dl><dt>未払金</dt><dd>' + dto.yf + '</dd></dl>' +
                                      '<dl><dt>実の髪</dt><dd>' + dto.sf + '</dd></dl><dl><dt>決済日</dt><dd>' + dto.jstime + '</dd></dl>' +
                                      '<dl><dt>配布状態</dt><dd>' + dto.ff + '</dd></dl><dl><dt>業務概要</dt><dd>' + dto.mulx + '</dd></dl>' +
                                      '</div></li>';
                            }
                            $("#UCurrencyUserDetailItemList").append(html);
                        }, function () {
                            $("#UCurrencyUserDetailItemList").append('<p class="dropload-noData">データがありません</p>');
                        });
                }
            });

            //查詢按鈕
            $("#searchBtn").on("click", function () {
                param.page = 1;
                $("#UCurrencyUserDetailItemList").empty();
                param["cat"] = $("#cat").val();
                dropload.unlock();
                dropload.noData(false);
                dropload.resetload();
            })

        }

        controller.onRouteChange = function () {

        };
    };

    return controller;
});