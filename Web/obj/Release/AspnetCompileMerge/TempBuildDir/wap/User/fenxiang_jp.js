
define(['text!fenxiang_jp.html', 'jquery'], function (fenxiang_jp, $) {

    var controller = function (name) {
        $("#title").html("共有を広める");
        $("body").addClass("shareqr");
        $('#footerlink').css('display', 'block');
        $('#shopfooter').css('display', 'none');
        appView.html(fenxiang_jp);

        utils.AjaxPostNotLoadding("/User/UserWeb/GetMainData", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                if (map) {
                    ////推广链接
                    var siteUrl = map.siteUrl;
                    var qrcodeUrl = map.fxqrcodeUrl;
                    //$("#siteUrl").attr("href", siteUrl);
                    //$("#siteUrl").html(siteUrl);

                    ////复制链接
                    //var clip = new ZeroClipboard(document.getElementById("copyUrl"));
                    if (map.first_userId == "noexit") location.href = "/Home/exitUserLogin";
                    //二维码
                    var qrcode = map.qrcode;
                    $("#qrcode").attr("src", "/UpLoad/qrcode/" + qrcode);
                    $("#userName").html(map.userName);
                    window._bd_share_config = {
                        common: {
                            bdText: '共有を広める',
                            bdDesc: '共有を広める',
                            bdUrl: siteUrl,
                            bdPic: qrcodeUrl
                        },
                        share: [{
                            "bdSize": 16
                        }],

                        image: [{
                            viewType: 'list',
                            viewPos: 'top',
                            viewColor: 'black',
                            viewSize: '16',
                            viewList: ['qzone', 'tsina', 'huaban', 'tqq', 'renren']
                        }],
                        selectShare: [{
                            "bdselectMiniList": ['qzone', 'tqq', 'kaixin001', 'bdxc', 'tqf']
                        }]
                    }
                    with (document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion=' + ~(-new Date() / 36e5)];

                }



            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $("body").removeClass("shareqr");
        };
    };

    return controller;
});