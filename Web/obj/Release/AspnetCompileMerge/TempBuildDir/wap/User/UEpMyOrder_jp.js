
define(['text!UEpMyOrder_jp.html', 'jquery'], function (UEpMyOrder, $) {

    var controller = function (name) {

        //设置标题
        $("#title").html("我的挂单");
        var statusDto = { 0: "挂卖中", 1: "部分售出", 2: "全部售出", 3: "已完成", 4: "已取消" };
        var statusList = [{ id: -1, value: "全部" }, { id: 0, value: "挂卖中" }, { id: 1, value: "部分售出" }, { id: 2, value: "全部售出" }, { id: 3, value: "已完成" }, { id: 4, value: "已取消" }, ]
        var agentJj = 0;
        var bei = 0;

        //初始化加载数据
        utils.AjaxPostNotLoadding("/User/UEpMyOrder/GetAgent", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UEpMyOrder);
                //清空查询条件按钮
                $("#clearQueryBtn").bind("click", function () {
                    utils.clearQueryParam();
                })

                agentJj = result.msg;
                bei = result.result.paramValue;

                //$("#xuqiu").html("挂卖数量需是" + bei + "的倍数");

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        dom.prev().val("");
                    });
                });

                //绑定展开搜索更多
                utils.bindSearchmoreClick();
                //初始化日期选择框
                utils.initCalendar(["startTime", "endTime"]);

                //查询参数
                this.param = utils.getPageData();

                //取消按钮窗口
                openCancleDialog = function (id, number, waitNum, saleNum) {
                    $("#dialogFormCont").css("display", "none");
                    $("#dialogCancelCont").css("display", "block");
                    $("#dialogTitle").html("确定取消挂卖吗？")
                    $("#gmdh").html(number);
                    $("#dssl").html(waitNum);
                    $("#gmsl").html(saleNum);
                    $("#sureBtn").html("确定取消")
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        utils.AjaxPost("/User/UEpMyOrder/SaveCancel", { id: id }, function (result) {
                            utils.showOrHiddenPromp();
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("取消成功");
                                agentJj = result.msg;
                                searchMethod();

                            }
                        });
                    });
                    utils.showOrHiddenPromp();
                }

                //挂卖按钮窗口
                $("#gmBtn").bind("click", function () {
                    $("#dialogFormCont").css("display", "block");
                    $("#dialogCancelCont").css("display", "none");
                    $("#dialogTitle").html("EP挂卖")
                    $("#wtNum").html(agentJj);
                    $("#sureBtn").html("确定挂卖")
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        var val = $("#saleNum").val();
                        if (val == 0 || isNaN(val) || val < 0) {
                            utils.showErrMsg("挂卖数量格式错误");
                        } else if ($("#phone").val() == 0) {
                            utils.showErrMsg("请填写手机号");
                        } else if ($("#phone").val() == 0) {
                            utils.showErrMsg("请填写QQ号");
                        } else {
                            var data = { saleNum: val, phone: $("#phone").val(), qq: $("#qq").val() };
                            utils.AjaxPost("/User/UEpMyOrder/SaveRecord", data, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("挂卖成功");
                                    agentJj = result.msg;
                                    searchMethod();
                                }
                            });
                        }
                    });
                    utils.showOrHiddenPromp();
                })

                //分页插件
                var dropload = $('#UEpMyOrderdatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData('/User/UEpMyOrder/GetListPage', param, me,
                            function (rows) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                                    rows[i]["status"] = statusDto[rows[i]["flag"]];
                                    var dto = rows[i];

                                    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.number + '</time><span class="sum">' + dto.waitNum + '</span>';
                                    if (dto.status == "已完成") {
                                        html += '<span class="status ship">' + dto.status + '</span>';
                                    } else {
                                        html += '<span class="status noship">' + dto.status + '</span>';
                                    }
                                    html += '<i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo"><div class="btnbox"><ul class="tga2">' +
                                    '<li><button class="sdelbtn" onclick=\'openCancleDialog(' + dto.id + ',"' + dto.number + '",' + dto.waitNum + ',' + dto.saleNum + ')\'>取消挂卖</button></li>' +
                                    '<li><button class="smallbtn" onclick=\'location.href="#UEpMyOrderDetail/' + dto.id + '"\'>交易明细</button></li>' +
                                    '</ul></div>' +
                                    '<dl><dt>挂卖单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>挂卖日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                    '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl><dl><dt>待出售数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                                    '<dl><dt>已出售数量</dt><dd>' + dto.scNum + '</dd></dl><dl><dt>挂卖数量</dt><dd>' + dto.saleNum + '</dd></dl>' +
                                    '</div></li>';
                                }
                                $("#UEpMyOrderitemList").append(html);
                            }, function () {
                                $("#UEpMyOrderitemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });

                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UEpMyOrderitemList").empty();
                    param["startTime"] = $("#startTime").val();
                    param["endTime"] = $("#endTime").val();
                    param["flag"] = $("#flag").val();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //查询按钮
                $("#searchBtn").bind("click", function () { searchMethod(); })

                //初始化下拉框
                utils.InitMobileSelect('#flagName', '选择状态', statusList, null, [0], null, function (indexArr, data) {
                    $("#flagName").val(data[0].value);
                    $("#flag").val(data[0].id);
                });
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});