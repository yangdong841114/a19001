
define(['text!UFamilyTree.html', 'jquery'], function (UFamilyTree, $) {

    var controller = function (name) {
        $("#title").html("系譜圖");

        //獲取樣式
        getTitleClass = function (u) {
            if (u.isPay != 1) {
                return 'level0'
            } else if (u.uLevel == 3) {
                return 'level1';
            } else if (u.uLevel == 4) {
                return 'level4';
            } else if (u.uLevel == 5) {
                return 'level3';
            }
        }

        //跳轉用戶的系譜圖
        jumpUserId = function (us) {
            utils.AjaxPostNotLoadding("/User/UFamilyTree/GetAppFamilyTreeByCondition", { userId: us }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    var map = result.map;
                    if (!map || map.length == 0) {
                        utils.showErrMsg("未找到系譜圖內容");
                    } else {
                        bindTree(map);
                        $("#other").html("上线链路：" + result.other);
                    }
                }
            });
        }

        bindTree = function (map) {
            for (var i = 1; i < 8; i++) {
                var html = ""
                $("#kk" + i).attr('class', '')

                if (map[i + ""]) {
                    $("#kk" + i).addClass('userpedigree');
                    var dto = map[i + ""];
                    if (dto.id > 0) {
                        if (dto.dpj=="yes")
                            html = '<a href="javascript:jumpUserId(\'' + dto.userId + '\');"><span>' + dto.userId + '</span></a>';//html = '<a href="javascript:jumpUserId(\'' + dto.userId + '\');"><span>' + dto.userId + '(对碰奖)</span></a>';
                        else
                            html = '<a href="javascript:jumpUserId(\'' + dto.userId + '\');"><span>' + dto.userId + '</span></a>';
                        html += '<div class="pedigreedata">' +
                           '<table><tr><td>' + dto.encash0 + '</td><th>總pv</th><td>' + dto.encash1 + '</td></tr><tr><td>' + dto.new0 + '</td><th>新pv</th><td>' + dto.new1 + '</td>' +
                           '</tr><tr><td>' + dto.spare0 + '</td><th>余pv</th><td>' + dto.spare1 + '</td></tr><tr><td>' + dto.reLCount + '</td><th>直推</th><td>' + dto.reRCount + '</td></tr></table></div>';
                        $("#kk" + i).addClass(getTitleClass(dto));
                    } else {
                        if (dto.isPay == 1) {
                            html = '<a href="#UserRegister/' + dto.fatherID + '' + dto.treePlace + '"><span>空位註冊</span></a>';
                            html += '<div class="pedigreedata">' +
                           '<table><tr><td>' + dto.encash0 + '</td><th>總pv</th><td>' + dto.encash1 + '</td></tr><tr><td>' + dto.new0 + '</td><th>新pv</th><td>' + dto.new1 + '</td>' +
                           '</tr><tr><td>' + dto.spare0 + '</td><th>余pv</th><td>' + dto.spare1 + '</td></tr><tr><td>' + dto.reLCount + '</td><th>直推</th><td>' + dto.reRCount + '</td></tr></table></div>';
                        } else {
                            html = '<a href="javascript:void(0);"><span>空位</span></a><div class="pedigreedata">' +
                           '<table><tr><td>0</td><th>總pv</th><td>0</td></tr><tr><td>0</td><th>新pv</th><td>0</td>' +
                           '</tr><tr><td>0</td><th>余pv</th><td>0</td></tr><td>0</td><th>直推</th><td>0</td></tr></table></div>';

                        }
                    }
                    $("#kk" + i).html(html);
                }
            }
        }


        //初始化加載數據
        utils.AjaxPostNotLoadding("/User/UFamilyTree/GetAppFamilyTree", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UFamilyTree);
                var map = result.map;
                bindTree(map);
                $("#other").html("上线链路：" + result.other);
                //查詢按鈕
                $("#queryBtn").on("click", function () {
                    utils.AjaxPost("/User/UFamilyTree/GetAppFamilyTreeByCondition", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            bindTree(result.map);
                        }
                    });
                })


                //我的系譜圖按鈕
                $("#selfBtn").on("click", function () {
                    utils.AjaxPost("/User/UFamilyTree/GetAppFamilyTree", {}, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            $("#userId").val("");
                            bindTree(result.map);
                            $("#other").html("上线链路：" + result.other);
                        }
                    });
                })
            }

        });

        controller.onRouteChange = function () {
            //銷毀模態窗口
        };
    };

    return controller;
});
