
define(['text!UChargesl.html', 'jquery'], function (UChargesl, $) {

    var controller = function (name) {

        var isInitTab2 = false;


        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期選擇框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查詢條件按鈕
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查詢按鈕
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加載數據
                searchMethod();
            }
        }

        $("#title").html("算力");
        appView.html(UChargesl);


        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UChargeDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UCharge/GetListPagesl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["ksTime"] = utils.changeDateFormat(rows[i]["ksTime"]);
                            rows[i]["jsTime"] = utils.changeDateFormat(rows[i]["jsTime"]);
                            rows[i]["status"] = rows[i].ispay == 1 ? "待審核" : "已通過";

                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>認購日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>認購月數</dt><dd>' + dto.rgys + '</dd></dl><dl><dt>開始時間</dt><dd>' + dto.ksTime + '</dd></dl>' +
                                  '<dl><dt>結束時間</dt><dd>' + dto.jsTime + '</dd></dl>' +
                                  '<dl><dt>消耗兌換碼</dt><dd>' + dto.xhdhm + '</dd></dl><dl><dt>返PV值</dt><dd>' + dto.fpv + '</dd></dl>' +
                                  '</div></li>';
                            //lightboxArray.push(lightboxId)
                        }
                        $("#UChargeItemList").append(html);

                        //初始化圖片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#UChargeItemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UChargeItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }



        //充值金額離開焦點
        $("#rgys").bind("blur", function () {
            //運算
            utils.AjaxPostNotLoadding("/User/UCharge/Calsl", { rgys: $("#rgys").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    var map = result.map;
                    $("#ksTime").html(map.ksTime);
                    $("#jsTime").html(map.jsTime);
                    $("#xhdhm").html(map.xhdhm);
                }
            })

        })

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UCharge/InitViewsl", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var map = result.map;
                if (map.dqyxrq&&map.dqyxrq != "")
                    $("#dqyxrq").html(utils.changeDateFormat(map.dqyxrq));
                if($("#dqyxrq").html()=="")$("#dqyxrq").html("当前未购买算力或算力已到期");

                $("#upUlevel1Yc").val(map.upUlevel1Yc);
                $("#upUlevel1Fpv").val(map.upUlevel1Fpv);
                $("#upUlevel1Yslf").val(map.upUlevel1Yslf);
                $("#upUlevel1Yfpv").val(map.upUlevel1Yfpv);

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "sysBankName") {
                            $("#sysBankId").val("");
                            $("#toBankCard").empty();
                            $("#toBankUser").empty();
                        }

                        dom.prev().val("");
                    });
                });


                //*****************************************************銀行匯款 start **********************************************************//

                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();

                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#rgys").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    var isChecked = true;
                    if (val == 0) {
                        utils.showErrMsg("請輸入認購月數");
                    } else {
                        $("#qr_rgys").html($("#rgys").val());
                        utils.showOrHiddenPromp();
                    }
                });

                //確認保存
                $("#sureBtn").bind("click", function () {
                    var formdata = new FormData();
                    formdata.append("rgys", $("#rgys").val());
                    utils.AjaxPostForFormData("/User/UCharge/SaveChargesl", formdata, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            location.href = "#UChargesl";
                        }
                    });
                });

                //*****************************************************銀行匯款 end **********************************************************//
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});