
define(['text!UMemberInfo_jp.html', 'jquery'], function (UMemberInfo, $) {

    var controller = function (memberId) {
       
        //设置标题
        $("#title").html("会員資料")
        var pmap = null;

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);
            $("span").each(function (index, ele) {
                if (dto[this.id]) {
                   $(this).html(dto[this.id]);
                }
            });
            $("p").each(function (index, ele) {
                if (dto[this.id]) {
                    if (this.id == "uLevel") {
                        $(this).html(cacheMap["ulevel"][dto[this.id + ""]]);
                    }
                    else {
                        $(this).html(dto[this.id]);
                    }

                }
            });
            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                  $(this).val(dto[this.id]);
                }
            });
            $("#treePlace").html(dto["treePlace"] == 0 ? "左端" : "右端");
        }
        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function(e){
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            //if (data.length > 2) {
            //    $("#area").val(data[2].value);
            //}
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMemberInfo);
                //初始化銀行帳號下拉框
                var bankLit = cacheList["UserBank"];
                utils.InitMobileSelect('#bankName', '銀行を開く', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#bankName").val(data[0].name);
                });

                //台湾，日本，中国，美国，韩国，香港，新加坡，越南，菲律宾
                var gjdqList = [{ id: '台湾', value: "台湾" }, { id: '日本', value: "日本" }, { id: '中国', value: "中国" }, { id: '美国', value: "米国" }, { id: '韩国', value: "韓国" }, { id: '香港', value: "香港" }, { id: '新加坡', value: "シンガポール" }, { id: '越南', value: "ベトナム" }, { id: '菲律宾', value: "フィリピン" }];
                //初始化下拉框
                utils.InitMobileSelect('#bankAddress', '国家地区', gjdqList, null, [0], null, function (indexArr, data) {
                    $("#bankAddress").val(data[0].value);
                    //$("#accounttypeId").val(data[0].id);
                });


                //省市區選擇
                //var proSet = utils.InitMobileSelect('#province', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);
                //var citySet = utils.InitMobileSelect('#city', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);
                //var areaSet = utils.InitMobileSelect('#area', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);


                var dto = result.result;
                //初始表单默认值
                setDefaultFormValue(dto);

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });


                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                        //if (jdom.attr("id") == 'bankCard') {
                        //    var card = jdom.val();
                        //    var reg = /^(\d{16,19})$/g;
                        //    if (!reg.test(jdom.val())) {
                        //        utils.showErrMsg("銀行卡號位數必須為16-19位");
                        //        jdom.focus();
                        //        checked = false;
                        //        return false;
                        //    }
                        //}
                    });
                    if (checked) {
                        var fs = {};
                        $("#lemeinfo input").each(function () {
                            fs[this.id] = $(this).val();
                        });
                        utils.AjaxPost("/User/UMemberInfo/UpdateMember", fs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存に成功しました！");
                            }
                        });
                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});