
define(['text!UCurrency_jp.html', 'jquery'], function (UCurrency, $) {

    var controller = function (name) {
        var me = this;
        //設置標題
        $("#title").html("収益照会");
        appView.html(UCurrency);

        //清空查詢條件按鈕
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //點擊選項卡切換
        $('.tabtga ul li').click(function () {
            var $this = $(this);
            var $t = $this.index();
            $('.tabtga ul li').removeClass();
            $this.addClass('active');
            var $contentbox = $('.contentbox');
            $contentbox.css('display', 'none');
            $contentbox.eq($t).css('display', 'block');
        });

        //查詢參數
        this.param = utils.getPageData();

        //初始化日期選擇框
        utils.initCalendar(["startTime", "endTime"]);

        //設置高度
        //utils.setHeight();

        //獲取匯總的默認數據
        setTotalDefault = function () {
            return '<li><div class="lead"><h5>全賞合計</h5><span>0</span></div>' +
                                  '<dl><dt>直進賞</dt><dd>0</dd></dl><dl><dt>巡回当たり賞</dt><dd>0</dd></dl>' +
                                  '<dl><dt>指導者たいとう賞</dt><dd>0</dd></dl><dl><dt>初年度忠誠賞</dt><dd>0</dd></dl>' +
                                  '<dl><dt>指導者配当賞</dt><dd>0</dd></dl><dl><dt>売り上げ引上げ</dt><dd>0</dd></dl>' +
                                  '<dl><dt>実の髪</dt><dd>0</dd></dl>' +
                                  '</div></li>';
        }

        //獎金詳情
        toCurrencyDetail = function (addDate) {
            location.href = "#UCurrencyUserDetail_jp/" + addDate;
        }

        var dropload = $('#UCurrencyDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData('/User/UCurrency/GetByUserSumCurrency', param, me,
                    function (rows, footers) {
                        var footer = footers[0];
                        var totleHtml = "";
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addDate"] = utils.changeDateFormat(rows[i]["addDate"], 'date');
                            rows[i]["cat1"] = rows[i]["cat1"].toFixed(2);
                            rows[i]["cat2"] = rows[i]["cat2"].toFixed(2);
                            rows[i]["cat3"] = rows[i]["cat3"].toFixed(2);
                            rows[i]["cat4"] = rows[i]["cat4"].toFixed(2);
                            rows[i]["cat5"] = rows[i]["cat5"].toFixed(2);
                            rows[i]["cat6"] = rows[i]["cat6"].toFixed(2);
                            rows[i]["cat7"] = rows[i]["cat7"].toFixed(2);
                            rows[i]["yf"] = rows[i]["yf"].toFixed(2);
                            rows[i]["fee1"] = rows[i]["fee1"].toFixed(2);
                            rows[i]["fee2"] = rows[i]["fee2"].toFixed(2);
                            rows[i]["fee3"] = rows[i]["fee3"].toFixed(2);
                            rows[i]["sf"] = rows[i]["sf"].toFixed(2);
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addDate + '</time><span class="sum">+' + dto.yf + '</span>' +
                                  '実の髪：<span class="sum">$' + dto.sf + '</span><i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo"><div class="btnbox"><button class="seditbtn" onclick=\'toCurrencyDetail("' + dto.addDate + '")\'>ボーナスの内訳</button></div>' +
                                  '<dl><dt>決済日</dt><dd>' + dto.addDate + '</dd></dl>' +
                                  '<dl><dt>直進賞</dt><dd>' + dto.cat1 + '</dd></dl><dl><dt>巡回当たり賞</dt><dd>' + dto.cat2 + '</dd></dl>' +
                                  '<dl><dt>指導者たいとう賞</dt><dd>' + dto.cat3 + '</dd></dl><dl><dt>初年度忠誠賞</dt><dd>' + dto.cat4 + '</dd></dl>' +
                                  '<dl><dt>指導者配当賞</dt><dd>' + dto.cat5 + '</dd></dl><dl><dt>売り上げ引上げ</dt><dd>' + dto.cat6 + '</dd></dl>' +
                                  '<dl><dt>実の髪</dt><dd>' + dto.sf + '</dd></dl>' +
                                  '</div></li>';
                        }
                        $("#UCurrencyItemList").append(html);
                        totleHtml += '<li><div class="lead"><h5>全賞合計</h5><span>' + footer.yf + '</span></div>' +
                                  '<dl><dt>直進賞</dt><dd>' + footer.cat1 + '</dd></dl><dl><dt>巡回当たり賞</dt><dd>' + footer.cat2 + '</dd></dl>' +
                                  '<dl><dt>指導者たいとう賞</dt><dd>' + footer.cat3 + '</dd></dl><dl><dt>初年度忠誠賞</dt><dd>' + footer.cat4 + '</dd></dl>' +
                                  '<dl><dt>指導者配当賞</dt><dd>' + footer.cat5 + '</dd></dl><dl><dt>売り上げ引上げ</dt><dd>' + footer.cat6 + '</dd></dl>' +
                                  '<dl><dt>実の髪</dt><dd>' + footer.sf + '</dd></dl>' +
                                  '</div></li>';
                        $("#footerUl").html(totleHtml);

                    }, function () {
                        $("#footerUl").html(setTotalDefault());
                        $("#UCurrencyItemList").append('<p class="dropload-noData">データがありません</p>');
                    });
            }
        });

        //查詢按鈕
        $("#searchBtn").on("click", function () {
            param.page = 1;
            $("#UCurrencyItemList").empty();
            $("#footerUl").html(setTotalDefault());
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        })

        controller.onRouteChange = function () {
            dropload = null;
            delete dropload;
        };
    };

    return controller;
});