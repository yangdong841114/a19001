
define(['text!ULiuShui_jp.html', 'jquery'], function (ULiuShui, $) {

    var controller = function (name) {

        $("#title").html("水揚げ帳");
        appView.html(ULiuShui);

        //清空查詢條件按鈕
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //查詢條件賬戶類型選擇框
        var journalClassList = $.extend(true, [], cacheList["JournalClass"]);
        journalClassList.splice(0, 0, { id: 0, name: '全部' });
        utils.InitMobileSelect('#accountName', 'アカウントのタイプ', journalClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#accountName").val(data[0].name);
            $("#accountId").val(data[0].id);
        })

        //綁定展開搜索更多
        utils.bindSearchmoreClick();

        //初始化日期選擇框
        utils.initCalendar(["startTime", "endTime"]);

        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#ULiuShuidatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/ULiuShui/GetLiushuiDetailListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            if (rows[i]["addtime"] != null && rows[i]["addtime"] != "") {
                                rows[i]["addtime"] = utils.changeDateFormat(rows[i]["addtime"]);
                            }
                            rows[i]["epotins"] = rows[i]["epotins"].toFixed(3);
                            rows[i]["last"] = rows[i]["last"].toFixed(2);
                            rows[i].accountId = cacheMap["JournalClass"][rows[i].accountId];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + (dto.addtime == null ? "" : dto.addtime) + '</time><span class="sum">' + dto.accountId + '</span>';
                            if (dto.optype == '支出') {
                                html += '<span><font class="status-red">-' + dto.epotins + '</font></span>';
                            } else {
                                html += '<span><font class="status-success">+' + dto.epotins + '</font></span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>業務概要</dt><dd>' + dto.abst + '</dd></dl><dl><dt>日付</dt><dd>' + dto.addtime + '</dd></dl><dl><dt>アカウントのタイプ</dt><dd>' + dto.accountId + '</dd></dl>' +
                            '<dl><dt>収支</dt><dd>' + dto.optype + '</dd></dl><dl><dt>金額</dt><dd>' + dto.epotins + '</dd></dl>' +
                            '<dl><dt>残額</dt><dd>' + dto.last + '</dd></dl><dl>' +
                            '</div></li>';
                        }
                        $("#ULiuShuiitemList").append(html);
                        Total();
                    }, function () {
                        $("#ULiuShuiitemList").append('<p class="dropload-noData">データがありません</p>');
                        Total();
                    });
            }
        });


        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#ULiuShuiitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["accountId"] = $("#accountId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();


        }
        //查詢計算收入和支出總和
        function Total() {
            utils.AjaxPostNotLoadding("/User/ULiuShui/GetTotalMoney", { accountId: $("#accountId").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#srMoney").html(result.msg);
                    $("#zcMoney").html(result.other);
                }
            });
        }

        //查詢按鈕
        $("#searchBtn").on("click", function () {
            searchMethod();

        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});