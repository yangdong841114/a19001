
define(['text!UProduct.html', 'jquery', 'text!NotJMS.html'], function (UProduct, $, NotJMS) {

    var controller = function (name) {
        //設置標題
        $("#title").html("商品管理")

        var dto = null;
        var editList = {};
        var dropload = null;
        var editor = null;
        var shelveList = [{ id: 0, value: "全部" }, { id: 0, value: "已下架" }, { id: 0, value: "已上架" }];
        //查詢參數
        this.param = utils.getPageData();

        clearForm = function () {
            $("#id").val("");
            $("#imgUrl").val("");
            $("#productCode").val("");
            $("#productName").val("");
            $("#price").val("");
            $("#fxPrice").val("");
            document.getElementById("showImg").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            utils.setEditorHtml(editor, "")
        }

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["isShelve"] = $("#isShelve").val();
            param["productName"] = $("#productName2").val();
            param["productCode"] = $("#productCode2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //選項卡切換
        tabClick = function (index, bb) {
            if (index == 1) {
                document.getElementById("mainDiv").style.display = "block";
                document.getElementById("deployDiv").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active");
                if (bb) {
                    searchMethod();
                }
            } else {
                document.getElementById("mainDiv").style.display = "none";
                document.getElementById("deployDiv").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                dto = null;
                clearForm();
                $("#mainDiv").css("display", "none");
                $("#deployDiv").css("display", "block");
            }
        }


        //加載供應商信息
        utils.AjaxPostNotLoadding("/User/UMerchant/GetRecord", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                //判斷是否存在供應商
                var merchant = result.result;

                if (!merchant || merchant.flag == 1) {
                    appView.html(NotJMS);
                } else {

                    appView.html(UProduct);

                    //初始化編輯器
                    editor = new Quill("#editor", {
                        modules: {
                            toolbar: utils.getEditorToolbar()
                        },
                        theme: 'snow'
                    });

                    //清空查詢條件按鈕
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //是否上架選擇
                    utils.InitMobileSelect('#isShelveName', '是否上架', shelveList, null, [0], null, function (indexArr, data) {
                        $("#isShelveName").val(data[0].name);
                        $("#isShelve").val(data[0].id);
                    });

                    //隱藏提示框
                    $(".hideprompt").click(function () {
                        utils.showOrHiddenPromp();
                    });

                    //初始化日期選擇框
                    utils.initCalendar(["startTime", "endTime"]);

                    //綁定展開搜索更多
                    utils.bindSearchmoreClick();

                    //綁定清除按鈕
                    utils.CancelBtnBind();

                    //上架商品
                    shelveProduct = function (id) {
                        utils.AjaxPost("/User/UProduct/Shelve", { id: id }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("上架成功");
                                searchMethod();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }

                    //下架商品
                    cancelShelveProduct = function (id) {
                        utils.AjaxPost("/User/UProduct/CancelShelve", { id: id }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("下架成功");
                                searchMethod();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }

                    //刪除按鈕
                    deleteRecord = function (id) {
                        $("#sureBtn").unbind();
                        //確認刪除
                        $("#sureBtn").bind("click", function () {
                            utils.AjaxPost("/User/UProduct/Delete", { id: id }, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.status == "success") {
                                    utils.showSuccessMsg("刪除成功");
                                    searchMethod();
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            });
                        })
                        utils.showOrHiddenPromp();
                    }

                    //預覽圖片
                    $("#imgFile").bind("change", function () {
                        var url = URL.createObjectURL($(this)[0].files[0]);
                        document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
                    })


                    //保存發布商品
                    $("#saveProductBtn").bind("click", function () {
                        //金額校驗
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        //非空校驗
                        if ($("#productCode").val() == 0) {
                            utils.showErrMsg("商品編碼不能為空");
                        } else if ($("#productName").val() == 0) {
                            utils.showErrMsg("商品名稱不能為空");
                        } else if ($("#imgFile").val() == 0 && !dto) { //新增必須上傳圖片，編輯時可以不用上傳
                            utils.showErrMsg("請選擇上傳的圖片");
                        } else if (!g.test($("#fxPrice").val())) {
                            utils.showErrMsg("金額格式錯誤");
                        } else if (editor.getText() == 0) {
                            utils.showErrMsg("請輸入商品內容");
                        } else {
                            var formdata = new FormData();
                            if (dto) {
                                formdata.append("id", $("#id").val());
                                formdata.append("imgUrl", $("#imgUrl").val());
                            }
                            formdata.append("productName", $("#productName").val());
                            formdata.append("productCode", $("#productCode").val());
                            formdata.append("price", $("#price").val());
                            formdata.append("fxPrice", $("#fxPrice").val());
                            formdata.append("num", $("#num").val());
                            formdata.append("cont", utils.getEditorHtml(editor));
                            formdata.append("imgFile", $("#imgFile")[0].files[0]);

                            utils.AjaxPostForFormData("/User/UProduct/SaveOrUpdate", formdata, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    clearForm();
                                    tabClick(1, true);
                                    utils.showSuccessMsg("保存成功！");
                                }
                            });
                        }
                    })

                    //關閉發布商品
                    $("#closeDeployBtn").bind("click", function () {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                    })

                    //編輯商品
                    editRecord = function (id) {
                        dto = editList[id];
                        $("#id").val(id);
                        $("#imgUrl").val(dto.imgUrl);
                        $("#productCode").val(dto.productCode);
                        $("#productName").val(dto.productName);
                        $("#price").val(dto.price);
                        $("#fxPrice").val(dto.fxPrice);
                        $("#num").val(dto.num);
                        document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';
                        utils.setEditorHtml(editor, dto.cont);
                        $("#mainDiv").css("display", "none");
                        $("#deployDiv").css("display", "block");
                    }

                    //分頁加載控件
                    dropload = $('#Productdatalist').dropload({
                        scrollArea: window,
                        domDown: { domNoData: '<p class="dropload-noData"></p>' },
                        loadDownFn: function (me) {
                            utils.LoadPageData("/User/UProduct/GetListPage", param, me,
                                function (rows, footers) {
                                    var html = "";
                                    for (var i = 0; i < rows.length; i++) {
                                        rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                                        rows[i]["status"] = rows[i].isShelve == 1 ? "已下架" : "已上架";

                                        var dto = rows[i];
                                        html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.productCode + '</time>';
                                        if (dto.status == "已上架") {
                                            html += '<span class="ship">' + dto.status + '</span>';
                                        } else {
                                            html += '<span class="noship">' + dto.status + '</span>';
                                        }
                                        if (dto.flag == 1) {
                                            html += '<span class="noship">待審核</span>';
                                        }
                                        html += '&nbsp;<span class="sum">' + dto.productName + '</span><i class="fa fa-angle-right"></i></div>' +
                                        '<div class="allinfo">' +
                                        '<div class="btnbox"><ul class="tga3">';

                                        //未審核的才能刪除
                                        if (dto.flag == 1) {
                                            html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>刪除</button></li>';
                                        } else {
                                            if (dto.isShelve == 1) {
                                                html += '<li><button class="smallbtn" onclick="shelveProduct(\'' + dto.id + '\')">上架</button></li>';
                                            } else {
                                                html += '<li><button class="sdelbtn" onclick="cancelShelveProduct(\'' + dto.id + '\')">下架</button></li>';
                                            }
                                        }
                                        html += '<li><button class="sdelbtn" onclick=\'editRecord(' + dto.id + ')\'>編輯</button></li>' +
                                        '</ul></div>' +
                                        '<dl><dt>商品編碼</dt><dd>' + dto.productCode + '</dd></dl><dl><dt>商品名稱</dt><dd>' + dto.productName + '</dd></dl>' +
                                        '<dl><dt>商品圖片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                                        '<dl><dt>價格</dt><dd>' + dto.fxPrice + '</dd></dl>' +
                                        '<dl><dt>庫存</dt><dd>' + dto.num + '</dd></dl>' +
                                        '<dl><dt>是否上架</dt><dd>' + dto.status + '</dd></dl><dl><dt>發布日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                        '</div></li>';
                                        editList[dto.id] = dto;
                                    }
                                    $("#ProductitemList").append(html);
                                }, function () {
                                    $("#ProductitemList").append('<p class="dropload-noData">暫無數據</p>');
                                });
                        }
                    });


                    //查詢按鈕
                    $("#searchBtn").on("click", function () {
                        searchMethod();
                    })
                }
            }
        });


        controller.onRouteChange = function () {

        };
    };

    return controller;
});