
define(['text!UProductShop1_jp.html', 'jquery'], function (UProductShop1, $) {

    var controller = function (id) {

        //设置标题
        $("#title").html("オンラインショッピング");
       

        utils.AjaxPostNotLoadding("/User/UProductShop/InitCartCount1", {}, function (result) {

            if (result.status == "success") {
                appView.html(UProductShop1);
            
                //广告图
                var banners = result.map.banners;
                var list_Merchant = result.map.list_Merchant;
                if (banners && banners.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < banners.length; i++) {
                        banHtml += '<li><img src="' + banners[i].imgUrl + '" /></li>';
                    }
                    $("#slides").html(banHtml);
                }

                //滚动广告图
                $(".main_visual").hover(function () {
                    $("#btn_prev,#btn_next").fadeIn()
                }, function () {
                    $("#btn_prev,#btn_next").fadeOut()
                });

                //$dragBln = false;

                $(".main_image").touchSlider({
                    flexible: true,
                    speed: 500,
                    delayTime: 3000,
                    btn_prev: $("#btn_prev"),
                    btn_next: $("#btn_next"),
                    paging: $(".flicking_con a"),
                    counter: function (e) {
                        $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
                    }
                });

              

              
                //查询参数
                this.param = utils.getPageData()
             
              
                var dropload = $('#UProductShopdatalist').dropload({
                    scrollArea: window,
                    autoLoad:true,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/User/UProductShop/GetListPageAll", param, me,
                            function (rows, footers) {
                                var html = "";

                                for (var k = 0; k < list_Merchant.length; k++) {
                                    html += ' <li><div class="gwclistT"><p><img src="' + list_Merchant[k].imgUrl + '" /></p>' +
							                '<h2>' + list_Merchant[k].name + '</h2>' +
							                '<a href="#UProductShop/' + list_Merchant[k].uid + '">商家に入る</a>' +
							                '<div class=" clear"></div>' +
						                    '</div>';

                                    var count3 = 0;
                                    for (var i = 0; i < rows.length && count3<3; i++) {
                                        var dto = rows[i];
                                        if (dto.uid == list_Merchant[k].uid)
                                        {
                                        count3++;
                                        if(count3==1)
                                        html += '<ul class="tga3 gwclistC">';
                                        html += '<li><a href="#ProductDetail/' + dto.id + '">' +
                                              '<p><img data-toggle="lightbox" src="' + dto.imgUrl + '"  class="img-thumbnail" alt=""></p>' +
                                              '<span>￥' + dto.price + '&nbsp;&nbsp;&nbsp;</span>' +
                                              '</a></li>';
                                        if (count3 == 3 || i == (rows.length - 1))
                                            html += '</ul>';
                                        }
                                        
                                    }

                                    html += '<div class=" clear"></div><p>' + list_Merchant[k].businessScope + '</p>' +
					                    '</li>';
                                }

                              
                                $("#UProductShopitemList").html(html);
                            }, function () {
                                $("#UProductShopitemList").append('<p class="dropload-noData">データがありません</p>');
                            });
                    }
                });

                //searchMethod();
                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UProductShopitemList").empty();
                   
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

               

                
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});