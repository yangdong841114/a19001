
define(['text!UregSuccess.html', 'jquery'], function (UregSuccess, $) {

    var controller = function (name) {

        $("#title").html("登録成功");
        appView.html(UregSuccess);

        $("#userId").html("<span>会員番号：</span>" + regSuccess.userId);
        $("#regmoney").html("<span>登録金額：</span>￥" + regSuccess.regMoney);
        $("#ulevel").html("<span>登録レベル：</span>" + cacheMap["ulevel"][regSuccess.uLevel]);

        controller.onRouteChange = function () {
        };
    };

    return controller;
});