
define(['text!UMemberPassed.html', 'jquery'], function (UMemberPassed, $) {

    var controller = function (name) {

        //设置标题
        $("#title").html("会員をかいけつした")
        appView.html(UMemberPassed);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["passStartTime", "passEndTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UMemberPasseddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UMemberPassed/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["passTime"] = utils.changeDateFormat(rows[i]["passTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            rows[i].isLock = rows[i].isLock == 0 ? "否" : "是";
                            rows[i].isFt = rows[i].isFt == 0 ? "否" : "是";

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.passTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '登録金額：<span class="sum">' + dto.regMoney + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会員番号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会員名</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>推薦者番号</dt><dd>' + dto.reName + '</dd></dl><dl><dt>接点人番号</dt><dd>' + dto.fatherName + '</dd></dl>' +
                            '<dl><dt>会員レベル</dt><dd>' + dto.uLevel + '</dd></dl><dl><dt>登録金額</dt><dd>' + dto.regMoney + '</dd></dl>' +
                            '<dl><dt>連絡電話</dt><dd>' + dto.phone + '</dd></dl><dl><dt>返球するかどうか</dt><dd>' + dto.isFt + '</dd></dl>' +
                            '<dl><dt>凍結しますか</dt><dd>' + dto.isLock + '</dd></dl><dl><dt>登録日</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>開通日</dt><dd>' + dto.passTime + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#UMemberPasseditemList").append(html);
                    }, function () {
                        $("#UMemberPasseditemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UMemberPasseditemList").empty();
            param["passStartTime"] = $("#passStartTime").val();
            param["passEndTime"] = $("#passEndTime").val();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});