
define(['text!InfoGG.html', 'jquery', 'j_easyui', 'datetimepicker'], function (InfoGG, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "广告管理");
        appView.html(InfoGG);

        //初始化表单区域
        $('#dlgInfoWZ').dialog({
            title: '发布广告',
            width: 850,
            height: 500,
            cache: false,
            modal: true
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });


     
        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveBtnInfoWZ").bind("click", function () {
            var formdata = new FormData();
            formdata.append("id", $("#id").val() == 0 ? "" : $("#id").val());
            formdata.append("fltype", $("#fltype").val());
            formdata.append("linkUrl", $("#linkUrl").val());
            formdata.append("imgFile", $("#imgFile")[0].files[0]);
            utils.AjaxPostForFormData("InfoWZ/SaveGG", formdata, function (result) {

                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                }
                else {
                    utils.showSuccessMsg("保存成功！");
                    $('#dlgInfoWZ').dialog("close");
                    queryGrid();
                }

            })


        })

        //打开明细
        openDetailDialog = function (row) {
            $("#id").val(row.id);
            $("#fltype").val(row.fltype);
            $("#linkUrl").val(row.linkUrl);
            $("#picu").attr("src", row.imgUrl);
            $("#picu").attr("data-image", row.imgUrl);
            $("#picu").lightbox();
            $("#imgFile").val("");
            $('#dlgInfoWZ').dialog({ title: '修改广告' }).dialog("open");
        }

        $("#imgFile").change(function () {

            $("#picu").attr("src", URL.createObjectURL($(this)[0].files[0]));

        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
              { field: 'fltype', title: '广告类型', width: '10%' },
              {
                  field: '_img', title: '广告图片', width: '10%', formatter: function (val, row, index) {
                      return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="广告图片" class="img-thumbnail" alt="" width="100">';
                  }
              },
             { field: 'linkUrl', title: '链接地址', width: '20%' },
             { field: 'llcount', title: '点击量', width: '10%' },
             {
                 field: 'status', title: '是否上架', width: '8%', align: 'center', formatter: function (val, row, index) {
                     if (row.isTop == 2) {
                         return '<font style="color:green;">是</font>';
                     } else {
                         return '<font style="color:red;">否</font>';
                     }
                 }
             },
             { field: 'addTime', title: '发布日期', width: '10%' },
             {
                 field: '_deleteRecord', title: '操作', width: '15%', formatter: function (val, row, index) {
                     var cont = "";
                     cont += '<a href="javascript:void(0);" rowIndex ="' + index + '" class="recordEdit" >编辑</a>&nbsp;&nbsp;'+
                         '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >删除</a>';

                     if (row.isTop == 2) {
                         cont+= '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordCancelTop" >下架</a>&nbsp;&nbsp;';
                     } else {
                         cont+= '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordTop" >上架</a>&nbsp;&nbsp;';
                     }

                     return cont;
                 }
             }
            ]],
            toolbar: [{
                text: "发布广告",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $("#title").val("");
                    $("#id").val("");
                    $("#linkUrl").val("");
                    $("#picu").attr("src", "/UpLoad/product/NotImg.gif");
                  
                    $('#dlgInfoWZ').dialog({ title: '发布广告' }).dialog("open");
                }
            }
            ],
            url: "InfoWZ/GetListPageGG"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {

            //置顶
            $(".recordTop").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("确定上架吗？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/SaveTop", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("上架成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })

            //取消置顶
            $(".recordCancelTop").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("确定下架吗？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/CancelTop", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("下架成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })


            $(".img-thumbnail").each(function (index, ele) {
                $(this).lightbox();
            })

            //删除
            $(".recordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该记录？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("删除成功");
                                $('#dlgInfoWZ').dialog("close");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })

            //点击编辑按钮编辑
            $(".recordEdit").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = grid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openDetailDialog(row);
                })
            })



        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "title2" ? "title" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlgInfoWZ').dialog("destroy");
        };
    };

    return controller;
});