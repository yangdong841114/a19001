
define(['text!DataDictionary.html', 'jquery', 'j_easyui'], function (DataDictionary, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "数据字典");


        //初始化下拉框
        $.ajax({
            url: "DataDictionary/GetRootList",
            type: "POST",
            success: function (result) {
                if (result.status == "success") {

                    appView.html(DataDictionary);
                    //根节点类型
                    var rootObj = {};
                    var list = result.list;

                    //初始化查询区select
                    $("#parentId2").empty();
                    $("#parentId2").append("<option value='0'>--全部--</option>");
                    $("#parentId").empty();
                    $("#parentId").append("<option value='-1'>--新建根节点--</option>");
                    if (list && list.length > 0) {
                        for (var i = 0; i < list.length; i++) {
                            rootObj[list[i].id] = list[i].name;
                            rootObj[list[i].id + "obj"] = list[i];
                            $("#parentId2").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>");
                            $("#parentId").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>");
                        }
                    }

                    //数据类型改变事件
                    $("#parentId").on("click", function () {
                        var val = $("#parentId").val();
                        if (val == "-1") {
                            $("#type")[0].disabled = false;
                            $("#type").val("");
                        } else {
                            var rec = rootObj[val + "obj"];
                            $("#type")[0].disabled = true;
                            $("#type").val(rec.type);
                        }
                    });

                    $('#dlgDataDictionary').dialog({
                        title: '编辑记录',
                        width: 500,
                        height: 300,
                        cache: false,
                        modal: true
                    })

                    //初始化按钮
                    $(".easyui-linkbutton").each(function (i, ipt) {
                        $(ipt).linkbutton({});
                    });

                    //清除错误样式
                    $(".form-control").each(function () {
                        var dom = $(this);
                        dom.on("focus", function () {
                            dom.removeClass("inputError");
                        })
                    });

                    //保存按钮
                    $("#saveDataDictionaryBtn").bind("click", function () {
                        var checked = true;
                        if ($("#parentId").val() == "-1") {
                            $("#parentId").addClass("inputError");
                            checked = false;
                        } else if ($("#type").val() == "") {
                            $("#type").addClass("inputError");
                            checked = false;
                        } else if ($("#name").val() == "") {
                            $("#name").addClass("inputError");
                            checked = false;
                        } else {
                            checked = true;
                        }
                        //$(".form-control").each(function () {
                        //    var dom = $(this);
                        //    if (dom.val() == 0 && dom[0].id != "remark") {
                        //        dom.addClass("inputError");
                        //        checked = false;
                        //    }
                        //});
                        if (checked) {
                            var data = { parentId: $("#parentId").val() == "-1" ? "0" : $("#parentId").val(), type: $("#type").val(), name: $("#name").val(), remark: $("#remark").val(), id: $("#id").val() };
                            utils.AjaxPost("DataDictionary/SaveOrUpdate", data, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg(result.msg);
                                    //关闭弹出框
                                    $('#dlgDataDictionary').dialog("close");
                                    //重刷grid
                                    queryGrid();

                                }
                            });
                        }
                        return false;
                    })


                    //编辑数据
                    editRecord = function () {
                        var obj = grid.datagrid("getSelected");
                        if (obj == null) { utils.showErrMsg("请选择需要编辑的行！"); }
                        else {
                            //类型、类型编码不可编辑
                            $("#parentId")[0].disabled = true;
                            $("#type")[0].disabled = true;
                            //清空表单数据
                            $('#editDataDictionary').form('reset');
                            //根据选中的行记录加载数据
                            $("#parentId").val(obj.parentId);
                            $("#type").val(obj.type);
                            $("#name").val(obj.name);
                            $("#remark").val(obj.remark);
                            $("#id").val(obj.id);
                            $('#dlgDataDictionary').dialog({ title: '编辑记录' }).dialog("open");

                        }
                    }

                    //添加数据
                    newRecord = function () {
                        //类型可选择
                        $("#parentId")[0].disabled = false;
                        //类型编码不可编辑,根据选择
                        $("#type")[0].disabled = false;
                        //清空表单数据
                        $('#editDataDictionary').form('reset');
                        //清空id（编辑、添加使用同一个表单，所以这里需要清除）
                        $("#id").val('');
                        $('#dlgDataDictionary').dialog({ title: '添加记录' }).dialog("open");
                    }

                    //删除记录
                    //removeRecord = function () {
                    //    var obj = grid.datagrid("getSelected");
                    //    if (obj == null) { utils.showErrMsg("请选择需要删除的行！"); }
                    //    else {
                    //        utils.confirm("您确定要删除吗？", function () {
                    //            utils.AjaxPost("DataDictionary/Delete", { id: obj.id }, function (result) {
                    //                if (result.status == "fail") {
                    //                    utils.showErrMsg(result.msg);
                    //                    //重刷grid
                    //                    queryGrid();
                    //                } else {
                    //                    utils.showSuccessMsg(result.msg);
                    //                }
                    //            });
                    //        });
                    //    }
                    //}

                    //初始化表格
                    var grid = utils.newGrid("dg", {
                        columns: [[
                            { field: 'ck', title: '文本', checkbox: true, },
                            { field: 'id', title: 'ID', width: '10%' },
                            { field: 'type', title: '类型编码', width: '15%' },
                            { field: 'parentName', title: '数据类型', width: '15%' },
                            { field: 'name', title: '名称', width: '15%' },
                            { field: 'remark', title: '描述', width: '43%' }
                        ]],
                        toolbar: [{
                            text: "添加",
                            iconCls: 'icon-add',
                            handler: newRecord
                        }, '-', {
                            text: "编辑",
                            iconCls: 'icon-edit',
                            handler: editRecord
                        }
                            //, '-', {
                            //    text: "删除",
                            //    iconCls: 'icon-cancel',
                            //    handler: removeRecord
                            //}

                        ],
                        url: "DataDictionary/GetListChildren"
                    }, null, function (data) {
                        if (data && data.rows) {
                            for (var i = 0; i < data.rows.length; i++) {
                                data.rows[i]["parentName"] = rootObj[data.rows[i].parentId];
                            }
                        }
                        return data;
                    })

                    //查询grid
                    queryGrid = function () {
                        var objs = $("#DataDictionaryQueryForm").serializeObject();
                        var queryObj = {};
                        $.each(objs, function (name, val) {
                            if (val != 0) {
                                var name = name == "parentId2" ? "parentId" : "name";
                                queryObj[name] = val;
                            }
                        });
                        grid.datagrid("options").queryParams = queryObj;
                        grid.datagrid("reload");
                    }

                    //查询按钮
                    $("#submit").on("click", function () {
                        queryGrid();
                    })


                }
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlgDataDictionary').dialog("destroy");
        };
    };

    return controller;
});