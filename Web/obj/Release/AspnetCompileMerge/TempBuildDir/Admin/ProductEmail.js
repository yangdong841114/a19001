﻿
define(['text!ProductEmail.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (ProductEmail, $) {

    var controller = function (name) {

        $("#center").panel("setTitle", "产品专业答疑");
        appView.html(ProductEmail);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#ProductEmail", toUid: 0 }, function () { });

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //获取焦点事件
        $(".form-control").each(function (index, ele) {
            var current = $(this);
            current.on("focus", function () {
                if (current.hasClass("inputError")) {
                    current.removeClass("inputError");
                }
            })
        })

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始发邮件文本编辑器
        var sendEditor = new Quill("#scontent", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //初始回邮件文本编辑器
        var receiveEditor = new Quill("#rcontent", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //*************************************************** 发件箱start **************************************************************/

        //初始化发邮件弹出框
        $('#sendDlg').dialog({
            title: '发邮件',
            width: 600,
            height: 500,
            cache: false,
            modal: true
        })

        //设置发件箱弹出表单
        setSendDataForm = function (isDisabled, dto) {
            if (dto) {
                $("#stitle").val(dto.title);
                utils.setEditorHtml(sendEditor, dto.content)
                $("#stoUser").val(dto.toUser);
            } else {
                $("#stitle").val("");
                utils.setEditorHtml(sendEditor, "")
                $("#stoUser").val("");
            }
            $("#stitle").attr("disabled", isDisabled);
            sendEditor.enable(!isDisabled);
            $("#stoUser").attr("disabled", isDisabled);
            if (isDisabled) {
                $("#saveSendBtn").hide();
                $("#allDiv").hide();
            } else {
                $("#saveSendBtn").show();
                $("#allDiv").show();
            }
        }

        //发邮件按钮弹出表单
        $("#sendAddBtn").bind("click", function () {
            setSendDataForm(false);
            $('#sendDlg').dialog({ title: "发邮件" }).dialog("open");
        })

        //查看发件箱邮件信息
        openSendDetailDialog = function (row) {
            setSendDataForm(true, row);
            $('#sendDlg').dialog({ title: "邮件明细" }).dialog("open");
        }

        var sendGrid = undefined;
        //切换发件箱面板时，初始化发件箱grid
        $('[data-tab]').on('shown.zui.tab', function (e) {
            if (e.target.innerText == "发件箱") {
                if (!sendGrid) {
                    sendGrid = utils.newGrid("sendGrid", {
                        //title: '发件箱',
                        showFooter: true,
                        columns: [[
                         { field: 'toUser', title: '收件人', width: '16%' },
                         {
                             field: '_downTitle', title: '主题', width: '55%', formatter: function (val, row, index) {
                                 return '<a href="javascript:void(0);" rowIndex ="' + index + '" class="titleOpen" >' + row.title + '</a>';
                             }
                         },
                         { field: 'addTime', title: '发送时间', width: '19%' },
                         {
                             field: '_deleteRecord', title: '操作', width: '10%', formatter: function (val, row, index) {
                                 return '&nbsp;<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >删除</a>&nbsp;';
                             }
                         }
                        ]],
                        url: "ProductEmail/GetSendListPage"
                    }, null, function (data) {
                        if (data && data.rows) {
                            for (var i = 0; i < data.rows.length; i++) {
                                data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                            }
                        }
                        return data;
                    }, function () {
                        //点击打开邮件明细
                        $(".titleOpen").each(function (index, ele) {
                            var dom = $(this);
                            dom.on("click", function () {
                                var rows = sendGrid.datagrid("getRows");
                                var index = dom.attr("rowIndex");
                                var row = rows[index];
                                openSendDetailDialog(row);
                            })
                        })

                        //删除邮件
                        $(".recordDelete").each(function (index, ele) {
                            var dom = $(this);
                            dom.on("click", function () {
                                utils.confirm("您确定要删除该邮件？", function () {
                                    var dataId = dom.attr("dataId");
                                    utils.AjaxPost("ProductEmail/DeleteSend", { id: dataId }, function (result) {
                                        if (result.status == "success") {
                                            utils.showSuccessMsg(result.msg);
                                            querySendGrid();
                                        } else {
                                            utils.showErrMsg(result.msg);
                                        }
                                    });
                                });
                            })
                        })
                    });
                }
            }
        });

        //查询发件箱grid
        querySendGrid = function () {
            var objs = $("#sendQueryForm").serializeObject();
            if (objs) {
                for (name in objs) {
                    if (name == "title2") { objs["title"] = objs[name]; }
                    else if (name == "startTime2") { objs["startTime"] = objs[name]; }
                    else if (name == "endTime2") { objs["endTime"] = objs[name]; }
                }
            }
            sendGrid.datagrid("options").queryParams = objs;
            sendGrid.datagrid("reload");
        }

        //发件箱查询按钮
        $("#sendQueryBtn").on("click", function () {
            querySendGrid();
        })

        //发邮件保存按钮
        $("#saveSendBtn").on("click", function () {
            var checked = true;
            var box = $("#allMember")
            $("#stoUser").removeClass("inputError");
            if ($("#stoUser").val() == 0 && !box[0].checked) {
                $("#stoUser").addClass("inputError");
                checked = false;
            }
            if ($("#stitle").val() == 0) {
                $("#stitle").addClass("inputError");
                checked = false;
            }
            if (checked) {
                if (sendEditor.getText() == 0) {
                    utils.showErrMsg("邮件内容不能为空");
                } else {
                    var data = { title: $("#stitle").val(), content: utils.getEditorHtml(sendEditor), toUser: $("#stoUser").val(), isAll: box[0].checked ? "2" : "" };
                    utils.AjaxPost("ProductEmail/SaveSend", data, function (result) {
                        if (result.status == "success") {
                            utils.showSuccessMsg(result.msg);
                            $('#sendDlg').dialog("close");
                            querySendGrid();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            }
        })

        //*************************************************** 发件箱end **************************************************************/

        //阅读邮件
        readEmail = function (row, rindex) {
            if (row.isRead != 2) {
                utils.AjaxPostNotLoadding("ProductEmail/Read", { id: row.id }, function (result) {
                    if (result.status == "success") {
                        row.isRead = 2;
                        row.read = "是"
                        receiveGrid.datagrid("updateRow", { index: rindex, row: row });
                        receiveGrid.datagrid("acceptChanges");
                        receiveGrid.datagrid("refreshRow", rindex);

                    }
                });
            }
        }

        //*************************************************** 收件箱start **************************************************************/

        //初始化收件箱grid
        var receiveGrid = utils.newGrid("receiveGrid", {
            //title: '收件箱',
            showFooter: true,
            columns: [[
             {
                 field: '_isRead', title: '', width: '30', formatter: function (val, row, index) {
                     if (row.isRead == 1) {
                         return '<i class="icon icon-envelope-alt" ></i>';
                     } else {
                         return '<i class="icon icon-folder-open-alt"></i>'
                     }
                 }
             },
             { field: 'fromUser', title: '发件人', width: '15%' },
             {
                 field: '_downTitle', title: '主题', width: '45%', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" rowIndex ="' + index + '" class="recTitle" >' + row.title + '</a>';
                 }
             },
             { field: 'addTime', title: '发送时间', width: '20%' },
             { field: 'read', title: '是否阅读', width: '9%' },
             {
                 field: '_operateCc', title: '操作', width: '80', formatter: function (val, row, index) {

                     return '&nbsp;<a href="javascript:void(0);" rowIndex ="' + index + '" class="recevieLink" >回复</a>&nbsp;' +
                     '&nbsp;<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >删除</a>';
                 }
             }
            ]],
            url: "ProductEmail/GetReceiveListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["read"] = data.rows[i]["isRead"] == 2 ? "是" : "否";
                }
            }
            return data;
        }, function () {
            //点击主题回复邮件
            $(".recTitle").each(function () {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = receiveGrid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openReceiveDataFrom(row, index);
                })
            });

            //点击回复按钮回复邮件
            $(".recevieLink").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = receiveGrid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openReceiveDataFrom(row, index);
                })
            })

            //删除邮件
            $(".recordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该邮件？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("ProductEmail/DeleteReceive", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg(result.msg);
                                queryReceiveGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })
        });

        //打开回复弹出窗口
        openReceiveDataFrom = function (row, index) {
            $("#sourceId").val(row.id);
            $("#sfristId").val(row.id);
            if (row.fristId) {
                $("#sfristId").val(row.fristId);
            }
            readEmail(row, index);
            utils.AjaxPost("ProductEmail/GetReceiveList", { fristId: $("#sfristId").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    var list = result.list;
                    var user = "system";
                    var html = "";
                    for (var i = 0; i < list.length; i++) {
                        var dto = list[i];
                        var stt = dto.fromUser == user ? "" : "color:red;"
                        var fontcolor = dto.fromUser == user ? "#145ccd" : "red;"
                        html += '<div class="comment"><a href="javascript:vaoid(0);" class="avatar" style="' + stt + '">' +
                               '<i class="icon-user icon-2x"></i></a><div class="content">' +
                               '<div class="pull-right text-muted">' + utils.changeDateFormat(dto.addTime) + '&nbsp;&nbsp;</div>' +
                               '<div><table><tr><td style="width:100px;font-weight:bold;color:' + fontcolor + '">' + dto.fromUser + '</td><td>标题：' + dto.title + '</td></tr></table></div>' +
                               '<div class="text">' + dto.content + '</div></div></div>';
                    }
                    $("#receiveListHtml").html(html);
                    $("#rtitle").val("回复【" + row.title + "】");
                    $("#oldFromUser").val(row.fromUser);
                    utils.setEditorHtml(receiveEditor, "");
                    $('#receiveDlg').dialog({ title: "回复【" + row.fromUser + "】" }).dialog("open");
                }
            });
        }

        //初始化回复邮件弹出框
        $('#receiveDlg').dialog({
            title: '回复',
            width: 800,
            height: 680,
            cache: false,
            modal: true
        })

        //回复邮件保存按钮
        $("#saveRecBtn").on("click", function () {
            var fs = $("#receiveDataForm").serializeObject();
            var checked = true;
            //非空校验
            $.each(fs, function (name, val) {
                var current = $("#" + name);
                var parent = current.parent();
                if (val == 0) {
                    checked = false;
                    current.addClass("inputError");
                }
            });
            if (checked) {
                if (receiveEditor.getText() == 0) {
                    utils.showErrMsg("请输入回复内容");
                } else {
                    var data = { title: $("#rtitle").val(), content: utils.getEditorHtml(receiveEditor), toUser: $("#oldFromUser").val(), sourceId: $("#sourceId").val(), fristId: $("#sfristId").val() };
                    utils.AjaxPost("ProductEmail/SaveReceive", data, function (result) {
                        if (result.status == "success") {
                            utils.showSuccessMsg(result.msg);
                            $('#receiveDlg').dialog("close");
                            //if (sendGrid) {
                            //    querySendGrid();
                            //}
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            }
        })

        //查询收件箱grid
        queryReceiveGrid = function () {
            var objs = $("#recQueryForm").serializeObject();
            receiveGrid.datagrid("options").queryParams = objs;
            receiveGrid.datagrid("reload");
        }

        //收件箱查询按钮
        $("#recQueryBtn").on("click", function () {
            queryReceiveGrid();
        })

        //*************************************************** 收件箱end **************************************************************/


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#sendDlg').dialog("destroy");
            $('#receiveDlg').dialog("destroy");
        };
    };

    return controller;
});