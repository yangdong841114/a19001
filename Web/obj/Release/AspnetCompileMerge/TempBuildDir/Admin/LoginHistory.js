
define(['text!LoginHistory.html', 'jquery', 'j_easyui', 'datetimepicker'], function (LoginHistory, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "操作日志");
        appView.html(LoginHistory);

        var flagObj = { "0": "成功", "1": "失败（帐号不存在）", "2": "失败（密码验证失败）", "3": "失败（帐号未开通）", "4": "失败（帐号被冻结）", "5": "失败（帐号被登录锁定）" };
        var flagList = [{ id: "0", name: "成功" }, { id: "1", name: "失败（帐号不存在）" }, { id: "2", name: "失败（密码验证失败）" },
                        { id: "3", name: "失败（帐号未开通）" }, { id: "4", name: "失败（帐号被冻结）" }, { id: "5", name: "失败（帐号被登录锁定）" }];

        //初始化查询区select
        $("#flag").empty();
        $("#flag").append("<option value='-1'>--全部--</option>");
        if (flagList && flagList.length > 0) {
            for (var i = 0; i < flagList.length; i++) {
                $("#flag").append("<option value='" + flagList[i].id + "'>" + flagList[i].name + "</option>"); 
            }
        }

        //初始化日期
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '登录账号', width: '13%' }]],
            columns: [[
             { field: 'userName', title: '姓名', width: '13%' },
             { field: 'loginIp', title: 'IP', width: '13%' },
             { field: 'flag', title: '状态', width: '13%' },
             { field: 'isMan', title: '是否前台', width: '13%' },
             { field: 'loginTime', title: '登录时间', width: '13%' },
             { field: 'mulx', title: '描述', width: '22%' },
            ]],
            url: "LoginHistory/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["loginTime"] = utils.changeDateFormat(data.rows[i]["loginTime"]);
                    data.rows[i]["flag"] = flagObj[data.rows[i]["flag"]];
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});