
'use strict';

(function (win) {
    //配置baseUrl
    //var baseUrl = document.getElementById('main').getAttribute('data-baseurl');

    /*
     * 文件依赖
     */
    var config = {
        //baseUrl: baseUrl,           //依赖相对路径
        waitSeconds: 30, //加载js超时时间，30秒
        map: {
            '*': {
                'css': '../Content/js/css.min'
            }
        },
        paths: {                    //如果某个前缀的依赖不是按照baseUrl拼接这么简单，就需要在这里指出
            director: '../Content/js/director.min',
            jquery: '../Content/js/jquery-3.2.1.min',
            underscore: '../Content/js/underscore',
            zui: '../Content/js/zui',
            datetimepicker: '../Content/js/datetimepicker.min',
            j_easyui: '../Content/easyui/jquery.easyui.min',
            ztree: '../Content/ztree/js/jquery.ztree.all.min',
            flexslider: '../Content/js/jquery.flexslider-min',
            ZeroClipboard: '../Content/ueditor/third-party/zeroclipboard/ZeroClipboard.min',
            quill: '../Content/quill/quill',
            text: '../Content/js/text'             //用于requirejs导入html类型的依赖
        },
        shim: {                     //引入没有使用requirejs模块写法的类库。
            underscore: {
                exports: '_'
            },
            jquery: {
                exports: '$'
            },
            director: {
                exports: 'Router'
            },
            ztree: {
                exports: 'ztree',
                deps: ['jquery',
                    'css!../Content/ztree/css/zTreeStyle/zTreeStyle.css'
                ]
            },
            j_easyui: {
                exports: 'j_easyui',
                deps: ['jquery',
                    'css!../Content/easyui/themes/material/easyui.css',
                    'css!../Content/easyui/themes/icon.css',
                ]
            },
            zui: {
                exports: 'zui',
                deps: ['jquery', 'css!../Content/css/zui2.css']
            },
            flexslider: {
                exports: 'flexslider',
                deps: ['jquery']
            },
            ZeroClipboard: {
                exports: 'ZeroClipboard',
            },
            quill: {
                exports: 'quill',
                deps: ['jquery', 'css!../Content/quill/quill.css']
            },
            datetimepicker: {
                exports: 'datetimepicker',
                deps: ['jquery', 'zui', 'css!../Content/css/datetimepicker.min.css']
            },
        }
    };

    require.config(config);
    require(['jquery', 'router', '../Content/js/CommonUtil', 'ZeroClipboard','quill', 'j_easyui', 'ztree', 'zui', 'flexslider'], function ($, router, util, ZeroClipboard,quill) {

        win.appView = $('#center');      //用于各个模块控制视图变化
        win.$ = $;                          //暴露必要的全局变量，没必要拘泥于requirejs的强制模块化
        win._ = _;
        win.Quill = quill;
        win.utils = util;
        win.cacheMap = null;
        win.cacheList = null;
        win.pass2 = undefined;
        win.pass3 = undefined;
        win.menuPass = undefined;
        window.ZeroClipboard = ZeroClipboard;
        win.areaData = null;
        win.LoginUser = undefined;

        //注册404路由
        router.toUrl("NotFound", "NotFound.js");
        //注册无权限路由
        router.toUrl("NoPermission", "NoPermission.js");
        //注册安全密码路由
        router.toUrl("Password2/:page", "Password2.js");
        //注册交易密码路由
        router.toUrl("Password3/:page", "Password3.js");

        router.configure({
            //未匹配到URL
            notfound: function () { location.href = "#NotFound"; },
            before: function () {
                util.DestoryAllPopover();
                win.appView.empty();
                win.appView.html("<table width=\"100%\"><tr>" +
                "<td align=\"center\" style=\"padding-top:150px;padding-bottom:150px;\"><i class=\"icon icon-spin icon-spinner-snake\" style=\"font-size:150px;\"></i>" +
                    "</td></tr></table>");

                //获取请求地址
                var hash = window.location.hash;
                hash = hash.substring(1, hash.length);
                var param = undefined;
                if (hash.indexOf("/") != -1) {
                    param = hash.substring(hash.indexOf("/") + 1, hash.length);
                    hash = hash.substring(0, hash.indexOf("/"));
                }
                //是否需要校验安全密码
                if (win.menuPass && win.menuPass[hash] == 2 && !win.pass2 && hash.indexOf("Password2") == -1 && hash.indexOf("Password3") == -1) {
                    var url = "#Password2/" + hash;
                    if (param) { url = url + "." + param; }
                    location.href = url;
                    return false;
                }
                    //是否需要校验交易密码
                else if (win.menuPass && win.menuPass[hash] == 3 && !win.pass3 && hash.indexOf("Password2") == -1 && hash.indexOf("Password3") == -1) {
                    var url = "#Password3/" + hash;
                    if (param) { url = url + "." + param; }
                    location.href = url;
                    return false;
                }
            },
        });


        function ShowTakeCash() {
            if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "") {
                utils.showErrMsg("请先完善银行信息！");
            } else {
                location.href = "#UTakeCash";
            }
        }

        //菜单
        function menuFix() {
            var sfEls = document.getElementById("nav").getElementsByTagName("li");
            for (var i = 0; i < sfEls.length; i++) {
                sfEls[i].onmouseover = function () {
                    this.className += (this.className.length > 0 ? " " : "") + "sfhover";
                }
                sfEls[i].onMouseDown = function () {
                    this.className += (this.className.length > 0 ? " " : "") + "sfhover";
                }
                sfEls[i].onMouseUp = function () {
                    this.className += (this.className.length > 0 ? " " : "") + "sfhover";
                }
                sfEls[i].onmouseout = function () {
                    this.className = this.className.replace(new RegExp("( ?|^)sfhover\\b"),
                    "");
                }
            }
        };
        
        //退出
        function exitAction() {
            util.confirm("您确定要退出登录吗？", function () {
                location.href = "/Home/Index";
            });
        };

        //退出登录
        $("#exitBtn").on('click', function () {
            exitAction();
        });

        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);
                var map = result.map;
                $("#bankName").val(map.userInfo.bankName);
                $("#bankCard").val(map.userInfo.bankCard);
                $("#bankUser").val(map.userInfo.bankUser);
                $("#bankAddress").val(map.userInfo.bankAddress);
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        util.AjaxPostNotLoadding("UserWeb/GetBaseData", {}, function (result) {
            if (result.status == "fail") {
                util.utils.showErrMsg("加载基础数据失败");
                return;
            }
            var baseset = result.map["baseSet"];      //基础设置
            win.LoginUser = result.map["LoginUser"];

            win.areaMap = result.map["area"];
            if (areaMap) {
                win.areaData = areaMap[0];
                for (var i = 0; i < win.areaData.length; i++) {
                    var dto = win.areaData[i];
                    if (areaMap[dto.id]) {
                        dto["childs"] = areaMap[dto.id];
                        for (var j = 0; j < dto["childs"].length; j++) {
                            var child = dto["childs"][j];
                            if (areaMap[child.id]) {
                                child["childs"] = areaMap[child.id];
                            }
                        }
                    }
                }
            }

            //消息提醒
            var notic = result.map.notic;
            if (notic && notic.length > 0) {
                var html = "<table>";
                for (var i = 0; i < notic.length; i++) {
                    var o = notic[i];
                    html += "<tr><td><a href='" + o.url + "'>" + o.msg + "</a></td></tr>";
                }
                html += "</table>";
                $.messager.show({
                    title: '消息提醒',
                    msg: html,
                    height: 100,
                    timeout: 60000,
                    showType: 'slide'
                });
                $("#newMsgAudio")[0].play();
            }

            //copyright
            $("#copyright").html(baseset.copyright);
            //网站名称
            $(document).attr("title", baseset.sitename);

            var dataCache = result.map["cahceData"];  //数据字典缓存数据
            win.cacheList = dataCache; //list形式缓存
            //map形式缓存
            if (dataCache != null) {
                win.cacheMap = {};
                for (name in dataCache) {
                    win.cacheMap[name] = {};
                    var list = dataCache[name];
                    if (list != null && list.length > 0) {
                        for (var i = 0; i < list.length; i++) {
                            var obj = list[i];
                            win.cacheMap[name][obj.id] = obj.name;
                        }
                    }
                }
            }

            //初始化菜单start
            util.AjaxPostNotLoadding("UserWeb/GetMenu", {}, function (result) {
                if (result.status == "success") {
                    var html = "<li><a href=\"#main\">网站首页</a></li>";
                    var list = result.list;
                    win.menuPass = {};
                    for (var i = 0; i < list.length; i++) {
                        var node = list[i];
                        if (node.isMenu == "Y") {
                            var id = node.id;
                            html += "<li><a href=\"javascript:void(0);\">" + node.resourceName + "</a><ul>";
                            for (var j = 0; j < list.length; j++) {
                                var child = list[j];
                                
                                if (id == child.parentResourceId) {
                                    var url = (child.curl + "").replace("User/", "");
                                    win.menuPass[url] = child.pass;
                                    //注册路由
                                    router.toUrl(url, url + ".js");
                                    router.toUrl(url+"/:id", url + ".js");
                                    if (child.isShow == 0) {
                                        if (child.id == "1010303") {
                                            html += "<li><div style=\"color:#fff; text-align:center;\"  onclick=\"ShowTakeCash()\">" + child.resourceName + "</div></li>";
                                        } else {
                                            html += "<li><a href=\"#" + url + "\">" + child.resourceName+ "</a></li>";
                                        }
                                    }
                                }
                            }
                            html += "</ul></li>"
                        }
                    }
                    html += "<li><a id='exitA' href=\"javascript:exitAction();\">安全退出</a></li>";
                    $("#menuUl").html(html);
                    menuFix();

                    //退出登录
                    $("#exitA").on('click', function () {
                        exitAction();
                    });

                    //注册主页路由
                    router.toUrl("main", "main.js");
                    //注册文章明细路由
                    router.toUrl("UArticleDetail/:id", "UArticleDetail.js");
                    
                    //加载主页
                    location.href = "#main";
                }
            });
 
        });

    });


})(window);
