
define(['text!CurrencyUser.html', 'jquery', 'j_easyui', 'datetimepicker'], function (CurrencyUser, $) {

    var controller = function (addDate) {
        //设置标题
        $("#center").panel("setTitle", "会员奖金");
        appView.html(CurrencyUser);

        if (addDate) {
            $("#startTime").val(addDate);
            $("#endTime").val(addDate);
            $("#dateTd1")[0].style.display = 'none';
            $("#dateTd2")[0].style.display = 'none';
            $("#reback").on("click", function () {
                history.go(-1);
            });
        } else {
            $("#reback")[0].style.display = 'none';
            //初始化日期选择框
            $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
        }

        

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
            columns: [[
                { field: 'addDate', title: '结算日期', width: '100' },
                { field: 'userName', title: '会员姓名', width: '100' },
                { field: 'cat1', title: '直推奖', width: '100' },
                { field: 'cat2', title: '循环对碰奖', width: '100' },
                { field: 'cat3', title: '领导对等奖', width: '100' },
                { field: 'cat4', title: '忠诚奖', width: '100' },
                { field: 'cat5', title: '领袖分紅奖', width: '100' },
                { field: 'cat6', title: '分销提成奖', width: '100' },
                { field: 'sf', title: '实发', width: '100' },
                {
                    field: '_operate', title: '操作', width: '70', align: 'center', formatter: function (val, row, index) {
                        if (row.userName != '合计：') {
                            return '&nbsp;<a href="javascript:void(0);" agm="' + row.addDate + row.uid + '" class="gridFildTo" >获奖明细</a>&nbsp;';
                        } else {
                            return '';
                        }
                    }
                }
            ]],
            onDblClickRow: function (index, data) {
                location.href = '#CurrencyUserDetail/' + (data.addDate+data.uid);
            },
            url: "Currency/GetByUserSumCurrency?addDate=" + addDate
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addDate"] = utils.changeDateFormat(data.rows[i]["addDate"], 'date');
                    data.rows[i]["cat1"] = data.rows[i]["cat1"].toFixed(2);
                    data.rows[i]["cat2"] = data.rows[i]["cat2"].toFixed(2);
                    data.rows[i]["cat3"] = data.rows[i]["cat3"].toFixed(2);
                    data.rows[i]["cat4"] = data.rows[i]["cat4"].toFixed(2);
                    data.rows[i]["cat5"] = data.rows[i]["cat5"].toFixed(2);
                    data.rows[i]["cat6"] = data.rows[i]["cat6"].toFixed(2);
                    data.rows[i]["cat7"] = data.rows[i]["cat7"].toFixed(2);
                    data.rows[i]["yf"] = data.rows[i]["yf"].toFixed(2);
                    data.rows[i]["fee1"] = data.rows[i]["fee1"].toFixed(2);
                    data.rows[i]["fee2"] = data.rows[i]["fee2"].toFixed(2);
                    data.rows[i]["fee3"] = data.rows[i]["fee3"].toFixed(2);
                    data.rows[i]["sf"] = data.rows[i]["sf"].toFixed(2);
                }
                data.footer[0].userName = '合计：';
            }
            return data;
        }, function () {
            //行查看明细按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var agm = $(dom).attr("agm");
                    location.href = '#CurrencyUserDetail/' + agm;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        $("#ExportExcel").on("click", function () {
            location.href = "Currency/ExportBonusDetailExcel?addDate=" + addDate;
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});