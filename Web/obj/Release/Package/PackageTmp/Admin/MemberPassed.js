
define(['text!MemberPassed.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberPassed, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "已开通会员");
        appView.html(MemberPassed);

        //初始化下拉框
        $("#empty").empty();
        $("#empty").append("<option value=''>--全部--</option>");
        $("#empty").append("<option value='0'>实单</option>");
        $("#empty").append("<option value='1'>空单</option>");
        $("#rLevel").empty();
        $("#rLevel").append("<option value=''>--全部--</option>");
        if (cacheList["rLevel"] && cacheList["rLevel"].length > 0) {
            for (var i = 0; i < cacheList["rLevel"].length; i++) {
                $("#rLevel").append("<option value='" + cacheList["rLevel"][i].id + "'>" + cacheList["rLevel"][i].name + "</option>");
            }
        }

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始发邮件文本编辑器
        var sendEditor = new Quill("#scontent", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //初始化发邮件弹出框
        $('#sendDlg').dialog({
            title: '发邮件',
            width: 600,
            height: 500,
            cache: false,
            modal: true
        })

        //初始化群发短信弹出框
        $('#msgDlg').dialog({
            title: '群发短信',
            width: 620,
            height: 380,
            cache: false,
            modal: true
        })

        //冻结会员
        disabledMember = function () {
            lockMember(1);
        }
        //启用会员
        enabledMember = function () {
            lockMember(0);
        }
        //重置密码
        redPwdMember = function () {
            lockMember(2);
        }

        //启用或冻结会员实现
        lockMember = function (isLock) {
            //获取选中的行（数组）
            var rows = grid.datagrid("getSelections");
            var msg = "";
            if(isLock == 0)
                msg = "启用";
            if (isLock == 1)
                msg = "冻结";
            if (isLock == 2)
                msg = "重置密码";
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要" + msg + "的会员！"); }
            else {
                utils.confirm("您确定要" + msg + "选中的会员吗？", function () {
                    var data = { isLock: isLock };
                    for (var i = 0; i < rows.length; i++) {
                        data["ids[" + i + "]"] = rows[i].id;
                    }
                    utils.AjaxPost("MemberPassed/LockMember", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            //重刷grid
                            queryGrid();
                        }
                    });
                });
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员名称', width: '100' },
             { field: 'uLevel', title: '会员级别', width: '80' },
             { field: 'rLevel', title: '荣誉级别', width: '80' },
          
             //{ field: 'bankName', title: '开户行', width: '100' },
             //{ field: 'bankCard', title: '开户帐号', width: '130' },
             //{ field: 'bankUser', title: '开户名', width: '100' },
             //{ field: 'code', title: '身份证号', width: '140' },
             { field: 'phone', title: '联系电话', width: '100' },
             //{ field: 'address', title: '联系地址', width: '150' },
             //{ field: 'qq', title: 'QQ', width: '100' },
             { field: 'reName', title: '推荐人编号', width: '80' },
           
             { field: 'passTime', title: '开通日期', width: '130' },
             { field: 'byopen', title: '操作人', width: '80' },
          
             { field: 'isLock', title: '是否冻结', width: '70' },
          
             {
                 field: '_operate', title: '操作', width: '160', align: 'center', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildEdit" >编辑</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildTo" >进入前台</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" userId="' + row.userId + '" class="gridFildEmail" >发邮件</a>';
                 }
             }
            ]],
            onDblClickRow: function (index, data) {
                location.href = '#MemberInfo/' + data.id;
            },
            toolbar: [{
                text: "启用",
                iconCls: 'icon-man',
                handler: enabledMember
            }, '-', {
                text: "冻结",
                iconCls: 'icon-lock',
                handler: disabledMember
            }, '-', {
                text: "重置密码",
                iconCls: 'icon-redo',
                handler: redPwdMember
            }
            ],
            url: "MemberPassed/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["passTime"] = utils.changeDateFormat(data.rows[i]["passTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    var rr = cacheMap["rLevel"][data.rows[i].rLevel];
                    data.rows[i].rLevel = rr ? rr : "无";
                    data.rows[i].empty = data.rows[i].empty == 0 ? "否" : "是";
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                    data.rows[i].isFt = data.rows[i].isFt == 0 ? "否" : "是";
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });
            //行进入前台按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("确定用该帐号进入前台吗？", function () {
                        utils.AjaxPost("MemberPassed/ToForward", { id: memberId }, function (result) {
                            if (result.status == "success") {
                                location.href = "/wap/User/index.html";
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
            //行发邮件按钮
            $(".gridFildEmail").each(function (i, dom) {
                dom.onclick = function () {
                    var userId = $(dom).attr("userId");
                    $("#stoUser").val(userId);
                    $('#sendDlg').dialog({ title: "发邮件" }).dialog("open");
                }
            });
        })

        //发邮件保存按钮
        $("#saveSendBtn").on("click", function () {
            var fs = $("#sendModalForm").serializeObject();
            var checked = true;
            //非空校验
            $.each(fs, function (name, val) {
                var current = $("#" + name);
                var parent = current.parent();
                if (val == 0) {
                    checked = false;
                    current.addClass("inputError");
                }
            });
            if (checked) {
                if (sendEditor.getText() == 0) {
                    utils.showErrMsg("邮件内容不能为空");
                } else {
                    var data = { title: $("#stitle").val(), content: utils.getEditorHtml(sendEditor), toUser: $("#stoUser").val() };
                    utils.AjaxPost("EmailBox/SaveSend", data, function (result) {
                        if (result.status == "success") {
                            utils.showSuccessMsg(result.msg);
                            $('#sendDlg').dialog("close");
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            }
        });

        $("#msgBtn").on("click", function () {
            $("#msgContent").removeClass("inputError");
            $("#msgContent").val("");
            $("#ckcount").val("0");
            var rows = grid.datagrid("getSelections");
            if (rows != null && rows.length > 0) { $("#ckcount").val(rows.length); }
            $('#msgDlg').dialog("open");
        })

        //删除错误样式
        $("#msgContent").on("focus", function () {
            $("#msgContent").removeClass("inputError");
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassed/ExportOpenExcel";
        })

        //群发勾选会员按钮
        $("#sendCheckedBtn").on("click", function () {
            var current = $("#msgContent");
            if (current.val() == 0) {
                current.addClass("inputError")
            } else {
                var rows = grid.datagrid("getSelections");
                if (rows == null || rows.length == 0) { utils.showErrMsg("请勾选需要发送会员！"); }
                else {
                    var data = {};
                    for (var i = 0; i < rows.length; i++) {
                        var row = rows[i];
                        data["phones[" + i + "]"] = row.phone;
                    }
                    data["msg"] = current.val();
                    data["flag"] = 1;
                    utils.AjaxPost("MemberPassed/SendMessage", data, function (result) {
                        if (result.status == "success") {
                            utils.showSuccessMsg("发送成功");
                            $('#msgDlg').dialog("close");
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            }
        });

        //群发所有会员按钮
        $("#sendAllBtn").on("click", function () {
            var current = $("#msgContent");
            if (current.val() == 0) {
                current.addClass("inputError")
            } else {
                var data = {};
                data["msg"] = current.val();
                data["flag"] = 2;
                utils.AjaxPost("MemberPassed/SendMessage", data, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("发送成功");
                        $('#msgDlg').dialog("close");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#sendDlg').dialog("destroy");
            $('#msgDlg').dialog("destroy");
        };
    };

    return controller;
});