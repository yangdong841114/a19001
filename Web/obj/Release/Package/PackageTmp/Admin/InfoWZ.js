
define(['text!InfoWZ.html', 'jquery', 'j_easyui', 'datetimepicker'], function (InfoWZ, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "文章管理");
        appView.html(InfoWZ);

        //初始化表单区域
        $('#dlgInfoWZ').dialog({
            title: '发布文章',
            width: 850,
            height: 600,
            cache: false,
            modal: true
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });


        //初始化编辑器
        var editor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveBtnInfoWZ").bind("click", function () {
            if ($("#title").val() == 0) {
                utils.showErrMsg("请输入主题");
            }
            else if (editor.getText() == 0) {
                utils.showErrMsg("请输入内容");
            } else {
                utils.AjaxPost("InfoWZ/SaveByUeditor", { infocontent: utils.getEditorHtml(editor), fltype: $("#fltype").val(), title: $("#title").val(), infoJj: $("#infoJj").val(), id: $("#id").val() == 0 ? "" : $("#id").val() }, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("操作成功");
                        $('#dlgInfoWZ').dialog("close");
                        queryGrid();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
        })

        //打开明细
        openDetailDialog = function (row) {
            $("#title").val(row.title);
            $("#id").val(row.id);
            $("#fltype").val(row.fltype);
            $("#infoJj").val(row.infoJj);
            utils.setEditorHtml(editor, row.infocontent)
            $('#dlgInfoWZ').dialog({ title: '修改文章' }).dialog("open");
        }

        //启用或冻结会员实现
        delWZ = function () {
            //获取选中的行（数组）
            var rows = grid.datagrid("getSelections");
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要删除的文章！"); }
            else {
                utils.confirm("您确定要删除吗？", function () {
                    var data = { isLock: 1 };
                    for (var i = 0; i < rows.length; i++) {
                        data["ids[" + i + "]"] = rows[i].id;
                    }
                    utils.AjaxPost("InfoWZ/DeletePl", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            //重刷grid
                            queryGrid();
                        }
                    });
                });
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userId', title: '会员编号', width: '10%' },
             { field: 'userName', title: '会员姓名', width: '10%' },
             {
                 field: '_title', title: '主题', width: '25%', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" rowIndex ="' + index + '" class="titleOpen" >' + row.title + '</a>';
                 }
             },
            { field: 'infoJj', title: '摘要', width: '25%' },
             { field: 'llcount', title: '阅读量', width: '6%' },
              { field: 'plcount', title: '评论量', width: '6%' },
               { field: 'dzcount', title: '点赞量', width: '6%' },
             { field: 'addTime', title: '发布日期', width: '15%' },
             {
                 field: '_deleteRecord', title: '操作', width: '15%', formatter: function (val, row, index) {
                     var cont = "";
                     if (row.isTop == 1) {
                         cont = '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordTop" >置顶</a>&nbsp;&nbsp;';
                     } else {
                         cont = '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordCancelTop" >取消置顶</a>&nbsp;&nbsp;';
                     }
                     cont += '<a href="javascript:void(0);" rowIndex ="' + index + '" class="recordEdit" >编辑</a>&nbsp;&nbsp;' +
                            '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >删除</a>';
                     return cont;
                 }
             }
            ]],
            toolbar: [{
                text:"发布文章",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $("#title").val("");
                    $("#id").val("");
                    utils.setEditorHtml(editor, "")
                    $('#dlgInfoWZ').dialog({ title: '发布文章' }).dialog("open");
                }
            },
            '-', {
            text: "删除选中",
            iconCls: 'icon-lock',
            handler: delWZ
        }
            ],
            url: "InfoWZ/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {

            //置顶
            $(".recordTop").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("确定置顶吗？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/SaveTop", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("置顶成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })

            //取消置顶
            $(".recordCancelTop").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("确定取消置顶吗？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/CancelTop", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("取消成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })

            //点击标题打开文章明细
            $(".titleOpen").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = grid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openDetailDialog(row);
                })
            })

            //点击编辑按钮编辑
            $(".recordEdit").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = grid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openDetailDialog(row);
                })
            })

            //删除
            $(".recordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该记录？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("删除成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "title2" ? "title" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlgInfoWZ').dialog("destroy");
        };
    };

    return controller;
});