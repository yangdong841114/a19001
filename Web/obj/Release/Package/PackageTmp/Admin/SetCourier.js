﻿
define(['text!SetCourier.html', 'jquery', 'j_easyui', 'datetimepicker'], function (SetCourier, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "操作日志");
        appView.html(SetCourier);
        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field: 'com', title: '快递公司', width: '30%' },
             { field: 'no', title: '对应编码', width: '40%' }
            ]],
            url: "ParameterSet/GetSetCourier"
        }, null, function (data) {
            //if (data && data.rows) {
            //    for (var i = 0; i < data.rows.length; i++) {
            //        data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
            //    }
            //}
            return data;
        })
        controller.onRouteChange = function () {

        };
        $("#UpdateCourier").click(function (){
            $.ajax({
                url: "ParameterSet/SetCourier",           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                success: function (result) {
                   
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(XMLHttpRequest, textStatus, errorThrown);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        });
    };

    return controller;
});