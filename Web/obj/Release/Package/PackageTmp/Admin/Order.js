
define(['text!Order.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (Order, $) {

    var controller = function (name) {
        $("#center").panel("setTitle", "商品订单");

        var detailGrid = undefined;
        var grid = undefined;

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        var statusDto = { 1: "待发货", 2: "已发货", 3: "已收货", 4: "已取消" }

        utils.AjaxPostNotLoadding("Order/GetLogisticList", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                appView.html(Order);

                //var logList = result.map.logList;
                ////初始化下拉框
                //$("#wlNo").empty();
                //$("#wlNo").append("<option value=''>--请选择--</option>");
                //if (logList && logList.length > 0) {
                //    for (var i = 0; i < logList.length; i++) {
                //        var vo = logList[i];
                //        $("#wlNo").append("<option value='" + vo.logNo + "'>" + vo.logName + "</option>");
                //    }
                //}

                //初始化日期选择框
                $(".form-date").datetimepicker(
                    {
                        language: "zh-CN",
                        weekStart: 1,
                        todayBtn: 1,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        minView: 2,
                        forceParse: 0,
                        format: "yyyy-mm-dd"
                    });

                //初始化按钮
                $(".easyui-linkbutton").each(function (i, ipt) {
                    $(ipt).linkbutton({});
                });

                //初始化发货弹出框
                $('#dlg').dialog({
                    title: '发货',
                    width: 450,
                    height: 210,
                    cache: false,
                    modal: true
                })

                //初始化物流信息
                $('#msgdlg').dialog({
                    title: '查看物流信息',
                    width: 600,
                    height: 400,
                    cache: false,
                    modal: true
                })

                //获取焦点事件
                $(".form-control").each(function (index, ele) {
                    var current = $(this);
                    current.on("focus", function () {
                        if (current.hasClass("inputError")) {
                            current.removeClass("inputError");
                        }
                    })
                })

                //发货保存按钮
                $("#saveSendBtn").bind("click", function () {
                    var fs = $("#sendForm").serializeObject();
                    var checked = true;
                    //非空校验
                    $.each(fs, function (name, val) {
                        var current = $("#" + name);
                        var parent = current.parent();
                        if (val == 0) {
                            checked = false;
                            current.addClass("inputError");
                        }
                    });

                    if (checked) {
                        var rows = grid.datagrid("getSelections");
                        if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要发货的订单！"); }
                        else {
                            var data = {};
                            var count = 0;
                            for (var i = 0; i < rows.length; i++) {
                                var row = rows[i];
                                if (row.status == 1) {
                                    data["list[" + count + "].id"] = row.id;
                                    data["list[" + count + "].logName"] = $("#wlNo").val();//$("#wlNo").find("option:selected").text();
                                    data["list[" + count + "].logNo"] = $("#logNo").val();
                                    data["list[" + count + "].wlNo"] = $("#wlNo").val();
                                    count++;
                                }
                            }
                            if (count == 0) { utils.showErrMsg("您选择的订单不是【待发货】，不能发货！"); }
                            else {
                                utils.AjaxPost("Order/LogisticOrder", data, function (result) {
                                    if (result.status == "success") {
                                        utils.showSuccessMsg("发货成功");
                                        $('#dlg').dialog("close");
                                        queryGrid();
                                    } else {
                                        utils.showErrMsg(result.msg);
                                    }
                                });
                            }
                        }
                    }
                })

                //查询按钮
                $("#submit").bind("click", function () {
                    queryGrid();
                })

                //初始化表格
                grid = utils.newGrid("dg", {
                    singleSelect: false, //禁用单选
                    title: '订单列表',
                    showFooter: true,
                    frozenColumns: [[{ field: 'userId', title: '粉丝编号', width: '100' }]],
                    columns: [[
                        { field: 'ck', title: '文本', checkbox: true, },
                        { field: 'id', title: '订单编号', width: '170' },
                        { field: 'userName', title: '昵称', width: '100' },
                        { field: 'typeId', title: '订单类型', width: '80' },
                        { field: 'mname', title: '发货人', width: '100' },
                        { field: 'receiptName', title: '收货人', width: '100' },
                        { field: 'phone', title: '联系电话', width: '120' },
                        { field: 'address', title: '收货地址', width: '300' },
                        { field: 'productNum', title: '购买总数', width: '80' },
                        { field: 'productMoney', title: '总金额', width: '80' },
                        { field: 'sxf', title: '手续费', width: '80' },
                        {
                            field: '_status', title: '状态', width: '80', formatter: function (val, row, index) {
                                var cont = ''
                                if (row.status == 1) {
                                    cont = '<font style="color:red">待发货</font>';
                                } else if (row.status == 2) {
                                    cont = '<font style="color:#f1a325">已发货</font>';
                                } else if (row.status == 4) {
                                    cont = '<font style="color:#f1a325">已取消</font>';
                                } else {
                                    cont = '<font style="color:green">已完成</font>';
                                }
                                return cont;
                            }
                        },
                        //{ field: 'logName', title: '物流公司', width: '130' },
                        //{ field: 'logNo', title: '物流单号', width: '130' },
                        { field: 'orderDate', title: '购买日期', width: '130' },
                        {
                            field: '_operate', title: '操作', width: '100', formatter: function (val, row, index) {
                                var tt = "";
                                if (row.status == 1) {
                                    tt += '&nbsp;<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >取消</a>';
                                } else {
                                    tt += '&nbsp;&nbsp;<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordLog" >查看物流</a>';
                                }
                                return tt;
                            }
                        },
                    ]],
                    toolbar: [{
                        text: "发货",
                        iconCls: 'icon-add',
                        handler: function () {
                            var rows = grid.datagrid("getSelections");
                            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要发货的订单！"); }
                            else {
                                var checked = true
                                for (var i = 0; i < rows.length; i++) {
                                    var row = rows[i];
                                    if (row.status != 1) {
                                        utils.showErrMsg("订单【" + row.id + "】不是【待发货】状态，不能发货！");
                                        checked = false;
                                    }
                                }
                                if (checked) {
                                    $("#wlNo").val("");
                                    $("#logNo").val("");
                                    $('#dlg').dialog("open");
                                }
                            }
                        }
                    }
                    ],
                    onClickRow: function (index, data) {
                        detailGrid.datagrid("loadData", data.items);
                    },
                    url: "Order/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["orderDate"] = utils.changeDateFormat(data.rows[i]["orderDate"]);
                            data.rows[i].statusName = statusDto[data.rows[i].status];
                            data.rows[i].typeId = '消费订单';
                            var contry = data.rows[i].contry == null || data.rows[i].contry == "0" || data.rows[i].contry == 0 ? "" : data.rows[i].contry;
                            var provinceName = data.rows[i].provinceName == null || data.rows[i].provinceName == "0" || data.rows[i].provinceName == 0 ? "" : data.rows[i].provinceName;
                            var cityName = data.rows[i].cityName == null || data.rows[i].cityName == "0" || data.rows[i].cityName == 0 ? "" : data.rows[i].cityName;
                            var areaName = data.rows[i].areaName == null || data.rows[i].areaName == "0" || data.rows[i].areaName == 0 ? "" : data.rows[i].areaName;
                            var address = data.rows[i].address == null || data.rows[i].address == "0" || data.rows[i].address == 0 ? "" : data.rows[i].address;
                            data.rows[i].address = contry + provinceName + cityName + areaName + address;
                        }
                    }
                    return data;
                }, function () {

                    //查看物流信息
                    $(".recordLog").each(function (index, ele) {
                        var dom = $(this);
                        dom.on("click", function () {
                            var dataId = dom.attr("dataId");
                            utils.AjaxPost("Order/GetLogisticMsg", { id: dataId }, function (result) {
                                if (result.status == "success") {
                                    var dto = result.result;
                                    var html = "<table><tr><td colspan='2' style='font-weight:bold;'>物流公司：" + dto.company + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物流单号：" + dto.no + "</td></tr>";
                                    $("#logMsgDiv").empty();
                                    if (dto.list && dto.list.length > 0) {
                                        for (var i = 0; i < dto.list.length; i++) {
                                            var vo = dto.list[i];
                                            html += "<tr><td style='width:130px;'>" + utils.changeDateFormat(vo.datetime) + "</td><td>" + vo.remark + "</td></tr>";
                                            //html += '<div class="col-sm-12" style="text-align:left;" id="logMsgDiv"><div class="form-group">' +
                                            //        '<label for="exampleInputAddress1">' + utils.changeDateFormat(vo.datetime) + '</label>' +
                                            //        '<label for="exampleInputAddress1" class="text-yellow">' + vo.remark + '</label>' +
                                            //        '</div></div>';
                                        }
                                    }
                                    html += "</tabel>"
                                    $("#logMsgDiv").html(html);
                                    $('#msgdlg').dialog("open");
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            });
                        })
                    });

                    //删除按钮
                    $(".recordDelete").each(function (index, ele) {
                        var dom = $(this);
                        dom.on("click", function () {
                            utils.confirm("您确定要取消该订单吗？", function () {
                                var dataId = dom.attr("dataId");
                                utils.AjaxPost("Order/DeleteOrder", { id: dataId }, function (result) {
                                    if (result.status == "success") {
                                        utils.showSuccessMsg("取消成功");
                                        queryGrid();
                                    } else {
                                        utils.showErrMsg(result.msg);
                                    }
                                });
                            });
                        })
                    })
                })

                //订单详情grid
                detailGrid = utils.newGrid("detailGrid", {
                    showFooter: true,
                    pagination: false,
                    columns: [[
                        { field: 'hid', title: '订单编号', width: '20%' },
                        { field: 'productNumber', title: '产品编码', width: '20%' },
                        { field: 'productName', title: '产品名称', width: '30%' },
                        { field: 'price', title: '价格', width: '10%' },
                        { field: 'num', title: '数量', width: '10%' },
                        { field: 'total', title: '金额', width: '10%' },
                    ]]
                }, null, function (data) {
                    return data;
                })
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
            $('#msgdlg').dialog("destroy");
        };
    };

    return controller;
});
