
define(['text!ChangePassword.html', 'jquery', 'j_easyui', 'zui'], function (ChangePassword, $) {

    var controller = function (name) {
       
        $("#center").panel("setTitle", "修改密码");
        appView.html(ChangePassword);

        //失去焦点事件
        $(".form-control").each(function (index,ele) {
            var current = $(this);
            var parent = current.parent();
            var before = current.parent().parent().children().eq(0);
            current.on("focus", function (event) {
                parent.removeClass("has-error");
                utils.destoryPopover(current);
            });
        });

        //保存数据
        saveData = function (oldPass, newPass, flag,cls) {
            var data = { oldPass: oldPass, newPass: newPass, flag: flag };
            utils.AjaxPost("ChangePassword/ChangePassword", data, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg("修改成功！");
                    //清空表单
                    $("." + cls).each(function (index, ele) {
                        $(this).val("");
                    });
                }
            });
        }

        //修改登录密码
        $("#changBtn1").on('click', function () {
            var checked = true;
            $(".pss1").each(function (index, ele) {
                var current = $(this);
                var parent = current.parent();
                if (current.val() == 0) {
                    checked = false;
                    parent.addClass("has-error");
                    utils.showPopover(current, "不能为空", "popover-danger");
                }
            })

            if (checked) {
                if ($("#password").val() != $("#passwordRe").val()) {
                    var current = $("#passwordRe");
                    current.parent().addClass("has-error");
                    utils.showPopover(current, "确认新密码与新登录密码不一致", "popover-danger");
                } else {
                    saveData($("#oldPassword").val(), $("#password").val(), 1, "pss1");
                }
            }
        });

        //修改安全密码
        $("#changBtn2").on('click', function () {
            var checked = true;
            $(".pss2").each(function (index, ele) {
                var current = $(this);
                var parent = current.parent();
                if (current.val() == 0) {
                    checked = false;
                    parent.addClass("has-error");
                    utils.showPopover(current, "不能为空", "popover-danger");
                }
            })

            if (checked) {
                if ($("#passOpen").val() != $("#passOpenRe").val()) {
                    var current = $("#passOpenRe");
                    current.parent().addClass("has-error");
                    utils.showPopover(current, "确认新密码与新安全密码不一致", "popover-danger");
                } else {
                    saveData($("#oldPassOpen").val(), $("#passOpen").val(), 2, "pss2");
                }
            }
        });

        //修改交易密码
        $("#changBtn3").on('click', function () {
            var checked = true;
            $(".pss3").each(function (index, ele) {
                var current = $(this);
                var parent = current.parent();
                if (current.val() == 0) {
                    checked = false;
                    parent.addClass("has-error");
                    utils.showPopover(current, "不能为空", "popover-danger");
                }
            })

            if (checked) {
                if ($("#threepass").val() != $("#threepassRe").val()) {
                    var current = $("#threepassRe");
                    current.parent().addClass("has-error");
                    utils.showPopover(current, "确认新密码与新交易密码不一致", "popover-danger");
                } else {
                    saveData($("#oldThreepass").val(), $("#threepass").val(), 3, "pss3");
                }
            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});