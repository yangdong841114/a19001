
define(['text!ShopPassed.html', 'jquery', 'j_easyui'], function (ShopPassed, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "已开通报单中心");
        appView.html(ShopPassed);


        //冻结
        disabledShop = function () {
            lockShop(1);
        }
        //启用
        enabledShop = function () {
            lockShop(0);
        }

        //启用或冻结会员实现
        lockShop = function (isLock) {
            //获取选中的行（数组）
            var rows = grid.datagrid("getSelections");
            var msg = isLock == 0 ? "启用" : "冻结";
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要" + msg + "的报单中心！"); }
            else{
                utils.confirm("您确定要" + msg + "选中的报单中心吗？", function () {
                    var data = {isLock:isLock};
                    for (var i = 0; i < rows.length; i++) {
                        data["ids[" + i + "]"] = rows[i].id;
                    }
                    utils.AjaxPost("ShopPassed/LockShop", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            //重刷grid
                            queryGrid();
                        }
                    });
                });
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '12.5%' }]],
            columns: [[
             { field: 'ck', title: '文本', checkbox: true, },
             { field: 'userName', title: '会员名称', width: '12.5%' },
             { field: 'uLevel', title: '会员级别', width: '12.5%' },
             { field: 'applyAgentTime', title: '申请日期', width: '12.5%' },
             { field: 'openAgentTime', title: '开通日期', width: '12.5%' },
             { field: 'agentIslock', title: '是否冻结', width: '10.5%' },
             { field: 'agentOpUser', title: '操作人', width: '12.5%' },
             {
                 field: '_operate', title: '操作', width: '55', align: 'center', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildMemberList" >会员列表</a>';
             }
             }
            ]],
            toolbar: [{
                    text: "启用",
                    iconCls: 'icon-man',
                    handler: enabledShop
                }, '-', {
                    text: "冻结",
                    iconCls: 'icon-lock',
                    handler: disabledShop
                }
            ],
            url: "ShopPassed/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["applyAgentTime"] = utils.changeDateFormat(data.rows[i]["applyAgentTime"]);
                    data.rows[i]["openAgentTime"] = utils.changeDateFormat(data.rows[i]["openAgentTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    data.rows[i].agentIslock = data.rows[i].agentIslock == 0 ? "否" : "是";
                }
            }
            return data;
        }, function () {
            //行会员列表按钮
            $(".gridFildMemberList").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#ShopMemberList/" + memberId;
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});