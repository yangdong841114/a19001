
define(['text!MemberAdmin.html', 'jquery', 'j_easyui'], function (MemberAdmin, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "管理员设置");

        var atp = "N";

        //初始化加载数据
        utils.AjaxPostNotLoadding("MemberAdmin/GetAdminType", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                atp = result.msg;
                appView.html(MemberAdmin);

                $('#dlg').dialog({
                    title: '编辑记录',
                    width: 400,
                    height: 450,
                    cache: false,
                    modal: true
                })


                //初始化编辑区input text
                $(".easyui-textbox").each(function (i, ipt) {
                    $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });
                });

                //初始化input password
                $(".easyui-passwordbox").each(function (i, ipt) {
                    $(ipt).passwordbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });
                });

                //初始化按钮
                $(".easyui-linkbutton").each(function (i, ipt) {
                    $(ipt).linkbutton({});
                });

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    $('#editModalForm').form('submit', {
                        url: 'MemberAdmin/SaveOrUpdate',
                        onSubmit: function () {
                            var ok = $(this).form('validate');
                            if (ok) {
                                if ($("#password").passwordbox("getValue") != 0 && $("#password").passwordbox("getValue") != $("#passwordRe").passwordbox("getValue")) {
                                    utils.showErrMsg("登录密码与确认密码不一致");
                                    ok = false;
                                } else if ($("#passOpen").passwordbox("getValue") != 0 && $("#passOpen").passwordbox("getValue") != $("#passOpenRe").passwordbox("getValue")) {
                                    utils.showErrMsg("安全密码与确认密码不一致");
                                    ok = false;
                                } else if ($("#threepass").passwordbox("getValue") != 0 && $("#threepass").passwordbox("getValue") != $("#threepassRe").passwordbox("getValue")) {
                                    utils.showErrMsg("交易密码与确认密码不一致");
                                    ok = false;
                                } else {
                                    $.messager.progress({
                                        title: '请稍后',
                                        msg: '正在处理中...',
                                        interval: 100,
                                        text: ''
                                    });
                                }
                            }
                            return ok;
                        },
                        success: function (result) {
                            $.messager.progress("close");
                            var result = eval('(' + result + ')');
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //关闭弹出框
                                $('#dlg').dialog("close");
                                //重刷grid
                                queryGrid();
                            }
                        }
                    });
                    return false;
                })


                //编辑数据
                editRecord = function () {
                    var obj = grid.datagrid("getSelected");
                    if (obj == null) { utils.showErrMsg("请选择需要编辑的行！"); }
                    else {
                        if (atp == "N") {
                            $(".easyui-passwordbox").each(function (i, ipt) {
                                $(ipt).passwordbox({ disabled: true,required:false });
                            });
                        } else {
                            $(".easyui-passwordbox").each(function (i, ipt) {
                                $(ipt).passwordbox({ disabled: false, required: false });
                            });
                        }
                        //清空表单数据
                        $('#editModalForm').form('reset');
                        //根据选中的行记录加载数据
                        $('#editModalForm').form('load', obj);
                        $('#dlg').dialog({ title: '编辑管理员' }).dialog("open");

                    }
                }

                //添加数据
                newRecord = function () {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    $(".easyui-passwordbox").each(function (i, ipt) {
                        $(ipt).passwordbox({ disabled: false, required: true });
                    });
                    //清空id（编辑、添加使用同一个表单，所以这里需要清除）
                    $("#id").val('');
                    $('#dlg').dialog({ title: '添加管理员' }).dialog("open");
                }

                //删除记录
                removeRecord = function () {
                    var obj = grid.datagrid("getSelected");
                    if (obj == null) { utils.showErrMsg("请选择需要删除的行！"); }
                    else {
                        utils.confirm("您确定要删除吗？", function () {
                            utils.AjaxPost("MemberAdmin/Delete", { id: obj.id }, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg(result.msg);
                                    //重刷grid
                                    queryGrid();
                                }
                            });
                        });
                    }
                }

                //初始化表格
                var grid = utils.newGrid("dg", {
                    columns: [[
                     { field: 'ck', title: '文本', checkbox: true, },
                     { field: 'userId', title: '管理员编号', width: '10%' },
                     { field: 'userName', title: '姓名', width: '15%' },
                     { field: 'addTime', title: '注册日期', width: '15%' },
                     { field: 'nickName', title: '拥有角色', width: '58%' }
                    ]],
                    toolbar: [{
                        text: "添加",
                        iconCls: 'icon-add',
                        handler: newRecord
                    }, '-', {
                        text: "编辑",
                        iconCls: 'icon-edit',
                        handler: editRecord
                    }
                    , '-', {
                        text: "删除",
                        iconCls: 'icon-cancel',
                        handler: removeRecord
                    }

                    ],
                    url: "MemberAdmin/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                        }
                    }
                    return data;
                })

                //查询grid
                queryGrid = function () {
                    var objs = $("#QueryForm").serializeObject();
                    var queryObj = {};
                    $.each(objs, function (name, val) {
                        if (val != 0) {
                            var name = name == "userId2" ? "userId" : name;
                            queryObj[name] = val;
                        }
                    });
                    grid.datagrid("options").queryParams = queryObj;
                    grid.datagrid("reload");
                }

                //查询按钮
                $("#submit").on("click", function () {
                    queryGrid();
                })
            }
        });


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});