
define(['text!SystemBank.html', 'jquery', 'j_easyui'], function (SystemBank, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "公司账户设置");
        appView.html(SystemBank);

        //初始化模态对话框
        $('#dlgSystemBank').dialog({
            title: '编辑记录',
            width: 400,
            height: 240,
            cache: false,
            modal: true
        })

        //初始化编辑区input text
        $(".easyui-textbox").each(function (i, ipt) {
            $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveSystemBankBtn").bind("click", function () {
            $('#editSystemBank').form('submit', {
                url: 'SystemBank/SaveOrUpdate',
                onSubmit: function () {
                    var ok = $(this).form('validate');
                    if (ok) {
                        $.messager.progress({
                            title: '请稍后',
                            msg: '正在处理中...',
                            interval: 100,
                            text: ''
                        });
                    }
                    return ok;
                },
                success: function (result) {
                    $.messager.progress("close");
                    var result = eval('(' + result + ')');
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlgSystemBank').dialog("close");
                        //重刷grid
                        queryGrid();
                        if (result.msg.indexOf("添加") != -1) {
                            init();
                        }
                    }
                }
            });
            return false;
        })


        //编辑数据
        editRecord = function () {
            var obj = grid.datagrid("getSelected");
            if (obj == null) { utils.showErrMsg("请选择需要编辑的行！"); }
            else {
                //清空表单数据
                $('#editSystemBank').form('reset');
                //根据选中的行记录加载数据
                $('#editSystemBank').form('load', obj);
                $('#dlgSystemBank').dialog({ title: '编辑公司帐号' }).dialog("open");

            }
        }

        //添加数据
        newRecord = function () {
            //清空表单数据
            $('#editSystemBank').form('reset');
            //清空id（编辑、添加使用同一个表单，所以这里需要清除）
            $("#id").val('');
            $('#dlgSystemBank').dialog({title: '添加公司帐号'}).dialog("open");
        }

        //删除记录
        removeRecord = function () {
            var obj = grid.datagrid("getSelected");
            if (obj == null) { utils.showErrMsg("请选择需要删除的行！"); }
            else {
                utils.confirm("您确定要删除吗？", function () {
                    utils.AjaxPost("SystemBank/Delete", { id: obj.id }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            //重刷grid
                            queryGrid();
                        }
                    });
                });
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field:'ck',title: '文本', checkbox:true,},
             { field: 'bankName', title: '开户行', width: '33%' },
             { field: 'bankCard', title: '银行帐号', width: '33%' },
             { field: 'bankUser', title: '开户名', width: '33%' },
            ]],
            toolbar: [{
                text:"添加",
                iconCls: 'icon-add',
                handler: newRecord
            }, '-', {
                text: "编辑",
                iconCls: 'icon-edit',
                handler: editRecord
            }
            , '-', {
                text: "删除",
                iconCls: 'icon-cancel',
                handler: removeRecord
            }

            ],
            url: "SystemBank/GetListPage"
        }, null)

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs) {
                for (name in objs) {
                    if (name == "bankName2") { objs["bankName"] = objs[name]; }
                    else if (name == "bankCard2") { objs["bankCard"] = objs[name]; }
                    else if (name == "bankUser2") { objs["bankUser"] = objs[name]; }
                }
            }
            
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlgSystemBank').dialog("destroy");
        };
    };

    return controller;
});