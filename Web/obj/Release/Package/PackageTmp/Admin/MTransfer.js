
define(['text!MTransfer.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MTransfer, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "转账查询");
        appView.html(MTransfer);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化转账类型下拉框
        var list = cacheList["MTransfer"];
        $("#typeId").empty();
        $("#typeId").append("<option value='0'>--全部--</option>");
        if (list && list.length > 0) {
            for (var i = 0; i < list.length; i++) {
                $("#typeId").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>"); 
            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field: 'typeId', title: '转账类型', width: '150' },
             { field: 'fromUserId', title: '转出账户',width:'100' },
             { field: 'fromUserName', title: '会员姓名', width: '100' },
             { field: 'toUserId', title: '转入账户', width: '130' },
             { field: 'toUserName', title: '会员姓名', width: '100' },
             { field: 'epoints', title: '转账金额', width: '100' },
             { field: 'addTime', title: '转账日期', width: '130' },
            ]],
            url: "MTransfer/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    
                    if (data.rows[i]["typeId"] == "90") data.rows[i]["typeId"] = "TOCC币转自身兑换码";
                    else data.rows[i]["typeId"] = cacheMap["MTransfer"][data.rows[i].typeId];
                }
            }
            return data;
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/MTransfer/ExportTransferExcel";
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});