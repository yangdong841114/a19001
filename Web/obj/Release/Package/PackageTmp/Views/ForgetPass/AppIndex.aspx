﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body>
    <header>
        <div class="">
            <i class="backicon"><a href="/Home/AppIndex"><img src="/Content/APP/User/images/btnback.png" /></a></i>
            <h2>忘記密碼</h2>
        </div>
        <div class="clear"></div>
    </header>
    <div class="entryinfo">
        <dl>
            <dt>賬號</dt>
            <dd>
                <input type="text" class="entrytxt" required="required" placeholder="請輸入您的賬號" id="acoungId" />
                <span class="erase"><i class="fa fa-times-circle-o"></i></span>
            </dd>
        </dl>
        <div class="btnbox">
            <button class="bigbtn" id="nextBtn">下壹步</button>
        </div>
        <div class="original" style="padding-bottom:3rem;"> <a href="/Home/AppIndex">返回登錄</a> </div>
    </div>
    <script type="text/javascript">
        $(function () {

            //文本框右侧删除按钮
            $(".erase").each(function () {
                var dom = $(this);
                dom.bind("click", function () {
                    dom.prev().val("");
                })
            });

            //显示错误消息
            showErrorMsg = function (msg) {
                var msgbox = new $.zui.Messager('提示消息：' + msg, {
                    type: 'danger',
                    icon: 'warning-sign',
                    placement: 'center',
                    parent: 'body',
                    close: true
                });
                msgbox.show();
            }

            //下一步按钮
            $("#nextBtn").on("click", function () {
                if ($("#acoungId").val() == 0) {
                    showErrorMsg("請輸入您的賬號");
                } else {
                    $.ajax({
                        url: "/ForgetPass/CheckUser",
                        type: "POST",
                        data: "id=" + $("#acoungId").val(),
                        success: function (result) {
                            if (result.status == "fail") {
                                showErrorMsg(result.msg);
                            } else {
                                location.href = "/ForgetPass/CheckCode";
                            }
                        }
                    });
                }
            })
        });
    </script>
</body>
</html>

