﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body>
    <header>
        <div class="">
            <h2>&nbsp;&nbsp;&nbsp;&nbsp;網站已關閉</h2>
        </div>
        <div class="clear"></div>
    </header>
    <div class="entryinfo">
        <div style="text-align:center;font-size:1.4rem;padding:5rem 0rem 5rem 0rem;">
            <%:ViewData["msg"]%>
        </div>
    </div>
</body>
</html>