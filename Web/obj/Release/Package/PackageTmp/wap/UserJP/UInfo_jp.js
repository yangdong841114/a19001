
define(['text!UInfo_jp.html', 'jquery'], function (UInfo_jp, $) {

    var controller = function (name) {
        var me = this;
        //设置标题
        $("#title").html("ホームページ");
        appView.html(UInfo_jp);

        $('#footerlink').css('display', 'none');
        $('#shopfooter').css('display', 'block');

        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                if (map.first_userId == "noexit") $('#limyCenterMan').html('<a href="/Home/exitUserLogin">私の</a>');
            }
        });


        var banners;
        var banners_yxhw;
        var banners_zxzx;
        var banners_rmwz;
        var banners_rmsp;
        //点击选项卡切换
        $('.shopheader ul li').click(function () {
            var $this = $(this);
            var $t = $this.index();
			var figureid = "";
            if ($t==0) {
             banners = banners_yxhw;
			 figureid="figureid0";
            }
            if ($t == 1) {
                banners = banners_zxzx;
				figureid="figureid1";
            }
            if ($t == 2) {
                banners = banners_rmwz;
				figureid="figureid2";
            }
            if ($t == 3) {
                banners = banners_rmsp;
				figureid="figureid3";
            }

            if (banners && banners.length > 0) {
                var banHtml = "";
                for (var i = 0; i < banners.length; i++) {
                    banHtml += '<li class="sw-slide"><a href="#UInfoDetail/' + banners[i].id + '"><img src="' + banners[i].imgUrl + '" /></a></li>';
                }
								
				var banjshtml = "<script type='text/javascript'>$('#"+ figureid +"').swipeslider();</script>";
				
                $("#uinfobanner").html("<figure id='"+ figureid +"' class='swipslider' ><ul class='sw-slides' id='slides_yxhw'>" + banHtml + "</ul></figure>");
                $("#banjshtml").html(banjshtml);
            }
            else
                $("#uinfobanner").html("");
            //滚动广告图
            //InitHImgs_yxhw();


            $('.shopheader ul li').removeClass();
            $this.addClass('active');
            var $contentbox = $('.contentboxInfo');
            $contentbox.css('display', 'none');
            $contentbox.eq($t).css('display', 'block');
        });


       
        showInfo=function(id,url)
        {
            utils.AjaxPostNotLoadding("/User/UserWeb/GetModelInfo", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("ポイントの取得に成功しました");
                    if (url.indexOf("biopcc.com") >= 0)
                    {

                    }
                    else
                    window.open(url);
                }
                else {
                    utils.showErrMsg(result.msg);
                    window.open(url);
                }
            });
        }
        



        ////查询参数
        //this.param_zxzx = utils.getPageData();

        //var dropload_zxzx = $('#div_zxzx').dropload({
        //    scrollArea: window,
        //    domDown: { domNoData: '<p class="dropload-noData"></p>' },
        //    loadDownFn: function (me) {
        //        utils.LoadPageData("/User/UserWeb/GetListPageInfozxzx", param_zxzx, me,
        //            function (rows, footers) {
        //                var html = "";
        //                for (var i = 0; i < rows.length; i++) {
        //                    rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

        //                    var dto = rows[i];
        //                    html += '<li class="imgam0">' +
		//	            '<a href="#UInfoDetail/' + dto.id + '">' +
		//		        '<h2>' + dto.title + '</h2>' +
		//		        '<div class="newsimg"></div>' +
		//		        '<p>' + dto.infoJj + '</p>' +
		//		        '</a>' +
		//	            '<div class="aider">' +
		//		        '<span class="source">' + utils.changeDateFormat(dto.addTime) + '</span>' +
		//		        '<div class="interactive">' +
		//			    '<span class="reading">' + dto.llcount + '</span>' +
		//			    '<span class="comment">' + dto.plcount + '</span>' +
		//			    '<span class="great">' + dto.dzcount + '</span>' +
		//		        '</div>' +
		//		        '<div class="clear"></div>' +
		//	            '</div>' +
		//	            '<div class="clear"></div>' +
		//                '</li>';;
        //                }
        //                $("#zxzxList").append(html);
        //            }, function () {
        //                $("#zxzxList").append('<p class="dropload-noData">暫無數據</p>');
        //            });
        //    }
        //});
        ////查询方法
        //searchMethod_zxzx = function () {
        //    param_zxzx.page = 1;
        //    $("#zxzxList").empty();
        //    dropload_zxzx.unlock();
        //    dropload_zxzx.noData(false);
        //    dropload_zxzx.resetload();
        //}
        


        utils.AjaxPostNotLoadding("/User/UserWeb/UInfo", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                if (map.first_userId == "noexit")
                    {
                    $("#UInfofb").bind('click', function () {location.href = "/Home/exitUserLogin";})
                    }
                //商品
                var ListInfo = map.ListInfo;
                if (ListInfo && ListInfo.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < ListInfo.length; i++) {
                      
                        banHtml += '<li><a href="#ProductDetail/' + ListInfo[i].id + '">' +
                                        '<p class="shopimg"><a href="#ProductDetail/' + ListInfo[i].id + '"><img  src="' + ListInfo[i].imgUrl + '"   alt=""></a></p>' +
                                        '<div class="shoppic">' +
					                   '<h2>' + ListInfo[i].productName + '</h2>' +
                	                   '<p><span>＄' + ListInfo[i].price + '&nbsp;&nbsp;&nbsp;</span></p>' +
					                   '<div class="buyshop">' +
						               '<div class="percentage">' +
							           '<span>残り ' + ListInfo[i].num + ' 件</span>' +
							           '<p style="width: 46%"></p>' +
						               '</div>' +
						               '<a href="#ProductDetail/' + ListInfo[i].id + '" class="buyitnow" >即刻買いあさる</a>' +
						               '<div class="clear"></div>' +
					                   '</div>' +
				                       '</div>' +
				                       '<div class="clear"></div>' +
                                        '</a></li>';
                    }
                    $("#product").html(banHtml);
                }
              
                //优选好物-广告图
                banners_yxhw = map.banners_yxhw;
                banners_zxzx = map.banners_zxzx;
                banners_rmwz = map.banners_rmwz;
                banners_rmsp = map.banners_rmsp;
				
                banners = map.banners_yxhw;
                if (banners && banners.length > 0) {
       
					var banHtml = "";
					for (var i = 0; i < banners.length; i++) {
                        banHtml += '<li class="sw-slide"><a href=\'javascript:showInfo(' + banners[i].id + ',"' + banners[i].linkUrl + '")\' ><img src="' + banners[i].imgUrl + '" /></a></li>';
                    }
					
					var banjshtml = "<script type='text/javascript'>$('#figureid0').swipeslider();</script>";
				
					$("#uinfobanner").html("<figure id='figureid0' class='swipslider'  ><ul class='sw-slides' id='slides_yxhw'>" + banHtml + "</ul></figure>");
                $("#banjshtml").html(banjshtml);
                        

                }
                else
                    $("#uinfobanner").html("");
                ////滚动广告图
                //InitHImgs_yxhw();
                //最新资讯-文章
                var zxzxList = map.zxzxList;
                if (zxzxList && zxzxList.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < zxzxList.length; i++) {
                        banHtml += '<li class="imgam0">' +
			            '<a href="#UInfoDetail/' + zxzxList[i].id + '">' +
				        '<h2>' + zxzxList[i].title + '</h2>' +
				        '<div class="newsimg"></div>' +
				        '<p>' + zxzxList[i].infoJj + '</p>' +
				        '</a>' +
			            '<div class="aider">' +
				        '<span class="source">' + utils.changeDateFormat(zxzxList[i].addTime) + '</span>' +
				        '<div class="interactive">' +
					    '<span class="reading">' + zxzxList[i].llcount + '</span>' +
					    '<span class="comment">' + zxzxList[i].plcount + '</span>' +
					    '<span class="great">' + zxzxList[i].dzcount + '</span>' +
				        '</div>' +
				        '<div class="clear"></div>' +
			            '</div>' +
			            '<div class="clear"></div>' +
		                '</li>';
                    }
                    $("#zxzxList").html(banHtml);
                }
               
                //最新资讯-文章
                var rmwzList = map.rmwzList;
                if (rmwzList && rmwzList.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < rmwzList.length; i++) {
                        banHtml += '<li class="imgam0">' +
			            '<a href="#UInfoDetail/' + rmwzList[i].id + '">' +
				        '<h2>' + rmwzList[i].title + '</h2>' +
				        '<div class="newsimg"></div>' +
				        '<p>' + rmwzList[i].infoJj + '</p>' +
				        '</a>' +
			            '<div class="aider">' +
				        '<span class="source">' + utils.changeDateFormat(rmwzList[i].addTime) + '</span>' +
				        '<div class="interactive">' +
					    '<span class="reading">' + rmwzList[i].llcount + '</span>' +
					    '<span class="comment">' + rmwzList[i].plcount + '</span>' +
					    '<span class="great">' + rmwzList[i].dzcount + '</span>' +
				        '</div>' +
				        '<div class="clear"></div>' +
			            '</div>' +
			            '<div class="clear"></div>' +
		                '</li>';
                    }
                    $("#rmwzList").html(banHtml);
                }
               
                //最新资讯-文章
                var rmspList = map.rmspList;
                if (rmspList && rmspList.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < rmspList.length; i++) {
                       
                        var show_video = '<video controls="" autoplay="" name="media"><source src="' + rmspList[i].linkUrl + '" type="video/mp4"></video>';
                        if (show_video.indexOf("player.youku") >= 0 && show_video.indexOf(".swf") <= 0) show_video = ' <iframe height=300 width="100%" src="' + rmspList[i].linkUrl + '" frameborder=0 "allowfullscreen"></iframe>';
                        if (show_video.indexOf("youtube.com") >= 0) show_video = '<iframe  width="100%" height="300" src="' + rmspList[i].linkUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        banHtml += '<li>' +
                       '' +
                       '<h2>' + rmspList[i].title + '</h2>' +
                      show_video +
                     
                       '<a href="#UInfoDetail/' + rmspList[i].id + '">' +
                       '<div class="aider">' +
                       '<span class="source">' + utils.changeDateFormat(rmspList[i].addTime) + '</span>' +
                       '<div class="interactive">' +
                       '<span class="reading">' + rmspList[i].llcount + '</span>' +
                       '<span class="comment">' + rmspList[i].plcount + '</span>' +
                       '<span class="great">' + rmspList[i].dzcount + '</span>' +
                       '</div>' +
                       '<div class="clear"></div>' +
                       '</div>' +
                       '<div class="clear"></div></a>' +
                       '</li>';
                    }
                    $("#rmspList").html(banHtml);

                    
                }
               
               


        } else {
                utils.showErrMsg(result.msg);
                }
           });
       

       
       
        controller.onRouteChange = function () {
         
        };
    };

    return controller;
});