
define(['text!NotJMS.html', 'jquery'], function (NotJMS, $) {

    var controller = function (prePage) {
        //设置标题
        $("#title").html("商品の管理");
        appView.html(NotJMS);

        controller.onRouteChange = function () {
        };
    };

    return controller;
});