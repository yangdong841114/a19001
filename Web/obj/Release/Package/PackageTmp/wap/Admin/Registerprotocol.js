
define(['text!Registerprotocol.html', 'jquery'], function (Registerprotocol, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("注册协议");

        utils.AjaxPostNotLoadding("/Admin/Registerprotocol/InitView", {}, function (result) {
            if (result.status == "success") {
                appView.html(Registerprotocol);

                var editor = new Quill("#editor", {
                    modules: {
                        toolbar: utils.getEditorToolbar()
                    },
                    theme: 'snow'
                });
                var cont = "";
                if (result.result) {
                    utils.setEditorHtml(editor, result.result.content)
                } 

                $("#saveBtn").on("click", function () {
                    if (editor.getText() == 0) {
                        utils.showErrMsg("请输入内容");
                    } else {
                        utils.AjaxPost("/Admin/Registerprotocol/SaveByUeditor", { content: utils.getEditorHtml(editor) }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("保存成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                })
            }
        });

        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});