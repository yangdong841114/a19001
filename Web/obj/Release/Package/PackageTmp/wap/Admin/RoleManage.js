
define(['text!RoleManage.html', 'jquery', 'ztree'], function (RoleManage, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("角色管理")
        appView.html(RoleManage);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })


        var editRoleList = {};
        var roleTypeList = [{ id: 0, value: "全部" }, { id: 1, value: "前台角色" }, { id: 2, value: "后台角色" }]
        var roleTypeList2 = [{ id: 1, value: "前台角色" }, { id: 2, value: "后台角色" }]
        var roleFormHtml = '<input type="hidden" id="id" name="id" /><input type="hidden" id="roleType" name="roleType" />' +
            '<dl><dt>角色类型</dt><dd><input type="text" required="required" class="entrytxt" id="roleTypeName" placeholder="请选择" readonly /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>角色名称</dt><dd><input type="text" required="required" class="entrytxt" id="roleName" placeholder="请输入角色名称" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>描述</dt><dd><input type="text" required="required" class="entrytxt" id="remark" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    if (prev[0].id == "roleTypeName") {
                        $("#roleType").val("");
                    }
                    prev.val("");
                });
            });
        }

        //角色类型查询框
        utils.InitMobileSelect('#roleType2Name', '角色类型', roleTypeList, null, [0], null, function (indexArr, data) {
            $("#roleType2Name").val(data[0].value);
            $("#roleType2").val(data[0].id);
        });

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //添加角色按钮
        $("#addRoleBtn").bind("click", function () {
            $("#prompTitle").html("添加角色");
            $("#prompCont").empty();
            $("#prompCont").html(roleFormHtml);
            clearData();
            //角色类型选择框
            utils.InitMobileSelect('#roleTypeName', '角色类型', roleTypeList2, null, [0], null, function (indexArr, data) {
                $("#roleTypeName").val(data[0].value);
                $("#roleType").val(data[0].id);
            });
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () { saveOrUpdateRole(); });
            utils.showOrHiddenPromp();
        });

        //编辑角色按钮
        editRole = function (id) {
            $("#prompTitle").html("添加角色");
            $("#prompCont").empty();
            $("#prompCont").html(roleFormHtml);
            var dto = editRoleList[id];
            $("#id").val(dto.id);
            $("#roleType").val(dto.roleType);
            $("#roleName").val(dto.roleName);
            $("#roleTypeName").val(dto.roleTypeName);
            $("#remark").val(dto.remark);
            clearData();
            //角色类型选择框
            utils.InitMobileSelect('#roleTypeName', '角色类型', roleTypeList2, null, [0], null, function (indexArr, data) {
                $("#roleTypeName").val(data[0].value);
                $("#roleType").val(data[0].id);
            });
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () { saveOrUpdateRole(); });
            utils.showOrHiddenPromp();
        }

        //保存或编辑角色
        saveOrUpdateRole = function () {
            if ($("#roleType").val() == 0) {
                utils.showErrMsg("请选择角色类型");
            } else if ($("#roleName").val() == 0) {
                utils.showErrMsg("请输入角色名称");
            } else {
                var data = { roleType: $("#roleType").val(), roleName: $("#roleName").val(), id: $("#id").val(), remark: $("#remark").val() };
                utils.AjaxPost("/Admin/RoleManage/SaveOrUpdate", data, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            }
        }

        //删除角色
        deleteRole = function (id) {
            $("#prompTitle").html("确定删除该角色吗？");
            $("#prompCont").empty();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/RoleManage/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.msg == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //跳转分配会员
        toAddMemberRole = function (roleId) {
            var dto = editRoleList[roleId];
            location.href = "#RoleAddMemberList/" + (dto.roleType == 1 ? 0 : 1) + "" + roleId;
        }

        //跳转到已分配会员页面
        toRoleMember = function (roleId) {
            location.href = "#RoleMemberList/"+roleId;
        }

        //查询参数
        this.param = utils.getPageData();
        //this.memberParam = utils.getPageData();

        //角色列表初始化
        var dropload = $('#RoleManagedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/RoleManage/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["roleTypeName"] = rows[i].roleType == 1 ? "前台角色" : "后台角色";

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.roleName + '</time><span class="sum">' + dto.roleTypeName + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">' +
                            '<li><button class="sdelbtn" onclick=\'editRole(' + dto.id + ')\'>编辑</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'deleteRole(' + dto.id + ')\'>删除</button></li>' +
                            '<li><button class="smallbtn" onclick="toAddMemberRole(' + dto.id + ')">分配会员</button></li>' +
                            '</ul><ul class="tga3"><li style="padding-top:1rem;"><button class="smallbtn" onclick="openSelectMenu(\'' + dto.id + '\')">分配菜单</button></li>' +
                            '<li style="padding-top:1rem;"><button class="seditbtn" onclick="toRoleMember(' + dto.id + ')">已分配会员</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>角色ID</dt><dd>' + dto.id + '</dd></dl><dl><dt>角色名称</dt><dd>' + dto.roleName + '</dd></dl>' +
                            '<dl><dt>角色类型</dt><dd>' + dto.roleTypeName + '</dd></dl><dl><dt>描述</dt><dd>' + dto.remark + '</dd></dl>' +
                            '</div></li>';
                            editRoleList[dto.id] = dto;
                        }
                        $("#RoleManageitemList").append(html);
                    }, function () {
                        $("#RoleManageitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#RoleManageitemList").empty();
            param["roleName"] = $("#roleName2").val();
            param["roleType"] = $("#roleType2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        ///***************************************** 菜单tree START **********************************************************************************/

        //ztree设置
        var treesetting = {
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id", 	              // id编号字段名
                    pIdKey: "parentResourceId",   // 父id编号字段名
                    rootPId: null
                },
                key: {
                    name: "resourceName"    //显示名称字段名
                }
            },
            check: {
                autoCheckTrigger: true,
                enable: true,
                chkStyle: "checkbox"
            },
            view: {
                showLine: false,            //不显示连接线
                dblClickExpand: false       //禁用双击展开
            },
            callback: {
                onClick: function (e, treeId, node) {
                    resourceTree.checkNode(node, !node.checked, true);
                    //if (node.isParent) {
                    //    resourceTree.expandNode(node);
                    //}
                }
            }
        };

        var beforeMenu = null, afterMenu = null; //前端菜单、后台菜单

        var resourceTree = undefined; //ztree对象

        //打开分配菜单
        openSelectMenu = function (roleId) {
            var dto = editRoleList[roleId];
            //加载角色已有菜单
            utils.AjaxPostNotLoadding("/Admin/RoleManage/GetCheckedMenus", { roleId: roleId }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg("加载角色已分配菜单失败，" + result.msg);
                } else {
                    var menus = dto.roleType == 1 ? beforeMenu : afterMenu;
                    $("#menuRoleId").val(roleId);
                    $("#title").html("分配菜单");
                    $("#mainDiv").css("display", "none");
                    $("#addMenuDiv").css("display", "block")
                    //生成树
                    reInitTree(menus, result.list);

                    //关闭按钮
                    $("#addMenuBackBtn").unbind();
                    $("#addMenuBackBtn").bind("click", function () {
                        $("#title").html("角色管理");
                        $("#addMenuDiv").css("display", "none");
                        $("#mainDiv").css("display", "block");
                    })

                    //分配菜单保存按钮
                    $("#addMenuBtn").unbind();
                    $("#addMenuBtn").bind("click", function () {
                        var nodes = resourceTree.getCheckedNodes(true);
                        if (!nodes || nodes.length == 0) {
                            utils.showErrMsg("您还未勾选任何菜单，不能保存");
                        } else {
                            var roleId = $("#menuRoleId").val();
                            var data = {};
                            for (var i = 0; i < nodes.length; i++) {
                                data["list[" + i + "].id"] = $("#menuRoleId").val();
                                data["list[" + i + "].resourceId"] = nodes[i].id;
                            }
                            utils.AjaxPost("/Admin/RoleManage/SaveRoleResorce", data, function (result) {
                                if (result.status == "success") {
                                    $("#title").html("角色管理");
                                    $("#addMenuDiv").css("display", "none");
                                    $("#mainDiv").css("display", "block");
                                    utils.showSuccessMsg(result.msg);
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            });
                        }
                        return false;
                    })

                    ;
                }
            });
        }

        //重新生成树
        reInitTree = function (menus, checkedMenus) {
            //先销毁树
            if (resourceTree) {
                resourceTree.destroy();
            }
            //var newMenus = $.map(menus, function (n) { return n; });
            var newMenus = $.extend(true, [], menus);
            //选中已分配的菜单
            if (checkedMenus && checkedMenus.length > 0) {
                for (var i = 0; i < checkedMenus.length; i++) {
                    var checkNode = checkedMenus[i];
                    for (var j = 0; j < newMenus.length; j++) {
                        var node = newMenus[j];
                        if (checkNode.id == node.id) {
                            newMenus[j]["checked"] = true;
                            break;
                        }
                    }
                }
            }
            //生成树
            resourceTree = $.fn.zTree.init($("#roleMenuTree"), treesetting, newMenus);
            //查找根节点并自动展开
            var rootNodes = resourceTree.getNodesByFilter(function (node) {
                if (node.parentResourceId == null || node.parentResourceId == 0) {
                    return true;
                }
            });
            if (rootNodes && rootNodes.length > 0) {
                for (var i = 0; i < rootNodes.length; i++) {
                    resourceTree.expandNode(rootNodes[i], true, false, true);
                }
            }
        }

        //加载系统前台菜单和后台菜单
        utils.AjaxPostNotLoadding("/Admin/RoleManage/GetMenuMap", null, function (result) {
            if (result) {
                beforeMenu = result.before;
                afterMenu = result.after;
            }
        });

        ///***************************************** 菜单tree END **********************************************************************************/

        controller.onRouteChange = function () {
        };
    };

    return controller;
});