
define(['text!main.html', 'jquery'], function (main, $) {

    var controller = function (name) {
        ////设置标题
        //$("#center").panel("setTitle","会员注册");
        appView.html(main);

        //退出按钮
        $("#exitBtn").on('click', function () {
            utils.showOrHiddenPromp();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                location.href = "/AppManageLogin.html";
            });
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });


        utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetNotic", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                if (map) {
                    
                    $("#fastMenu").children().each(function () {
                        var dom = $(this);
                        var dataUrl = dom.attr("dataurl");
                        if (dataUrl && dataUrl != 0) {
                            dataUrl = dataUrl.substring(1);
                            if (menuPass[dataUrl]!= undefined) {
                                dom.css("display", "block");
                            }
                        }
                    })

                    $("#waitOperateDiv").children().each(function () {
                        var dom = $(this);
                        var dataUrl = dom.attr("dataurl");
                        if (dataUrl && dataUrl != 0) {
                            dataUrl = dataUrl.substring(1);
                            if (menuPass[dataUrl] != undefined) {
                                dom.css("display", "block");
                            }
                        }
                    })

                    //待开通列表
                    var memberList = map["Member"];
                    var memberHtml = '<li><span class="name">暂无</span></li>';
                    if (memberList && memberList.length > 0) {
                        memberHtml = "";
                        for (var i = 0; i < memberList.length; i++) {
                            var dto = memberList[i];
                            memberHtml += '<li><span class="name">' + dto.userId + '&nbsp;&nbsp;&nbsp;&nbsp;</span>注册金额：<span class="price">￥' +
                                dto.money + '&nbsp;&nbsp;&nbsp;&nbsp;</span><time>' + utils.changeDateFormat(dto.addTime) + '</time></li>';
                        }
                    }
                    $("#MemberUl").html(memberHtml);

                    //提现申请列表
                    var TakeCashList = map["TakeCash"];
                    var TakeCashHtml = '<li><span class="name">暂无</span>';
                    if (TakeCashList && TakeCashList.length > 0) {
                        TakeCashHtml = "";
                        for (var i = 0; i < TakeCashList.length; i++) {
                            var dto = TakeCashList[i];
                            TakeCashHtml += '<li><span class="name">' + dto.userId + '&nbsp;&nbsp;&nbsp;&nbsp;</span>提现金额：<span class="price">￥' +
                                dto.money + '&nbsp;&nbsp;&nbsp;&nbsp;</span><time>' + utils.changeDateFormat(dto.addTime) + '</time></li>';
                        }
                    }
                    $("#TakeCashUl").html(TakeCashHtml);

                    //待开通报单中心列表
                    var ShopMemberList = map["Shop"];
                    var ShopMemberHtml = '<li><span class="name">暂无</span>';
                    if (ShopMemberList && ShopMemberList.length > 0) {
                        ShopMemberHtml = "";
                        for (var i = 0; i < ShopMemberList.length; i++) {
                            var dto = ShopMemberList[i];
                            ShopMemberHtml += '<li><span class="name">' + dto.userId + '&nbsp;&nbsp;&nbsp;&nbsp;</span>申请金额：<span class="price">￥' +
                                dto.money + '&nbsp;&nbsp;&nbsp;&nbsp;</span><time>' + utils.changeDateFormat(dto.addTime) + '</time></li>';
                        }
                    }
                    $("#shopMemberUl").html(ShopMemberHtml);

                    //充值申请列表
                    var waitChargeList = map["Charge"];
                    var waitChargeHtml = '<li><span class="name">暂无</span>';
                    if (waitChargeList && waitChargeList.length > 0) {
                        waitChargeHtml = "";
                        for (var i = 0; i < waitChargeList.length; i++) {
                            var dto = waitChargeList[i];
                            waitChargeHtml += '<li><span class="name">' + dto.userId + '&nbsp;&nbsp;&nbsp;&nbsp;</span>充值金额：<span class="price">￥' +
                                dto.money + '&nbsp;&nbsp;&nbsp;&nbsp;</span><time>' + utils.changeDateFormat(dto.addTime) + '</time></li>';
                        }
                    }
                    $("#waitChargeUl").html(waitChargeHtml);

                    var emailList = map["Email"];
                    var vo = emailList[0];
                    if (vo.count == 0) {
                        $("#emailCount").empty();
                        $("#emailCount").css("display", "none");
                    } else {
                        $("#emailCount").html(vo.count);
                        $("#emailCount").css("display", "block");
                    }

                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});