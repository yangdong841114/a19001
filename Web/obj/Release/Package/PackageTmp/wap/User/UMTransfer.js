
define(['text!UMTransfer.html', 'jquery'], function (UMTransfer, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期選擇框
                    utils.initCalendar(["startTime", "endTime"]);
                    //綁定展開搜索更多
                    utils.bindSearchmoreClick();

                    //清空查詢條件按鈕
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })
                    //查詢條件轉賬類型選擇框
                    var transfer = $.extend(true, [], cacheList["MTransfer"]);
                    transfer.splice(0, 0, { id: 0, name: '全部' });
                    utils.InitMobileSelect('#typeName2', '轉賬類型', transfer, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                        $("#typeName2").val(data[0].name);
                        $("#typeId2").val(data[0].id);
                    })

                    //查詢按鈕
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加載數據
                searchMethod();
            }
        }

        //設置標題
        $("#title").html("賬戶轉賬");
        appView.html(UMTransfer);


        var dto = null;

        //轉賬明細
        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UMTransferdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UMTransfer/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["typeId"] = cacheMap["MTransfer"][rows[i].typeId];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.typeId + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>轉賬日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>轉賬類型</dt><dd>' + dto.typeId + '</dd></dl><dl><dt>轉出賬戶</dt><dd>' + dto.fromUserId + '</dd></dl>' +
                                  '<dl><dt>會員姓名</dt><dd>' + dto.fromUserName + '</dd></dl><dl><dt>轉入賬戶</dt><dd>' + dto.toUserId + '</dd></dl>' +
                                  '<dl><dt>會員姓名</dt><dd>' + dto.toUserName + '</dd></dl><dl><dt>轉賬金額</dt><dd>' + dto.epoints + '</dd></dl>' +
                                  '</div></li>';
                        }
                        $("#UMTransferitemList").append(html);
                    }, function () {
                        $("#UMTransferitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UMTransferitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["typeId"] = $("#typeId2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //設置表單默認數據
        setDefaultValue = function (dto) {
            $("#agentDhm").html(dto.account.agentDhm);
            $("#agentTod").html(dto.account.agentTod);
            $("#agentTocc").html(dto.account.agentTocc);
            $("#toUserId").val("");
            $("#epoints").val("");
            $("#typeName").val("");
            $("#typeId").val("");
            $("#toUserName").empty();
            $("#userId").val(dto.userId);
            $("#userName").val(dto.userName);
        }

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UMTransfer/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMTransfer);
                dto = result.result;
                var MTransferList = cacheList["MTransfer"]; //[{ id: '91', name: "兑换码转他人兑换码" }, { id: '60', name: "TOD币转自身兑换码" }, { id: '61', name: "TOD币转自身TOCC币" }, { id: '92', name: "TOD币转自身MOBY码" }];
                //轉賬類型選擇框
                utils.InitMobileSelect('#typeName', '轉賬類型', MTransferList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#typeName").val(data[0].name);
                    $("#typeId").val(data[0].id);
                    if ($("#typeId").val() == "60" || $("#typeId").val() == "61" || $("#typeId").val() == "90" || $("#typeId").val() == "92" || $("#typeId").val() == "95")
                    {
                        $("#toUserId").val($("#userId").val());
                        $("#toUserName").html($("#userName").val());
                    }
                    else
                    {
                        $("#toUserId").val("");
                        $("#toUserName").html("");
                    }
                    })

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "typeName") {
                            $("#typeId").val("");
                        } else if (prev[0].id == "toUserId") {
                            $("#toUserName").empty();
                        }
                        dom.prev().val("");
                    });
                });

                //初始默認值
                setDefaultValue(dto);

                //會員編號離開焦點事件
                $("#toUserId").on("blur", function () {
                    $("#toUserName").empty();
                    var current = $("#toUserId");
                    var parent = current.parent();
                    if (current.val() != 0) {
                        utils.AjaxPostNotLoadding("/User/UMTransfer/GetUserName", { userId: current.val() }, function (result) {
                            if (result.status == "fail" || result.msg == "會員不存在") {
                                $("#toUserName").empty()
                            } else {
                                $("#toUserName").html(result.msg);
                            }
                        });
                    }
                })

                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#typeId").val();
                    var current = $("#typeId");
                    var parent = current.parent();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (val == 0) {
                        utils.showErrMsg("請選擇轉賬類型");
                    } else if (!g.test($("#epoints").val())) {
                        utils.showErrMsg("轉賬金額格式不正確");
                    } else if (val == 91 && $("#toUserId").val() == 0) {
                        utils.showErrMsg("請錄入轉賬會員");
                    } else {
                        $("#zzlx").html($("#typeName").val());
                        $("#zzje").html($("#epoints").val());
                        $("#hybh").html($("#toUserId").val());
                        utils.showOrHiddenPromp();
                    }
                })

                //確認按鈕
                $("#sureBtn").on('click', function () {
                    var data = { typeId: $("#typeId").val(), epoints: $("#epoints").val(), toUserId: $("#toUserId").val() };
                    utils.AjaxPost("/User/UMTransfer/SaveMTransfer", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                        }
                    });
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});