
define(['text!UCharge_jp.html', 'jquery'], function (UCharge, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期選擇框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查詢條件按鈕
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查詢按鈕
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加載數據
                searchMethod();
            }
        }

        $("#title").html("口座チャージ");
        appView.html(UCharge);

        var accounttypeList = [{ id: '36', value: "兌換コード" }, { id: '37', value: "TOD貨幣" }];
        //初始化下拉框
        utils.InitMobileSelect('#accounttypeValue', 'アカウントのタイプ', accounttypeList, null, [0], null, function (indexArr, data) {
            $("#accounttypeValue").val(data[0].value);
            $("#accounttypeId").val(data[0].id);
        });

        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UChargeDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UCharge/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = rows[i].ispay == 1 ? "審査待ち" : "通過しました";
                            rows[i].accounttypeId = cacheMap["JournalClass"][rows[i].accounttypeId];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">+' + dto.accounttypeId + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>チャージ日付</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>かわせ銀行</dt><dd>' + dto.fromBank + '</dd></dl><dl><dt>チャージ金額</dt><dd>' + dto.epoints + '</dd></dl>' +
                                  '<dl><dt>送金時間</dt><dd>' + dto.bankTime + '</dd></dl><dl><dt>' +
                                  '送金証明書</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="送金証明書" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                  '<dl><dt>送金銀行</dt><dd>' + dto.toBank + '</dd></dl><dl><dt>銀行カード番号</dt><dd>' + dto.bankCard + '</dd></dl>' +
                                  '<dl><dt>口座を作る</dt><dd>' + dto.bankUser + '</dd></dl><dl><dt>状態</dt><dd>' + dto.status + '</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UChargeItemList").append(html);

                        //初始化圖片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UChargeItemList").append('<p class="dropload-noData">データがありません</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UChargeItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //設置表單默認數據
        setDefaultValue = function () {
            $("#sysBankName").val("");
            $("#sysBankId").val("0");
            $("#toBankCard").empty();
            $("#toBankUser").empty();
            $("#fromBank").val("");
            $("#epoints").val("");
            $("#bankTime").val("");
            $("#file").val("");
        }

        //充值金額離開焦點
        $("#epoints").bind("blur", function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var dom = $(this);
            if (g.test(dom.val())) {
                var intVal = parseInt(dom.val());
                var val = intVal + parseFloat((Math.random()).toFixed(2));
                dom.val(val);
            }
        })

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UCharge/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var list = result.list;

                //初始化匯入銀行下拉框
                var bankData = [];
                if (list && list.length > 0) {
                    for (var i = 0; i < list.length; i++) {
                        bankData.push({ id: list[i].id + "|" + list[i].bankCard + "|" + list[i].bankUser, value: list[i].bankName });
                    }
                }
                //匯入銀行選擇框
                utils.InitMobileSelect('#sysBankName', '送金銀行', bankData, null, [0], null, function (indexArr, data) {
                    var val = data[0].id;
                    if (val != 0) {
                        var v = val.split("|");
                        $("#toBankCard").html(v[1]);
                        $("#toBankUser").html(v[2]);
                    } else {
                        $("#toBankCard").empty();
                        $("#toBankUser").empty();
                    }
                    $("#sysBankId").val(val);
                    $("#sysBankName").val(data[0].value);
                })

                //初始化匯款時間
                utils.initCalendar(["bankTime"]);

                //初始默認值
                setDefaultValue();

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "sysBankName") {
                            $("#sysBankId").val("");
                            $("#toBankCard").empty();
                            $("#toBankUser").empty();
                        }

                        dom.prev().val("");
                    });
                });


                //*****************************************************銀行匯款 start **********************************************************//

                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();

                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#sysBankId").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    var isChecked = true;
                    if (val == 0) {
                        utils.showErrMsg("送金銀行を選択してください");
                    } else if ($("#fromBank").val() == 0) {
                        utils.showErrMsg("送金銀行に入力してください");
                    } else if (!g.test($("#epoints").val())) {
                        utils.showErrMsg("チャージ金額のフォーマットが正しくないです");
                    } else if ($("#bankTime").val() == 0) {
                        utils.showErrMsg("送金の時間を選んでください");
                    } else if ($("#file").val() == 0) {
                        utils.showErrMsg("送金証明書をアップロードしてください");
                    } else {
                        $("#czhryh").html($("#sysBankName").val());
                        $("#czkh").html($("#toBankCard").html());
                        $("#czkhm").html($("#toBankUser").html());
                        $("#czje").html($("#epoints").val());
                        utils.showOrHiddenPromp();
                    }
                });

                //確認保存
                $("#sureBtn").bind("click", function () {
                    var formdata = new FormData();
                    var bankId = $("#sysBankId").val().split("|")[0];
                    formdata.append("sysBankId", bankId);
                    formdata.append("fromBank", $("#fromBank").val());
                    formdata.append("epoints", $("#epoints").val());
                    formdata.append("bankTime", $("#bankTime").val());
                    formdata.append("accounttypeId", $("#accounttypeId").val());
                    formdata.append("img", $("#file")[0].files[0]);
                    utils.AjaxPostForFormData("/User/UCharge/SaveCharge", formdata, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作が成功しました！");
                            dto = result.result;
                            setDefaultValue(dto);
                            //grid.datagrid("reload");
                        }
                    });
                });

                //*****************************************************銀行匯款 end **********************************************************//
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});