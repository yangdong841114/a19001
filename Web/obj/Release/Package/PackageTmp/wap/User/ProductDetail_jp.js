
define(['text!ProductDetail_jp.html', 'jquery'], function (ProductDetail, $) {

    var controller = function (id) {

        var g = /^\d+(\.{0,1}\d+){0,1}$/;

        //设置标题
        $("#title").html("商品の詳細");
        if (!id || id == 0) {
            utils.showErrMsg("読み込みに失敗しました商品が見つかりませんでした");
        } else {
            utils.AjaxPostNotLoadding("/User/UProductShop/GetModel", { id: id }, function (result) {
                appView.html(ProductDetail);
                if (result.status == "success") {
                    var dto = result.result;
                    var btnZxHref = $("#btnZx").attr("href");
                    btnZxHref += "&ProductId=" + id;
                    $("#btnZx").attr("href", btnZxHref);
                    $("#productCode").html(dto.productCode);
                    if (dto.id =="0") location.href = "/Home/exitUserLogin";
                    $("#price").html(dto.price);
                    $("#num").html(dto.num);
                    $("#fxprice").html(dto.fxPrice);
                    $("#total").html(" "+dto.price+" ");
                    $("#productName").html(dto.productName);
                    $("#imgUrl").attr("src", dto.imgUrl);
                    $("#productId").val(dto.id);
                    $("#imgUrl").attr("data-image", dto.imgUrl);
                    if (dto.cont == ""||dto.cont ==null )
                        $("#cont").html("詳しい紹介はまだありません");
                    else
                        $("#cont").html(dto.cont);
                  
                    var count = parseInt(result.other);
                    $("#cartCount").html(count);

                    //减少按钮
                    $("#subBtn").bind("click", function () {
                        var val = parseInt($("#buyNum").val());
                        if (val > 1) {
                            $("#buyNum").val(val - 1)
                            $("#total").html((val - 1) * dto.price);
                        }
                    });

                    //增加按钮
                    $("#addBtn").bind("click", function () {
                        var val = parseInt($("#buyNum").val());
                        $("#buyNum").val(val + 1)
                        $("#total").html((val + 1) * dto.price);
                    });


                    $("#buyNum").on("blur", function () {
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        if (g.test($("#buyNum").val())) {
                            $("#total").html($("#buyNum").val() * dto.price);
                        }
                    })

                    //放入购物车
                    $("#saveBtn").on('click', function () {
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        if (!g.test($("#buyNum").val())) {
                            utils.showErrMsg("ショッピング数量フォーマットが間違っています");
                        } else {
                            var data = { productId: $("#productId").val(), num: $("#buyNum").val() };
                            utils.AjaxPost("/User/UShoppingCart/Save", data, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("加入成功！");
                                    count = parseInt(result.other);
                                    $("#cartCount").html(count);
                                }
                            });
                        }
                    });

                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }


        controller.onRouteChange = function () {
        };
    };

    return controller;
});