
define(['text!UInfoDetail.html', 'jquery'], function (UInfoDetail, $) {

    var controller = function (id) {

        //设置标题
        $("#title").html("詳情");
        appView.html(UInfoDetail);

        if (!id || id == 0) {
            utils.showErrMsg("加載失敗！未找到詳情");
        } else {
            utils.AjaxPostNotLoadding("/User/UserWeb/GetModelInfo", { id: id }, function (result) {
              
                if (result.status == "success") {
                    $("#infoId").val(id);
                    var map = result.map;
                    if (map.first_userId == "noexit") location.href = "/Home/exitUserLogin";
                    //商品
                    var dtoInfo = map.dtoInfo;
                    $("#dzcount").val(dtoInfo.dzcount);
                    var html = ' <h1>' + dtoInfo.title + '</h1> ' +
                               '<h5>发布者：' + dtoInfo.userName + '　　发布时间：<time>' + utils.changeDateFormat(dtoInfo.addTime) + '</time></h5>' +
                               dtoInfo.infocontent;
                    if (dtoInfo.imgUrl != "" && dtoInfo.imgUrl != null&&dtoInfo.type=="gg") html += '<img src="' + dtoInfo.imgUrl + '" />';

                    if (dtoInfo.linkUrl != "" && dtoInfo.linkUrl != null && dtoInfo.type == "sp")
                    {
                        if (dtoInfo.linkUrl.indexOf("player.youku") >= 0 && dtoInfo.linkUrl.indexOf(".swf") <= 0) html += ' <iframe height=300 width="100%" src="' + dtoInfo.linkUrl + '" frameborder=0 "allowfullscreen"></iframe>';
                        else if (dtoInfo.linkUrl.indexOf("youtube.com") >= 0) html += '<iframe  width="100%" height="300" src="' + dtoInfo.linkUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        else
                       {
                         html += '<video controls="" autoplay="" name="media"><source src="' + dtoInfo.linkUrl + '" type="video/mp4"></video>';
                        }
                    }
                    $("#article").html(html);
                    //alert(html);
                    html = '<span class="source">' + utils.changeDateFormat(dtoInfo.addTime) + '</span>' +
                       '<div class="interactive">' +
                       '<span class="reading">' + dtoInfo.llcount + '</span>' +
                       '<span class="comment" id="comment">' + dtoInfo.plcount + '</span>' +
                       '<span class="great" id="great">' + dtoInfo.dzcount + '</span>' +
                       '</div>' +
                        '<div class="clear"></div>';
                       $("#aider").html(html);


                      //评论
                       var infoplList = map.infoplList;
                       if (infoplList && infoplList.length > 0) {
                           var banHtml = "";
                           for (var i = 0; i < infoplList.length; i++) {
                               banHtml +=
                              ' <li>' +
                              '<div class="aider">' +
                              '<span class="source">' + utils.changeDateFormat(infoplList[i].addTime) + '</span>' +
                              '<div class="interactive">' +
                              '</div>' +
                              '<div class="clear"></div>' +
                              '</div>' +
                              '<p class="leavemes">' + infoplList[i].infoplcontent + '</p>' +
                              '</li>';
                           }
                           $("#leavebox").html(banHtml);
                       }

                    //点赞
                    plDZ = function () {
                        utils.AjaxPost("/User/UserWeb/plDZ", { id: id }, function (result) {
                               if (result.status == "success") {
                                   utils.showSuccessMsg("点赞成功！");
                                   $("#great").html($("#great").html()*1+1);
                               }
                               else {
                                   utils.showErrMsg(result.msg);
                               }
                           });
                    }
                    //点贬
                    plDP = function () {
                        utils.AjaxPost("/User/UserWeb/plDP", { id: id }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("点贬成功！");
                                $("#comment").html($("#comment").html() * 1 + 1);
                            }
                            else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }

                    //评论
                    $("#btnreply").bind("click", function () {
                       
                        utils.AjaxPost("/User/UserWeb/pl", { id: id, infoplcontent: $("#infoplcontent").html() }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("评论成功！");
                                var mypl = result.result;
                                //location.href = "#UInfoDetail/" + id;
                                var banHtml =
                             ' <li>' +
                             '<div class="aider">' +
                             '<span class="source">' + utils.changeDateFormat(mypl.addTime) + '</span>' +
                             '<div class="interactive">' +
                             '</div>' +
                             '<div class="clear"></div>' +
                             '</div>' +
                             '<p class="leavemes">' + mypl.infoplcontent + '</p>' +
                             '</li>';
                                $("#leavebox").html(banHtml+$("#leavebox").html());
                            }
                            else {
                                utils.showErrMsg(result.msg);
                            }
                        });

                    })

                

                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }


        controller.onRouteChange = function () {
        };
    };

    return controller;
});