
define(['text!UProductShop.html', 'jquery'], function (UProductShop, $) {

    var controller = function (name) {

        //设置标题
        $("#title").html("在線購物");


        utils.AjaxPostNotLoadding("/User/UProductShop/InitCartCount", {}, function (result) {

            if (result.status == "success") {
                appView.html(UProductShop);

                //广告图
                var banners = result.map.banners;
                if (banners && banners.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < banners.length; i++) {
                        banHtml += '<li><img src="' + banners[i].imgUrl + '" /></li>';
                    }
                    $("#slides").html(banHtml);
                }

                //滚动广告图
                $(".main_visual").hover(function () {
                    $("#btn_prev,#btn_next").fadeIn()
                }, function () {
                    $("#btn_prev,#btn_next").fadeOut()
                });

                //$dragBln = false;

                $(".main_image").touchSlider({
                    flexible: true,
                    speed: 500,
                    delayTime: 3000,
                    btn_prev: $("#btn_prev"),
                    btn_next: $("#btn_next"),
                    paging: $(".flicking_con a"),
                    counter: function (e) {
                        $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
                    }
                });

                //清空查询条件按钮
                $("#clearQueryBtn").bind("click", function () {
                    utils.clearQueryParam();
                })

                var count = parseInt(result.other);
                $("#cartCount").html(count);

                //查询参数
                this.param = utils.getPageData()

                var dropload = $('#UProductShopdatalist').dropload({
                    scrollArea: window,
                    autoLoad:true,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/User/UProductShop/GetListPage", param, me,
                            function (rows, footers) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    var dto = rows[i];
                                    html += '<li><a href="#ProductDetail/' + dto.id + '">' +
                                          '<p><img data-toggle="lightbox" src="' + dto.imgUrl + '"  class="img-thumbnail" alt=""></p>' +
                                          '<h2>' + dto.productName + '</h2><span>＄' + dto.price + '美元&nbsp;&nbsp;&nbsp;</span>' +
                                          '</a></li>';
                                }
                                $("#UProductShopitemList").html(html);
                            }, function () {
                                $("#UProductShopitemList").html('<p class="dropload-noData">暫無數據</p>');
                            });
                    }
                });

                //searchMethod();
                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UProductShopitemList").empty();
                    param["productName"] = $("#productName").val();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //搜索按钮点击事件
                $("#queryProductBtn").bind("click", function () {
                    searchMethod();
                });

                
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});