
define(['text!Password2_jp.html', 'jquery'], function (Password2, $) {

    var controller = function (prePage) {
        //设置标题
        $("#title").html("セキュリティパスワードを検証");
        appView.html(Password2);

        $(".logingb").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });

        //验证密码
        checkPassWord = function () {
            var val = $("#password").val();
            if (val == 0) {
                utils.showErrMsg("セキュリティパスワードを検証");
            } else {
                utils.AjaxPost("/User/UserWeb/CheckUPass2", { pass2: val }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        pass2 = true;
                        if (prePage.indexOf(".") != -1) {
                            var val = prePage.split(".");
                            location.href = "#" + val[0] + "/" + val[1];
                        } else {
                            location.href = "#" + prePage;
                        }
                    }
                });
            }
        }

        //提交按钮
        $("#sureBtn").on("click", function () {
            checkPassWord();
        });

        $("#password").focus();

        controller.onRouteChange = function () {
            document.onkeydown = undefined;
        };
    };

    return controller;
});