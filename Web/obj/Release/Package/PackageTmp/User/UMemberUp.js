
define(['text!UMemberUp.html', 'jquery', 'j_easyui', 'zui'], function (UMemberUp, $) {

    var controller = function (name) {

        var dto = null;

        //下拉框选择改变时执行的函数
        changeFunction = function () {
            var val = $("#newId").val();
            if (val == 0) {
                $("#rmoney").html("0");
            } else {
                utils.AjaxPostNotLoadding("UMemberUp/GetRegMoney", { ulevel: val }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#rmoney").html(result.msg);
                    }
                })
            }
        }

        //设置表单默认数据
        setDefaultValue = function (dto) {
            //晋升级别解除改变事件
            $("#newId").unbind("change", changeFunction);

            $("#userId").html(dto.userId);
            $("#userName").html(dto.userName);
            $("#agentDz").html(dto.account.agentDz);
            $("#nowLevel").html(cacheMap["ulevel"][dto.uLevel]);
            $("#rmoney").html("0");

            //初始化晋升级别下拉框
            $("#newId").empty();
            $("#newId").append("<option value='0'>--请选择--</option>");
            if (cacheList["ulevel"] && cacheList["ulevel"].length > 0) {
                var list = cacheList["ulevel"];
                for (var i = 0; i < list.length; i++) {
                    var u = list[i];
                    if (u.id > dto.uLevel) {
                        $("#newId").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>");
                    }
                }
            }

            //晋升级别改变事件
            $("#newId").bind("change", changeFunction);
        }

        

        //加载会员信息
        utils.AjaxPostNotLoadding("UMemberUp/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMemberUp);
                dto = result.result;
                
                //初始默认值
                setDefaultValue(dto);

                //绑定获取焦点事件
                $("#newId").bind("focus", function () {
                    var current = $("#newId");
                    var parent = current.parent();
                    current.on("focus", function (event) {
                        parent.removeClass("has-error");
                        utils.destoryPopover(current);
                    });
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#newId").val();
                    var current = $("#newId");
                    var parent = current.parent();
                    if (val == 0) {
                        parent.addClass("has-error");
                        utils.showPopover(current, "请选择要晋升的级别", "popover-danger");
                    } else {
                        utils.confirm("确定要晋升吗？", function () {
                            utils.AjaxPost("UMemberUp/Save", { newId: val }, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("操作成功！");
                                    dto = result.result;
                                    setDefaultValue(dto);
                                    grid.datagrid("reload");
                                }
                            });
                        })
                    }

                })

                //初始化表格
                var grid = utils.newGrid("dg", {
                    title:"晋升记录",
                    columns: [[
                     { field: 'userId', title: '会员编号', width: '14%' },
                     { field: 'userName', title: '会员姓名', width: '14%' },
                     { field: 'oldId', title: '晋升前级别', width: '14%' },
                     { field: 'newId', title: '晋升后级别', width: '14%' },
                     { field: 'money', title: '缴纳点值', width: '14%' },
                     { field: 'addTime', title: '晋升日期', width: '14%' },
                     { field: 'createUser', title: '操作人', width: '14%' },
                    ]],
                    url: "UMemberUp/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["oldId"] = cacheMap["ulevel"][data.rows[i].oldId];
                            data.rows[i]["newId"] = cacheMap["ulevel"][data.rows[i].newId];
                            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                        }
                    }
                    return data;
                })

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});