
define(['text!UTakeCash.html', 'jquery', 'j_easyui', 'zui','datetimepicker'], function (UTakeCash, $) {

    var controller = function (name) {

        var dto = null;

        var fee = 0;

        //设置默认数据
        setDefaultValue = function (dto) {
            $("#agentJj").html(dto.account.agentJj);
            $("#agentsj").html("0");
            $("#epoints").val("");
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("UTakeCash/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UTakeCash);
                dto = result.result;
                
                fee = dto.regMoney;

                //初始默认值
                setDefaultValue(dto);

                //绑定获取焦点事件
                $("#epoints").bind("focus", function () {
                    var current = $("#epoints");
                    var parent = current.parent();
                    parent.removeClass("has-error");
                    utils.destoryPopover(current);
                });

                //绑定离开焦点事件
                $("#epoints").bind("blur", function () {
                    var val = $("#epoints").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (g.test(val)) {
                        var txfee = parseFloat(val) - (parseFloat(val) * (fee / 100));
                        $("#agentsj").html(txfee);
                    }
                });

                //初始化日期选择框
                $(".form-date").datetimepicker(
                {
                    language: "zh-CN",
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#epoints").val();
                    var current = $("#epoints");
                    var parent = current.parent();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (val == 0) {
                        parent.addClass("has-error");
                        utils.showPopover(current, "请录入提现金额", "popover-danger");
                    } else if (!g.test(val)) {
                        parent.addClass("has-error");
                        utils.showPopover(current, "提现金额格式不正确", "popover-danger");
                    }
                    else {
                        utils.confirm("确定提交吗？", function () {
                            utils.AjaxPost("UTakeCash/SaveTakeCash", { epoints: val }, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("操作成功！");
                                    dto = result.result;
                                    setDefaultValue(dto);
                                    grid.datagrid("reload");
                                }
                            });
                        })
                    }

                })

                //初始化表格
                var grid = utils.newGrid("dg", {
                    title:"提现记录",
                    columns: [[
                     { field: 'epoints', title: '提现金额', width: '20%' },
                     { field: 'fee', title: '手续费', width: '20%' },
                     { field: 'rmoney', title: '实际金额', width: '20%' },
                     { field: 'addtime', title: '提现日期', width: '20%' },
                     { field: 'status', title: '状态', width: '20%' },
                    ]],
                    url: "UTakeCash/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            if (data.rows[i].isPay == 1)
                                data.rows[i]["status"] = "待审核";
                            if (data.rows[i].isPay == 2)
                                data.rows[i]["status"] = "已通过";
                            if (data.rows[i].isPay == 3)
                                data.rows[i]["status"] = "已取消";
                            data.rows[i]["rmoney"] = data.rows[i].epoints - data.rows[i].fee;
                            data.rows[i]["addtime"] = utils.changeDateFormat(data.rows[i]["addtime"]);
                        }
                    }
                    return data;
                })

                //查询grid
                queryGrid = function () {
                    var objs = $("#QueryForm").serializeObject();
                    grid.datagrid("options").queryParams = objs;
                    grid.datagrid("reload");
                }

                //查询按钮
                $("#mgQueryBtn").on("click", function () {
                    queryGrid();
                })

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});