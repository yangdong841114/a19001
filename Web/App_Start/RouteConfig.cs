﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Admin、User文件夹下JS需走路由配置并进行权限校验,其他静态资源则略过
            routes.RouteExistingFiles = true;
            routes.IgnoreRoute("Content/{*relpath}");
            routes.IgnoreRoute("UpLoad/{*relpath}");
            routes.IgnoreRoute("wap/{*relpath}");
            routes.IgnoreRoute("User/zfbcode/{*relpath}");
            routes.IgnoreRoute("User/wxbcode/{*relpath}");
            routes.IgnoreRoute("AppManageLogin.html");
            routes.IgnoreRoute("ManageLogin.html");
            routes.IgnoreRoute("Admin/{filename}.html");
            routes.IgnoreRoute("User/{filename}.html");
            routes.IgnoreRoute("UserJP/{filename}.html");

            //由StaticFileController.AdminJs方法返回请求的JS
            //返回前经由MyActionFilterAttribute进行权限校验
            routes.MapRoute(name: "AdminJs",
                  url: "Admin/{filename}.js",
                 defaults: new { controller = "StaticFile", action = "AdminJs" }
            );

            //由StaticFileController.UserJs方法返回请求的JS
            //返回前经由MyActionFilterAttribute进行权限校验
            routes.MapRoute(name: "UserJs",
                  url: "User/{filename}.js",
                 defaults: new { controller = "StaticFile", action = "UserJs" }
            );

            //由StaticFileController.UserJs方法返回请求的JS
            //返回前经由MyActionFilterAttribute进行权限校验
            routes.MapRoute(name: "UserJPJs",
                  url: "UserJP/{filename}.js",
                 defaults: new { controller = "StaticFile", action = "UserJPJs" }
            );

            //请求时经由MyActionFilterAttribute进行权限校验
            routes.MapRoute(
                name: "adminRoute",
                url: "Admin/{controller}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional },
                namespaces: new[] { "Web.Admin.Controllers" }
            ).DataTokens["UseNamespaceFallback"] = false;

            //请求时经由MyActionFilterAttribute进行权限校验
            routes.MapRoute(
                name: "userRoute",
                url: "User/{controller}/{action}/{id}",
                defaults: new {id = UrlParameter.Optional },
                namespaces: new[] { "Web.User.Controllers" }
            ).DataTokens["UseNamespaceFallback"] = false;

            //请求时经由MyActionFilterAttribute进行权限校验
            routes.MapRoute(
                name: "userjpRoute",
                url: "UserJP/{controller}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional },
                namespaces: new[] { "Web.UserJP.Controllers" }
            ).DataTokens["UseNamespaceFallback"] = false;


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Web.Common.Controllers" }
            ).DataTokens["UseNamespaceFallback"] = false;

        }
    }
}