﻿using Model;
using Spring.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : SpringMvcApplication
    {
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            //AuthConfig.RegisterAuth();

            AreaRegistration.RegisterAllAreas();
            log4net.Config.XmlConfigurator.Configure();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //定义定时器  
            System.Timers.Timer myTimer = new System.Timers.Timer(5000);
            myTimer.Elapsed += new System.Timers.ElapsedEventHandler(myTimer_Elapsed);
            myTimer.Enabled = true;
            myTimer.AutoReset = true; 
        }
        //时钟事件
        void myTimer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {

            YourTask();
        }
        void YourTask()
        {
            //string site = "http://www.biopcc.com";
            //string url = site + "/Home/AutoSlEmail";
            //System.Net.HttpWebRequest myHttpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            //System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)myHttpWebRequest.GetResponse();
            //System.IO.Stream receiveStream = myHttpWebResponse.GetResponseStream();
        }
        protected void Application_End(object sender, EventArgs e)
        {
            //下面的代码是关键，可解决IIS应用程序池自动回收的问题  
            System.Threading.Thread.Sleep(1000);

            //这里设置你的web地址，可以随便指向你的任意一个aspx页面甚至不存在的页面，目的是要激发Application_Start  
            //string site = "http://www.biopcc.com";
            //string url = site + "/Home/AutoSlEmail";
            //System.Net.HttpWebRequest myHttpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            //System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)myHttpWebRequest.GetResponse();
            //System.IO.Stream receiveStream = myHttpWebResponse.GetResponseStream();
        }  
        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            HttpApplication happ = (HttpApplication)sender;
            HttpContext context = happ.Context;
            if (context!=null)
            {
                //string url = context.Request.RawUrl;
                

                //if (url.StartsWith("/Admin"))
                //{
                //    if (IsAdminCheckUrl(url))
                //    {
                //        Member m = (Member)HttpContext.Current.Session["MemberUser"];
                //        //Member m = (Member)Session["MemberUser"];
                //        Console.Write(url);
                //    }
                    
                //}
                //if (url.StartsWith("/User"))
                //{
                //    Console.Write(url);
                //}
                //Member m = (Member)Session["MemberUser"];
                //Member m2 = (Member)Session["LoginUser"];

                //if (m2 != null)
                //{

                //}
            }
            //if (url.Contains("module"))
            //{
            //    context.Response.StatusCode = 404;
            //    context.Response.Write("s");
            //    context.Response.End();
            //}
            
        }

    }
}