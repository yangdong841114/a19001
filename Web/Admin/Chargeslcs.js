
define(['text!Chargeslcs.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Chargeslcs, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "超时未续费");
        appView.html(Chargeslcs);

      

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

      

      

      

      

       

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
            columns: [[
             { field: 'userName', title: '会员名称', width: '100' },
             { field: 'uLevel', title: '会员级别', width: '80' },
             { field: 'rLevel', title: '荣誉级别', width: '80' },
          
             //{ field: 'bankName', title: '开户行', width: '100' },
             //{ field: 'bankCard', title: '开户帐号', width: '130' },
             //{ field: 'bankUser', title: '开户名', width: '100' },
             //{ field: 'code', title: '身份证号', width: '140' },
             { field: 'phone', title: '联系电话', width: '100' },
             //{ field: 'address', title: '联系地址', width: '150' },
             //{ field: 'qq', title: 'QQ', width: '100' },
             { field: 'reName', title: '推荐人编号', width: '80' },
           
             { field: 'passTime', title: '开通日期', width: '130' },
             { field: 'byopen', title: '操作人', width: '80' },
          
             { field: 'isLock', title: '是否冻结', width: '70' }
            ]],
           
            url: "MemberPassed/GetListPageslcs"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["passTime"] = utils.changeDateFormat(data.rows[i]["passTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    var rr = cacheMap["rLevel"][data.rows[i].rLevel];
                    data.rows[i].rLevel = rr ? rr : "无";
                    data.rows[i].empty = data.rows[i].empty == 0 ? "否" : "是";
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                    data.rows[i].isFt = data.rows[i].isFt == 0 ? "否" : "是";
                }
            }
            return data;
        }, function () {
            
           
            
        })

        

      

        //删除错误样式
        $("#msgContent").on("focus", function () {
            $("#msgContent").removeClass("inputError");
        })

      

       

      

        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#sendDlg').dialog("destroy");
            $('#msgDlg').dialog("destroy");
        };
    };

    return controller;
});