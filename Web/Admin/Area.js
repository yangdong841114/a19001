
define(['text!Area.html', 'jquery', 'j_easyui'], function (Area, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "省市区管理");

        //ztree对象引用
        var resourceTree = null;

        //保存成功时的回调函数
        //parentNode:添加节点的上级节点
        //updateNode:更新的原节点
        //newObj:添加或更新操作后返回的对象
        saveSuccessCallBack = function (parentNode, updateNode, newObj) {
            //存在updateNode表示是更新操作，否则是添加节点操作
            if (updateNode != null) {
                updateNode.name = newObj.name;
                resourceTree.updateNode(updateNode);
            } else {
                resourceTree.addNodes(parentNode, -1, newObj);
            }
        }

        //清空表单
        clearForm = function () {
            //清空隐藏域
            $("#id").val("");
            $("#parentId").val("");
            //清空标题
            $("#formTitle").html("&nbsp;");
            //清空表单
            $('#editModalForm').form('reset');
        }

        utils.AjaxPostNotLoadding("Area/GetList", null, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                var saveMsg = "";      //保存时提示消息
                var parentNode = null; //插入tree节点的上级节点
                var updateNode = null; //更新tree节点时的原节点

                //ztree设置
                var treesetting = {
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id", 	              // id编号字段名
                            pIdKey: "parentId",           // 父id编号字段名
                            rootPId: null
                        },
                        key: {
                            name: "name"    //显示名称字段名
                        }
                    },
                    view: {
                        //showLine: false,            //不显示连接线
                        dblClickExpand: false       //禁用双击展开
                    },
                    callback: {
                        onClick: function (e, treeId, node) {
                            if (node.isParent) {
                                resourceTree.expandNode(node);
                            }
                            //组合表单数据
                            var obj = {
                                id: node.id, name: node.name,parent:"无",
                                parentId: node.parentId
                            }
                            var parent = node.getParentNode();
                            if (parent != null) {
                                obj["parent"] = parent.name;
                            }

                            parentNode = parent;
                            updateNode = node;
                            //清空表单数据
                            $('#editModalForm').form('reset');
                            //根据选中的行记录加载数据
                            $('#editModalForm').form('load', obj);
                            $("#formTitle").html("编辑 【" + node.name+"】");
                            saveMsg = "您确定要保存对【" + node.name + "】的更改吗？";
                        }
                    }
                };

                //初始化布局，HTML
                appView.html(Area);
                $('#resourceLayout').layout();

                //生成树
                resourceTree = $.fn.zTree.init($("#resourceTree"), treesetting, result.list);

                //初始化编辑区input text
                $(".easyui-textbox").each(function (i, ipt) {
                    $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });
                });


                //添加同级
                $("#addSameBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择添加同级节点的位置");
                    } else {
                        node = nodes[0];
                        var obj = {
                            id: "", parentId: '0' ,parent:"无"
                        }
                        var parent = node.getParentNode();
                        if (parent != null) {
                            obj["parent"] = parent.name;
                            obj["parentId"] = parent.id;
                        }

                        //添加节点位置的上级节点
                        updateNode = null;
                        if (obj.parentId == "0") {
                            parentNode = null;
                        } else {
                            parentNode = parent;
                        }

                        //清空表单数据
                        $('#editModalForm').form('reset');
                        //根据选中的行记录加载数据
                        $('#editModalForm').form('load', obj);
                        $("#formTitle").html("【" + node.name + "】添加同级");
                        saveMsg = "您确定要在【" + node.name + "】添加同级节点吗？";
                    }
                })

                //添加下级
                $("#addSubBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择上级节点");
                    } else {
                        node = nodes[0];
                        var obj = {
                            id: "", parentId: node.id ,parent:node.name
                        }

                        //添加节点位置的上级节点
                        updateNode = null;
                        parentNode = node;

                        //清空表单数据
                        $('#editModalForm').form('reset');
                        //根据选中的行记录加载数据
                        $('#editModalForm').form('load', obj);
                        $("#formTitle").html("【" + node.name + " 】添加下级");
                        saveMsg = "您确定要为【" + node.name + "】添加下级节点吗？";
                    }
                })

                //删除
                $("#deleteBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择需要删除的节点");
                    } else {
                        node = nodes[0];
                        utils.confirm("您确定要删除【" + node.name + "】及其下级所有子节点吗？", function () {
                            utils.AjaxPost("Area/Delete", { id: node.id }, function (result) {
                                if (result.status == "删除成功") {
                                    resourceTree.removeChildNodes(node);//清空当前节点的子节点
                                    resourceTree.removeNode(node);      //清空当前节点
                                    utils.showSuccessMsg("删除成功");
                                    
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            })
                        });
                    }
                })

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var val = $("#parent").val();
                    if (!val || val == 0) { return; }
                    if ($("#editModalForm").form('validate')) {
                        utils.confirm(saveMsg, function () {
                            $('#editModalForm').form('submit', {
                                url: 'Area/SaveOrUpdate',
                                onSubmit: function () {
                                    var ok = $(this).form('validate');
                                    if (ok) {
                                        $.messager.progress({
                                            title: '请稍后',
                                            msg: '正在处理中...',
                                            interval: 100,
                                            text: ''
                                        });
                                    }
                                    return ok;
                                },
                                success: function (result) {
                                    $.messager.progress("close");
                                    var result = eval('(' + result + ')');
                                    if (result.status == "fail") {
                                        utils.showErrMsg(result.msg);
                                    } else {
                                        saveSuccessCallBack(parentNode, updateNode, result.result); //更新tree节点或添加节点
                                        utils.showSuccessMsg(result.msg);
                                        clearForm();        //清空表单
                                    }
                                }
                            });
                        })
                    }
                });

            }
        });

        controller.onRouteChange = function () {
            //销毁ztree
            if (resourceTree) {
                resourceTree.destroy();
            }
        };
    };

    return controller;
});