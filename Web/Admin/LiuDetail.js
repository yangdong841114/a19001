
define(['text!LiuDetail.html', 'jquery', 'j_easyui', 'datetimepicker'], function (LiuDetail, $) {

    var controller = function (uid) {
        //设置标题
        $("#center").panel("setTitle", "会员流水账明细");
        appView.html(LiuDetail);
        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });
        //$("#tabTitle").html(userId+"的流水账明细列表");

        //初始化查询区select
        $("#accountId").empty();
        $("#accountId").append("<option value='0'>--全部--</option>");
        var lit = cacheList["JournalClass"];
        if (lit && lit.length > 0) {
            for (var i = 0; i < lit.length; i++) {
                $("#accountId").append("<option value='" + lit[i].id + "'>" + lit[i].name + "</option>"); 
            }
        }
        
        //返回按钮
        $("#backBtn").bind("click", function () {
            history.go(-1);
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter:true,
            columns: [[
             { field: 'userId', title: '会员编号', width: '10%' },
             { field: 'abst', title: '业务摘要', width: '35%' },
             { field: 'accountId', title: '账户类型', width: '10%' },
             { field: 'optype', title: '收支', width: '10%' },
             { field: 'epotins', title: '金额', width: '15%' },
             {
                 field: 'last', title: '余额', width: '15%', formatter: function (value, row, index) {
                     var str = "<a href='javascript:void(0)' onclick='EditBalance(" + row['id'] +"," + row['last'] + ")'>" + row.last + "</a>";
                     return str;
                 }
             },
             { field: 'addtime', title: '日期', width: '15%' },
            ]],
            url: "LiuShui/GetLiushuiDetailListPage?uid=" + uid
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addtime"] = utils.changeDateFormat(data.rows[i]["addtime"]);
                    data.rows[i].accountId = cacheMap["JournalClass"][data.rows[i].accountId];
                    //data.rows[i]["epotins"] = data.rows[i]["epotins"].toFixed(2);
                    data.rows[i]["last"] = data.rows[i]["last"].toFixed(2);
                }
            }
            return data;
        })

        var userid;
        //初始化加载数据
        utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetBaseData", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                userid = result.map["id"];
            }

        });

        EditBalance = function (id, last) {
            if (userid == 2) {
                //alert(id);
                //alert(last);
                $("#editid").val(id);
                $("#Balance").val(last);
                $('#dlg').dialog({ title: '修改余额' }).dialog("open");
            }
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/LiuShui/ExportUserLiuShuiExcel?uid=" + uid;
        })

        //保存按钮
        $("#saveBtn").bind("click", function () {
            if ($("#Balance").val() == 0) {
                utils.showErrMsg("请输入修改金额");
            } else {
                var data = { id: $("#editid").val(), last: $("#Balance").val() };
                utils.AjaxPost("LiuShui/ReviseBalance", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("修改成功！");
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();
                    }
                });
                //utils.AjaxPost("LiuShui/ReviseBalance", data, function (result) {
                //    if (result.status == "fail") {
                //        alert(2);
                //        utils.showErrMsg(result.msg);
                //    } else {
                //        alert(3);
                //        utils.showSuccessMsg("修改成功！");
                        
                //    }
                //});
            }
            //queryGrid();
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});