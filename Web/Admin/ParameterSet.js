
define(['text!ParameterSet.html', 'jquery'], function (ParameterSet, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "参数设置");


        utils.AjaxPostNotLoadding("ParameterSet/InitView", {}, function (result) {
            appView.html(ParameterSet);
            //check和input值转换
            $("#chk_transferToccDHMkg").click(function () {
                if ($('#chk_transferToccDHMkg').is(':checked'))
                    $("#transferToccDHMkg").val("开");
                else
                    $("#transferToccDHMkg").val("关");
            });

            $("#chk_transferTodToOtherTodkg").click(function () {
                if ($('#chk_transferTodToOtherTodkg').is(':checked'))
                    $("#transferTodToOtherTodkg").val("开");
                else
                    $("#transferTodToOtherTodkg").val("关");
            });

            $("#chk_transferToccToTodkg").click(function () {
                if ($('#chk_transferToccToTodkg').is(':checked'))
                    $("#transferToccToTodkg").val("开");
                else
                    $("#transferToccToTodkg").val("关");
            });
            //初始化赋值
            var rt = result.map;
            var formInputs = $("#ParamSetForm").serializeObject();
            if (rt != null) {
                for (name in rt) {
                    if (formInputs[name] != undefined) {
                        var obj = rt[name];
                        $("#" + name).val(obj.paramValue);
                    }
                }
            }

            //根据值设置checkbox
            if ($("#transferToccDHMkg").val() == "开")
                $("#chk_transferToccDHMkg").attr('checked', true);
            else
                $("#chk_transferToccDHMkg").attr('checked', false);

            if ($("#transferTodToOtherTodkg").val() == "开")
                $("#chk_transferTodToOtherTodkg").attr('checked', true);
            else
                $("#chk_transferTodToOtherTodkg").attr('checked', false);

            if ($("#transferToccToTodkg").val() == "开")
                $("#chk_transferToccToTodkg").attr('checked', true);
            else
                $("#chk_transferToccToTodkg").attr('checked', false);


            //注册离开，获取焦点事件
            var fs = $("#ParamSetForm").serializeArray();
            $.each(fs, function (index, obj) {
                var name = obj.name;
                var current = $("#" + name);
                current.on("focus", function () {
                    current.removeClass("inputError");

                });
            })

            //保存
            $("#submit").on('click', function () {
                var fs = $("#ParamSetForm").serializeObject();
                var checked = true;
                var i = 0;
                var dt = {};
                //非空校验
                $.each(fs, function (name, val) {
                    if (name != "chk_transferToccDHMkg" && name != "chk_transferTodToOtherTodkg" && name !="chk_transferToccToTodkg") {
                        var current = $("#" + name);
                        //if (val == 0 && name != "userIdPrefix") {
                        //    current.addClass("inputError");
                        //}
                        if (current.hasClass("inputError")) {
                            checked = false;
                        }
                        dt["list[" + i + "].paramCode"] = name;
                        dt["list[" + i + "].paramValue"] = val;
                        i++;
                    }
                });
                //$.each(fs, function (name, val) {
                //    if (name != "chk_transferTodToOtherTodkg" && name != "chk_transferTodToOtherTodkg") {
                //        var current = $("#" + name);
                //        //if (val == 0 && name != "userIdPrefix") {
                //        //    current.addClass("inputError");
                //        //}
                //        if (current.hasClass("inputError")) {
                //            checked = false;
                //        }
                //        dt["list[" + i + "].paramCode"] = name;
                //        dt["list[" + i + "].paramValue"] = val;
                //        i++;
                //    }
                //});
                //$.each(fs, function (name, val) {
                //    if (name != "chk_transferToccToTodkg" && name != "chk_transferToccToTodkg") {
                //        var current = $("#" + name);
                //        //if (val == 0 && name != "userIdPrefix") {
                //        //    current.addClass("inputError");
                //        //}
                //        if (current.hasClass("inputError")) {
                //            checked = false;
                //        }
                //        dt["list[" + i + "].paramCode"] = name;
                //        dt["list[" + i + "].paramValue"] = val;
                //        i++;
                //    }
                //});
                //提交表单
                if (checked) {
                    utils.AjaxPost("ParameterSet/Save", dt, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("保存成功！");
                        }
                    });

                } else {
                    utils.showErrMsg("您有参数未填写，不能保存！");
                }

            })
        });



        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});