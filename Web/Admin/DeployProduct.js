
define(['text!DeployProduct.html', 'jquery', 'ueditor'], function (DeployProduct, $) {

    var controller = function (id) {

        initData = function (dto) {

            appView.html(DeployProduct);


           

            var mlist;
            var mlistSm;


            initSecondProductType = function () {
                var selectValue = $('#productBigTypeId').val();
                $("#productTypeId").empty();
                if (mlistSm != null && mlistSm.length > 0) {
                    for (var i = 0; i < mlistSm.length; i++) {
                        var vo = mlistSm[i];
                        if (vo.parentId == selectValue)
                        $("#productTypeId").append("<option value='" + vo.id + "' dataName = '" + vo.name + "'>" + vo.name + "</option>");
                    }
                }
            };

            //获取父类
            utils.AjaxPostNotLoadding("ProductType/GetDefaultData", {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    var map = result.map;
                    mlist = map.mlist;
                    mlistSm = map.mlistSm;
                    //初始化下拉框
                    $("#productBigTypeId").empty();
                    if (mlist != null && mlist.length > 0) {
                        for (var i = 0; i < mlist.length; i++) {
                            var vo = mlist[i];
                            $("#productBigTypeId").append("<option value='" + vo.id + "' dataName = '" + vo.name + "'>" + vo.name + "</option>");
                        }
                    }

                    initSecondProductType();


                }
            });

          
            $('#productBigTypeId').on('change', function () {
                initSecondProductType();
            });



            //初始化编辑器
            $("#editor").height(document.body.offsetHeight - 390);
            var editor = new Quill("#editor", {
                modules: {
                    toolbar: utils.getEditorToolbar()
                },
                theme: 'snow'
            });
            utils.setEditorHtml(editor, dto ? dto.cont : "")

            if (dto) {
                //初始化图片
                $("#picu").attr("src", dto.imgUrl);
                $("#picu").attr("data-image", dto.imgUrl);
                $("#picu").lightbox();

                //初始化表单内容
                $("#productCode").val(dto.productCode);
                $("#productName").val(dto.productName);
                $("#num").val(dto.num);
                $("#fxPrice").val(dto.fxPrice);
                $("#id").val(dto.id);
                $("#imgUrl").val(dto.imgUrl);

                $("#productBigTypeId").val(dto.productBigTypeId);
                $("#productTypeId").val(dto.productTypeId);
            }

            //获取焦点事件
            $(".form-control").each(function (index, obj) {
                var current = $(this);
                var parent = current.parent();
                current.on("focus", function () {
                    parent.removeClass("has-error");
                    utils.destoryPopover(current);
                });
            })

            //返回
            $("#rebackBtn").on('click', function () { history.go(-1); });

            //保存
            $("#saveBtn").on('click', function () {
                var objs = $("#BaseSetForm").serializeObject();
                var checked = true;
                
                //非空校验
                if ($("#productCode").val() == 0) {
                    var current = $("#productCode");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "商品编码不能为空", "popover-danger");
                    checked = false;
                }
                if ($("#productName").val() == 0) {
                    var current = $("#productName");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "商品名称不能为空", "popover-danger");
                    checked = false;
                }
                //新增必须上传图片，编辑时可以不用上传
                if ($("#imgFile").val() == 0 && !dto) {
                    var current = $("#imgFile");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "请选择上传的图片", "popover-danger");
                    checked = false;
                }
                //金额校验
                var g = /^\d+(\.{0,1}\d+){0,1}$/;
               
                if (isNaN($("#fxPrice").val()) || parseFloat($("#fxPrice").val()) < 0) {
                    var current = $("#fxPrice");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "原价金额格式错误", "popover-danger");
                    checked = false;
                }

                //提交表单
                if (checked) {
                    if (editor.getText() == 0) {
                        utils.showErrMsg("请输入商品介绍内容");
                    } else {
                        var formdata = new FormData(); 
                        if (dto) {
                            formdata.append("id", $("#id").val());
                            formdata.append("imgUrl", $("#imgUrl").val());
                        }
                        formdata.append("productName", $("#productName").val());
                        formdata.append("productCode", $("#productCode").val());
                        formdata.append("num", $("#num").val());
                        formdata.append("fxPrice", $("#fxPrice").val());
                        formdata.append("cont", utils.getEditorHtml(editor));
                        formdata.append("imgFile", $("#imgFile")[0].files[0]);

                        var parentNmaePara = $("#productTypeId").find("option:selected").attr("dataName");
                        var parentIdPara = $("#productTypeId").find("option:selected").attr("value");
                        formdata.append("productTypeId", parentIdPara);
                        formdata.append("productTypeName", parentNmaePara);

                        parentNmaePara = $("#productBigTypeId").find("option:selected").attr("dataName");
                        parentIdPara = $("#productBigTypeId").find("option:selected").attr("value");
                        formdata.append("productBigTypeId", parentIdPara);
                        formdata.append("productBigTypeName", parentNmaePara);
                      

                        utils.AjaxPostForFormData("Product/SaveOrUpdate", formdata, function (result) {
                            if (result.status == "fail") {
                                if (result.msg == "商品编码已经存在") {
                                    var current = $("#productCode");
                                    var parent = current.parent();
                                    parent.addClass("has-error");
                                    utils.showPopover(current, result.msg, "popover-danger");
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            } else {
                                dto = result.result;
                                $("#id").val(dto.id);
                                $("#imgUrl").val(dto.imgUrl);
                                $("#imgFile").val(""); //文件上传置空
                                //如果图片有改动，重置图片路径
                                if ($("#picu").attr("src") != dto.imgUrl) {
                                    var murl = dto.imgUrl + "?etc=" + (new Date()).getTime();
                                    $("#picu").remove();
                                    var newImg = '<img data-toggle="lightbox" id="picu" src="' + murl + '" data-image="' + murl + '" data-caption="商品图片" class="img-thumbnail" alt="">';
                                    $("#imgDiv").append($(newImg));
                                    $("#picu").lightbox();
                                }
                                utils.showSuccessMsg("保存成功！");
                                history.go(-1);
                            }
                        });
                    }
                }
            })
        }

        //设置标题
        if (id && id > 0) {
            $("#center").panel("setTitle", "编辑商品");
            utils.AjaxPostNotLoadding("Product/GetModel", { id: id }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    initData(result.result);
                }
            });
        } else {
            $("#center").panel("setTitle", "发布商品");
            initData();
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});