
define(['text!MemberUp.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberUp, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "会员晋升");
        appView.html(MemberUp);

        //初始化表单区域
        $('#dlg').dialog({
            title: '添加记录',
            width: 500,
            height: 420,
            cache: false,
            modal: true
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始化会员级别
        defaultUlevel = function () {
            $("#newId").empty();
            var cList = cacheList["ulevel"];
            if (cList && cList.length > 0) {
                $("#newId").append("<option value='0'>--请选择--</option>");
                for (var i = 0; i < cList.length; i++) {
                    $("#newId").append("<option value='" + cList[i].id + "'>" + cList[i].name + "</option>");
                }
            }
        }

        //初始化荣誉级别
        defaultRlevel = function () {
            $("#newId").empty();
            var cList = cacheList["rLevel"];
            if (cList && cList.length > 0) {
                $("#newId").append("<option value='0'>--请选择--</option>");
                for (var i = 0; i < cList.length; i++) {
                    $("#newId").append("<option value='" + cList[i].id + "'>" + cList[i].name + "</option>");
                }
            }
        }
        

        //会员离开焦点
        $("#userId").on("blur", function () {
            if ($("#userId").val() != 0) {
                utils.AjaxPostNotLoadding("MemberUp/GetUser", { userId: $("#userId").val() }, function (result) {
                    if (result.status == "success") {
                        var dto = result.result;
                        if (result.msg == "不存在") {
                            $("#userName2").val("不存在");
                            $("#uLevel").val("");
                        } else {
                            $("#userName2").val(dto.userName);
                            //1:会员级别，2：荣誉级别
                            if ($("#type").val() == 1) {
                                $("#uLevel").val(cacheMap["ulevel"][dto.uLevel]);
                            } else {
                                var rl = cacheMap["rLevel"][dto.rLevel];
                                if (!rl) { $("#uLevel").val("无"); }
                                else { $("#uLevel").val(rl); }
                                
                            }
                        }
                    }
                });
            } else {
                $("#userName2").val("");
                $("#uLevel").val("");
            }
        })

        //清除错误样式
        $(".form-control").each(function () {
            var dom = $(this);
            dom.on("focus", function () {
                dom.removeClass("inputError");
            })
        });

        //保存按钮
        $("#saveBtn").bind("click", function () {
            var checked = true;
            //非空校验
            if ($("#userId").val() == 0) {
                checked = false;
                $("#userId").addClass("inputError");
            }
            if ($("#newId").val() == 0) {
                checked = false;
                $("#newId").addClass("inputError");
            }
            if (checked) {
                var data = { userId: $("#userId").val(), newId: $("#newId").val(), remark: $("#remark").val(), isBackRound: 0,cls:$("#type").val()};
                utils.AjaxPost("MemberUp/Save", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();
                    }
                });
            }
            return false;
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '10%' }]],
            columns: [[
             { field: 'cls', title: '操作类型', width: '10%' },
             { field: 'userName', title: '会员姓名', width: '10%' },
             { field: 'oldId', title: '晋升前级别', width: '10%' },
             { field: 'newId', title: '晋升后级别', width: '10%' },
             { field: 'money', title: '缴纳点值', width: '10%' },
             { field: 'addTime', title: '晋升日期', width: '14%' },
             { field: 'createUser', title: '操作人', width: '10%' },
             { field: 'remark', title: '备注', width: '16%' }
            ]],
            toolbar: [{
                text: "修改荣誉级别",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    $("#type").val("2");
                    $("#nntext").html("荣誉级别");
                    defaultRlevel();
                    $('#dlg').dialog({ title: '修改荣誉级别' }).dialog("open");
                }
            }
            ],
            url: "MemberUp/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    var cls = data.rows[i]["cls"];
                    if (cls != 2) {
                        data.rows[i]["oldId"] = cacheMap["ulevel"][data.rows[i].oldId];
                        data.rows[i]["newId"] = cacheMap["ulevel"][data.rows[i].newId];
                    } else {
                        var rl = cacheMap["rLevel"][data.rows[i].oldId];
                        data.rows[i]["oldId"] = rl?rl:"无";
                        data.rows[i]["newId"] = cacheMap["rLevel"][data.rows[i].newId];
                    }
                    data.rows[i]["cls"] = cls == 2 ? "修改荣誉级别" : "会员级别";
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#MemberUpQueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "userId2" ? "userId" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});