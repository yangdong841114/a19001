
define(['text!InfoSP.html', 'jquery', 'j_easyui', 'datetimepicker'], function (InfoSP, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "视频管理");
        appView.html(InfoSP);

        //初始化表单区域
        $('#dlgInfoWZ').dialog({
            title: '发布视频',
            width: 850,
            height: 200,
            cache: false,
            modal: true
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });


     
        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveBtnInfoWZ").bind("click", function () {
            var formdata = new FormData();
            formdata.append("title", $("#title").val());
            formdata.append("infoJj", $("#infoJj").val());
            formdata.append("linkUrl", $("#linkUrl").val());
            utils.AjaxPostForFormData("InfoWZ/SaveSP", formdata, function (result) {

                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                }
                else {
                    utils.showSuccessMsg("保存成功！");
                    $('#dlgInfoWZ').dialog("close");
                    queryGrid();
                }

            })


        })

        //打开明细
        openDetailDialog = function (row) {
            $("#id").val(row.id);
            $('#dlgInfoWZ').dialog({ title: '修改视频' }).dialog("open");
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
              { field: 'title', title: '视频名称', width: '10%' },
              { field: 'infoJj', title: '摘要', width: '40%' },
             { field: 'llcount', title: '点击量', width: '10%' },
            
             { field: 'addTime', title: '发布日期', width: '15%' },
             { field: 'linkUrl', title: '链接地址', width: '20%' },
             {
                 field: '_deleteRecord', title: '操作', width: '8%', formatter: function (val, row, index) {
                     var cont = "";
                     cont += '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >删除</a>';
                     return cont;
                 }
             }
            ]],
            toolbar: [{
                text: "发布视频",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $("#title").val("");
                    $("#id").val("");
                
                    $('#dlgInfoWZ').dialog({ title: '发布视频' }).dialog("open");
                }
            }
            ],
            url: "InfoWZ/GetListPageSP"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {

           


            $(".img-thumbnail").each(function (index, ele) {
                $(this).lightbox();
            })

            //删除
            $(".recordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该记录？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("InfoWZ/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("删除成功");
                                $('#dlgInfoWZ').dialog("close");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "title2" ? "title" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlgInfoWZ').dialog("destroy");
        };
    };

    return controller;
});