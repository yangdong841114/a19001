
define(['text!DataDictionaryDialog.html', 'jquery'], function (DataDictionaryDialog, $) {

    var dialog = function (name) {

        this.init = function () {
            editDialog.html(DataDictionaryDialog); //初始化html
            $("#name2").on('blur', function () {
                var val = $("#name2").val();
                if (val == 0) {
                    $("#name2").addClass("inputError");
                }
            });

            $("#editModalSaveBtn").on('click', function () {
                var val = $("#name2").val();
                if (val == 0) {
                    $("#name2").addClass("inputError");
                }
            });
        }

        this.title = function (title) {
            $("#editModalTitle").html(title);
        }


        //根节点类型
        var rootObj = {};

        //初始化下拉框
        $.ajax({
            url: "DataDictionary/GetRootList",
            type: "POST",
            success: function (result) {
                if (result.status == "success") {
                    var list = result.list;
                    $("#parentId").empty();
                    $("#pp").empty();
                    $("#parentId").append("<option value='0'>--全部--</option>");
                    $("#pp").append("<option value='0'>--无--</option>");
                    if (list && list.length > 0) {
                        for (var i = 0; i < list.length; i++) {
                            rootObj[list[i].id] = list[i].name;
                            $("#parentId").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>"); //为Select追加一个Option(下拉项)
                            $("#pp").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>");
                        }
                    }

                }
            }
        });
     
        

        //编辑数据
        editRecord = function () {
            var obj = grid.datagrid("getSelected");
            if (obj == null) { utils.showErrMsg("请选选择需要编辑的行！"); }
            else {
                
                $('#mmmm2').modal({
                    keyboard: false,
                    show: true,
                    position: 'center',
                    backdrop: 'static'
                })

                //utils.editModelShow("编辑记录", $("#DataDictionaryEditBody").html(), null, function () {
                //    var name = $("#pname").val();
                //    if (name == 0) {
                //        $("#pname").addClass("inputError");
                //    }
                //})
                //$("#pname").val(obj.name);
                //$("#ms").val(obj.remark);
                //$("#pp").val(obj.parentId);
                //$("#pp").attr("disabled", "disabled");

            }
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field:'ck',title: '文本', checkbox:true,},
             { field: 'id', title: 'ID', width:'24%'},
             { field: 'parentName', title: '数据类型', width: '25%' },
             { field: 'name', title: '名称', width: '25%' },
             { field: 'remark', title: '描述', width: '25%' }
            ]],
            toolbar: [{
                text:"添加",
                iconCls: 'icon-add',
                handler: function(){alert('edit')}
            }, '-', {
                text: "编辑",
                iconCls: 'icon-edit',
                handler: editRecord
            }, '-', {
                text: "删除",
                iconCls: 'icon-cancel',
                handler: function () { alert('help') }
            }

            ],
            url: "DataDictionary/GetListChildren"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["parentName"] = rootObj[data.rows[i].parentId];
                }
            }
            return data;
        })

        //查询按钮
        $("#submit").on("click", function () {
            var objs = $("#DataDictionaryQueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        })


   
    };

    return controller;
});