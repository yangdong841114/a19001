
define(['text!NotFound.html', 'jquery', 'zui', 'css!../Content/css/styleAdmin.css'], function (NotFound, $) {

    var controller = function (name) {
        appView.html(NotFound);
        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});