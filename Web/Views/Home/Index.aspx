﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<html lang="zh">--%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ログイン</title>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <script type="text/javascript" src="/Content/APP/js/language.js"></script>
</head>
<body class="loginbg">
   <div class="loginenter">
    <div class="loginlogo">
        <img src="/Content/APP/User/images/logo1.png" />
        <p></p>
    </div>
 

  
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconuser.png" /></dt>
            <dd>
              <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" /></span>
                <input type="text" class="entertxt" id="userId" placeholder="ユーザ名を入力してください" />
            </dd>
        </dl>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconpassword.png" /></dt>
            <dd> <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" /></span>
                <input type="password" class="entertxt" id="password" placeholder="パスワードを登録してください" />
            </dd>
        </dl>
        <div class="loginset">
            <label>
                <input type="checkbox" value="remember" id="remember" />覚えてください</label>
            <%--<a href="/wap/UserJP/index.html">日文</a>--%>
            <a href="/ForgetPass/AppIndex1">パスワードを忘れます？</a>
            <div class="clear"></div>
        </div>
        <div class="lbtnbox">
            <button class="loginbtn" onclick="login()">ログイン</button>
            <div  style="text-align:center; margin-top:10px;">
            <p><a style="color:#ffc41f;" href="/Home/AppReg1/bioplusx">新しいユーザを登録します</a></p>
        </div>
            <div class="languagebox">
                    <ul style="height:30px; display:-webkit-inline-box; margin-top:5px;">
                       <li onclick="setSeesion(1);" style="width:70px; height:auto;"><a href="/Home/AppIndex/wantlogin"><img style="height:30px;" src="/Content/APP/User/images/zhongguo.png" /></a></li>
                        <li onclick="setSeesion(2);" style="width:70px; height:auto;"><a href="/Home/Index/wantlogin"><img style="height:30px;" src="/Content/APP/User/images/riben.png" /></a></li>
                    </ul>
                </div>
        </div>
        
    </div>
     <div  style="display:none;">
    <script type="text/javascript">
      <%: @Html.Raw(ViewData["WebCode"])%>
        cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');
    </script>
          </div>
</body>
<script type="text/javascript">
    function setSeesion(type) {
        if (type == 1) {
            root = 'ft';
        }
        else {
            root = 'rb';
        }

        $.ajax({
            url: "/Common/setSession",
            type: "POST",
            data: { "type": root },
            success: function (result) {
                if (result.status == "fail") {
                    showErrorMsg(result.msg);
                } else {
                    if (type == 1) {
                        location.href = "#UInfo/10207";
                    } else {
                        location.href = "#UInfo_jp/10407";
                    }
                }
            }
        });
    }


    $(document).ready(function () {
        $(".erase").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });
    });

    //顯示錯誤消息
    showErrorMsg = function (msg) {
        var msgbox = new $.zui.Messager('ヒントメッセージ：' + msg, {
            type: 'danger',
            icon: 'warning-sign',
            placement: 'center',
            parent: 'body',
            close: true
        });
        msgbox.show();
    }

    //登錄校驗
    login = function () {
        if ($("#userId").val() == 0) {
            showErrorMsg("ユーザ名は空ではありません");
        } else if ($("#password").val() == 0) {
            showErrorMsg("パスワードが空です");
        }
        else {
            var data = "userId=" + $("#userId").val() + "&password=" + $("#password").val();
            if (document.getElementById("remember").checked) {
                data += "&remenber=1";
            } else {
                data += "&remenber=0";
            }
            $.ajax({
                url: "/Common/MemberLoginByPhone",
                type: "POST",
                data: data,
                success: function (result) {
                    if (result.status == "fail") {
                        showErrorMsg(result.msg);
                    } else {
                        location.href = "/wap/User/index_jp.html";
                    }
                }
            });
            //$("#loginForm").submit();
        }
    }

</script>

</html>
