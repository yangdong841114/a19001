﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body>
    <header>
        <div class="">
            <i class="backicon"><a a href="javascript:history.go(-1);">
                <img src="/Content/APP/User/images/btnback.png" /></a></i>
            <h2>セキュリティチェック</h2>
        </div>
        <div class="clear"></div>
    </header>
    <div class="maintag">
        <ul class="tga2">
            <li class="active" id="tabBtn1"><a href="javascript:tabClick(1);" id="tabA1">携帯認証</a></li>
            <li id="tabBtn2"><a href="javascript:tabClick(2);" id="tabA2">秘密保護の検証</a></li>
        </ul>
    </div>
    <div class="entryinfo">
        <input type="hidden" id="flag" value="1" />
        <dl class="passValidate" style="display: none;">
            <dt>秘密保護の問題</dt>
            <dd>
                <p><%:ViewData["question"]%></p>
            </dd>
        </dl>
        <dl class="passValidate" style="display: none;">
            <dt>秘密裏の答え</dt>
            <dd>
                <input type="text" class="entrytxt" required="required" id="answer" placeholder="秘密保護の答えを入力してください" />
                <span class="erase"><i class="fa fa-times-circle-o"></i></span>
            </dd>
        </dl>
        <dl class="phoneValidate">
            <dt>携帯の番号</dt>
            <dd>
                <input type="text" class="entrytxt" value="<%:ViewData["phone"]%>" readonly />
                <button class="getcode" id="jyfBtn">認証コードを送信</button>
            </dd>
        </dl>
        <dl class="phoneValidate">
            <dt>携帯の認証コード</dt>
            <dd>
                <input type="text" class="entrytxt" required="required" id="yzm" disabled="disabled" placeholder="受信した認証コードを入力してください" />
                <span class="erase"><i class="fa fa-times-circle-o"></i></span>
            </dd>
        </dl>
        <div id="msg" class="phoneValidate" style="width: 95%; text-align: center; font-size: 1.4rem; padding: 1rem 1rem 1rem 1rem;">
            &nbsp;<br />
            &nbsp;
        </div>
        <div class="btnbox">
            <button class="bigbtn" id="tjBtn">送信</button>
        </div>
        <div class="original" style="padding-bottom: 2rem;"><a href="/Home/AppIndex1">ログインに戻ります</a> </div>
    </div>
    <script type="text/javascript">

        //文本框右側刪除按鈕
        $(".erase").each(function () {
            var dom = $(this);
            dom.bind("click", function () {
                dom.prev().val("");
            })
        });

        //選項卡切換
        tabClick = function (index) {
            $("#tabBtn1").removeClass("active");
            $("#tabBtn2").removeClass("active");
            $("#tabBtn" + index).addClass("active");
            if (index == 1) {
                $("#flag").val("1");
                $(".phoneValidate").each(function () { $(this).css("display", "block"); });
                $(".passValidate").each(function () { $(this).css("display", "none"); });
            } else {
                $("#flag").val("2");
                $(".phoneValidate").each(function () { $(this).css("display", "none"); });
                $(".passValidate").each(function () { $(this).css("display", "block"); });
            }
        }

        $(function () {

            //顯示錯誤消息
            showErrorMsg = function (msg) {
                var msgbox = new $.zui.Messager('ヒントメッセージ：' + msg, {
                    type: 'danger',
                    icon: 'warning-sign',
                    placement: 'center',
                    parent: 'body',
                    close: true
                });
                msgbox.show();
            }

            //提交
            $("#tjBtn").on("click", function () {
                var flag = $("#flag").val();
                if (flag == 1 && $("#yzm").val() == 0) {
                    showErrorMsg("受信した認証コードを入力してください");
                } else if (flag == 2 && $("#answer").val() == 0) {
                    showErrorMsg("秘密保護の答えを入力してください");
                } else {
                    $.ajax({
                        url: "/ForgetPass/ValidateCode",
                        type: "POST",
                        data: "code=" + $("#yzm").val() + "&flag=" + flag + "&answer=" + $("#answer").val(),
                        success: function (result) {
                            if (result.status == "fail") {
                                showErrorMsg(result.msg);
                            } else {
                                location.href = "/ForgetPass/AppToChange";
                            }
                        }
                    });
                }
            });

            var flag = null;
            var s = 120;
            show = function () {
                if (s >= 1) {
                    $("#msg").html("認証コードが送信されました。メッセージを確認してください" + s + "秒後にシステムの再送信を要求します");
                } else {
                    clearInterval(flag);
                    s = 120;
                    $("#jyfBtn")[0].disabled = false;
                    $("#msg").html("");
                }
                s--;
            }

            //獲取校驗碼
            $("#jyfBtn").on("click", function () {
                $.ajax({
                    url: "/ForgetPass/SendPhoneCode",
                    data: "etc=" + (new Date()).getTime(),
                    type: "POST",
                    success: function (result) {
                        if (result.status == "success") {
                            flag = setInterval(show, 1000);
                            $("#yzm")[0].disabled = false;
                            $("#tjBtn")[0].disabled = false;
                            $("#jyfBtn")[0].disabled = true;
                        }
                    }
                });
            })
        });
    </script>
</body>
</html>
