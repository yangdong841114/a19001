
define(['text!serviceMan.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("サービス管理");
        appView.html(main);
        var items = menuItem[parentId];
        if (items && items.length > 0) {
            var banHtml = "";
            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                var url = (node.curl + "").replace("User/", "");
                if (node.isShow == 0) {
                    banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                }
            }
            $("#itemCont").html(banHtml);
        }

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});