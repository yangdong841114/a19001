
define(['text!UWealthscheme.html', 'jquery'], function (UWealthscheme, $) {

    var controller = function (id) {

        //设置标题
        $("#title").html("富の仕組み");
        appView.html(UWealthscheme);


        utils.AjaxPostNotLoadding("/User/UWealthscheme/InitView", {}, function (result) {
            if (result.status == "success") {
                var dto = result.result;
                //$("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#contTitle").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});