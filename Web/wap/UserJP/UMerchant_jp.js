
define(['text!UMerchant.html', 'jquery'], function (UMerchant, $) {

    var controller = function (name) {

        //设置标题
        $("#title").html("加盟企業")

        var dto = undefined;
        var indexChecked = [0, 0, 0];
        var successMsg = "提出成功！バックグラウンドの審査を待ってください";

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            var disabled = false;
            $("#showAudit").css("display", "none");
            $("#btnDiv").css("display", "none");
            if (dto.flag == 1) {
                disabled = true;
                $("#showAudit").css("display", "block");
            } else {
                $("#btnDiv").css("display", "block");
                $("#saveBtn").html("変更");
                successMsg = "修正成功";

                if (dto.flag == 3)
                {
                    utils.showOrHiddenPromp();
                    $("#refuseReason").html(dto.refuseReason);
                }
            }
            $("#id").val(dto.id);
            $("input").each(function (index, ele) {
                this.disabled = disabled;
                if (dto[this.id]) {
                    $(this).val(dto[this.id]);
                }
            });
            document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';
            document.getElementById("showImgYyzz").style.backgroundImage = 'url(' + dto.imgYyzzUrl + ')';
            document.getElementById("showImgSfzzm").style.backgroundImage = 'url(' + dto.imgSfzzmUrl + ')';
            document.getElementById("showImgSfzfm").style.backgroundImage = 'url(' + dto.imgSfzfmUrl + ')';
        }

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //加载供应商信息
        utils.AjaxPostNotLoadding("/User/UMerchant/GetRecord", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                dto = result.result;

                appView.html(UMerchant);

                utils.AjaxPostNotLoadding("/User/UserWeb/GetSjht", {}, function (result) {
                    if (result.status == "success") {
                        var map = result.map;
                        $("#sjhtContent").html(map.sjht);
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });


                //台湾，日本，中国，美国，韩国，香港，新加坡，越南，菲律宾
                var gjdqList = [{ id: '台湾', value: "台湾" }, { id: '日本', value: "日本" }, { id: '中国', value: "中国" }, { id: '美国', value: "美国" }, { id: '韩国', value: "韩国" }, { id: '香港', value: "香港" }, { id: '新加坡', value: "新加坡" }, { id: '越南', value: "越南" }, { id: '菲律宾', value: "菲律宾" }];
                //初始化下拉框
                utils.InitMobileSelect('#contry', '國家地區', gjdqList, null, [0], null, function (indexArr, data) {
                    $("#contry").val(data[0].value);
                    //$("#accounttypeId").val(data[0].id);
                });

                ////省市区选择
                //var proSet = utils.InitMobileSelect('#province', '都道府県を選択する', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);
                //var citySet = utils.InitMobileSelect('#city', '都道府県を選択する', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);
                //var areaSet = utils.InitMobileSelect('#area', '都道府県を選択する', areaData, null, indexChecked, null, function (indexArr, data) {
                //    selectProvince(data);
                //    indexChecked = indexArr;
                //}, initPosition);

                //初始表单默认值
                if (dto) {
                    setDefaultFormValue(dto);
                }
                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });
                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });


                //预览图片
                $("#imgFile").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileYyzz").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgYyzz").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileSfzzm").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgSfzzm").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileSfzfm").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgSfzfm").style.backgroundImage = 'url(' + url + ')';
                });

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;

                    //if ($("#read")[0].checked == false) {
                    //    checked = false;
                    //    utils.showErrMsg("請勾選商家合同");
                    //} 

                    var formdata = new FormData();
                  
                    formdata.append("imgFile", $("#imgFile")[0].files[0]);
                    formdata.append("imgFileYyzz", $("#imgFileYyzz")[0].files[0]);
                    formdata.append("imgFileSfzzm", $("#imgFileSfzzm")[0].files[0]);
                    formdata.append("imgFileSfzfm", $("#imgFileSfzfm")[0].files[0]);

                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (this.id != "imgFile" && this.id != "imgFileYyzz" && this.id != "imgFileSfzzm" && this.id != "imgFileSfzfm")
                            formdata.append(this.id ,$(this).val());
                      
                        if (jdom.val() == 0 && jdom[0].id != "id" && jdom[0].id != "imgFile" && jdom[0].id != "imgUrl" && jdom[0].id != "imgFileYyzz" && jdom[0].id != "imgYyzzUrl" && jdom[0].id != "imgFileSfzzm" && jdom[0].id != "imgSfzzmUrl" && jdom[0].id != "imgFileSfzfm" && jdom[0].id != "imgSfzfmUrl" && jdom[0].id != "provinceName" && jdom[0].id != "cityName" && jdom[0].id != "areaName") {
                            utils.showErrMsg(jdom.attr("placeholder"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                    });
                    if (checked) {
                        utils.AjaxPostForFormData("/User/UMerchant/SaveOrUpdate", formdata, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(successMsg);
                                setDefaultFormValue(result.result)
                            }
                        });
                    }
                });
            }
        });


        //打开注册协议
        openMerchantContract = function (index) {
            $("#sjhtdiv").css("display", "block");
            $("#divzcsj").css("display", "none");
        }
        //同意并注册
        agreesjht = function (ind) {
            $("#sjhtdiv").css("display", "none");
            $("#divzcsj").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});