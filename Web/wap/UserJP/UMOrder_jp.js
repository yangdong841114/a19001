
define(['text!UMOrder.html', 'jquery'], function (UMOrder, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("オーダー管理")
        $("body").addClass("shopmallbody");
        appView.html(UMOrder);

        var statusDto = { 1: "出荷待ち", 2: "発送済み", 3: "完了しました", 4: "削除しました" }
        var dropload = null;
        var logList = null;
        var dtoList = {};
        //查询参数
        this.param = utils.getPageData();

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //删除订单
        deleteOrder = function (id) {
            $("#propTitle").html("注文を削除しますか？");
            $("#prompCont").empty();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UMOrder/DeleteOrder", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        searchMethod();
                        utils.showSuccessMsg("削除に成功しました！");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //订单发货
        logisticOrder = function (id, wlNo, logName, logNo) {
            logName = logName ? logName : "";
            wlNo = wlNo ? wlNo : "";
            logNo = logNo ? logNo : "";
            $("#propTitle").html("物流情報");
            $("#prompCont").empty();
            var html = '<dl><dt>物流会社</dt><dd><input type="text" value="' + logName + '" required="required" class="entrytxt" id="logName" name="logName" placeholder="物流会社を選んでください" readonly />' +
                       '<span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                       '<dl><dt>物流番号</dt><dd><input type="text" required="required" value="' + logNo + '" class="entrytxt" id="logNo" name="logNo" placeholder="物流番号を入力してください" />' +
                       '<span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl><input type="hidden" id="wlNo" value="' + wlNo + '"/>';
            $("#prompCont").html(html);

            utils.InitMobileSelect('#logName', '物流業者', logList, { id: 'logNo', value: 'logName' }, [0], null, function (indexArr, data) {
                $("#wlNo").val(data[0].logNo);
                $("#logName").val(data[0].logName);
            })

            //取消按钮绑定
            utils.CancelBtnBind();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#logName").val() == 0) { utils.showErrMsg("物流会社を選んでください"); }
                else if ($("#logNo").val() == 0) { utils.showErrMsg("物流番号を記入してください"); }
                else {
                    var data = { "logNo": $("#logNo").val(), "id": id, "logName": $("#logName").val(), "wlNo": $("#wlNo").val() }
                    utils.AjaxPost("/User/UMOrder/LogisticOrder", data, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "success") {
                            utils.showSuccessMsg("出荷成功！");
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            });
            utils.showOrHiddenPromp();
        }

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UOrderItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["id"] = $("#id").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        };

        //查看物流
        showLogistic = function (id) {
            var dto = dtoList[id];
            $("#mshdz").html(dto.provinceName + "" + dto.cityName + "" + dto.areaName + "" + dto.address);
            $("#mshdh").html(dto.phone);
            $("#mshr").html(dto.receiptName);
            $("#mwlgs").html(dto.logName);
            $("#mshdh").html(dto.logNo);

            utils.AjaxPostNotLoadding("/User/UMOrder/GetLogisticMsg?id=" + id, {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#LogDetailMsg").empty();
                    var dto = result.result;

                    var html = "";
                    if (dto.list && dto.list.length > 0) {
                        var len = dto.list.length;
                        for (var i = len - 1; i >= 0; i--) {
                            var vo = dto.list[i];
                            var datetime = utils.getWlDatetime(vo.datetime);
                            html += "<li><time>" + datetime[0] + "<br />" + datetime[1] + "</time><span></span><p>" + vo.remark + "</p></li>";
                        }
                    } else {
                        html += "<li><time></time><span></span><p>物流情報はまだありません</p></li>";
                    }
                    $("#LogDetailMsg").html(html);
                    $(".promptbg").toggleClass("promptbottom");
                    $(".promtotal").toggleClass("promptbottom");
                }
            });
        }

        utils.AjaxPostNotLoadding("/User/UMOrder/GetLogisticList", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                //物流商
                logList = result.map.logList;


                //绑定展开搜索更多
                utils.bindSearchmoreClick();
                //初始化日期选择框
                utils.initCalendar(["startTime", "endTime"]);

                //隐藏提示框
                $(".hidetotal").click(function () {
                    $(".promptbg").toggleClass("promptbottom");
                    $(".promtotal").toggleClass("promptbottom");
                });

                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //分页插件
                dropload = $('#UOrderDatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData('/User/UMOrder/GetListPage', param, me,
                            function (rows) {
                                var html = "";
                                var maxCount = 0;
                                for (var i = 0; i < rows.length; i++) {
                                    var maxPrice = 0;
                                    var dto = rows[i];
                                    var items = dto.items;
                                    if (items && items.length > 0) {

                                        for (var j = 0; j < items.length; j++) {
                                            var item = items[j]
                                            maxPrice += item.total;
                                        }
                                    }
                                    rows[i]["orderDate"] = utils.changeDateFormat(rows[i]["orderDate"]);
                                    rows[i]["flag"] = rows[i].status;
                                    rows[i].status = statusDto[rows[i].status];
                                    rows[i].typeId = '消費注文書';
                                    rows[i].logName = rows[i].logName == null ? '' : rows[i].logName;
                                    rows[i].logNo = rows[i].logNo == null ? '' : rows[i].logNo;
                                    rows[i].address = rows[i].provinceName + rows[i].cityName + rows[i].areaName + rows[i].address;
                                    dtoList[dto.id] = dto;
                                    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)">注文番号：<span>' + dto.id + '</span>';

                                    if (dto.status == "已完成") {
                                        html += '<span class="status ship">' + dto.status + '</span>';
                                    } else {
                                        html += '<span class="status noship">' + dto.status + '</span>';
                                    }
                                    html += '<i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo"><div class="orderinfo" style="font-size:1.4rem;">';
                                    html += '</div><div class="cartlist">';
                                    items = dto.items;
                                    if (items && items.length > 0) {
                                        for (var j = 0; j < items.length; j++) {
                                            var item = items[j]
                                            maxPrice += item.total;
                                            html += '<dl><dt style="padding-top:1.4rem;"><img src="' + item.productImg + '" /></dt>' +
                                            '<dd><div class="cartcomminfo" style="font-size:1.4rem;">' +
                                            '<h2>' + item.productName + '</h2>' +
                                            '<h3>￥' + item.total + '</h3>' +
                                            '<dl><dt>購入数量</dt><dd><span>' + item.num + '</span></dd></dl>' +
                                            '<dl><dt>金額</dt><dd><span>￥' + item.total + '</span></dd></dl>' +
                                            '</div></dd></dl>';

                                        }
                                    }
                                    html += '<div class="ordertotal">' +
                                    '<p class="fleft">共に<span>' + items.length + '</span>商品の種類</p>' +
                                    '<p class="fleft">手数料：<span>' + dto.sxf + ' Tod</span></p>';
                                    if (dto.flag == 1) {
                                        html += '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="deleteOrder(\'' + dto.id + '\')">削除</a></p>' +
                                  '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="logisticOrder(\'' + dto.id + '\')">商品を発送する</a></p>';
                                        //html += '<li><button class="sdelbtn" onclick="deleteOrder(\'' + dto.id + '\')">删除</button></li>' +
                                        //'<li><button class="sclreabtn" onclick="logisticOrder(\'' + dto.id + '\')">发货</button></li>';
                                    } else if (dto.flag == 2) {
                                        html += '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="logisticOrder(\'' + dto.id + '\',\'' + dto.wlNo + '\',\'' + dto.logName + '\',\'' + dto.logNo + '\')">物流の見直し</a></p>' +
                                  '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">物流を見る</a></p>';
                                        //html += '<li><button class="sclreabtn" onclick="logisticOrder(\'' + dto.id + '\',\'' + dto.wlNo + '\',\'' + dto.logName + '\',\'' + dto.logNo + '\')">修改物流</button></li>' +
                                        //        '<li><button class="sdelbtn" onclick="showLogistic(\'' + dto.id + '\')">查看物流</button></li>';
                                    } else {
                                        html += '<p class="fright"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">物流を見る</a></p>';
                                    }
                                    html += '</div></div></div></li>';
                                }
                                $("#UOrderItemList").append(html);
                            }, function () {
                                $("#UOrderItemList").append('<p class="dropload-noData">データがありません</p>');
                            });
                    }
                });

                //查询按钮
                $("#searchBtn").on("click", function () {
                    searchMethod();
                })
            }
        });


        controller.onRouteChange = function () {
            $("body").removeClass("shopmallbody");
        };
    };

    return controller;
});