
define(['text!UChangePassword.html', 'jquery'], function (UChangePassword, $) {

    var controller = function (name) {

        var tab2Init = false;
        var tab3Init = false;

        //修改活動樣式
        changeClass = function (index) {
            $("#litab1").removeClass("active");
            $("#litab2").removeClass("active");
            $("#litab3").removeClass("active");
            document.getElementById("passDiv1").style.display = "none";
            document.getElementById("passDiv2").style.display = "none";
            document.getElementById("passDiv3").style.display = "none";
            document.getElementById("passDiv" + index).style.display = "block";
            $("#litab" + index).addClass("active");
        }

        //保存數據
        saveData = function (oldPass, newPass, flag, cls) {
            var data = { oldPass: oldPass, newPass: newPass, flag: flag };
            utils.AjaxPost("/User/UChangePassword/ChangePassword", data, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg("修正成功！");
                    //清空表單
                    $("." + cls).each(function (index, ele) {
                        $(this).val("");
                    });
                }
            });
        }

        //選項卡切換
        changeTab = function (index) {
            changeClass(index);
            if (index == 1) {
                $("#title").html("ログインパスワードを変更");
            } else if (index == 2) {
                $("#title").html("セキュリティパスワードの変更");
                if (!tab2Init) {
                    //綁定修改安全密碼按鈕
                    $("#changBtn2").on('click', function () {
                        if ($("#oldPassOpen").val() == 0) {
                            utils.showErrMsg("古いセキュリティパスワードは空にできません");
                        } else if ($("#passOpen").val() == 0) {
                            utils.showErrMsg("新しいセキュリティパスワードは空にできません");
                        } else if ($("#passOpen").val() != $("#passOpenRe").val()) {
                            utils.showErrMsg("新しいパスワードと新しいセキュリティパスワードが違っていることを確認します");
                        } else {
                            saveData($("#oldPassOpen").val(), $("#passOpen").val(), 2, "pss2");
                        }
                    });
                    tab2Init = true;
                }
            } else {
                $("#title").html("取引のパスワードを変更");
                if (!tab3Init) {
                    //綁定修改交易密碼按鈕
                    $("#changBtn3").on('click', function () {
                        if ($("#oldThreepass").val() == 0) {
                            utils.showErrMsg("古い取引のパスワードは空です");
                        } else if ($("#threepass").val() == 0) {
                            utils.showErrMsg("新規取引のパスワードは空にできません");
                        } else if ($("#threepass").val() != $("#threepassRe").val()) {
                            utils.showErrMsg("新しいパスワードと新しい取引のパスワードが違っていることを確認します");
                        } else {
                            saveData($("#oldThreepass").val(), $("#threepass").val(), 3, "pss3");
                        }
                    });
                    tab3Init = true;
                }
            }
        }

        //設置標題
        $("#title").html("ログインパスワードを変更")
        appView.html(UChangePassword);


        utils.CancelBtnBind();


        //修改登錄密碼
        $("#changBtn1").on('click', function () {
            if ($("#oldPassword").val() == 0) {
                utils.showErrMsg("古いログインパスワードは空にできません");
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("新しいログインパスワードは空にできません");
            } else if ($("#password").val() != $("#passwordRe").val()) {
                utils.showErrMsg("新しいパスワードと新しいログインパスワードが違っていることを確認します");
            } else {
                saveData($("#oldPassword").val(), $("#password").val(), 1, "pss1");
            }
        });





        controller.onRouteChange = function () {
        };
    };

    return controller;
});