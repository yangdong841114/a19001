
define(['text!shopMan_jp.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("モール");


        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                var map = result.map;
               
             
                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("User/", "");
                        if (node.isShow == 0) {
                            banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                        }
                    }
                    $("#itemCont").html(banHtml);
                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});
