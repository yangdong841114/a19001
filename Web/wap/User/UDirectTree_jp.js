
define(['text!UDirectTree_jp.html', 'jquery'], function (UDirectTree, $) {

    var controller = function (name) {
        $("#title").html("直進図");
        //展开
        openAndClose = function (id, recount) {
            if (recount>0  && !$("#cont" + id).attr("isload")) {
                utils.AjaxPost("/User/UDirectTree/GetAppDirectTree", { uid: id }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        var dto = result.result;
                        var childs = dto.reList;
                        var html = "";
                        if (childs && childs.length > 0) {
                            html += "<dl>";
                            for (var i = 0; i < childs.length; i++) {
                                var child = childs[i];
                                html += '<dt><a id="item' + child.id + '" href="javascript:openAndClose(\'' + child.id + '\',' + child.reCount + ');">';
                                if (child.reCount > 0) {
                                    html += '<span class="adult"></span>';
                                } else {
                                    html += '<span></span>';
                                }
                                html += '<i class="fa fa-user-circle-o"></i>' + child.dtreeName + '</a></dt><dd id="cont' + child.id + '">';
                                html += '</dd>'
                            }
                            html += "</dl>";
                            $("#cont" + id).html(html);
                        }
                        $("#item" + id).parent().toggleClass("bg");
                        $("#item" + id).parent().next().slideToggle();
                        $("#cont" + id).attr("isload", "true");
                    }
                });
            } else {
                $("#item" + id).parent().toggleClass("bg");
                $("#item" + id).parent().next().slideToggle();
            }
        }

        //初始化
        initTree = function (dto) {
            $("#usertree").empty();
            var childs = dto.reList;
            var html = '<dl><dt><a id="item' + dto.id + '" href="javascript:openAndClose(\'' + dto.id + '\',' + dto.reCount + ');">'
            if (dto.reCount > 0) {
                html += '<span  class="adult"></span>';
            }
            html += '<i class="fa fa-user-circle-o"></i>' + dto.dtreeName + '</a></dt><dd id="cont' + dto.id + '" isload="true">';
            if (childs && childs.length > 0) {
                html += "<dl>";
                for (var i = 0; i < childs.length; i++) {
                    var child = childs[i];
                    html += '<dt><a id="item' + child.id + '" href="javascript:openAndClose(\'' + child.id + '\',' + child.reCount + ');">';
                    if (child.reCount > 0) {
                        html += '<span class="adult"></span>';
                    }
                    html += '<i class="fa fa-user-circle-o"></i>' + child.dtreeName + '</a></dt><dd id="cont' + child.id + '">';
                    html += '</dd>'
                }
                html += "</dl>";
            }
            html += '</dd></dl>'
            $("#usertree").html(html);
        }

        //初始化加载数据
        utils.AjaxPostNotLoadding("/User/UDirectTree/GetAppDirectTree", { uid: 0 }, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UDirectTree);
                var dto = result.result;
                initTree(dto);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    utils.AjaxPost("/User/UDirectTree/GetAppDirectTreeByUserId", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            if (result.msg == "none") {
                                $("#usertree").empty();
                            } else {
                                var dto = result.result;
                                initTree(dto);
                            }
                        }
                    });
                })

                //我的直推图
                $("#selfBtn").on("click", function () {
                    utils.AjaxPost("/User/UDirectTree/GetAppDirectTree", { uid: 0 }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            var dto = result.result;
                            initTree(dto);
                        }
                    });
                })
            }

        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});