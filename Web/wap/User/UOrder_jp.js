
define(['text!UOrder_jp.html', 'jquery'], function (UOrder, $) {

    var controller = function (name) {
        //設置標題
        $("#title").html("私の注文")
        $("body").addClass("shopmallbody");
        appView.html(UOrder);

        var statusDto = { 1: "出荷待ち", 2: "発送済み", 3: "完了しました", 4: "キャンセルしました" }

        //清空查詢條件按鈕
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //綁定展開搜索更多
        utils.bindSearchmoreClick();
        //初始化日期選擇框
        utils.initCalendar(["startTime", "endTime"]);

        //查詢參數
        this.param = utils.getPageData();
        var dtoList = {};

        //隱藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //確認收貨
        sureReceive = function (id) {
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                var data = { "id": id }
                utils.AjaxPost("/User/UOrder/SaveSureReceive", data, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("商品の受け取りに成功した！");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查看物流
        showLogistic = function (id) {
            var dto = dtoList[id];
            $("#mshdz").html(dto.provinceName + "" + dto.cityName + "" + dto.areaName + "" + dto.address);
            $("#mshdh").html(dto.phone);
            $("#mshr").html(dto.receiptName);
            $("#mwlgs").html(dto.logName);
            $("#mshdh").html(dto.logNo);

            utils.AjaxPostNotLoadding("/User/UMOrder/GetLogisticMsg?id=" + id, {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#LogDetailMsg").empty();
                    var dto = result.result;

                    var html = "";
                    if (dto.list && dto.list.length > 0) {
                        var len = dto.list.length;
                        for (var i = len - 1; i >= 0; i--) {
                            var vo = dto.list[i];
                            var datetime = utils.getWlDatetime(vo.datetime);
                            html += "<li><time>" + datetime[0] + "<br />" + datetime[1] + "</time><span></span><p>" + vo.remark + "</p></li>";
                        }
                    } else {
                        html += "<li><time></time><span></span><p>物流情報はまだありません</p></li>";
                    }
                    $("#LogDetailMsg").html(html);
                    $(".promptbg").addClass("promptbottom");
                    $(".promtotal").addClass("promptbottom");
                    //$("#wuliudiv").show();
                    //$(".promptbg").show();
                }
            });
        }

        //分頁插件
        var dropload = $('#UOrderDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData('/User/UOrder/GetListPage', param, me,
                    function (rows) {
                        var html = "";
                        var maxCount = 0;
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            var maxPrice = 0;
                            var dto = rows[i];
                            var items = dto.items;
                            if (items && items.length > 0) {

                                for (var j = 0; j < items.length; j++) {
                                    var item = items[j]
                                    maxPrice += item.total;

                                }
                            }
                            rows[i]["orderDate"] = utils.changeDateFormat(rows[i]["orderDate"]);
                            rows[i]["flag"] = rows[i].status;
                            rows[i].status = statusDto[rows[i].status];
                            rows[i].typeId = '消費注文書';
                            rows[i].logName = rows[i].logName == null ? '' : rows[i].logName;
                            rows[i].logNo = rows[i].logNo == null ? '' : rows[i].logNo;
                            rows[i].address = rows[i].provinceName + rows[i].cityName + rows[i].areaName + rows[i].address;

                            dtoList[dto.id] = dto;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span>' + dto.id + '</span><span>$' + maxPrice + 'ドル</span>';

                            if (dto.status == "已完成") {
                                html += '<span class="status ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="status noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                             '<div class="allinfo"><div class="orderinfo" style="font-size:1.4rem;">';
                            html += '</div><div class="cartlist">';
                            items = dto.items;

                            //圖片插件變量
                            var lightboxId = "lightbox" + i;
                            lightboxArray.push(lightboxId);

                            if (items && items.length > 0) {
                                for (var j = 0; j < items.length; j++) {
                                    var item = items[j]
                                    maxPrice += item.total;
                                    html += '<dl><dt style="padding-top:1.4rem;"><img src="' + item.productImg + '" /></dt>' +
                                    '<dd><div class="cartcomminfo" style="font-size:1.4rem;">' +
                                    '<h2>' + item.productName + '</h2>' +
                                    '<h3>$' + item.total + 'ドル</h3>' +
                                    '<dl><dt>購入数量</dt><dd><span>' + item.num + '</span></dd></dl>' +
                                    '<dl><dt>金額</dt><dd><span>$' + item.total + 'ドル</span></dd></dl>' +
                                    '</div></dd></dl>';
                                }
                            }
                            html += '<div class="ordertotal">' +
                              '<p class="fleft">共に<span>' + items.length + '</span>商品の種類<br>' +
                             '事業者名:<span>' + dto.mname + '</span><br>' +
                             '連絡電話:<span>' + dto.salephone + '</span><br>' +
                            '</p>';
                            if (dto.flag == 2) {
                                html += '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="sureReceive(\'' + dto.id + '\')">受入確認</a></p>' +
                                    '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">物流を見る</a></p>';
                            } else if (dto.flag == 3) {
                                html += '<p class="fright"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">物流を見る</a></p>';
                            }
                            html += '</div>';
                            html += '</div></div>';

                        }
                        $("#UOrderItemList").append(html);
                        //初始化圖片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UOrderItemList").append('<p class="dropload-noData">データがありません</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UOrderItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["id"] = $("#id").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        };

        //查詢按鈕
        $("#searchBtn").on("click", function () {
            searchMethod();
        })
        $("#closewuliu").on("click", function () {
            $(".promptbg").removeClass("promptbottom");
            $(".promtotal").removeClass("promptbottom");
        })
        controller.onRouteChange = function () {
            $("body").removeClass("shopmallbody");
        };
    };

    return controller;
});