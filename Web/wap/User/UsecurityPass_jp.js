
define(['text!UsecurityPass_jp.html', 'jquery'], function (UsecurityPass, $) {

    var controller = function (name) {

        $("#title").html("セキュリティ設定")
        appView.html(UsecurityPass);
        var isFrist = "Y";
        var selectInit = false;

        utils.CancelBtnBind();

        var questionList = $.extend(true, [], cacheList["question"]);
        utils.InitMobileSelect('#question', '秘密保護の問題', questionList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#question").val(data[0].name);
        })

        InitView = function (result) {
            isFrist = result.msg;
            if (isFrist == "Y") {
                $("#oldDiv").css("display", "none");
                $("#fristQuestion").css("display", "block");
                $("#oldQuestion").css("display", "none");
            } else {
                $("#questionVal").html(result.other);
                $("#oldDiv").css("display", "block");
                $("#fristQuestion").css("display", "none");
                $("#oldQuestion").css("display", "block");
            }
        }


        utils.AjaxPostNotLoadding("/User/UsecurityPass/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                InitView(result);
                //修密保答案
                $("#saveBtn").on('click', function () {
                    var checked = true;
                    var data = { answer: $("#answer").val() };
                    if ($("#answer").val() == 0) {
                        utils.showErrMsg($("#answer").attr("emptymsg"));
                    } else if (isFrist == "Y" && $("#question").val() == 0) {
                        utils.showErrMsg($("#question").attr("emptymsg"));
                    } else if (isFrist != "Y" && $("#oldAnswer").val() == 0) {
                        utils.showErrMsg($("#oldAnswer").attr("emptymsg"));
                    } else {
                        if (isFrist == "Y") {
                            data["question"] = $("#question").val();
                        } else {
                            data["oldAnswer"] = $("#oldAnswer").val();
                        }
                        utils.AjaxPost("/User/UsecurityPass/SaveSecurityPass", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("設定成功！");
                                //清空表单
                                $(".entrytxt").each(function (index, ele) {
                                    $(this).val("");
                                });
                                InitView(result);
                            }
                        });
                    }
                });
            }
        });




        controller.onRouteChange = function () {
        };
    };

    return controller;
});