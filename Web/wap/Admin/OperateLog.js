
define(['text!OperateLog.html', 'jquery'], function (OperateLog, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("操作日志")
        appView.html(OperateLog);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        

        //绑定展开搜索更多
        utils.bindSearchmoreClick();


        utils.AjaxPostNotLoadding("/Admin/OperateLog/GetListType", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var isrecordNameList = [{ id: 0,name: "全部"}];
                for (var i = 0; i < result.rows.length; i++) {
                    var dto = result.rows[i].recordName;
                    isrecordNameList.push({ id: dto, name: dto });
                }
                //业务类型选择框
                utils.InitMobileSelect('#recordName', '选择业务类型', isrecordNameList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
                    $("#recordName").val(data[0].name);
                    $("#isrecordName").val(data[0].id);
                });
            }
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#OperateLogdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/OperateLog/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)" style="overflow: hidden"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '<span class="sum">' + dto.recordName + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>操作人</dt><dd>' + dto.userId + '</dd></dl><dl><dt>姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>操作时间</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>IP</dt><dd>' + dto.ipAddress + '</dd></dl>' +
                            '<dl><dt>业务类型</dt><dd>' + dto.recordName + '</dd></dl><dl><dt>业务摘要</dt><dd>' + dto.mulx + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#OperateLogitemList").append(html);
                    }, function () {
                        $("#OperateLogitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#OperateLogitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userId"] = $("#userId").val();
            if ($("#isrecordName").val() != 0) {
                param["recordName"] = $("#isrecordName").val();
            }
            else {
                param["recordName"] = "";
            }

            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});