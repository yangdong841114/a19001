﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class OperateLogController : Controller
    {
        public IOperateLogBLL opBll { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(OperateLog model)
        {
            Member current = (Member)Session["LoginUser"];
            PageResult<OperateLog> result = opBll.GetListPage(model, current);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询业务类型
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListType()
        {
            Member current = (Member)Session["LoginUser"];
            PageResult<OperateLog> result = opBll.GetListType(current);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

    }
}
