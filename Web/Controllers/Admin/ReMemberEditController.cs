﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 修改会员关系Controller
    /// </summary>
    public class ReMemberEditController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }
        //修改推荐关系业务处理接口
        public IReMemberEditBLL rmeBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="ReMemberEdit">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(ReMemberEdit model)
        {
            PageResult<ReMemberEdit> result = rmeBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 获取会员信息
        /// </summary>
        /// <param name="userId">会员编号</param>
        /// <returns></returns>
        public JsonResult GetModelMsg(string userId)
        {
            ResponseDtoMap<string, string> response = new ResponseDtoMap<string, string>("fail");
            if (userId == null)
            {
                response.msg = "会员编号为空";
            }
            try
            {
                Member mm = memberBLL.GetModelByUserId(userId);
                if (mm == null) { response.msg = "会员不存在"; }
                else
                {
                    Dictionary<string, string> da = new Dictionary<string, string>();
                    da.Add("userName", mm.userName);
                    da.Add("reName", mm.reName);
                    response.map = da;
                    response.status = "success";
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询会员出错，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存会员晋升记录
        /// </summary>
        /// <param name="model">保存对象</param>
        /// <returns></returns>
        public JsonResult Save(ReMemberEdit model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                rmeBLL.SaveReMemberEdit(model, current);
                response.msg = "保存成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "保存失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
