﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 账户充值Controller
    /// </summary>
    public class AccountUpdateController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        //账户充值业务处理接口
        public IAccountUpdateBLL apBLL { get; set; }
        public static AccountUpdate para_AccountUpdate_ExportExcel;

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="ShopSet">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(AccountUpdate model)
        {
            para_AccountUpdate_ExportExcel = model;
            PageResult<AccountUpdate> result = apBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 获取会员信息
        /// </summary>
        /// <param name="userId">会员编号</param>
        /// <returns></returns>
        public JsonResult GetModelMsg(string userId)
        {
            ResponseDtoMap<string, string> response = new ResponseDtoMap<string, string>("fail");
            if (userId == null)
            {
                response.msg = "会员编号为空";
            }
            try
            {
                Member mm = memberBLL.GetModelByUserId(userId);
                if (mm == null) { response.msg = "会员不存在"; }
                else
                {
                    Dictionary<string, string> da = new Dictionary<string, string>();
                    da.Add("userName", mm.userName);
                    response.map = da;
                    response.status = "success";
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "查询会员出错，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存账户充值记录
        /// </summary>
        /// <param name="model">保存对象</param>
        /// <returns></returns>
        public JsonResult Save(AccountUpdate model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                apBLL.UpdateMemberAcount(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 导出后台给会员充值记录excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportAccountUpdateExcel()
        {
            DataTable dt = apBLL.GetListPageDataTable(para_AccountUpdate_ExportExcel); //apBLL.GetAccountUpdateExcel();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("epoints", "充值金额");
            htb.Add("accountTypeName", "账户类型");
            htb.Add("addTime", "充值日期");
            htb.Add("addUser", "操作人");
            htb.Add("remark", "备注");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
        }

    }
}
