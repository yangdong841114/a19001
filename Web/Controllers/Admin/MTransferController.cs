﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 转账查询Controller
    /// </summary>
    public class MTransferController : Controller
    {
        public IMTransferBLL transferBLL { get; set; }
        public static MTransfer para_MTransfer_ExportExcel;

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(MTransfer model)
        {
            para_MTransfer_ExportExcel = model;
            PageResult<MTransfer> page = transferBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 导出转账excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportTransferExcel()
        {
            DataTable dt = transferBLL.GetListPageDataTable(para_MTransfer_ExportExcel); //transferBLL.GetTransferExcel();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("typeIdName", "转账类型");
            htb.Add("fromUserId", "转出账户");
            htb.Add("fromUserName", "会员姓名");
            htb.Add("toUserId", "转入账户");
            htb.Add("toUserName", "会员姓名");
            htb.Add("epoints", "转账金额");
            htb.Add("addTime", "转账日期");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
        }
    }
}
