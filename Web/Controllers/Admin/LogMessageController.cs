﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class LogMessageController : Controller
    {
        public ILogMessageBLL msgBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(LogMessage model)
        {
            PageResult<LogMessage> result = msgBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = msgBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 清空表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult RemoveAll()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                msgBLL.RemoveAll();
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
