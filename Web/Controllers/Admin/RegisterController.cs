﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class RegisterController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IProductBLL productBLL { get; set; }

        /// <summary>
        /// 分页查询商品
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetProductListPage(Product model)
        {
            if (model == null) { model = new Product(); }
            model.isShelve = 2;
            PageResult<Product> page = productBLL.GetListPage(model, "m.id,productCode,productName,m.imgUrl,price,m.addTime,isShelve");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取默认数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDefaultData(int uid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            try
            {
                response.map = memberBLL.GetRegisterDefaultMsg(uid);
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取注册金额
        /// </summary>
        /// <param name="ulevel"></param>
        /// <returns></returns>
        public JsonResult GetRegMoney(int ulevel)
        {
            ResponseDtoData response = new ResponseDtoData();
            try
            {
                object[] objs = memberBLL.GetRegMoneyAndNum(ulevel);
                response.msg = objs[0].ToString();
            }
            catch (Exception) { response.status = "fail"; response.msg = "出现错误，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 注册会员
        /// </summary>
        /// <param name="mb">会员信息</param>
        /// <param name="read">是否勾选注册协议</param>
        /// <returns></returns>
        public JsonResult RegisterMember(Member mb, string read)
        {
            //System.Threading.Thread.Sleep(3000);
            ResponseDtoData response = new ResponseDtoData("fail");
            if (mb == null) { response.msg = "表单信息为空"; }
            else if (read == null || read.Length == 0) { response.msg = "请先勾选注册协议"; }
            else
            {
                try
                {
                    Member re = memberBLL.GetModelByUserId(mb.reName);
                    if (re == null) { throw new ValidateException("推荐人不存在"); }
                    //找最左边那条线员的第1个直推会员是否必须放在自己系谱图最左边那条线的最下边
                    if (re.reCount == 0)
                    {
                        string father = memberBLL.GetLeftUserId(re.id.Value);
                        mb.fatherName = father;
                        mb.treePlace = -1;
                    }
                    Member mm = memberBLL.SaveMember(mb);
                    response.result = mm;
                    response.status = "success";
                }
                catch (ValidateException ex) { response.msg = ex.Message; }
                catch (Exception e) { response.msg = e.Message; }// "出现错误，请联系管理员"; }
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 检查用户编号
        /// </summary>
        /// <param name="userId">用户编码</param>
        /// <param name="flag">1:检查用户是否存在，2：检查接点人编码，3：检查推荐人编码，4，检查报单中心编码</param>
        /// <returns></returns>
        public JsonResult CheckUserId(string userId, int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                response = memberBLL.CheckUserId(userId, flag);
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "出现错误，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
