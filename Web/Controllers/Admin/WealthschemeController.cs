﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 财富方案Controller
    /// </summary>
    public class WealthschemeController : Controller
    {
        public INewsBLL newBLL { get; set; }

        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                News n = newBLL.GetModelByTypeId(ConstUtil.NEWS_CFFA);
                response.Success();
                response.result = n;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存财富方案
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveByUeditor(News model)
        {
            if (model == null) { model = new News(); }
            
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                model.typeId = ConstUtil.NEWS_CFFA; // 财富方案
                newBLL.SaveOrUpdate(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
