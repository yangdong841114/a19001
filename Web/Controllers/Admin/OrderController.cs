﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 订单处理Controller
    /// </summary>
    public class OrderController : Controller
    {
        public IOrderBLL orderBLL { get; set; }

        public ILogisticBLL lgBLL { get; set; }

        private static OrderHeader param;

        /// <summary>
        /// 分页查询订单
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(OrderHeader model)
        {
            if (model == null) { model = new OrderHeader(); }
            param = model;
            PageResult<OrderHeader> page = orderBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteOrder(string id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                orderBLL.DeleteOrder(id);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 订单发货
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public JsonResult LogisticOrder(List<OrderHeader> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                orderBLL.SaveLogistic(list);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询物流信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetLogisticMsg(string id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.result = orderBLL.GetLogisticMsg(id);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 获取物流信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetLogisticList()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                List<Logistic> list = lgBLL.GetList();
                di.Add("logList", list);
                response.map = di;
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <returns></returns>
        public ActionResult Print()
        {
            List<OrderHeader> list = orderBLL.GetList(param);
            ViewData["list"] = list;
            return View();
        }

        public ActionResult ExportXls()
        {
            DataTable dt = orderBLL.GetExcelList(param);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("id", "订单号");
            htb.Add("receiptName", "会员姓名");
            htb.Add("phone", "联系电话");
            htb.Add("address", "收货地址");
            htb.Add("productNum", "总数量");
            htb.Add("productMoney", "订单总金额");
            htb.Add("orderDate", "购买日期");
            htb.Add("productNumber", "商品编码");
            htb.Add("productName", "商品名称");
            htb.Add("num", "数量");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
            //string FilePath = Server.MapPath("~/Admin/excel/");
            //DataToExcel dte = new DataToExcel();
            //string filename = "";
            //try
            //{
            //    if (dt.Rows.Count > 0)
            //    {
            //        filename = dte.DataExcel(dt, "标题", FilePath, htb);
            //    }
            //    return File(FilePath + filename, "application/javascript", filename);
            //}
            //catch (Exception)
            //{
            //    dte.KillExcelProcess();
            //    throw;
            //}

        }
    }
}
