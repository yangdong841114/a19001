﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 奖金查询Controller
    /// </summary>
    public class CurrencyController : Controller
    {
        public ICurrencyBLL cyBll { get; set; }
        public static CurrencySum para_CurrencySum_ExportExcel;
        /// <summary>
        /// 分页查询（按日期汇总)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetAllSumCurrency(CurrencySum model)
        {
            para_CurrencySum_ExportExcel = model;//保存查询条件给excel
            PageResult<CurrencySum> result = cyBll.GetAllSumCurrency(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 分页查询（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetByUserSumCurrency(CurrencySum model)
        {
            para_CurrencySum_ExportExcel = model;//保存查询条件给excel
            PageResult<CurrencySum> result = cyBll.GetByUserSumCurrency(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询奖金明细记录（按某个会员)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetDetailListPage(Currency model)
        {
            PageResult<Currency> result = cyBll.GetDetailListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }


        //导出奖金excel
        public ActionResult ExportBonusExcel()
        {
            //DataTable dt = cyBll.GetBounsExcel();
            DataTable dt = cyBll.GetAllSumCurrencyDataTable(para_CurrencySum_ExportExcel);
            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("addDate", "结算日期");
            htb.Add("cat1", "直推奖");
            htb.Add("cat2", "循环对碰奖");
            htb.Add("cat3", "领导对等奖");
            htb.Add("cat4", "忠诚奖");
            htb.Add("cat5", "领袖分紅奖");
            htb.Add("cat6", "分销提成奖");
            htb.Add("yf", "应发");
        
            htb.Add("sf", "实发");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 导出奖金excel（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public ActionResult ExportBonusDetailExcel(CurrencySum model)
        {
            DataTable dt = cyBll.GetBounsDetailExcel(para_CurrencySum_ExportExcel);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addDate", "结算日期");
            htb.Add("cat1", "直推奖");
            htb.Add("cat2", "循环对碰奖");
            htb.Add("cat3", "领导对等奖");
            htb.Add("cat4", "忠诚奖");
            htb.Add("cat5", "领袖分紅奖");
            htb.Add("cat6", "分销提成奖");
            htb.Add("yf", "应发");

            htb.Add("sf", "实发");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 导出会员奖金明细excel（按用户汇总某天的数据)
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public ActionResult ExportUserBonusDetailExcel(CurrencySum model)
        {
            DataTable dt = cyBll.GetUserBonusDetailExcel(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("catName", "奖金名称");
            htb.Add("jstime", "结算日期");
            htb.Add("yf", "应发");
            htb.Add("fee1", "所得税");
            htb.Add("fee2", "管理费");
            htb.Add("fee3", "复消账户");
            htb.Add("sf", "实发");
            htb.Add("mulx", "业务摘要");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }
    }
}
