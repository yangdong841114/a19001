﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 菜单管理Controller
    /// </summary>
    public class ResourceController : Controller
    {
        public IResourceBLL recourceBLL { get; set; }

        /// <summary>
        /// 查询所有菜单资源
        /// </summary>
        /// <returns></returns>
        public JsonResult GetList()
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            try
            {
                List<Resource> result = recourceBLL.GetList("select * from Resource");
                response.list = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.status = "fail";
                response.msg = "加载菜单失败，请联系管理员";
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(Resource model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model == null || ValidateUtils.CheckNull(model.id)) { throw new ValidateException("操作对象为空"); }

                Resource r = null;

                //id＝"自动生成" =>新增，否则：更新
                if (model.id == "自动生成")
                {
                    r = recourceBLL.SaveResource(model);
                    response.msg = "添加成功";
                }
                else
                {
                    r = recourceBLL.UpdateResource(model);
                    response.msg = "更新成功";
                }
                response.result = r;
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除节点以及节点下的所有子节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(string id)
        {
            ResponseDtoData response = new ResponseDtoData();
            try
            {
                response.msg = recourceBLL.DelteResource(id);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        } 
    }
}
