﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 流水账Controller
    /// </summary>
    public class LiuShuiController : Controller
    {
        public IMemberAccountBLL accountBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }
        public static MemberAccount para_MemberAccount_ExportExcel;
        public static LiuShuiZhang para_LiuShuiZhang_ExportExcel;

        /// <summary>
        /// 分页查询会员账户余额
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetMemberAccountListPage(MemberAccount model)
        {
            para_MemberAccount_ExportExcel = model;//保存查询条件给excel
            PageResult<MemberAccount> page = accountBLL.GetListPage(model);
            MemberAccount total = accountBLL.GetTotalSummary(model);
            List<MemberAccount> list = new List<MemberAccount>();
            list.Add(total);
            page.footer = list;
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 分页查询流水账明细
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetLiushuiDetailListPage(LiuShuiZhang model)
        {
            para_LiuShuiZhang_ExportExcel = model;
            PageResult<LiuShuiZhang> page = liushuiBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取收入总金额和支出总金额
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTotalMoney(LiuShuiZhang model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model == null)
                {
                    model = new LiuShuiZhang();
                }
                response.msg = liushuiBLL.GetsrMoney(model) + "";         //收入总金额
                response.other = liushuiBLL.GetzcMoney(model) + "";    //支出总金额
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 修改记录余额
        /// </summary>
        /// <returns></returns>
        public JsonResult ReviseBalance(LiuShuiZhang model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                if (current.id == 2)
                {
                    int c = liushuiBLL.UpdateBalance(model,current);

                    response.Success();
                }
                else
                {
                    response.msg = "您无权修改该记录余额！";
                    response.Fail();
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "审核失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 导出流水excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportLiuShuiExcel()
        {
            //DataTable dt = accountBLL.GetLiuShuiExcel();
            DataTable dt = accountBLL.GetListPageDataTable(para_MemberAccount_ExportExcel);
            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("agentDhm", "兑换码");
            htb.Add("agentTod", "TOD币");
            htb.Add("agentFhz", "分红值");
            htb.Add("agentTocc", "TOCC币");
            htb.Add("agentDz", "点值");
            htb.Add("agentSl", "算力");
            htb.Add("agentBpc", "BPC");
            htb.Add("agentMoby", "MOBBY码");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
        }

        /// <summary>
        /// 导出用户流水excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportUserLiuShuiExcel(LiuShuiZhang model)
        {
            DataTable dt = liushuiBLL.GetUserLiuShuiExcel(para_LiuShuiZhang_ExportExcel);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("accountId", "业务类型");
            htb.Add("abst", "业务摘要");
            htb.Add("income", "收入金额");
            htb.Add("outlay", "支出金额");
            htb.Add("last", "余额");
            htb.Add("addtime", "添加时间");
            htb.Add("optype", "类型");
            htb.Add("addUser", "操作用户");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
        }

    }
}
