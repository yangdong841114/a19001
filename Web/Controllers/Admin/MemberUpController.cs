﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 会员晋升Controller
    /// </summary>
    public class MemberUpController : Controller
    {
        public IMemberUpBLL mupBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="MemberUp">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(MemberUp model)
        {
            PageResult<MemberUp> result = mupBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存会员晋升记录
        /// </summary>
        /// <param name="model">保存对象</param>
        /// <returns></returns>
        public JsonResult Save(MemberUp model, int isBackRound)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                if (model.cls == 1)
                {
                    mupBLL.SaveMemberUp(model, current, isBackRound);
                }
                else
                {
                    mupBLL.SaveRlevelUp(model, current);
                }
                response.msg = "保存成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "保存失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 获取会员信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetUser(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (ValidateUtils.CheckNull(userId))
                {
                    response.msg = "不存在";
                }
                else
                {
                    Member m = memberBLL.GetModelByUserId(userId);
                    if (m == null) { response.msg = "不存在"; }
                    else
                    {
                        Member rt = new Member();
                        rt.userName = m.userName;
                        rt.uLevel = m.uLevel;
                        rt.rLevel = m.rLevel;
                        response.result = rt;
                    }
                }
                response.Success();
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "获取失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
