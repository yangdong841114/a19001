﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 充值查询Controller
    /// </summary>
    public class ChargeController : Controller
    {
        public IChargeBLL chargeBLL { get; set; }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Charge model)
        {
            model.czType = "cz";
            PageResult<Charge> page = chargeBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPagesl(Charge model)
        {
            model.czType = "sl";
            PageResult<Charge> page = chargeBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 导出EXCEL
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportXls(Charge model)
        {
            DataTable dt = chargeBLL.GetExcelListPage(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员姓名");
            htb.Add("typeId", "充值帐户");
            htb.Add("fromBank", "汇出银行");
            htb.Add("bankTime", "汇款时间");
            htb.Add("epoints", "充值金额");
            htb.Add("toBank", "汇入银行");
            htb.Add("bankCard", "银行卡号");
            htb.Add("bankUser", "开户名");
            htb.Add("addTime", "充值日期");
            htb.Add("status", "状态");

            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 审核申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult AuditTakeCash(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = chargeBLL.UpdateAudit(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "审核失败，请联系管理员！"; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 取消申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelTakeCash(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = chargeBLL.UpdateCancel(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 导出转账excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportChargeExcel()
        {
            DataTable dt = chargeBLL.GetChargeExcel();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员姓名");
            //htb.Add("typeId", "充值帐户");
            htb.Add("fromBank", "汇出银行");
            htb.Add("bankTime", "汇款时间");
            htb.Add("epoints", "充值金额");
            htb.Add("toBank", "汇入银行");
            htb.Add("bankCard", "银行卡号");
            htb.Add("bankUser", "开户名");
            htb.Add("addTime", "充值日期");
            htb.Add("status", "状态");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");
            string filename = "";
            try
            {
                if (dt.Rows.Count > 0)
                {
                    //CreateExcel(dtexcel, "application/ms-excel", excel);
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                }
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
        }
    }
}
