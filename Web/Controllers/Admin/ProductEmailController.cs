﻿using BLL;
using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Admin.Controllers
{
    public class ProductEmailController : Controller
    {
        public IEmailBoxBLL emailBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        /// <summary>
        /// 分页查询发件箱
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetSendListPage(EmailBox model)
        {
            //默认查询id=1会员记录
            if (model == null) { model = new EmailBox(); }
            model.fromUid = 1;
            model.type = 99;//产品答疑
            PageResult<EmailBox> result = emailBLL.GetSendListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 分页查询收件箱
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetReceiveListPage(EmailBox model)
        {
            //默认查询id=1会员记录
            if (model == null) { model = new EmailBox(); }
            model.toUid = 1;
            model.type = 99;//产品答疑
            PageResult<EmailBox> result = emailBLL.GetReceiveListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询回复列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetReceiveList(EmailBox model)
        {
            ResponseDtoList<EmailBox> response = new ResponseDtoList<EmailBox>("fail");
            try
            {
                List<EmailBox> list = emailBLL.GetReceiveList(model);
                Member current = (Member)Session["LoginUser"]; //当前会员
                response.list = list;
                response.msg = current.userId;
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 发邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveSend(EmailBox model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                model.type = 99;//产品答疑
                //发送所有会员
                if (model.isAll != null && model.isAll == 2)
                {
                    string ss = emailBLL.SaveAllSend(model);
                }
                else
                {
                    //查询id=1会员记录
                    Member current = memberBLL.GetModelById(1);
                    int toId = emailBLL.SaveSend(model, current);

                    //消息提醒
                    if (toId > 1)
                    {
                        SystemMsg msg = new SystemMsg();
                        msg.isRead = 0;
                        msg.url = "#UEmailBox";
                        msg.msg = "您有新的邮件信息";
                        msg.toUid = toId;
                        msgBLL.Save(msg);
                    }
                }
                response.msg = "发送成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "发送失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 回复邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveReceive(EmailBox model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                model.type = 99;//产品答疑
                //查询id=1会员记录
                Member current = memberBLL.GetModelById(1);
                int toId = emailBLL.SaveReceive(model, current);

                //消息提醒
                if (toId > 1)
                {
                    SystemMsg msg = new SystemMsg();
                    msg.isRead = 0;
                    msg.url = "#UEmailBox";
                    msg.msg = "您有新的邮件信息";
                    
                    msg.toUid = toId;
                    msgBLL.Save(msg);
                }

                response.msg = "回复成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "回复失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除发件箱记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteSend(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前会员
                emailBLL.DeleteSend(id);
                response.msg = "删除成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "删除失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除收件箱记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteReceive(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                emailBLL.DeleteReceive(id);
                response.msg = "删除成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "删除失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 更新阅读状态
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Read(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                emailBLL.UpdateRead(id);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
