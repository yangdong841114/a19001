﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 直推图Controller
    /// </summary>
    public class DirectTreeController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 查询直推图
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetDirectTreeList(string userId)
        {
            ResponseDtoList<Member> response = new ResponseDtoList<Member>("fail");
            try
            {
                List<Member> list = memberBLL.GetDirectTreeList(userId);

                //直推图上线链路
                if (userId == null || userId == "")
                {
                    response.other = memberBLL.GetParentLinkString(2, 1);
                }
                else {
                    Member mm = memberBLL.GetModelByUserId(userId);
                    if (mm != null)
                    {
                        response.other = memberBLL.GetParentLinkString(2, mm.id.Value);
                    }
                }
                response.list = list;
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }

            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GetAppDirectTree(int uid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                if (uid == 0) { uid = 1; }
                response.other = memberBLL.GetParentLinkString(2, uid);
                Member result = memberBLL.GetDirectTree(uid, current);
                response.result = result;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }
            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GetAppDirectTreeByUserId(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int uid = 1;
                Member current = (Member)Session["LoginUser"];
                Member result = null;
                if (string.IsNullOrEmpty(userId))
                {
                    response.other = memberBLL.GetParentLinkString(2, uid);
                    result = memberBLL.GetDirectTree(uid, current);
                }
                else
                {
                    Member mm = memberBLL.GetModelByUserId(userId);
                    uid = mm.id.Value;
                    if (mm == null)
                    {
                        response.msg = "none";
                    }
                    else
                    {
                        response.other = memberBLL.GetParentLinkString(2, uid);
                        result = memberBLL.GetDirectTree(uid, current);
                    }
                }
                response.result = result;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }
            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


    }
}
