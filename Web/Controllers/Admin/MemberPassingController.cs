﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;



namespace Web.Admin.Controllers
{
    /// <summary>
    /// 待开通会员Controller
    /// </summary>
    public class MemberPassingController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 分页查询待开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null) {
                model = new Member();
            }
            model.isPay = 0;
            string fields = "id,reName,shopName,userId,userName,regMoney,uLevel,phone,addTime";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 开通会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <param name="flag">0:实单,1:空单</param>
        /// <returns></returns>
        public JsonResult OpenMember(int id,int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == 0) {
                response.msg = "开通会员为空";
            }
            else
            {
                try
                {
                    Member current = (Member)Session["LoginUser"];
                    memberBLL.OpenMember(id, flag, current);
                    response.msg = "开通成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == 0)
            {
                response.msg = "要删除的会员为空";
            }
            else
            {
                try
                {
                    memberBLL.DeleteById(id);
                    response.msg = "删除成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        //导出未开通会员excel
        public ActionResult ExportNotOpenExcel()
        {
            int type = 0;//未开通
            DataTable dt = memberBLL.GetMemberExcelList(type);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addTime", "注册日期");
            htb.Add("fatherName", "接点人编号");
            htb.Add("reName", "推荐人编号");
            htb.Add("shopName", "报单中心");
            htb.Add("uLevel", "会员等级");
            htb.Add("regMoney", "注册金额");
            htb.Add("phone", "联系电话");

            
            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath=Server.MapPath("~/Admin/excel/");
            
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename,htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }


    }
}
