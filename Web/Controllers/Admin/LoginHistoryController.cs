﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class LoginHistoryController : Controller
    {
        public ILoginHistoryBLL lhBll { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(LoginHistory model)
        {
            Member current = (Member)Session["LoginUser"]; //当前会员
            PageResult<LoginHistory> result = lhBll.GetListPage(model, current);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

    }
}
