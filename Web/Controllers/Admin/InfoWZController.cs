﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 公告管理Controller
    /// </summary>
    public class InfoWZController : Controller
    {
        public IInfoBLL infoBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListPage(Info model)
        {
            if (model == null) { model = new Info(); }
            model.type="wz"; //公告管理
            PageResult<Info> page = null;
            page = infoBLL.GetListPage(model, "id,userId,userName,llcount,plcount,dzcount,title,fltype,addTime,infocontent,infoJj,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageGG(Info model)
        {
            if (model == null) { model = new Info(); }
            model.type="gg"; //公告管理
            PageResult<Info> page = null;
            page = infoBLL.GetListPage(model, "id,userId,userName,llcount,plcount,dzcount,title,fltype,addTime,linkUrl,imgUrl,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageSP(Info model)
        {
            if (model == null) { model = new Info(); }
            model.type = "sp"; //公告管理
            PageResult<Info> page = null;
            page = infoBLL.GetListPage(model, "id,userId,userName,llcount,plcount,dzcount,title,infoJj,addTime,linkUrl,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Info n = infoBLL.GetModelById(id);
                response.Success();
                response.result = n;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                infoBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletePl(List<int> ids, int isLock)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (ids == null || ids.Count == 0)
            {
                response.msg = "要操作的会员为空";
            }
            else
            {
                try
                {
                    int c = infoBLL.DeletePl(ids);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveByUeditor(Info model)
        {
            if (model == null) { model = new Info(); }
            
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                model.type = "wz"; 
                if (model.id != null)
                {
                    infoBLL.Update(model, mb);
                }
                else
                {
                    model.isTop = 1;
                    model.dpcount = 0;
                    model.dzcount = 0;
                    model.imgBigUrl = "";
                    model.imgUrl = "";
                    model.llcount = 0;
                    model.plcount = 0;
                    infoBLL.Save(model, mb);
                }
                
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult SaveTop(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                infoBLL.SaveTop(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelTop(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                infoBLL.SaveCancelTop(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveGG(Info model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member current = (Member)Session["LoginUser"];
                model.type = "gg";
                if (model.id != null)
                {
                    if (imgFile!=null)
                    model.imgUrl = UploadImg(imgFile);
                    model.title = "GG";
                    model.infocontent = "GG";
                    infoBLL.Update(model, current);
                }
                else
                {
                    //新增商品必须上传图片
                    model.imgUrl = UploadImg(imgFile);
                    model.isTop = 1;
                    model.dpcount = 0;
                    model.dzcount = 0;
                    model.infocontent = "GG";
                    model.title = "GG";
                    model.llcount = 0;
                    model.plcount = 0;
                    model.id = infoBLL.Save(model, current);
                }
                    
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult SaveSP(Info model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member current = (Member)Session["LoginUser"];
                model.type = "sp";
                model.imgUrl = "";
                model.isTop = 1;
                model.dpcount = 0;
                model.dzcount = 0;
                model.infocontent = "sp";
                model.llcount = 0;
                model.plcount = 0;
                model.id = infoBLL.Save(model, current);

                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img)
        {
            if (img == null) { throw new ValidateException("请上传商品图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = "InfoGG_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/banner/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/banner/" + endUrl;
        }


    }
}
