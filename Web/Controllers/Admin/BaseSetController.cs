﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 基础设置
    /// </summary>
    public class BaseSetController : Controller
    {
        public IBaseSetBLL setBLL { get; set; }


        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                BaseSet model = setBLL.GetModel(); //基础配置
                List<Banner> banners = setBLL.GetBannerList(); //广告图
                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("baseSet", model);
                di.Add("banners", banners);
                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存设置
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult Save(BaseSet model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = setBLL.SaveModel(model);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传广告图片
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UploadBanner(HttpPostedFileBase img,int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try{
                if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("广告图ID不能为空"); }
                if (img == null) { throw new ValidateException("请上传图片"); }
                string oldFileName = img.FileName;
                int lastIndex = oldFileName.LastIndexOf(".");
                string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
                if (!img.ContentType.StartsWith("image/"))
                {
                    throw new ValidateException("只能上传图片");
                }
                if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
                TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
                string endUrl = "banner" + id.ToString() + suffix;
                string newFileName = Server.MapPath("~/UpLoad/banner/") + endUrl;
                img.SaveAs(newFileName);
                Banner ban = new Banner();
                ban.id = id;
                ban.imgUrl = "/UpLoad/banner/" + endUrl;
                setBLL.SaveBanner(ban);
                response.msg = ban.imgUrl;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception e) { response.msg = "操作失败，请联系管理员！"+e.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
