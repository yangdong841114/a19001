﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 角色管理Controller
    /// </summary>
    public class RoleManageController : Controller
    {
        public IRoleBLL roleBLL { get; set; }

        public IResourceBLL resourceBLL { get; set; }

        /// <summary>
        /// 获取角色已分配的菜单
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        public JsonResult GetCheckedMenus(int roleId)
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>("fail");
            try
            {
                List<Resource> list = resourceBLL.GetRoleResource(roleId);
                response.status = "success";
                response.list = list;
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
            
        }

        /// <summary>
        /// 获取系统菜单
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenuMap()
        {
            //前台菜单
            List<Resource> list2 = resourceBLL.GetList("select * from Resource where id like '101%' or id like '102%' ");
            //后台菜单,根据当前登录用户获取
            //如果当前登录用户是超级管理员则获取所有菜单，否则只能获取已分配的菜单
            Member current = (Member)Session["LoginUser"];
            List<Resource> list1 = null;
            if (current.userId == ConstUtil.SUPER_ADMIN)
            {
                list1 = resourceBLL.GetList("select * from Resource where id like '100%' or id like '103%'");
            }
            else
            {
                list1 = resourceBLL.GetConstantParentListByUserAndParent(current.id.Value, "100");
                List<Resource> list11 = resourceBLL.GetConstantParentListByUserAndParent(current.id.Value, "103");
                for(var i=0;i<list11.Count;i++){
                    list1.Add(list11[i]);
                }
            }
            
            Dictionary<string, List<Resource>> di = new Dictionary<string, List<Resource>>();
            di.Add("before", list2);
            di.Add("after", list1);
            return Json(di, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Role model)
        {
            Member current = (Member)Session["LoginUser"]; //当前登录会员
            PageResult<Role> page = roleBLL.GetListPage(model, current);
            return Json(page, JsonRequestBehavior.AllowGet); 
        }

        /// <summary>
        /// 分页查询角色会员分配关系
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetRoleMemberPage(Role model)
        {
            Member current = (Member)Session["LoginUser"]; //当前登录会员
            PageResult<Role> page = roleBLL.GetRoleMemberPage(model, current);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 分页查询未分配roleId角色的会员
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetNotRoleMemberPage(Role model)
        {
            Member current = (Member)Session["LoginUser"]; //当前登录会员
            PageResult<Role> page = roleBLL.GetNotRoleMemberPage(model, current);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(Role model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model == null) { throw new ValidateException("操作对象为空"); }

                if (ValidateUtils.CheckIntZero(model.id))
                {
                    roleBLL.Save(model);
                    response.msg = "添加成功";
                }
                else
                {
                    roleBLL.Update(model);
                    response.msg = "更新成功";
                }
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData();
            try
            {
                response.msg = roleBLL.Delete(id);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除角色会员分配关系
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteRm(List<int> list)
        {
            ResponseDtoData response = new ResponseDtoData();
            try
            {
                response.msg = roleBLL.DeleteRm(list);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存角色会员关系
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public JsonResult SaveRm(List<Role> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                roleBLL.SaveRm(list);
                response.status = "success";
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存角色分配的菜单
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public JsonResult SaveRoleResorce(List<Role> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                roleBLL.SaveRoleResorce(list);
                response.status = "success";
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
