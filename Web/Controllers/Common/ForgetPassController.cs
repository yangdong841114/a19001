﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Text.RegularExpressions;


namespace Web.Common.Controllers
{
    /// <summary>
    /// 找回密码Controller
    /// </summary>
    public class ForgetPassController : Controller
    {
        public IBaseSetBLL setBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        /// <summary>
        /// 判断是否手机访问
        /// </summary>
        /// <returns></returns>
        private bool IsMobile()
        {
            return true;
            string str_u = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex b = new Regex(@"android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (!(b.IsMatch(str_u) || v.IsMatch(str_u.Substring(0, 4))))
            {
                //PC访问   
                return false;
            }
            else
            {
                //手机访问   
                return true;
            }
        }


        /// <summary>
        /// 进入忘记密码页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //判断网站是否关闭
            BaseSet set = setBLL.GetModel();
            if (set.isclose == 2)
            {
                ViewData["sitename"] = set.sitename;
                ViewData["copyright"] = set.copyright;
                Response.Redirect("/Home/Close");
                return null;
            }
            else
            {
                if (IsMobile())
                {
                    return View("~/Views/ForgetPass/AppIndex.aspx");
                }
                else
                {
                    return View();
                }
            }
        }


        /// <summary>
        /// 进入忘记密码页面
        /// </summary>
        /// <returns></returns>
        public ActionResult AppIndex1()
        {
            //判断网站是否关闭
            BaseSet set = setBLL.GetModel();
            if (set.isclose == 2)
            {
                ViewData["sitename"] = set.sitename;
                ViewData["copyright"] = set.copyright;
                Response.Redirect("/Home/Close");
                return null;
            }
            else
            {
                if (IsMobile())
                {
                    return View("~/Views/ForgetPass/AppIndex1.aspx");
                }
                else
                {
                    return View();
                }
            }
        }

        /// <summary>
        /// 验证找回的登录帐号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CheckUser(string id)
        {
            ResponseData response = new ResponseData("fail");
            if (id == null) { response.msg = "登录帐号错误"; }
            else
            {
                Member mb = memberBLL.GetModelByUserId(id);
                if (mb == null) { response.msg = "登录帐号错误"; }
                else
                {
                    //记录找回的帐号，只使用一次,标记通过了用户名验证
                    Session["forget_userId"] = mb;
                    response.Success();
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 校验手机验证码页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CheckCode()
        {
            //判断网站是否关闭
            BaseSet set = setBLL.GetModel();
            ViewData["sitename"] = set.sitename;
            ViewData["copyright"] = set.copyright;
            if (set.isclose == 2)
            {
                Response.Redirect("/Home/Close");
                return null;
            }
            else
            {
                object o = Session["forget_userId"];
                //未通过用户名验证的请求直接返回用户验证页面
                if (Session == null || o == null)
                {
                    Response.Redirect("/ForgetPass/Index");
                    return null;
                }
                else
                {
                    Member m = (Member)o;
                    Session["forget_phone"] = m.phone; //手机号码
                    Session["forget_uid"] = m.id;      //用户ID 
                    Session["forget_userId"] = null; // 清除标记
                    string p = m.phone.Substring(0, 3);
                    string p2 = m.phone.Substring(8, 13);//原来是8,3
                    Session["sendcodeTimes"] = null;
                    Session["getBackAnswer"] = m.answer; //密保答案
                    Session["getBackExistsAnswer"] = string.IsNullOrEmpty(m.answer)?"N":"Y";
                    ViewData["phone"] = p + "*****" + p2;
                    ViewData["question"] = DESEncrypt.DecryptDES(m.question, ConstUtil.SALT);
                    if (IsMobile())
                    {
                        return View("~/Views/ForgetPass/AppCheckCode.aspx");
                    }
                    else
                    {
                        return View();
                    }
                }
            }
        }

        /// <summary>
        /// 校验手机验证码页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CheckCode1()
        {
            //判断网站是否关闭
            BaseSet set = setBLL.GetModel();
            ViewData["sitename"] = set.sitename;
            ViewData["copyright"] = set.copyright;
            if (set.isclose == 2)
            {
                Response.Redirect("/Home/Close");
                return null;
            }
            else
            {
                object o = Session["forget_userId"];
                //未通过用户名验证的请求直接返回用户验证页面
                if (Session == null || o == null)
                {
                    Response.Redirect("/ForgetPass/AppIndex1");
                    return null;
                }
                else
                {
                    Member m = (Member)o;
                    Session["forget_phone"] = m.phone; //手机号码
                    Session["forget_uid"] = m.id;      //用户ID 
                    Session["forget_userId"] = null; // 清除标记
                    string p = m.phone.Substring(0, 3);
                    string p2 = m.phone.Substring(8, 3);
                    Session["sendcodeTimes"] = null;
                    Session["getBackAnswer"] = m.answer; //密保答案
                    Session["getBackExistsAnswer"] = string.IsNullOrEmpty(m.answer) ? "N" : "Y";
                    ViewData["phone"] = p + "*****" + p2;
                    ViewData["question"] = DESEncrypt.DecryptDES(m.question, ConstUtil.SALT);
                    if (IsMobile())
                    {
                        return View("~/Views/ForgetPass/AppCheckCode1.aspx");
                    }
                    else
                    {
                        return View();
                    }
                }
            }
        }

        /// <summary>
        /// 获取校验码
        /// </summary>
        /// <returns></returns>
        public JsonResult SendPhoneCode()
        {
            ResponseData response = new ResponseData("fail");
            object o = Session["forget_phone"];
            if (o != null)
            {
                object t = Session["sendcodeTimes"];
                DateTime now = DateTime.Now; //当前时间
                if (t == null || DateTime.Compare((DateTime)t, now) >= 0)
                {
                    //发送间隔120秒
                    DateTime next = now.AddSeconds(120); //120秒后可在再次发送
                    Session["sendcodeTimes"] = next;
                    BaseSet set = setBLL.GetModel();
                    string code = GetRandom();
                    Session["forget_phone_code"] = code;
                    string msg = "【" + set.sitename + "】验证码为" + code + "（切勿将验证码告知他人），请在页面中输入完成验证";
                    string phone = o.ToString();
                    noticeBLL.SendMessage(phone, msg);
                    response.Success();
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 校验手机验证码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public JsonResult ValidateCode(string code,string flag,string answer)
        {

            ResponseData response = new ResponseData("fail");
            if (flag == "1")
            {
                if (ValidateUtils.CheckNull(code)){response.msg = "验证码错误";}
                else
                {
                    object o = Session["forget_phone_code"];
                    if (o == null) { response.msg = "验证码错误"; }
                    else
                    {
                        string cc = o.ToString();
                        if (cc == code)
                        {
                            response.Success();
                            Session["forget_phone_code"] = null;
                            Session["validate_code"] = 1;
                            Session["getBackAnswer"] = null;
                            Session["getBackExistsAnswer"] = null;
                        }
                        else
                        {
                            response.msg = "验证码错误";
                        }
                    }
                }
            }
            else {
                string exists = Session["getBackExistsAnswer"].ToString();
                if (exists == "N") { response.msg = "该账号还未设置密保"; }
                else if (ValidateUtils.CheckNull(answer)) { response.msg = "密保答案错误"; }
                else if (DESEncrypt.EncryptDES(answer,ConstUtil.SALT) != Session["getBackAnswer"].ToString()) { response.msg = "密保答案错误"; }
                else
                {
                    response.Success();
                    Session["forget_phone_code"] = null;
                    Session["validate_code"] = 1;
                    Session["getBackAnswer"] = null;
                    Session["getBackExistsAnswer"] = null;
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private string GetRandom()
        {
            int iSeed = 10;
            Random ro = new Random(10);
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
            int iResult = ran.Next(999999);
            string s = iResult.ToString().PadLeft(6, '0');
            return s;
        }

        /// <summary>
        /// 进入修改密码页面
        /// </summary>
        /// <returns></returns>
        public ActionResult ToChange()
        {
            object o = Session["validate_code"];
            if (o != null && Convert.ToInt32(o) == 1)
            {
                Session["validate_code"] = null;
                if (IsMobile())
                {
                    return View("~/Views/ForgetPass/AppToChange.aspx");
                }
                else
                {
                    return View();
                } 
            }
            else
            {
                Response.Redirect("/ForgetPass/Index");
                return null;
            }
        }


        public ActionResult AppToChange1()
        {
            object o = Session["validate_code"];
            if (o != null && Convert.ToInt32(o) == 1)
            {
                Session["validate_code"] = null;
                if (IsMobile())
                {
                    return View("~/Views/ForgetPass/AppToChange1.aspx");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                Response.Redirect("/ForgetPass/Index");
                return null;
            }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="pass1"></param>
        /// <param name="pass2"></param>
        /// <param name="pass3"></param>
        /// <returns></returns>
        public ActionResult ChangePss(string pass1, string pass2, string pass3)
        {
            ResponseData response = new ResponseData("fail");
            try 
	        {	        
		        object o = Session["forget_uid"];
                if (o == null) { response.msg = "jump"; }
                else
                {
                    int uid = Convert.ToInt32(o);
                    Member m = new Member();
                    m.password = pass1;
                    m.passOpen = pass2;
                    m.threepass = pass3;
                    m.id = Convert.ToInt32(o);
                    memberBLL.UpdateForgetPass(m);
                    response.Success();
                }
	        }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "重置密码失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
