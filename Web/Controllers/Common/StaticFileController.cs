﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Common.Controllers
{
    /// <summary>
    /// 控制返回点js文件
    /// </summary>
    public class StaticFileController : Controller
    {

        /// <summary>
        /// 针对Admin文件夹下js请求返回
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public ActionResult AdminJs(string filename)
        {
            object o = Session["JS_Permission"];
            if(o!=null && o.ToString() == "NotPermission"){
                string path = Server.MapPath("~/Admin/NoPermission.js");
                return File(path, "application/javascript", "NoPermission.js");
            }else{
                string path = Server.MapPath("~/Admin/" + filename + ".js");
                return File(path, "application/javascript", filename + ".js");
            }
        }

        /// <summary>
        /// 针对User文件夹下js请求返回
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public ActionResult UserJs(string filename)
        {
            object o = Session["JS_Permission"];
            if (o != null && o.ToString() == "NotPermission")
            {
                string path = Server.MapPath("~/User/NoPermission.js");
                return File(path, "application/javascript", "NoPermission.js");
            }
            else
            {
                string path = Server.MapPath("~/User/" + filename + ".js");
                return File(path, "application/javascript", filename + ".js");
            }
        }

        /// <summary>
        /// 针对User文件夹下js请求返回
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public ActionResult UserJPJs(string filename)
        {
            object o = Session["JS_Permission"];
            if (o != null && o.ToString() == "NotPermission")
            {
                string path = Server.MapPath("~/UserJP/NoPermission.js");
                return File(path, "application/javascript", "NoPermission.js");
            }
            else
            {
                string path = Server.MapPath("~/UserJP/" + filename + ".js");
                return File(path, "application/javascript", filename + ".js");
            }
        }
    }
}
