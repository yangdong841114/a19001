﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Text.RegularExpressions;


namespace Web.Common.Controllers
{
    public class HomeController : Controller
    {
        public IResourceBLL resourceBLL { get; set; }
        public IBaseSetBLL setBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IParamSetBLL paramSetBLL { get; set; }

        public INewsBLL newsBLL { get; set; }

        public IProductBLL productBLL { get; set; }

        public IDataDictionaryBLL ddBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }

        /// <summary>
        /// 获取客户端IP地址
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        private string GetIP()
        {
            //如果客户端使用了代理服务器，则利用HTTP_X_FORWARDED_FOR找到客户端IP地址
            string userHostAddress = null;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                userHostAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
            }
            //否则直接读取REMOTE_ADDR获取客户端IP地址
            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            //前两者均失败，则利用Request.UserHostAddress属性获取IP地址，但此时无法确定该IP是客户端IP还是代理IP
            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = Request.UserHostAddress;
            }
            //最后判断获取是否成功，并检查IP地址的格式（检查其格式非常重要）
            if (!string.IsNullOrEmpty(userHostAddress) && IsIP(userHostAddress))
            {
                return userHostAddress;
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// 检查IP地址格式
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }


        /// <summary>
        /// List权限列表转Map
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetPermissionMap(List<Resource> list)
        {
            Dictionary<string, string> di = new Dictionary<string, string>();
            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    Resource r = list[i];
                    if (!ValidateUtils.CheckNull(r.curl))
                    {
                        if (!di.ContainsKey(r.curl))
                        {
                            di.Add(r.curl, r.id);
                        }
                    }
                }
            }
            return di;
        }


        public JsonResult AutoSlEmail()
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                if (Request.Url.Host.IndexOf("biopcc.com") != -1)//正式网址才请求POC转帐
                paramSetBLL.AutoSlEmail();
            }
            catch (ValidateException va) { response.msg = va.Message + " 失败"; }
            catch (Exception ex) { response.msg = "失败，请联系管理员！" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 进入PC前台登录页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string id)
        {

            string key = "remenber";

            //判断网站是否关闭

            BaseSet set = setBLL.GetModel();
            var str = set.WebCode;
            ViewData["WebCode"] = str;
            if (set.isclose == 2)
            {
                ViewData["msg"] = set.msg;
                ViewData["sitename"] = set.sitename;
                ViewData["copyright"] = set.copyright;
                return View("~/Views/Home/AppClose.aspx");
            }

            HttpCookie cookie = Request.Cookies[key];
            if (cookie != null && cookie.Value != null && cookie.Value.Length > 0)
            {
                string val = cookie.Value;

                string value = DESEncrypt.DecryptDES(val, ConstUtil.LOGIN_SALT);
                string[] v = value.Split('&');
                string ip = GetIP(); //获取IP
                if (v.Length >= 2 && v[1] == ip)
                {
                    string userId = v[0];
                    Member mm = memberBLL.GetModelByUserId(userId);
                    if (mm != null)
                    {
                        mm.passOpen = null;
                        mm.password = null;
                        mm.threepass = null;
                        mm.question = null;
                        mm.answer = null;
                        //获取用户前台权限
                        List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
                        Dictionary<string, string> di = GetPermissionMap(list);
                        mm.permission = di;
                        Session["MemberUser"] = mm;
                        Response.Redirect("/wap/User/index.html");
                        return null;
                    }
                    else
                    {

                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            else
            {
                if (id == "wantlogin") return View();
                else
                {
                    //模拟一个会员可以进入到商城首页
                    Member mm = memberBLL.GetModelByUserId("bioplusx");
                    mm.userId = "noexit";
                    //获取用户前台权限
                    List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
                    Dictionary<string, string> di = GetPermissionMap(list);
                    mm.permission = di;
                    Session["MemberUser"] = mm;
                    Response.Redirect("/wap/User/index.html#UInfo/10207");
                    return null;
                }
            }
        }

        //前台退出登录
        public ActionResult exitUserLogin()
        {
            Session["MemberUser"] = null;
            Session["WantLogin"] = "want";
            string key = "remenber";
            HttpCookie cookie = Request.Cookies[key];
            if (cookie != null)
            {
                cookie.Value = "";
                Response.Cookies.Add(cookie);
            }
            Response.Redirect("/Home/AppIndex/wantlogin");
            return null;
        }

        /// <summary>
        /// 进入手机端前台登录页
        /// </summary>
        /// <returns></returns>
        public ActionResult AppIndex(string id)
        {
            string key = "remenber";

            //判断网站是否关闭

            BaseSet set = setBLL.GetModel();
            var str= set.WebCode;
            ViewData["WebCode"] = str;
            if (set.isclose == 2)
            {
                ViewData["msg"] = set.msg;
                ViewData["sitename"] = set.sitename;
                ViewData["copyright"] = set.copyright;
                return View("~/Views/Home/AppClose.aspx");
            }

            HttpCookie cookie = Request.Cookies[key];
            if (cookie != null && cookie.Value != null && cookie.Value.Length > 0)
            {
                string val = cookie.Value;

                string value = DESEncrypt.DecryptDES(val, ConstUtil.LOGIN_SALT);
                string[] v = value.Split('&');
                string ip = GetIP(); //获取IP
                if (v.Length >= 2 && v[1] == ip)
                {
                    string userId = v[0];
                    Member mm = memberBLL.GetModelByUserId(userId);
                    if (mm != null)
                    {
                        mm.passOpen = null;
                        mm.password = null;
                        mm.threepass = null;
                        mm.question = null;
                        mm.answer = null;
                        //获取用户前台权限
                        List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
                        Dictionary<string, string> di = GetPermissionMap(list);
                        mm.permission = di;
                        Session["MemberUser"] = mm;
                        Response.Redirect("/wap/User/index.html");
                        return null;
                    }
                    else
                    {
                       
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            else
            {
                if (id == "wantlogin") return View();
                else
                { 
                //模拟一个会员可以进入到商城首页
                Member mm = memberBLL.GetModelByUserId("bioplusx");
                mm.userId = "noexit";
                //获取用户前台权限
                List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
                Dictionary<string, string> di = GetPermissionMap(list);
                mm.permission = di;
                Session["MemberUser"] = mm;
                Response.Redirect("/wap/User/index.html#UInfo/10207");
                return null;
                }
            }

        }



        /// <summary>
        /// 后台安全退出
        /// </summary>
        /// <returns></returns>
        public JsonResult ExitB()
        {
            ResponseData response = new ResponseData("fail");
            Session["LoginUser"] = null;
            response.Success();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 推广注册链接
        /// </summary>
        /// <returns></returns>
        public ActionResult Reg(string id)
        {
            //没有推荐人或推荐人不存在、或推荐人未开通，直接跳转到登录页面
            if (id == null)
            {
                Response.Redirect("/Home/Index");
                return null;
            }
            else
            {
                //检查推荐人是否存在、是否开通
                Member mem = memberBLL.GetModelByUserId(id);
                Member shp = mem;

                if (mem == null || mem.isPay == 0)
                {
                    Response.Redirect("/Home/Index");
                    return null;
                }
                else
                {
                    //报单中心
                    if (shp.isAgent != 2)
                    {
                        shp = ((Member)memberBLL.GetModelById(1));
                    }
                    ViewData["shopName"] = shp.userId;
                    ViewData["shopUserName"] = shp.userName;

                    //推荐人
                    ViewData["reName"] = mem.userId;
                    ViewData["reUserName"] = mem.userName;

                    BaseSet set = setBLL.GetModel();
                    //网站名称
                    ViewData["sitename"] = set.sitename + "-推广注册";
                    //版权信息
                    ViewData["copyright"] = set.copyright;


                    Dictionary<string, ParameterSet> ps = paramSetBLL.GetDictionaryByCodes("regPrice",
                            "userIdPrefix");
                    //注册协议
                    News n = newsBLL.GetModelByTypeId(ConstUtil.NEWS_ZCXY);
                    ViewData["zcxy"] = ""; //注册协议
                    if (n != null) { ViewData["zcxy"] = n.content; }

                    double price = Convert.ToDouble(ps["regPrice"].paramValue); //每单注册金额
                    ViewData["pk"] = 0; //普卡注册金额
                    ViewData["yk"] = 0; //银卡注册金额
                    ViewData["jk"] =0; //金卡注册金额
                    ViewData["userIdPrefix"] = ps["userIdPrefix"].paramValue; //用户名前缀
                }
                if (IsMobile())
                {
                    return View("~/Views/Home/AppReg.aspx");
                }
                else
                {
                    return View();
                }
            }
        }

        /// <summary>
        /// 推广注册链接
        /// </summary>
        /// <returns></returns>
        public ActionResult AppReg(string id)
        {
            //没有推荐人或推荐人不存在、或推荐人未开通，直接跳转到登录页面
            if (id == null)
            {
                Response.Redirect("/Home/Index");
                return null;
            }
            else
            {
                //检查推荐人是否存在、是否开通
                Member mem = memberBLL.GetModelByUserId(id);
                Member shp = mem;

                if (mem == null || mem.isPay == 0)
                {
                    Response.Redirect("/Home/Index");
                    return null;
                }
                else
                {
                    //报单中心
                    if (shp.isAgent != 2)
                    {
                        shp = ((Member)memberBLL.GetModelById(1));
                    }
                    ViewData["shopName"] = shp.userId;
                    ViewData["shopUserName"] = shp.userName;

                    //推荐人
                    ViewData["reName"] = mem.userId;
                    ViewData["reUserName"] = mem.userName;

                    BaseSet set = setBLL.GetModel();
                    //网站名称
                    ViewData["sitename"] = set.sitename + "-推广注册";
                    //版权信息
                    ViewData["copyright"] = set.copyright;


                    Dictionary<string, ParameterSet> ps = paramSetBLL.GetDictionaryByCodes("regPrice",
                            "userIdPrefix");
                    //注册协议
                    News n = newsBLL.GetModelByTypeId(ConstUtil.NEWS_ZCXY);
                    ViewData["zcxy"] = ""; //注册协议
                    if (n != null) { ViewData["zcxy"] = n.content; }

                    double price = Convert.ToDouble(ps["regPrice"].paramValue); //每单注册金额
                    ViewData["pk"] = 0; //普卡注册金额
                    ViewData["yk"] = 0; //银卡注册金额
                    ViewData["jk"] = 0; //金卡注册金额
                    ViewData["userIdPrefix"] = ps["userIdPrefix"].paramValue; //用户名前缀
                }
                if (IsMobile())
                {
                    return View("~/Views/Home/AppReg.aspx");
                }
                else
                {
                    return View();
                }
            }
        }



        public ActionResult AppReg1(string id)
        {
            //没有推荐人或推荐人不存在、或推荐人未开通，直接跳转到登录页面
            if (id == null)
            {
                Response.Redirect("/Home/Index");
                return null;
            }
            else
            {
                //检查推荐人是否存在、是否开通
                Member mem = memberBLL.GetModelByUserId(id);
                Member shp = mem;

                if (mem == null || mem.isPay == 0)
                {
                    Response.Redirect("/Home/Index");
                    return null;
                }
                else
                {
                    //报单中心
                    if (shp.isAgent != 2)
                    {
                        shp = ((Member)memberBLL.GetModelById(1));
                    }
                    ViewData["shopName"] = shp.userId;
                    ViewData["shopUserName"] = shp.userName;

                    //推荐人
                    ViewData["reName"] = mem.userId;
                    ViewData["reUserName"] = mem.userName;

                    BaseSet set = setBLL.GetModel();
                    //网站名称
                    ViewData["sitename"] = set.sitename + "-登録を広める";
                    //版权信息
                    ViewData["copyright"] = set.copyright;


                    Dictionary<string, ParameterSet> ps = paramSetBLL.GetDictionaryByCodes("regPrice",
                            "userIdPrefix");
                    //注册协议
                    News n = newsBLL.GetModelByTypeId(ConstUtil.NEWS_ZCXY);
                    ViewData["zcxy"] = ""; //注册协议
                    if (n != null) { ViewData["zcxy"] = n.content; }

                    double price = Convert.ToDouble(ps["regPrice"].paramValue); //每单注册金额
                    ViewData["pk"] = 0; //普卡注册金额
                    ViewData["yk"] = 0; //银卡注册金额
                    ViewData["jk"] = 0; //金卡注册金额
                    ViewData["userIdPrefix"] = ps["userIdPrefix"].paramValue; //用户名前缀
                }
                if (IsMobile())
                {
                    return View("~/Views/Home/AppReg1.aspx");
                }
                else
                {
                    return View();
                }
            }
        }


        /// <summary>
        /// 获取注册银行
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRegBank()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            Dictionary<string, object> result = new Dictionary<string, object>();
            List<DataDictionary> lit = ddBLL.GetList("select * from DataDictionary where parentId = 52");
            result.Add("area", areaBLL.GetTreeModelList()); //省市区数据
            result.Add("bankList", lit);
            //注册协议
            News n = newsBLL.GetModelByTypeId(ConstUtil.NEWS_ZCXY);
            if (n != null) { result.Add("zcxy", n.content); }
            response.map = result;
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 分页查询商品
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetProductListPage(Product model)
        {
            if (model == null) { model = new Product(); }
            model.isShelve = 2;
            PageResult<Product> page = productBLL.GetListPage(model, "id,productCode,productName,imgUrl,price,addTime,isShelve");
            return Json(page, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 注册会员
        /// </summary>
        /// <param name="mb">会员信息</param>
        /// <param name="read">是否勾选注册协议</param>
        /// <returns></returns>
        public JsonResult RegisterMember(Member mb, string read)
        {
            //System.Threading.Thread.Sleep(3000);
            ResponseDtoData response = new ResponseDtoData("fail");
            if (mb == null) { response.msg = "提交信息为空"; }
            else if (read == null || read.Length == 0) { response.msg = "请先勾选注册协议"; }
            else
            {
                try
                {
                    Member re = memberBLL.GetModelByUserId(mb.reName);
                    if (re == null) { throw new ValidateException("推荐人不存在"); }
                    //找最左边那条线员的第1个直推会员是否必须放在自己系谱图最左边那条线的最下边
                    //if(re.reCount==0)
                    //{
                    string father = memberBLL.GetLeftUserId(re.id.Value);
                    mb.fatherName = father;
                    mb.treePlace = -1;
                    //}

                    Member mm = memberBLL.SaveMember(mb);

                    //消息提醒
                    SystemMsg msg = new SystemMsg();
                    msg.isRead = 0;
                    msg.toUid = 0;
                    msg.url = "#MemberPassing";
                    msg.msg = "您有待开通的会员";
                    msg.recordId = mm.id;
                    msg.recordTable = "Member";
                    msgBLL.Save(msg);
                    response.status = "success";
                }
                catch (ValidateException ex) { response.msg = ex.Message; }
                catch (Exception) { response.msg = "出现错误，请联系管理员"; }
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 检查用户编号
        /// </summary>
        /// <param name="userId">用户编码</param>
        /// <param name="flag">1:检查用户是否存在，2：检查接点人编码，3：检查推荐人编码，4，检查报单中心编码</param>
        /// <returns></returns>
        public JsonResult CheckUserId(string userId, int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response = memberBLL.CheckUserId(userId, flag);
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "出现错误，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 判断是否手机访问
        /// </summary>
        /// <returns></returns>
        private bool IsMobile()
        {
            return true;
            string str_u = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex b = new Regex(@"android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (!(b.IsMatch(str_u) || v.IsMatch(str_u.Substring(0, 4))))
            {
                //PC访问   
                return false;
            }
            else
            {
                //手机访问   
                return true;
            }
        }
    }
}
