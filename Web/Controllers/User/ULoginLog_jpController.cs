﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 登录日志Controller
    /// </summary>
    public class ULoginLog_jpController : Controller
    {
        public ILoginHistoryBLL lhBll { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(LoginHistory model)
        {
            if (model == null) { model = new LoginHistory(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.uid = mb.id;
            PageResult<LoginHistory> result = lhBll.GetListPage(model, mb);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

    }
}
