﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 待开通会员Controller
    /// </summary>
    public class UMemberPassing_jpController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 分页查询待开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.isPay = 0;
            model.shopid = mb.id;
            string fields = "id,reName,shopName,userId,userName,regMoney,uLevel,phone,addTime,fatherName";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 开通会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <returns></returns>
        public JsonResult OpenMember(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == 0)
            {
                response.msg = "开通会员为空";
            }
            else
            {
                try
                {
                    //默认开通实单
                    Member current = (Member)Session["MemberUser"];
                    memberBLL.OpenMember(id, 0, current);
                    response.msg = "开通成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == 0)
            {
                response.msg = "要删除的会员为空";
            }
            else
            {
                try
                {
                    memberBLL.DeleteById(id);
                    response.msg = "删除成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
