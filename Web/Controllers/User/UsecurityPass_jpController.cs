﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 流水账Controller
    /// </summary>
    public class UsecurityPass_jpController : Controller
    {

        public IMemberBLL memberBLL { get; set; }


        /// <summary>
        /// 初始化页面，是否已经设置过密保
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            Member mb = (Member)Session["MemberUser"];
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member mm = memberBLL.GetModelById(mb.id.Value);
                Session["fristSecurityPass"] = "Y";
                response.msg = "Y";
                if (!string.IsNullOrEmpty(mm.question) && !string.IsNullOrEmpty(mm.answer))
                {
                    response.msg = "N";
                    response.other = DESEncrypt.DecryptDES(mm.question, ConstUtil.SALT);
                    Session["fristSecurityPass"] = "N";
                }
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "初始化页面出错，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 修改密保
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public JsonResult SaveSecurityPass(Member m)
        {
            Member mb = (Member)Session["MemberUser"];
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //是否第一次设置密保
                string fristSecurityPass = Session["fristSecurityPass"].ToString();
                m.fristAnswer = fristSecurityPass;
                m.id = mb.id;
                memberBLL.SaveChangeSecurityPass(m);
                Member mm = memberBLL.GetModelById(m.id.Value);
                response.other = DESEncrypt.DecryptDES(mm.question, ConstUtil.SALT);
                response.msg = "N";
                response.status = "success";
                Session["fristSecurityPass"] = "N";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "设置出错，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
    }
}
