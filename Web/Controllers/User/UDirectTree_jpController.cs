﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 直推图Controller
    /// </summary>
    public class UDirectTree_jpController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 查询当前登录会员直推图
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDefaultDirectTree()
        {
            ResponseDtoList<Member> response = new ResponseDtoList<Member>("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                List<Member> list = memberBLL.GetDirectTreeList(null, current);
                response.list = list;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }

            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


        /// <summary>
        /// 根据userId查询直推图，userId需是当前登录用户或其下级节点
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetDirectTreeByUserId(string userId)
        {
            ResponseDtoList<Member> response = new ResponseDtoList<Member>("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                List<Member> list = memberBLL.GetDirectTreeList(userId, current);
                response.list = list;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }

            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GetAppDirectTree(int uid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {


                Member current = (Member)Session["MemberUser"];
                if (uid == 0) { uid = current.id.Value; }
                string language = "";
                try
                {
                    language = Session["language"].ToString();
                }
                catch
                { }
                Member result = memberBLL.GetDirectTree(uid, current,language);
                response.result = result;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }
            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GetAppDirectTreeByUserId(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int uid = 1;
                Member current = (Member)Session["MemberUser"];
                Member result = null;
                if (string.IsNullOrEmpty(userId))
                {
                    uid = current.id.Value;
                    string language = "";
                    try
                    {
                        language = Session["language"].ToString();
                    }
                    catch
                    { }
                    result = memberBLL.GetDirectTree(uid, current, language);
                }
                else
                {

                    Member mm = memberBLL.GetModelByUserId(userId);
                    uid = mm.id.Value;
                    if (mm == null)
                    {
                        response.msg = "none";
                    }
                    else
                    {
                        string language = "";
                        try
                        {
                            language = Session["language"].ToString();
                        }
                        catch
                        { }
                        result = memberBLL.GetDirectTree(uid, current, language);
                    }
                }
                response.result = result;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }
            var jsonResult = Json(response, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

    }
}
