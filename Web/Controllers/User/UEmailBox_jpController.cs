﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 邮件中心Controller
    /// </summary>
    public class UEmailBox_jpController : Controller
    {
        public IEmailBoxBLL emailBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public JsonResult GetFromUserName(EmailBox model)
        {
            //默认查询当前会员发件箱
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new EmailBox(); }
            model.fromUid = current.id;
            var fromUserName = model.productId == null ? "system" : emailBLL.GetFromUserName(model.productId.Value);
            model.fromUserName = model.type == 3 && fromUserName != "system" ? fromUserName : "プラットフォーム";
            List<EmailBox> list = new List<EmailBox>() {
                model
            };
            PageResult<EmailBox> result = emailBLL.GetSendListPage(model);
            result.rows = list;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 分页查询发件箱
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetSendListPage(EmailBox model)
        {
            //默认查询当前会员发件箱
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new EmailBox(); }
            model.fromUid = current.id;
            PageResult<EmailBox> result = emailBLL.GetSendListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 分页查询收件箱
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetReceiveListPage(EmailBox model)
        {
            //默认查询当前会员收件箱
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new EmailBox(); }
            model.toUid = current.id;
            PageResult<EmailBox> result = emailBLL.GetReceiveListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询回复列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetReceiveList(EmailBox model)
        {
            ResponseDtoList<EmailBox> response = new ResponseDtoList<EmailBox>("fail");
            try
            {
                List<EmailBox> list = emailBLL.GetReceiveList(model);
                Member current = (Member)Session["MemberUser"]; //当前会员
                response.list = list;
                response.msg = current.userId;
                response.status = "success";

            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetReceiveType(EmailBox model)
        {
            ResponseDtoList<EmailBox> response = new ResponseDtoList<EmailBox>("fail");
            try
            {
                List<EmailBox> list = emailBLL.GetReceiveList(model);
                Member current = (Member)Session["MemberUser"]; //当前会员
                response.list = list;
                response.msg = list[0].type.ToString();
                response.status = "success";

            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 发邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveSend(EmailBox model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                Member current = (Member)Session["MemberUser"]; //当前会员
                int toId = emailBLL.SaveSend(model, current);

                //消息提醒
                SystemMsg msg = new SystemMsg();
                msg.isRead = 0;
                msg.url = toId == 1 ? "#EmailBox" : "#UEmailBox";
                msg.msg = "您有新的邮件信息";
                msg.toUid = toId == 1 ? 0 : toId;
                msgBLL.Save(msg);

                response.msg = "发送成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "发送失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 回复邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveReceive(EmailBox model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                Member current = (Member)Session["MemberUser"]; //当前会员
                int toId = emailBLL.SaveReceive(model, current);

                //消息提醒
                SystemMsg msg = new SystemMsg();
                msg.isRead = 0;
                msg.url = "#EmailBox";
                msg.msg = "您有新的邮件信息";
                model.type = model.type;
                msg.toUid = toId == 1 ? 0 : toId;
                msgBLL.Save(msg);

                response.msg = "回复成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "回复失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除发件箱记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteSend(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前会员
                emailBLL.DeleteSend(id);
                response.msg = "删除成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "删除失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除收件箱记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteReceive(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前会员
                emailBLL.DeleteReceive(id);
                response.msg = "删除成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "删除失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 更新阅读状态
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Read(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前会员
                emailBLL.UpdateRead(id);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
    }
}
