﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 商品管理Controller
    /// </summary>
    public class UProduct_jpController : Controller
    {
        public IProductBLL productBLL { get; set; }

        /// <summary>
        /// 查询商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.Success();
                response.result = productBLL.GetModel(id);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Product model)
        {
            if (model == null) { model = new Product(); }
            Member current = (Member)Session["MemberUser"];
            model.uid = current.id;
            PageResult<Product> page = productBLL.GetListPage(model, "m.id,m.productCode,m.productName,m.cont,m.imgUrl,m.price,m.fxPrice,m.num,m.addTime,m.isShelve,m.flag,m.productTypeId,m.productTypeName,m.productBigTypeId,m.productBigTypeName");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveOrUpdate(Product model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.price = model.fxPrice;
                //model.fxPrice = model.price;
                //当前登录用户
                Member current = (Member)Session["MemberUser"];
                Product newPro = null;
                if (model.cont.Length > 1000000)
                    throw new ValidateException("商品内容太长。请小于100万");
                if (ValidateUtils.CheckIntZero(model.id))
                {
                    //先检查商品编码
                    if (productBLL.isExistsProductCode(model.productCode, 0))
                    {
                        throw new ValidateException("商品编码已经存在");
                    }
                    //新增商品必须上传图片
                    model.imgUrl = UploadImg(imgFile);
                    newPro = productBLL.SaveProduct(model, current);
                }
                else
                {
                    //先检查商品编码
                    if (productBLL.isExistsProductCode(model.productCode, model.id.Value))
                    {
                        throw new ValidateException("商品编码已经存在");
                    }
                    //修改时，如有更新图片则重新上传
                    if (imgFile != null)
                    {
                        model.imgUrl = UploadImg(imgFile);
                    }
                    newPro = productBLL.UpdateProduct(model, current);
                }

                response.Success();
                response.result = newPro;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img)
        {
            if (img == null) { throw new ValidateException("请上传商品图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = "product_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }

        /// <summary>
        /// 上架商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Shelve(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = productBLL.SaveShelve(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 下架商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelShelve(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = productBLL.SaveCancelShelve(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = productBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
