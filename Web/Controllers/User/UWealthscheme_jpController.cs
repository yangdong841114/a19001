﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 财富方案Controller
    /// </summary>
    public class UWealthscheme_jpController : Controller
    {
        public INewsBLL newBLL { get; set; }

        /// <summary>
        /// 获取财富方案
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                News n = newBLL.GetModelByTypeId(ConstUtil.NEWS_CFFA);
                response.Success();
                response.result = n;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
