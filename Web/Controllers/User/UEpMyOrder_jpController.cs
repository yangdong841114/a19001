﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.User.Controllers
{
    /// <summary>
    /// EP交易市场Controller
    /// </summary>
    public class UEpMyOrder_jpController : Controller
    {
        public IEpSaleRecordBLL epsBLL { get; set; }

        public IMemberAccountBLL accBLL { get; set; }

        public IEpBuyRecordBLL epbBLL { get; set; }

        public IParamSetBLL psBLL { get; set; }

        /// <summary>
        /// 分页查询我挂卖的EP列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(EpSaleRecord model)
        {
            if (model == null)
            {
                model = new EpSaleRecord();
            }

            Member current = (Member)Session["MemberUser"];
            model.uid = current.id;
            PageResult<EpSaleRecord> page = epsBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 导出我的挂单EXCEL
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportMyOrderXls(EpSaleRecord model)
        {
            if (model == null)
            {
                model = new EpSaleRecord();
            }

            Member current = (Member)Session["MemberUser"];
            model.uid = current.id;
            DataTable dt = epsBLL.GetExportList(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("number", "单号");
            htb.Add("waitNum", "待出售数量");
            htb.Add("scNum", "已出售数量");
            htb.Add("saleNum", "挂卖数量");
            htb.Add("status", "状态");
            htb.Add("addTime", "挂卖日期");

            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                if (dt.Rows.Count > 0)
                {
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                }
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAgent()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                MemberAccount acc = accBLL.GetModel(current.id.Value);
                Dictionary<string, ParameterSet> di = psBLL.GetDictionaryByCodes("epSaleBei");
                response.msg = acc.agentJj + "";
                response.result = di["epSaleBei"];
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 新增挂卖记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveRecord(EpSaleRecord model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                if (model == null)
                {
                    model = new EpSaleRecord();
                }
                model.uid = current.id;
                epsBLL.SaveRecord(model, current);

                MemberAccount acc = accBLL.GetModel(current.id.Value);
                response.msg = acc.agentJj + "";

                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 取消挂卖记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveCancel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                epsBLL.SaveCancel(id, current);

                MemberAccount acc = accBLL.GetModel(current.id.Value);
                response.msg = acc.agentJj + "";

                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
