﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 我的挂单-交易明细Controller
    /// </summary>
    public class UEpMyOrderDetail_jpController : Controller
    {

        public IEpBuyRecordBLL epbBLL { get; set; }

        /// <summary>
        /// 分页查询挂卖记录交易明细
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(EpBuyRecord model)
        {
            if (model == null)
            {
                model = new EpBuyRecord();
            }

            Member current = (Member)Session["MemberUser"];
            model.suid = current.id;
            PageResult<EpBuyRecord> page = epbBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 卖家确认收款
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult PaySure(EpBuyRecord model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                epbBLL.SaveSurePay(model, current);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
