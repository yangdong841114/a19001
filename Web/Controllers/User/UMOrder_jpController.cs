﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 订单管理Controller
    /// </summary>
    public class UMOrder_jpController : Controller
    {
        public IOrderBLL orderBLL { get; set; }

        public ILogisticBLL lgBLL { get; set; }

        /// <summary>
        /// 分页查询订单
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(OrderHeader model)
        {
            if (model == null) { model = new OrderHeader(); }
            Member current = (Member)Session["MemberUser"];
            model.saleUid = current.id;
            PageResult<OrderHeader> page = orderBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteOrder(string id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                orderBLL.DeleteOrder(id);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 获取物流商信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetLogisticList()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                List<Logistic> list = lgBLL.GetList();
                di.Add("logList", list);
                response.map = di;
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询物流信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetLogisticMsg(string id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.result = orderBLL.GetLogisticMsg(id);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 订单发货
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public JsonResult LogisticOrder(OrderHeader order)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (order != null)
                {
                    List<OrderHeader> list = new List<OrderHeader>();
                    list.Add(order);
                    orderBLL.SaveLogistic(list);
                    response.Success();
                    response.msg = "操作成功";
                }
                else
                {
                    response.msg = "操作内容为空";
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
