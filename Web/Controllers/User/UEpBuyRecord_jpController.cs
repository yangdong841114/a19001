﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.User.Controllers
{
    /// <summary>
    /// EP交易购买记录Controller
    /// </summary>
    public class UEpBuyRecord_jpController : Controller
    {
        //public　IEpSaleRecordBLL epsBLL { get; set; }

        public IEpBuyRecordBLL epbBLL { get; set; }


        /// <summary>
        /// 分页查询买单列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(EpBuyRecord model)
        {
            if (model == null)
            {
                model = new EpBuyRecord();
            }

            //Member current = (Member)Session["MemberUser"];
            //model.uid = current.id;
            PageResult<EpBuyRecord> page = epbBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 导出EXCEL
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportBuyerXls(EpBuyRecord model)
        {
            if (model == null)
            {
                model = new EpBuyRecord();
            }

            DataTable dt = epbBLL.GetExportList(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "买家编号");
            htb.Add("number", "买入单号");
            htb.Add("buyNum", "买入数量");
            htb.Add("addTime", "购买日期");
            htb.Add("snumber", "挂卖单号");
            htb.Add("payMoney", "应付金额");
            htb.Add("status", "状态");
            htb.Add("buytype", "购买类型");
            htb.Add("suserId", "卖家编号");
            htb.Add("batchNumber", "批量单号");
            htb.Add("opUserId", "操作人");
            htb.Add("phone", "手机");
            htb.Add("qq", "QQ");
            htb.Add("bankName", "开户行");
            //htb.Add("status", "状态");
            htb.Add("bankCard", "银行卡号");
            htb.Add("bankUser", "开户名");
            htb.Add("bankAddress", "开户支行");
            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                if (dt.Rows.Count > 0)
                {
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                }
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }


        /// <summary>
        /// 取消买单记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveCancel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                epbBLL.SaveCancel(id, current);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 买家确认付款
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult PayMoney(EpBuyRecord model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                epbBLL.SavePay(model, current, false);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 批量付款
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult BatchPayMoney(List<EpBuyRecord> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                epbBLL.SaveBatchPay(list, current, false);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
