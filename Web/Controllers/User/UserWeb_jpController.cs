﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.IO;
using ThoughtWorks.QRCode.Codec;


namespace Web.User.Controllers
{
    public class UserWeb_jpController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IBaseSetBLL setBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public INewsBLL newBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }
        public IParamSetBLL PsetBLL { get; set; }
        public IProductTypeBLL producttypeBLL { get; set; }
        public IInfoBLL infoBLL { get; set; }
        public IInfoplBLL infoplBLL { get; set; }
        public IProductBLL productBLL { get; set; }
        public IMTransferBLL tranBLL { get; set; }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetUserInfoMessage()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                string first_userId = mm.userId;

                Dictionary<string, object> result = new Dictionary<string, object>();
                MemberAccount acc = accountBLL.GetModel(mm.id.Value);
                result.Add("account", acc);
                Member m = memberBLL.GetModelByIdNoPassWord(mm.id.Value);
                Member m2 = new Member();
                m2.userId = m.userId;
                m2.userName = m.userName;
                m2.uLevel = m.uLevel;
                m2.rLevel = m.rLevel;
                m2.bankName = m.bankName;
                m2.bankCard = m.bankCard;
                m2.bankUser = m.bankUser;
                m2.bankAddress = m.bankAddress;
                result.Add("userInfo", m2);
                result.Add("first_userId", first_userId);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSjht()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                //News n = newBLL.GetModelByTypeId(ConstUtil.NEWS_SJHT);
                //di.Add("sjht", n.content);
                di.Add("sjht", "商家合同");
                response.Success();
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 获取PC前台菜单信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenu()
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["MemberUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            if (mm.isAgent != 2)
            {
                List<Resource> lit = new List<Resource>();
                if (list != null && list.Count > 0)
                {
                    foreach (Resource r in list)
                    {
                        if (r.id != ConstUtil.MENU_SHOP && r.parentResourceId != ConstUtil.MENU_SHOP)
                        {
                            lit.Add(r);
                        }
                    }
                }
                response.list = lit;
            }
            else
            {
                response.list = list;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取APP前台一级菜单
        /// </summary>
        /// <returns></returns>
        public JsonResult GetWapMenu()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            //ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["MemberUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByPartentId(mm.id.Value, "102");
            Dictionary<string, object> di = new Dictionary<string, object>();
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            if (mm.isAgent != 2)
            {
                List<Resource> lit = new List<Resource>();
                if (list != null && list.Count > 0)
                {
                    foreach (Resource r in list)
                    {
                        if (r.id != ConstUtil.WAP_MENU_SHOP && r.parentResourceId != ConstUtil.WAP_MENU_SHOP)
                        {
                            lit.Add(r);
                        }
                    }
                }
                di.Add("frist", lit);
            }
            else
            {
                di.Add("frist", list);
            }

            string root = "102";
            try
            {
                if (!string.IsNullOrEmpty(Session["language"].ToString()))
                {
                    if (Session["language"].ToString() == "rb")
                    {
                        root = "104";
                    }
                }
            }
            catch
            {
            }

            List<Resource> allList = resourceBLL.GetListByUserAndParent(mm.id.Value, root);
            di.Add("all", allList);
            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取基础数据
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public JsonResult GetBaseData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                string language = "";
                try
                {
                    language = Session["language"].ToString();
                }
                catch
                { }
                Dictionary<string, List<DataDictionary>> cahceData = dataDictionaryBLL.GetAllToDictionary(language);
                BaseSet set = setBLL.GetModel();
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                

                if (language == null || language == "") language = "ft";
                List<SystemMsg> mlist = msgBLL.GetList(mm.id.Value); //通知提醒
                result.Add("language", language);
                result.Add("notic", mlist);
                result.Add("LoginUser", mm.userId);
                result.Add("cahceData", cahceData); //数据字典缓存
                result.Add("baseSet", set);         //基础数据
                result.Add("area", areaBLL.GetTreeModelList()); //省市区数据
                result.Add("productType", producttypeBLL.GetTreeModelList()); //分类数据
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 获取主页数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMainData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                string first_userId = current.userId;
                di.Add("first_userId", first_userId);
                MemberAccount account = accountBLL.GetModel(current.id.Value);      //当前用户账户余额
                List<Banner> lit = setBLL.GetBannerList();                      //广告图
                List<Banner> banners = new List<Banner>();
                for (int i = 0; i < lit.Count; i++)
                {
                    Banner b = lit[i];
                    string path = Server.MapPath("~" + b.imgUrl);
                    if (System.IO.File.Exists(path))
                    {
                        banners.Add(b);
                    }
                }

                BaseSet set = setBLL.GetModel();                                    //基础设置
                //前7条文章
                News n = new News();
                n.page = 1;
                n.rows = 7;
                n.typeId = ConstUtil.NEWS_GG; //公告管理
                PageResult<News> page = newBLL.GetListPage(n, "id,addTime,title");
                if (page != null && page.rows != null)
                {
                    di.Add("newsList", page.rows);
                }

                di.Add("user", current);
                di.Add("userName", current.userName);
                di.Add("account", account);
                di.Add("banners", banners);
                di.Add("baseSet", set);         //基础数据
                //推广链接
                string siteUrl = "http://" + Request.Url.Host + "/Home/Reg/" + current.userId;
                di.Add("siteUrl", siteUrl);

                //是否存在二维码
                string filename = current.userId + ".jpg";
                string filepath = "~/Upload/qrcode/" + filename;
                if (!System.IO.File.Exists(Server.MapPath(filepath)))
                {
                    CreateCode_Choose(current.userId, siteUrl, "Byte", "M", 8, 4);
                }
                di.Add("qrcode", filename);
                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载数据失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectTypeData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=1");

                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);

                list = producttypeBLL.GetList("select * from ProductType where grade=2");
                di.Add("mlistSm", list);

                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetProjectTypeSmData(string parentId)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=2 and parentId=" + parentId);

                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);



                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        // 生成二维码
        /// </summary>
        /// <param name="strData">要生成的文字或者数字，支持中文。如： "4408810820 深圳－广州" 或者：4444444444</param>
        /// <param name="qrEncoding">三种尺寸：BYTE ，ALPHA_NUMERIC，NUMERIC</param>
        /// <param name="level">大小：L M Q H</param>
        /// <param name="version">版本：如 8</param>
        /// <param name="scale">比例：如 4</param>
        /// <returns></returns>
        public String CreateCode_Choose(string userId, string strData, string qrEncoding, string level, int version, int scale)
        {
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            string encoding = qrEncoding;
            switch (encoding)
            {
                case "Byte":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                    break;
                case "AlphaNumeric":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
                    break;
                case "Numeric":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.NUMERIC;
                    break;
                default:
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                    break;
            }

            qrCodeEncoder.QRCodeScale = scale;
            qrCodeEncoder.QRCodeVersion = version;
            switch (level)
            {
                case "L":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                    break;
                case "M":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                    break;
                case "Q":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
                    break;
                default:
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                    break;
            }
            //文字生成图片
            System.Drawing.Image image = qrCodeEncoder.Encode(strData);
            string filename = userId + ".jpg";
            string filepath = Server.MapPath(@"~\UpLoad\qrcode");
            //如果文件夹不存在，则创建
            if (!Directory.Exists(filepath))
                Directory.CreateDirectory(filepath);
            filepath = filepath + "\\" + filename;
            System.IO.FileStream fs = new System.IO.FileStream(filepath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
            image.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
            fs.Close();
            image.Dispose();
            return @"/UpLoad/qrcode/" + filename;
        }

        /// <summary>
        /// 验证前台安全密码
        /// </summary>
        /// <param name="pass2"></param>
        /// <returns></returns>
        public JsonResult CheckUPass2(string pass2)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "安全密码错误";
                Member cur = (Member)Session["MemberUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass2 = DESEncrypt.EncryptDES(pass2, ConstUtil.SALT);
                    if (pass2 == m.passOpen)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 验证前台交易密码
        /// </summary>
        /// <param name="pass3"></param>
        /// <returns></returns>
        public JsonResult CheckUPass3(string pass3)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "交易密码错误";
                Member cur = (Member)Session["MemberUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass3 = DESEncrypt.EncryptDES(pass3, ConstUtil.SALT);
                    if (pass3 == m.threepass)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UInfo()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                string first_userId = current.userId;
                di.Add("first_userId", first_userId);
                //商品
                List<Product> ListInfo = productBLL.GetList(" select * from Product where isShelve=2 ");
                di.Add("ListInfo", ListInfo);

                //优选好物-广告图
                List<Info> lit = infoBLL.GetList(" where type='gg' and fltype in ('优选好物','最新资讯','热门文章','热门视频') and isTop=2 order by addTime desc ");
                List<Info> banners_yxhw = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.fltype == "优选好物")
                        {

                            string path = Server.MapPath("~" + b.imgUrl);
                            if (System.IO.File.Exists(path))
                            {
                                banners_yxhw.Add(b);
                            }

                        }
                    }
                di.Add("banners_yxhw", banners_yxhw);
                //最新资讯-广告图
                //lit = infoBLL.GetList(" where type='gg' and fltype='最新资讯' and isTop=2 order by addTime desc ");
                List<Info> banners_zxzx = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.fltype == "最新资讯")
                        {
                            string path = Server.MapPath("~" + b.imgUrl);
                            if (System.IO.File.Exists(path))
                            {
                                banners_zxzx.Add(b);
                            }
                        }
                    }
                di.Add("banners_zxzx", banners_zxzx);
                //热门文章-广告图
                //lit = infoBLL.GetList(" where type='gg' and fltype='热门文章' and isTop=2 order by addTime desc ");
                List<Info> banners_rmwz = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.fltype == "热门文章")
                        {
                            string path = Server.MapPath("~" + b.imgUrl);
                            if (System.IO.File.Exists(path))
                            {
                                banners_rmwz.Add(b);
                            }
                        }
                    }
                di.Add("banners_rmwz", banners_rmwz);
                //热门视频-广告图
                //lit = infoBLL.GetList(" where type='gg' and fltype='热门视频' and isTop=2 order by addTime desc ");
                List<Info> banners_rmsp = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.fltype == "热门视频")
                        {
                            string path = Server.MapPath("~" + b.imgUrl);
                            if (System.IO.File.Exists(path))
                            {
                                banners_rmsp.Add(b);
                            }
                        }
                    }
                di.Add("banners_rmsp", banners_rmsp);

                lit = infoBLL.GetList(" where type in ('wz','sp')  order by addTime desc ");
                //最新资讯-文章
                //List<Info> zxzxList = infoBLL.GetListTop10(" where type='wz' and fltype='最新资讯' order by addTime desc ");
                List<Info> zxzxList = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.fltype == "最新资讯" && b.type == "wz")
                        {
                            zxzxList.Add(b);
                        }
                    }
                di.Add("zxzxList", zxzxList);

                //热门文章-文章
                //List<Info> rmwzList = infoBLL.GetListTop10(" where type='wz' and fltype='热门文章' order by addTime desc ");
                List<Info> rmwzList = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.fltype == "热门文章" && b.type == "wz")
                        {
                            rmwzList.Add(b);
                        }
                    }
                di.Add("rmwzList", rmwzList);

                //热门视频-文章
                //List<Info> rmspList = infoBLL.GetListTop10(" where type='sp'  order by addTime desc ");
                List<Info> rmspList = new List<Info>();
                if (lit != null)
                    for (int i = 0; i < lit.Count; i++)
                    {
                        Info b = lit[i];
                        if (b.type == "sp")
                        {
                            rmspList.Add(b);
                        }
                    }
                di.Add("rmspList", rmspList);

                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载数据失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageInfozxzx(Info model)
        {
            if (model == null) { model = new Info(); }
            model.type = "wz";
            model.fltype = "最新资讯";
            PageResult<Info> page = null;
            page = infoBLL.GetListPage(model, "id,userId,userName,llcount,plcount,dzcount,title,fltype,addTime,infocontent,infoJj,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPageInfo(Info model)
        {
            if (model == null) { model = new Info(); }
            Member current = (Member)Session["MemberUser"];
            model.uid = current.id;
            PageResult<Info> page = null;
            page = infoBLL.GetListPage(model, "id,userId,userName,llcount,plcount,dzcount,title,fltype,addTime,infocontent,infoJj,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 保存或更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveOrUpdateInfo(Info model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.type = "wz";
                //当前登录用户
                Member current = (Member)Session["MemberUser"];

                if (model.infocontent.Length > 1000000)
                    throw new ValidateException("内容太长。请小于100万");
                if (ValidateUtils.CheckIntZero(model.id))
                {
                    model.isTop = 1;
                    model.dpcount = 0;
                    model.dzcount = 0;
                    model.imgBigUrl = "";
                    model.imgUrl = "";
                    model.llcount = 0;
                    model.plcount = 0;
                    model.id = infoBLL.Save(model, current);
                    tranBLL.WK("WKFbwz", current);//挖矿
                }
                else
                {
                    infoBLL.Update(model, current);
                }

                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModelInfo(int id)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                if (current == null || current.userId == "noexit") { throw new ValidateException("未登录不能得到点值"); }
                string first_userId = current.userId;
                di.Add("first_userId", first_userId);
                infoBLL.Addllcount(id);
                Info dtoInfo = infoBLL.GetModelById(id);

                di.Add("dtoInfo", dtoInfo);
                if (dtoInfo.type == "sp")
                    tranBLL.WK("WKSp", current);//挖矿
                if (dtoInfo.type == "gg")
                    tranBLL.WK("WKGg", current);//挖矿
                DateTime dt_3dayago = DateTime.Now.AddDays(-7);
                List<Infopl> infoplList = infoplBLL.GetListModel(" where infoId=" + id + " and addTime>'" + dt_3dayago.Year + "-" + dt_3dayago.Month + "-" + dt_3dayago.Day + "' order by addTime desc ");
                di.Add("infoplList", infoplList);



                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载数据失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult plDZ(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string temp = id.ToString();
                Member current = (Member)Session["Memberuser"]; //当前用户
                System.Data.DataTable dt_infopl = infoplBLL.GetList(" infoId=" + id + " and DateDiff(dd,addTime,getdate())=0 and uid=" + current.id + " and type='dz'");
                if (dt_infopl.Rows.Count > 0) { throw new ValidateException("已点赞过了"); }
                Infopl mypl = new Infopl();
                mypl.addTime = DateTime.Now;
                mypl.infoId = id;
                mypl.infoplcontent = "点赞";
                mypl.type = "dz";
                mypl.uid = current.id;
                mypl.userId = current.userId;
                mypl.userName = current.userName;
                infoplBLL.Save(mypl, current);
                tranBLL.WK("WKPl", current);//挖矿
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult plDP(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string temp = id.ToString();
                Member current = (Member)Session["Memberuser"]; //当前用户
                System.Data.DataTable dt_infopl = infoplBLL.GetList(" infoId=" + id + " and DateDiff(dd,addTime,getdate())=0 and uid=" + current.id + " and type='dp'");
                if (dt_infopl.Rows.Count > 0) { throw new ValidateException("已点贬过了"); }
                Infopl mypl = new Infopl();
                mypl.addTime = DateTime.Now;
                mypl.infoId = id;
                mypl.infoplcontent = "点贬";
                mypl.type = "dp";
                mypl.uid = current.id;
                mypl.userId = current.userId;
                mypl.userName = current.userName;
                infoplBLL.Save(mypl, current);
                tranBLL.WK("WKPl", current);//挖矿
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult pl(int id, string infoplcontent)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string temp = id.ToString();
                Member current = (Member)Session["Memberuser"]; //当前用户
                System.Data.DataTable dt_infopl = infoplBLL.GetList(" infoId=" + id + " and DateDiff(dd,addTime,getdate())=0 and uid=" + current.id + " and type='pl'");
                if (dt_infopl.Rows.Count > 0) { throw new ValidateException("已评论过了"); }
                Infopl mypl = new Infopl();
                mypl.addTime = DateTime.Now;
                mypl.infoId = id;
                mypl.infoplcontent = infoplcontent;
                mypl.type = "pl";
                mypl.uid = current.id;
                mypl.userId = current.userId;
                mypl.userName = current.userName;
                infoplBLL.Save(mypl, current);
                tranBLL.WK("WKPl", current);//挖矿
                response.Success();
                response.result = mypl;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



    }
}
