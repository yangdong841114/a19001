﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 转账Controller
    /// </summary>
    public class UMTransferController : Controller
    {
        public IMTransferBLL transferBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IParamSetBLL paramBLL { get; set; }
        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                
                //获取用户最新信息
                Member user = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
                response.Success();
                response.result = user;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        } 

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(MTransfer model)
        {
            if (model == null) { model = new MTransfer(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.fromUid = mb.id.Value;
            PageResult<MTransfer> page = transferBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提交转账申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveMTransfer(MTransfer model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("transferToccToTodMin");
            double transferToccToTodMin = Convert.ToDouble(param["transferToccToTodMin"].paramValue);
            var msg = "";
            if(model.typeId==95){
                if (model.epoints < transferToccToTodMin)
                {
                    msg = "TOCC币转自身TOD币的数量须>="+transferToccToTodMin;
                    response.msg = msg;
                }
            }
            if(msg=="")
            {
                try
                {
                    //当前登录用户
                    Member mb = (Member)Session["MemberUser"];
                    int c = transferBLL.SaveMTransfer(model, mb);
                    response.Success();
                    response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "操作失败，请联系管理员！"; }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 查询用户名称
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetUserName(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelByUserId(userId);
                if (m == null)
                {
                    response.msg = "会员不存在";
                }
                else
                {
                    response.msg = m.userName;
                }
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
