﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 在线购物Controller
    /// </summary>
    public class UProductShopController : Controller
    {
        public IShoppingCartBLL cartBLL { get; set; }
        public IProductBLL productBLL { get; set; }
        public IMerchantBLL mcBLL { get; set; }
        public IBaseSetBLL setBLL { get; set; }

        /// <summary>
        /// 查询商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                response.Success();
                response.other = cartBLL.GetCartCount(current.id.Value) + "";
                Product pModel=productBLL.GetModel(id);
                if (current.userId == "noexit") pModel.id = 0;
                response.result = pModel;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 初始化购物车数量
        /// </summary>
        /// <returns></returns>
        public JsonResult InitCartCount()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();

                //广告图
                List<Banner> lit = setBLL.GetBannerList(5, 7);
                List<Banner> banners = new List<Banner>();
                for (int i = 0; i < lit.Count; i++)
                {
                    Banner b = lit[i];
                    string path = Server.MapPath("~" + b.imgUrl);
                    if (System.IO.File.Exists(path))
                    {
                        banners.Add(b);
                    }
                }
                result.Add("banners", banners);

                Member current = (Member)Session["Memberuser"]; //当前用户
                response.Success();
                response.map = result;
                response.other = cartBLL.GetCartCount(current.id.Value) + "";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitCartCount1()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();

                //广告图
                List<Banner> lit = setBLL.GetBannerList(5, 7);
                List<Banner> banners = new List<Banner>();
                for (int i = 0; i < lit.Count; i++)
                {
                    Banner b = lit[i];
                    string path = Server.MapPath("~" + b.imgUrl);
                    if (System.IO.File.Exists(path))
                    {
                        banners.Add(b);
                    }
                }
                result.Add("banners", banners);

                List<Merchant> list_Merchant = mcBLL.GetList(" where uid in (select uid from Product where isShelve=2) order by name");
                result.Add("list_Merchant", list_Merchant);
                Member current = (Member)Session["Memberuser"]; //当前用户
                response.Success();
                response.map = result;

            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 分页查询上架的商品
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Product model)
        {
            if (model == null) { model = new Product(); }
            model.isShelve = 2;
            PageResult<Product> page = productBLL.GetListPage(model, "m.uid,m.id,m.productCode,m.productName,m.imgUrl,m.price,m.fxPrice,m.addTime,m.isShelve");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageAll(Product model)
        {
            if (model == null) { model = new Product(); }
            model.isShelve = 2;
            PageResult<Product> page = productBLL.GetListPageAll(model, "m.uid,m.id,m.productCode,m.productName,m.imgUrl,m.price,m.fxPrice,m.addTime,m.isShelve");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

    }
}
