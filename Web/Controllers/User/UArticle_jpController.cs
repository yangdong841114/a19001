﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 公告管理Controller
    /// </summary>
    public class UArticle_jpController : Controller
    {
        public INewsBLL newBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListPage(News model)
        {
            if (model == null) { model = new News(); }
            model.typeId = ConstUtil.NEWS_GG; //公告管理
            PageResult<News> page = null;
            page = newBLL.GetListPage(model, "id,title,addTime,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                News n = newBLL.GetModelById(id);
                response.Success();
                response.result = n;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
