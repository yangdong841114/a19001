﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 会员账户Controller
    /// </summary>
    public class UMyAccount_jpController : Controller
    {
        public IMemberAccountBLL accountBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                MemberAccount account = accountBLL.GetModel(mb.id.Value);
                response.status = "success";
                response.result = account;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载数据失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
