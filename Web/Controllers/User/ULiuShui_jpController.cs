﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 流水账Controller
    /// </summary>
    public class ULiuShui_jpController : Controller
    {

        public ILiuShuiZhangBLL liushuiBLL { get; set; }


        /// <summary>
        /// 分页查询流水账明细
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetLiushuiDetailListPage(LiuShuiZhang model)
        {
            if (model == null)
            {
                model = new LiuShuiZhang();
            }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.uid = mb.id.Value;
            PageResult<LiuShuiZhang> page = liushuiBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 获取收入总金额和支出总金额
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTotalMoney(LiuShuiZhang model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model == null)
                {
                    model = new LiuShuiZhang();
                }
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.uid = mb.id.Value;
                response.msg = liushuiBLL.GetsrMoney(model) + "";         //收入总金额
                response.other = liushuiBLL.GetzcMoney(model) + "";    //支出总金额
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
