﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 订单处理Controller
    /// </summary>
    public class UOrder_jpController : Controller
    {
        public IOrderBLL orderBLL { get; set; }



        /// <summary>
        /// 分页查询订单
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(OrderHeader model)
        {
            if (model == null) { model = new OrderHeader(); }
            Member current = (Member)Session["MemberUser"];
            model.uid = current.id;
            PageResult<OrderHeader> page = orderBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 查询物流信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetLogisticMsg(string id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.result = orderBLL.GetLogisticMsg(id);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 订单确认收货
        /// </summary>
        /// <param name="oh"></param>
        /// <returns></returns>
        public JsonResult SaveSureReceive(OrderHeader oh)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                orderBLL.SaveSureReceive(oh);
                response.Success();
                response.msg = "操作成功";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
