﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.User.Controllers
{
    /// <summary>
    /// EP交易市场Controller
    /// </summary>
    public class UEpTrade_jpController : Controller
    {
        public IEpSaleRecordBLL epsBLL { get; set; }

        public IEpBuyRecordBLL epbBLL { get; set; }

        /// <summary>
        /// 分页查询挂卖的EP列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(EpSaleRecord model)
        {
            if (model == null)
            {
                model = new EpSaleRecord();
            }

            List<int> list = new List<int>();
            list.Add(0);
            list.Add(1);
            model.egtWaitNum = 0;
            model.flagList = list;
            PageResult<EpSaleRecord> page = epsBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 导出EXCEL
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportXls(EpSaleRecord model)
        {
            if (model == null)
            {
                model = new EpSaleRecord();
            }

            List<int> list = new List<int>();
            list.Add(0);
            list.Add(1);
            model.egtWaitNum = 0;
            model.flagList = list;
            DataTable dt = epsBLL.GetExportList(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("number", "挂卖单号");
            htb.Add("userId", "卖家编号");
            htb.Add("waitNum", "待出售数量");
            htb.Add("scNum", "已出售数量");
            htb.Add("saleNum", "挂卖数量");
            htb.Add("status", "状态");
            htb.Add("addTime", "挂卖日期");

            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                if (dt.Rows.Count > 0)
                {
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                }
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

        /// <summary>
        /// 购买ＥＰ
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveRecord(EpBuyRecord model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                if (model == null) { model = new EpBuyRecord(); }
                model.uid = current.id;
                epbBLL.SaveRecord(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 批量购买ＥＰ
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveRecordBatch(List<EpBuyRecord> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                //if (model == null) { model = new EpBuyRecord(); }
                //model.uid = current.id;
                epbBLL.SaveRecordList(list, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
