﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 账户托管Controller
    /// </summary>
    public class UDelegaMember_jpController : Controller
    {
        public IDelegaMemberBLL dmBLL { get; set; }

        /// <summary>
        /// 分页查询待开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(DelegaMember model)
        {
            if (model == null) { model = new DelegaMember(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.mainId = mb.id;
            PageResult<DelegaMember> page = dmBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存账户托管
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult Save(DelegaMember model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录会员
                model.mainId = current.id;
                model.mainUserId = current.userId;
                dmBLL.SaveDelegaMember(model);
                response.msg = "托管成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 子账户转入主账户
        /// </summary>
        /// <returns></returns>
        public JsonResult Transfer()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录会员
                dmBLL.SaveTransfer(current.id.Value);
                response.msg = "转入成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                dmBLL.Delete(id);
                response.msg = "删除成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
