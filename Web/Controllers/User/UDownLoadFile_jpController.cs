﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 下载专区Controller
    /// </summary>
    public class UDownLoadFile_jpController : Controller
    {
        public IDownLoadFileBLL dlBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(DownLoadFile model)
        {
            PageResult<DownLoadFile> result = dlBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

    }
}
