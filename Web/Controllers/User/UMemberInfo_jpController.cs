﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 会员资料Controller
    /// </summary>
    public class UMemberInfo_jpController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 查询会员信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetModel(int? memberId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                int id = memberId == null ? mb.id.Value : memberId.Value;
                Member mm = memberBLL.GetModelByIdNoPassWord(id);
                //只有报单中心才能编辑其下会员
                if (mm.shopid != mb.id && mm.id != mb.id)
                {
                    throw new ValidateException("该会员您无权操作!");
                }

                response.status = "success";
                response.result = mm;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询会员出错，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateMember(Member model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (model == null)
            {
                response.msg = "会员信息为空";
            }
            else
            {
                try
                {
                    Member login = (Member)Session["MemberUser"];
                    Member current = memberBLL.GetModelById(model.id.Value);
                    if (current.id != login.id && current.shopid != login.id) { throw new ValidateException("该会员您无权操作!"); }

                    int c = memberBLL.UpdateMember(model, login);
                    response.msg = "保存成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "保存失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
