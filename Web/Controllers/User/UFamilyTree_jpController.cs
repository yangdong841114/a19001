﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 系谱图Controller
    /// </summary>
    public class UFamilyTree_jpController : Controller
    {
        public IFamilyTreeBLL familyTreeBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 默认系谱图
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetFamilyTree(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member user = (Member)Session["MemberUser"]; //当前用户
                string val = familyTreeBLL.CreateTreeHtml(user.id.Value, 3, "UserRegister", "UMemberInfo");
                response.msg = val;
                //系谱图上线链路
                response.other = memberBLL.GetParentLinkString(1, user.id.Value);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询系谱图,只能查询当前用户及其下线节点
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ceng"></param>
        /// <returns></returns>
        public JsonResult GetFamilyTreeByCondition(string userId, int ceng)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member user = (Member)Session["MemberUser"]; //当前用户
                string val = "";
                if (userId != null && userId.Trim().Length > 0)
                {
                    //检查输入的用户是否存在,输入的用户是否是当前会员或其下线节点
                    Member mb = memberBLL.GetModelByUserId(userId);
                    //检查输入的用户是否存在
                    if (mb != null)
                    {
                        //检查输入的用户是否是当前会员或其下线节点
                        if (mb.pPath.Contains("," + user.id + ","))
                        {
                            val = familyTreeBLL.CreateTreeHtml(mb.id.Value, ceng, "UserRegister", "UMemberInfo");
                            //系谱图上线链路
                            response.other = memberBLL.GetParentLinkString(1, mb.id.Value);
                        }
                        else
                        {
                            val = "";
                        }
                    }
                    else
                    {
                        val = "";
                    }
                }
                else
                {
                    val = familyTreeBLL.CreateTreeHtml(user.id.Value, ceng, "UserRegister", "UMemberInfo");
                    //系谱图上线链路
                    response.other = memberBLL.GetParentLinkString(1, user.id.Value);
                }
                response.msg = val;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// APP默认系谱图
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetAppFamilyTree(string userId)
        {
            ResponseDtoMap<string, Member> response = new ResponseDtoMap<string, Member>("fail");
            try
            {
                Member user = (Member)Session["MemberUser"]; //当前用户
                Dictionary<string, Member> di = familyTreeBLL.GetMemberList(user.id.Value);
                //string val = familyTreeBLL.CreateTreeHtml(user.id.Value, 3, "UserRegister", "UMemberInfo");
                //response.msg = val;
                //系谱图上线链路
                response.other = memberBLL.GetParentLinkString(1, user.id.Value);
                response.map = di;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 查询APP系谱图,只能查询当前用户及其下线节点
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetAppFamilyTreeByCondition(string userId)
        {
            ResponseDtoMap<string, Member> response = new ResponseDtoMap<string, Member>("fail");
            try
            {
                Member user = (Member)Session["MemberUser"]; //当前用户
                Dictionary<string, Member> di = null;
                if (userId != null && userId.Trim().Length > 0)
                {
                    //检查输入的用户是否存在,输入的用户是否是当前会员或其下线节点
                    Member mb = memberBLL.GetModelByUserId(userId);
                    //检查输入的用户是否存在
                    if (mb != null)
                    {
                        //检查输入的用户是否是当前会员或其下线节点
                        if (mb.pPath.Contains("," + user.id + ",") || mb.id == user.id)
                        {
                            di = familyTreeBLL.GetMemberList(mb.id.Value);
                            //系谱图上线链路
                            response.other = memberBLL.GetParentLinkString(1, mb.id.Value);
                        }
                        else
                        {
                            di = familyTreeBLL.GetMemberList(user.id.Value);
                            //系谱图上线链路
                            response.other = memberBLL.GetParentLinkString(1, user.id.Value);
                        }
                    }
                    else
                    {
                        di = familyTreeBLL.GetMemberList(user.id.Value);
                        //系谱图上线链路
                        response.other = memberBLL.GetParentLinkString(1, user.id.Value);
                    }
                }
                else
                {
                    di = familyTreeBLL.GetMemberList(user.id.Value);
                    //系谱图上线链路
                    response.other = memberBLL.GetParentLinkString(1, user.id.Value);
                }
                response.map = di;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
