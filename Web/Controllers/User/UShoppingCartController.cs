﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 购物车Controller
    /// </summary>
    public class UShoppingCartController : Controller
    {
        public IShoppingCartBLL cartBLL { get; set; }

        public IOrderBLL orderBLL { get; set; }

        public IMemberBLL mbBLL { get; set; }

        public IParamSetBLL psBLL{get;set;}

        /// <summary>
        /// 查询购物车
        /// </summary>
        /// <returns></returns>
        public JsonResult GetList()
        {
            ResponseDtoMap<string,object> response = new ResponseDtoMap<string,object>("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                ShoppingCart cart = new ShoppingCart();
                cart.uid = current.id;
                List<ShoppingCart> list = cartBLL.GetListShoppingCart(cart);
                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("list", list);
                Member mm = mbBLL.GetModelAndAccountNoPass(current.id.Value);

                Dictionary<string, ParameterSet> param = psBLL.GetDictionaryByCodes("meiyuanConvertTod");
                double meiyuanConvertTod = Convert.ToDouble(param["meiyuanConvertTod"].paramValue);

                di.Add("memberInfo", mm);
                di.Add("meiyuanConvertTod", meiyuanConvertTod);
                response.Success();
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "加载失败，请联系管理员"+ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存购物车
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult Save(ShoppingCart model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                model.uid = current.id;
                model.userId = current.userId;
                cartBLL.SaveShoppingCart(model);
                response.other = cartBLL.GetCartCount(current.id.Value)+"";
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 更新购物车数量
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult Update(ShoppingCart model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                model.uid = current.id;
                model.userId = current.userId;
                cartBLL.UpdateShoppingCart(model);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAll()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                cartBLL.DeleteByUid(current.id.Value);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除购物车记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                cartBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 选中、取消选中
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult SaveChecked(int id,int isChecked)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                cartBLL.SaveChecked(id, isChecked);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 全选、取消全选
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult SaveCheckAll(int isChecked)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                cartBLL.SaveCheckAll(current.id.Value, isChecked);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提交订单
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrder(OrderHeader model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                model.uid = current.id;
                model.userId = current.userId;
                model.typeId = 2; //复消订单
                string msg = orderBLL.SaveFxOrder(model);
                if (msg == "success")
                {
                    response.Success();
                }
                else
                {
                    throw new ValidateException(msg);
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "提交失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
