﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 会员提现Controller
    /// </summary>
    public class UTakeCash_jpController : Controller
    {
        public ITakeCashBLL takeCashBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];


                //获取用户最新信息
                Member user = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
                //提现手续费率
                Dictionary<string, ParameterSet> di = paramBLL.GetDictionaryByCodes("txfee");
                //user.regMoney 字段冗余提现手续费率
                user.regMoney = Convert.ToDouble(di["txfee"].paramValue);

                response.Success();
                response.result = user;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(TakeCash model)
        {
            if (model == null) { model = new TakeCash(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.uid = mb.id.Value;
            PageResult<TakeCash> page = takeCashBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提交提现申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveTakeCash(TakeCash model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                int c = takeCashBLL.SaveTakeCash(model, mb);
                //消息提醒
                SystemMsg msg = new SystemMsg();
                msg.isRead = 0;
                msg.toUid = 0;
                msg.url = "#TakeCash";
                msg.msg = "您有新的提现申请";
                msg.recordId = c;
                msg.recordTable = "TakeCash";
                msgBLL.Save(msg);
                response.Success();
                response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "申请失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 取消提现申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelTakeCash(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                int c = takeCashBLL.UpdateCancel(id, mb);
                response.Success();
                response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
